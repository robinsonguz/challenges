## Version 1.4.1
## language: en

Feature: File Inclusion Level 2
  TOE:
    Hackazon
  Category:
    Local File Inclusion
  Location:
    http://192.168.1.71/account/help_articles
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement
    in PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Find local files
  Recommendation:
    Static selection of files to be included

  Background:
      Hacker's software:
      | <Software name> | <Version>   |
      | Kali Linux      | 4.19.0      |
      | Firefox         | 60.8.0      |
    TOE information:
      Given The vulnerable machine
      When I enter into the site
      Then the server is running PHP 5.4

  Scenario: Normal use case
    Given A page with some help articles
    When I click over one of them
    Then the page includes another file

  Scenario: Static detection
    Given That I don't have access the source code
    Then I can't make static detection

  Scenario: Dynamic detection
    Given The site
    When I try the URL with a local file
    """
    http://192.168.1.71/account/help_articles?page=/etc/passwd%00
    """
    Then I get the content of the file

  Scenario: Exploitation
    Given the vulnerable page
    When I try to include the "/var/log/apache2/access.log" file
    Then I get a "404" error
    When I try to include a file that I have permissions to read
    Then I use this payload
    """
    page=/var/www/hackazon/web/index.php%00
    """
    And the full link is
    """
    http://192.168.1.71/account/help_articles?
    page=/var/www/hackazon/web/index.php%00
    """
    Then The index page of the web server is rendered
    And I can conclude the server is vulnerable to LFI

  Scenario: Remediation
    Given The vulnerable page
    When limiting the "include" options to the files you want
    Then only a list of strict files can be read
    And the vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      3.3/10 (Low) — AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fix ability
      3.1/10 (Low) — E:F/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.8/10 (Medium) — MAV:L/MAC:L/MPR:L/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-11
