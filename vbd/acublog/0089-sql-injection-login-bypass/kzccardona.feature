## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Unsafe Input
  Location:
    http://testaspnet.vulnweb.com/login.aspx - username (field)
  CWE:
    CWE-089: Improper Neutralization of Special Elements used in an SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard Unsafe Inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit unsafe inputs
  Recommendation:
    Always use parametrized queries

  Background:
    |  <Software name>  |    <Version>    |
    | Microsoft Windows | 10.0.17763.437  |
    | Google Chrome     | 76.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site http://testaspnet.vulnweb.com
    And Entered a .NET site which uses SQL requests
    And IIS version 8.5
    And ASP.NET 2.0.5

  Scenario: Normal use case
    Given I access http://testaspnet.vulnweb.com
    When I go to "login" tab
    Then It's displayed a login form

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    Given I access http://testaspnet.vulnweb.com
    When I look for a post or comments session
    Then I find http://testaspnet.vulnweb.com/Comments.aspx?id=0
    And I try to modify the end of de URL to try a SQLi
    """
    http://testaspnet.vulnweb.com/Comments.aspx?id=0'
    """
    And I get an error that is espected if the site is vulnerable
    Then I recognize the site is vulnerable to a SQL injection

  Scenario: Exploitation
    Given I access http://testaspnet.vulnweb.com/login.aspx
    When I try to use SQLi in the login form
    Then I try first with the password field
    And I wrote in user field Admin and try some sentences in password field:
    """
    ' OR '1'='1' --
    ' OR '1'='1' /*
    ' OR '1'='1' #
    ' OR '1'='1' %00
    ' OR '1'='1' %16
    OR 1 = 1 -- ]
    """
    And click on login button
    And I can't have access
    When I try in username field
    Then I wrote the next sentence in field:
    """
    'or 1 = 1 --
    """
    And click on login button
    Then I have access
    And I can create new posts
    Then I can conclude that it's posible to pass the login form trough a SQLi

  Scenario: Remediation
    When the query is programmed it should use parametrized queries
    And use LINQ and Entity Framework that generates parametrized SQL commands
    Then the program can successfully avoid most of the known SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.2/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.9/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.5/10 (Medium) - CR:L/IR:M/AR:L/MAV:N/MAC:L/MPR:L/MUI:N

  Scenario: Correlations
    No correlations have been found to this date 2019-09-05
