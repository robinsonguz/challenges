## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
   Weak Cryptographic Algorithm
  Location:
    https://www.webscantest.com/
  CWE:
    CWE-327: Use of a Broken or Risky Cryptographic Algorithm
  Rule:
  REQ.0176. The system must restrict access to system objects that have
  sensitive content. It will only allow access to authorized users.
  Goal:
    View data traveling to the server
  Recommendation:
    Select a well-vetted algorithm that is currently considered to be strong by
    experts in the field, and use well-tested implementations.

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server www.webscantest.com through HTTPS
    And Entered to site ../login.php
    Then I can see there is a login page
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I access https://www.webscantest.com/login.php
    And I write an username in the login input
    And A password in the password input
    Then I push the Sign in button
    And I get access to the user pages

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server www.webscantest.com through HTTPS
    Then I execute the following command
    """
    $ sslscan www.webscantest.com
    ...
    Testing SSL server webscantest.com on port 443 using SNI name
    webscantest.com
    ...
    Preferred TLSv1.1  128 bits  ECDHE-RSA-AES128-SHA      Curve P-256 DHE 256
    Accepted  TLSv1.1  256 bits  ECDHE-RSA-AES256-SHA      Curve P-256 DHE 256
    ...
    """
    Then I see that they use weak cryptographic algorithms like SSLv1.1

  Scenario: Exploitation
    Given that I know that the server uses weak cryptographic algorithms
    Then I execute a MiTM attack on a network
    And force the targets to use a weak algorithm to connect to the server
    Then I can decrypt the data
    And have access to the server

  Scenario: Remediation
    Select a well-vetted algorithm that is currently considered to be strong by
    experts in the field, and use well-tested implementations.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    2.6/10 (Low) - AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    2.4/10 (Low) - E:P/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    3.1/10 (Low) - CR:H/IR:H/AR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-16
