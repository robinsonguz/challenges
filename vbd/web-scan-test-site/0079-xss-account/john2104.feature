## Version 1.4.1
## language: en

Feature:
   TOE:
    Web scanner test site
  Category:
    Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection')
  Location:
    http://www.webscantest.com/business/account.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page
    Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject Cross site scripting
  Recommendation:
    Protect each text box

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://192.168.60.130/
    And Entered to site .../member.php
    Then I can see there is section for members

  Scenario: Normal use case
    Given I access .../business/account.php?accountId=
    Then I can see there is section for members
    And it shows the account id

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to .../business/account.php?accountId=
    Then I can write the following script in the accountId HTML query
    """
    <script>alert("xss")</script>
    """
    Then I can see the alert showing in the screen

  Scenario: Exploitation
    Given I access to .../business/account.php?accountId=
    Then I can write the following script in the search bar
    """
    <script>alert(Document.cookie)</script>
    """
    Then I get the cookie in the alert as result
    And I conclude that it is possible to steal a session in this way

  Scenario: Remediation
    Escape external input before executing it

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.6 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-21
