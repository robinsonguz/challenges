## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
   Information Gathering
  Location:
    http://webscantest.com/
  CWE:
    CWE-1021: Improper Restriction of Rendered UI Layers or Frames
  Rule:
    REQ.062 Define standard configurations
  Goal:
    Perform a clickjacking attack
  Recommendation:
    Implement the header X-Frame-Options

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I access http://www.webscantest.com/
    Then I intercept the request
    Then I see that the server doesn't have the X-Frame-Options header
    And I figure out that is vulnerable to clickjacking

  Scenario: Exploitation
    Given I know that the server doesn't have the X-Frame-Options header
    Then I create a web page using an iframe like
    """
    <html>
    <head>
    <title>Clickjack test page</title>
    </head>
    <body>
    <p>Website is vulnerable to clickjacking!</p>
    <iframe src="http://www.webscantest.com/" width="500" height="500"></iframe>
    </body>
    </html>
    """
    And upload it to a server
    Then using a phishing attack an user can access it
    And I get information using the clickjacking attack

  Scenario: Remediation
    Implement the header X-Frame-Options

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
