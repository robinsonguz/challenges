## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
    Cross-Site Request Forgery
  Location:
    http://zero.webappsecurity.com/bank/account-activity.html
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.174: https://fluidattacks.com/web/rules/174/
  Goal:
    Execute an unintentional request
  Recommendation:
    Ensure that request are made from a trusted source

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Mozilla Firefox | 67.0.4      |
    | Burp Suite      | v1.7.36     |
    | Kali Linux      | 2019.1.a    |
  TOE information:
    Given I access the site http://zero.webappsecurity.com/
    And I can see a banking web page with different options
    When I look for the server of the site
    Then I see is running Apache Server 2.2.22

  Scenario: Normal use case
    When I access the Zero Bank site
    Then I see options to manage accounts, transfer funds and pay bills
    And I see the Account Summary with information about accounts

  Scenario: Static detection
    When I access the web site
    Then I can see that the source code is inaccesible

  Scenario: Dynamic detection
    Given I logged in with the credentials username/password
    And I evaluate the different functionalities
    When I repeat the Requests with Burpsuite
    Then I can see the Access-Control-Allow-Origin is set as follows
    """
    Access-Control-Allow-Origin: *
    """
    Then I can conclude the site accepts requests from untrusted sources

  Scenario: Exploitation
    Given that the Access-Control-Allow-Origin header is not securely set
    And I start Apache service at 192.168.1.5
    When I create a web page with a request to the vulnerable site
    """
    <html>
    <body>
    <p>Ups, something went wrong!</p>
    <img src=http://zero.webappsecurity.com/logout.html>
    </body>
    </html>
    """
    Then I open a session at http://zero.webappsecurity.com/
    Then I access to the following link
    """
    http://192.168.1.5/csrf.html
    """
    Then I reload the site http://zero.webappsecurity.com/
    And I see the session is closed

  Scenario: Remediation
    Given the Access-Control-Allow-Origins header is unsecurely set
    And I see the directive allows access to any origin
    Then the directive should be changed to allow access to trusted sources

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:P/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.9(Medium) - CR:L/IR:M/AR:M/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:N/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-14
