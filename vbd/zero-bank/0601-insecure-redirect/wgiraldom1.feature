## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank - Open Redirect
  Location:
    http://zero.webappsecurity.com/bank/redirect.html - url on query string
  Category:
    Web
  CWE:
    CWE-0601: URL Redirection to Untrusted Site ('Open Redirect') -variant-
      https://cwe.mitre.org/data/definitions/601.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Redirect the user to a illegitimate site
  Recommendation:
    Whitelist the allowed URLs, or make sure they belong to the same domain
    before redirecting.

  Background:
  Hacker Software:
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
    | Microsoft Windows |     10     |
  TOE information:
    Given I'm accessing the site through http://zero.webappsecurity.com/bank
    And I have default credentials
    Then I will start analysis.

  Scenario: Normal use case
    When logged in, I go to the Account Activity page.
    Then I can switch between tabs for fetching additional information.
    And transfer money between accounts.

  Scenario: Static detection
    Given the source code is inaccessible,
    When I inspect the site, I could not find it.

  Scenario: Dynamic detection
    When I press on a tab while the browser console logs network activity
    And the mouseclick event gets propagated,
    Then the navigator loads, via XMLHttpRequest, a resource called redirect
    And its extension is .html.
    When reviewing its response,
    Then I find it replies with a 302 status,
    And a Location header. [evidence](img1.png)
    When I try to put ?url=http://localhost
    Then my Web server delivers me the local index page.
    Then I know Open Redirection is happening.

  Scenario: Exploitation
    When a user gets a poisoned link,
    Then he could be a pishing victim,
    And he won't suspect he is leaving the bank page.
    And also, as the site contains other CSRF vulnerabilities,
    Then we could make the bank perform operations on user's behalf.

  Scenario: Extraction
    Given we are know sure of Open Redirection
    And that it works with any web page,
    When we study our user and the web sites he visits
    Then we could perform operations without him knowing.

  Scenario: Remediation
    Given redirection is an important feature for most sites,
    And this site must have it,
    When writing the site's code
    And constructing the redirection logic,
    Then a whitelist or domain-verification should be performed.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:H/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    Given this site contains CSRF via URL parameters,
    When used in conjunction with this vulnerability
    Then requests on user behalf are possible.
