## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
    XSS-Stored
  Location:
    http://zero.webappsecurity.com/admin/currencies-add.html - country,
    name (field)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit XSS stored vulnerability
  Recommendation:
    Escape the input data before executing it

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Mozilla Firefox | 67.0.4      |
    | Burp Suite      | v1.7.36     |
    | Kali Linux      | 2019.1.a    |
    | Nikto           | v2.1.5      |
  TOE information:
    Given I access the site http://zero.webappsecurity.com/admin/
    And I can see the admin panel
    When I look for the menu I see Users and Currencies options
    Then I go to the Currencies options
    And I see the Add Currency button

  Scenario: Normal use case
    When I access the admin panel
    Then I go to the currencies options and select Add Currency
    And I enter the ID, Country and Name values

  Scenario: Static detection
    When I access the web site
    Then I can see that the source code is inaccesible

  Scenario: Dynamic detection
    Given I can add a new currency in the admin panel options
    When I write the following script in the country or name fields
    """
    <script>alert(1);</script>
    """
    Then I see the inputs does not validate the data
    And the currency is succesfully created
    When I access the currencies list
    Then I see the created currency and the Alert box
    And I can conclude the currencies are persistent in the system

  Scenario: Exploitation
    Given I know the currencies are stored and used inside the website
    And I can enter data without being sanitized
    When I add a new currency with the following name value
    """
    <script>window.onload(alert("This is stored XSS"))</script>
    """
    Then I sign in with the username and password
    When I access the Pay Bills/Purchase Foreign Currency options
    Then I can see the alert box with the message This is stored XSS
    And I conclude it is possible to store malicious code on the site

  Scenario: Remediation
    Given the user can enter untrusted data when adding a new currency
    Then the country and name fields should be validated
    And that way, escape dangerous characters to avoid malicious code

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.9/10 (Low) - E:P/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    3.3 (Low) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-24
