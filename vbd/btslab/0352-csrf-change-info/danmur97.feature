## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
  Location:
    http://localhost:8056/btslab/vulnerability/csrf/
    changeinfo.php - info (field)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.231 Define biometric verification component
  Goal:
    Changing user info without warning him
  Recommendation:
    Use hidden random/unique tokens on forms. And verify sended token
     for determining the validity of the request.

  Background:
  Hacker's software:
    | <Software name>      | <Version>         |
    | Windows              | 10                |
    | Firefox              | 68.0.1            |
  TOE information:
    Given I am accessing the site "http://localhost:8056/btslab/"
    And it is built with php
    And uses sql database

  Scenario: Normal use case
    Given I am logged as admin user
    And I access
    """
    http://localhost:8056/btslab/
      vulnerability/csrf/changeinfo.php
    """
    And a form for setting user info is show
    When I supply a text
    And send the form
    And return to my user profile
    """
    http://localhost:8056/btslab/
      myprofile.php?id=1
    """
    Then I can see the supplied text on about field

  Scenario: Static detection
    Given ToE source code
    When I inspect 'changeinfo.php'
    Then I notice that the vulnerability is being caused a conditional
    And it is located at line 14
    """
    14  if(isset($_GET['info'])&&!empty($_GET['info']))
    15  {
    16     $info=$_GET['info'];
    17     $username=$_SESSION['username'];
    18     include($_SERVER['DOCUMENT_ROOT'].'/btslab/mysqlconnection.php');
    19     $result=mysql_query("Update users set about='$info' where
           username='$username' ") or die(mysql_error());;
    20     echo "<b style='color:red'>Description Changed</b>";
    21  }
    """
    And that this change-info request can be forged

  Scenario: Dynamic detection
    Given access with other valid username
    When sending change-info request
    Then I get redirected to
    """
    http://localhost:8056/btslab/vulnerability/csrf/changeinfo.php
    ?info=lol&change=Change
    """
    And I notice that form data is sent with GET request
    And that a validity token is missing
    Then csrf is feasible

  Scenario: Exploitation
    Given the absence of tokens
    And the attacker page: 'hacker-page.html'
    When log in as admin
    And visit the attacker page 'http://localhost/hacker-page.html'
    And see my profile page
    Then I see on 'about' field
    """
    About : some slander and other bad stuff
    """

  Scenario: Remediation
    Given the implementation of the token mechanism by adding
    And modifying code at 'changeinfo.php'
    """
    4  if(!isset($_POST['change'])){
    5    $_SESSION['csrf']=bin2hex(openssl_random_pseudo_bytes(32));
    6  ?>
    7      Enter New information about you:<br/><br/>
    8      <form action="changeinfo.php" method="POST">
    9      <input type="hidden" name="csrf" value="<?php echo
            $_SESSION['csrf'];?>"/>
    15  }
    16  if(isset($_POST['csrf'])&&hash_equals($_POST['csrf'],
        $_SESSION['csrf'])&&isset($_POST['info'])&&!empty($_POST['info']))
    17  {
    18    $info=$_POST['info'];
    23  }
    """
    When I set my 'about' field to 'ok'
    And visit the attacker page while logged in
    And see my profile page
    Then I see the same 'ok' as stated before
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (medium) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.4/10 (low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-26
