## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 2
  Category:
    Insecure Communication
  Location:
    192.168.0.4:23 TCP
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.181 Transmit data using secure protocols
  Goal:
    Obtain the credentials sent through Telnet
  Recommendation:
    Use a protocol that encrypts communication between devices

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 18.04.1 (x64)|
    | Mozilla Firefox | 63.0         |
    | Ettercap        | 0.8.2        |
    | Nmap            | 7.60         |
    | Wireshark       | 2.6.5        |
    | VM virtualBox   | 5.2.22       |
  TOE information:
    Given I am running Metasploitable 2 on VM VirtualBox
    And Apache version is 2.2.8
    And PHP version 5.2.4
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
  Using a connection via telnet
    Given The telnet protocol allows access to a remote computer
    And I have a virtualized machine with S.O Ubuntu 18.04
    Then I use the following command to access metasploitable
    """
    $ telnet 192.168.0.4
    """
    And I can use the virtualized machine to control metasploitable 2

  Scenario: Static detection
  No code available for this vulnerability

  Scenario: Dynamic detection
  Using nmap script to scan vulnerabilities
    Given I use nmap to scan ports
    """
    $ nmap -sS 192.168.0.4
    """
    Then I see that port 23 is open and running the telnet service
    Then I verify if the service supports encryption
    And I use the following command
    """
    $ nmap -p 23 192.168.0.4 --script telnet-encryption
    """
    And I get the following output
    """
    PORT   STATE SERVICE
    23/tcp open  telnet
    | telnet-encryption:
    |_  Telnet server does not support encryption
    """
    And I see that the service does not encrypt the traffic
    Then I conclude that the service allows to obtain sensitive information

  Scenario: Exploitation
  Using an MITM attack to obtain the credentials
    Given I have a virtualized machine in VM virtualbox
    And This machine communicates via telnet with metasploitable 2
    Then I use the following nmap command to scan the network
    """
    $ nmap -sP 192.168.0.0/24
    """
    Then I can see the devices that are inside the network
    And I identify the ip address of the virtualized machine
    """
    192.168.0.6
    """
    And I identify the ip address of metasploitable 2 machine
    """
    192.168.0.4
    """
    Then I use ettercap to make a MITM attack
    And The virtualized machine requests to connect to metasploitable 2
    And Send credentials to authenticate
    Then Ettercap captures the credentials sent
    """
    TELNET : 192.168.0.4:23 -> USER: msfadmin  PASS: msfadmin
    """
    Then I know the credentials and I can access the metasploitable machine
    Then I can conclude that Telnet is an insecure way to communicate

  Scenario: Remediation
  Using the ssh protocol to have a more secure communication
    Given SSH uses cryptographic techniques to ensure communication
    Then The virtualized machine and metasploitable are connected via ssh
    And I use ettercap to make a MITM attack
    And I see that ettercap can not get the credentials
    Then I use Wireshark to analyze the traffic
    And I see the ssh connection between the two machines in the traffic
    And I try to see the connection information
    But This information is encrypted and I do not get anything
    Then I can conclude that using ssh keeps more secure information sensitive

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.0/10 (Medium) - AV:N/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:U/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Medium) - CR:M/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-02-12