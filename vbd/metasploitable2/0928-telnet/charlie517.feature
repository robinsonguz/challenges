## Version 1.4.1
## language: en

Feature:
  TOE:
    metasploitable2
  Category:
    Clear Transmission of Sensitive Information.
  Location:
    192.168.1.201:23
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information.
  Rule:
    REQ.026 Encrypt client-side session information.
  Goal:
    Get remote shell on the machine.
  Recommendation:
    Protect data transmission from end to end.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | Metasploit      | 5.0.4-dev   |
  TOE information:
    Given I am scanning the server 192.168.1.201
    And telnet is runing on port 23
    And is running on Ubuntu 2.6.24
    And is running Telnet 0.17-35ubuntu1

  Scenario: Normal use case
    When I need to manage the server I can login with telnet
    Then I can have a remote shell
    And I can manage the server

  Scenario: Static detection
    No code given

  Scenario: Dynamic detection
    Given the telnet service is up and running
    And I can check this by running nmap
    When I run nmap I got the open ports and saw telnet service as shown here:
    """
    PORT     STATE SERVICE
    21/tcp   open  ftp
    22/tcp   open  ssh
    23/tcp   open  telnet
    """
    Then I can conclude that telnet service is open and accepting conections

  Scenario: Exploitation
    When I execute wireshark and sniff the network
    Then I got the output: [evidence](evidence.png)
    And I used the sniffed credentials as user and pass
    When I logged into the server I got the output:
    """
    Trying 192.168.1.201...
    Connected to 192.168.1.201.
    Escape character is '^]'.
    metasploitable login:msfadmin
    Password:
    No mail.
    msfadmin@metasploitable:~$
    """
    Then I can concluded that I got access to the server

  Scenario: Remediation
    Deactivate telnet service and only use SSH.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9/10 (high) - AV:N/AC:L/Au:S/C:C/I:C/A:C
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.5/10 (high) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (high) - CDP:H/TD:H/CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 13/06/2019