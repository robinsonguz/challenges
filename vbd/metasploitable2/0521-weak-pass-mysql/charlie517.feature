## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.1.201:3306 TCP
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.142 Change system default credentials.
  Goal:
    Brute force mysql password and get into root account.
  Recommendation:
    Change default passwords

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | metasploit      | 5.0.4-dev   |
  TOE information:
    Given I am scanning the server 192.168.1.201
    And see a mysql service that allows me login
    And the server is running MySQL version 5.0.51A
    And is running on Ubuntu 2.6.24.


  Scenario: Normal use case
  Users login into mysql from and see databases
    Given I scan the server
    And see mysql service open
    Then I can see [evidence](evidence1.png)

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
  The password used for root password on mysql server is the defult password.
    Given I see mysql service running
    And can login
    Then I can execute the following command:
    """
    $ nmap 192.168.1.201
    """
    Then I get the output:
    """
    3306/tcp open  mysql
    """
    Then I can conclude that mysql service is open and running.

  Scenario: Exploitation
  To get access to the database as root user we need to bruteforce it.
    Given the password is a weak password
    And mysql does not prevent multiple login attempts
    Then I can execute the following command:
    """
    msf5 use auxiliary/scanner/mysql/mysql_login
    msf5 set RHOST, PASS_FILE, USERNAME
    msf5 run
    """
    Then I get the output:
    """
    [evidence](evidence2.png)
    """
    Then I can conclude that I got access to the root user of the database.

  Scenario: Remediation
  Use a strong and long enough password with upper, lowercase and numbers
    Given I have changed mysql root password
    Then If I re-run my exploit with command:
    """
    msf5 use auxiliary/scanner/mysql/mysql_login
    msf5 set RHOST, PASS_FILE, USERNAME
    msf5 run
    """
    Then I get:
    """
    [evidence](evidence3.png)
    """
    Then I can confirm that the vulnerability wasn't exploited successfully.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (high) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.6/10 (high) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.6/10 (high) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-02-15