## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Shell Command Injection
  Location:
    shellol/challenges/challenge1.php- Injection String (field)
  CWE:
    CWE-78: OS Command Injeciton
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Disclose the contents of the /etc/passwd file using command injection
  Recommendation:
    Create a blacklist of dangerous characters

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |     65.0.2      |
  TOE information:
    Given I'm running OWASP on VMWare
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string
    And appends it to the "ls " shell command

  Scenario: Normal use case
  The site allows the user to enter a string to list the files in a folder
    Given I access the page shellol/challenges/challenge0.php
    And I enter the string "challenges"
    And I click the Inject! button
    Then the list of files in the challenges folder is shown
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The input is sanitized but only for the "script" word
    When I look at the source code
    And I check the code that prepares the input
    Then I notice there are two files that process it
    And the first one is sanitize.inc.php and it has these lines
    """
    24  if(isset($_REQUEST['sanitization_level']) and isset($_REQUEST['
        sanitization_type']) and $_REQUEST['sanitization_type']=='keyword'){
    25    switch($_REQUEST['sanitization_level']){
    ...
    45      case 'reject_high':
    46          foreach($params as $keyword){
    47            if(strstr($_REQUEST['inject_string'], $keyword)!='') {
    48              die("\n<br>Input rejected!");
    49            }
    50          }
    51          break;
    """
    And the second one is shell.php and it has these lines
    """
    45  $base_utility = 'ls';
    46  $base_filename = '/tmp/derp';
    47  $base_command = $base_utility . ' ' . $base_filename;
    ...
    53  include('../includes/sanitize.inc.php');
    ...
    59  switch ($_REQUEST['location']){
    60    case 'argument':
    61    $command = str_replace($base_filename, $_REQUEST['inject_string'],
                     $base_command);
    62    break;
    ...
    80  $descriptor_spec = array(
    81          0 => array('pipe','r'),
    82          1 => array('pipe','w'),
    83          2 => array('pipe','w')
    84  );
    85
    86  $process = proc_open($command, $descriptor_spec, $pipes);
    87
    88  if(is_resource($process)){
    89    fclose($pipes[0]);
    90    $output = stream_get_contents($pipes[1]);
    91    fclose($pipes[1]);
    92    $errors = stream_get_contents($pipes[2]);
    93    fclose($pipes[2]);
    94    proc_close($process);
    95  }
    ...
    106  echo '<b>Output:</b><br>';
    107  echo '<pre>'.$output.'</pre>';
    """
    Then I notice only some keywords are validated

  Scenario: Dynamic detection
  The input of the Inject! field takes shell commands without semicolons
    Given I enter "| cat index.php" in the text field
    And I click the Inject! button
    Then the contents of the index.php file are shown
    And it can be seen in [evidence](cat-index.png)
    And I conclude the application is vulnerable to shell command injection

  Scenario: Exploitation
  Removing files from the server
    Given I enter "| rm -v important.txt" in the input field
    And I click the Inject! button
    Then I get a confirmation message indicated the file was removed
    And it can be seen in [evidence](rm-important.png)

  Scenario: Extraction
  Retrieving the contents of the /etc/passwd file
    Given I enter "| cat /etc/passwd" in the input field
    And I click the Inject! button
    Then the contents of the server's /etc/passwd file are shown
    And it can be seen in [evidence](etc-passwd.png)

  Scenario: Remediation
  The php code can be fixed by creating a blacklist of dangerous characters
    Given I have patched the code by escaping the user input
    """
    59  switch ($_REQUEST['location']){
    60    case 'argument':
    61    $command = str_replace($base_filename, $_REQUEST['inject_string'],
                     $base_command);
    62    $command = preg_replace('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/',
                     '',$command);
    63    break;
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get no results and an error message is presented
    And it can be seen in [evidence](escaped-input.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:L/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-03-19
