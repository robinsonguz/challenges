## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    Cross-Site Scripting
  Location:
    MCIR/xssmh/challenges/challenge2.php - Injection String (field)
  CWE:
    CWE-79: Cross-Site Scripting
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Disclose the contents of the site cookies using stored xss
  Recommendation:
    Escape the input

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |     60.0.1      |
  TOE information:
    Given I'm running OWASP on VMWare
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that takes a string and creates a link
    And the link renders a page with the string I entered

  Scenario: Normal use case
  The site allows the user to enter a string that can be rendered as content
    Given I access the page MCIR/xssmh/challenges/challenge2.php
    And I enter the string "hello"
    And I click the Inject! button
    And I click the "See the output" link
    Then the string is shown as part of the site's content
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The input is not validated before being included in the html source code
    When I look at the source code
    And I check the code that prepares the input
    """
    43  $base_output = 'Foo! <img src="baz.jpg"><input type="text"
                             value="bar!"><script>a="javascript";</script>';
    ...
    54  switch ($_REQUEST['location']){
    55    case 'body':
    56      $output = str_replace('Foo!', $_REQUEST['inject_string'],
                     $base_output);
    57      break;
    ...
    89  if(isset($_REQUEST['persistent'])){
    90    $fhandle = fopen('pxss.html','w') or die('Whoops! Can\'t write to
                     our PXSS file. Did you run setup.sh?');
    91    fwrite($fhandle, $output);
    92    fclose($fhandle);
    93    echo "<a href='pxss.html'>See the output</a>";
    """
    Then I notice there is no validation on the input

  Scenario: Dynamic detection
  The input of the Inject! field is written into the pxss.html file
    Given I enter "<script>alert('hi');</script>" in the text field
    And I click the Inject! button
    And I click the "See the output" link
    Then I get an alert window that can be seen in [evidence](alert.png)
    And I conclude that my input was treated as code by the server
    And the application is vulnerable to cross-site scripting

  Scenario: Exploitation
  Retrieving all cookies
    Given I enter the following code in the input field
    """
    1  <script>
    2  document.cookie = "username=John Doe";
    3  var x = document.cookie;
    4  alert(x);
    5  </script>
    """
    And I click the Inject! button
    And I click the "See the output" link
    Then I get an alert window showing all the cookies
    And it is exemplified by the cookie I inserted
    And it can be seen in [evidence](cookies.png)

  Scenario: Remediation
  The php code can be fixed by escaping the input before writing it
    Given I have patched the code by escaping the user input
    """
    91    fwrite($fhandle, htmlspecialchars($output, ENT_QUOTES));
    """
    Then I can prevent the injection of the code shown above
    Given I re-run my exploit
    Then I get no alert window and my input is shown without getting executed
    And it can be seen in [evidence](escaped-output.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:H/IR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-02-19
