## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    XSS-Cross Site Scripting
  Location:
    http://localhost:9991/www/XSS/XSS_level3.php
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    show a JavaScript alert
  Recommendation:
    Whitelisting allowed inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accessing the site
    And a text box
    And The app is running on XAMPP 5.6.21

  Scenario: Normal use case
    Given a common text box asking for a name
    When I submit the text box with "Hey"
    Then the application responds "You are Hey"

  Scenario: Static detection
    When I watch the source code
    Then I can see a function inside the PHP code
    """
    if (isset($_GET["username"])) {
    $user =
    preg_replace("/<(.*)[S,s](.*)[C,c](.*)[R,r](.*)[I,i](.*)[P,p](.*)[T,t]>/i",
    "", $_GET["username"]);
    echo "Your name is "."$user";
    }
    """
    And As seen, some characters saved in variable "$user" are deleted
    And show a message with the variable
    """
    echo "Your name is "."$user";
    """

  Scenario: Dynamic detection
    Given a text box
    When I type "<h1>Hey</h1>"
    Then I can see [evidence1](image1.png)
    Then I can conclude that I can use labels

  Scenario: Exploitation
    Given I can execute use labels
    And that the characters are removed
    """
    /<(.*)[S,s](.*)[C,c](.*)[R,r](.*)[I,i](.*)[P,p](.*)[T,t]>/i
    """
    Then I can't use "<script>" labels
    When I think about other way to execute JavaScript code
    Then I find that I can use "<iframe>" HTML tag
    When I put the code inside the text box
    """
    <iframe src="javascript:alert('Hey');"></iframe>
    """
    Then I can show [evidence2](image2.png)

  Scenario: Remediation
    Given I have patched the code by doing
    """
    if (isset($_GET["username"])) {
      $string = $_GET["username"];
      $user = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
      echo "Your name is "."$user";
    }
    """
    Then the filter accepts only "[A-Za-z0-9 ]"
    When I put the code inside the text box
    """
    <iframe src="javascript:alert('Hey');"></iframe>
    """
    Then no alert is shown
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.2/10 (Medium) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    4.2/10 (Medium) - MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-09
