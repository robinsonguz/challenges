## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    File upload
  Location:
    http://192.168.1.53:9991/www/FileUpload/fileupload2.php - file (field)
  CWE:
    CWE-646: Reliance on File Name or Extension of Externally-Supplied File
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
  Goal:
    Upload a maliciously crafted file
  Recommendation:
    Define file types, size and check magin numbers for uploads

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali            | 4.19.0    |
    | Firefox         | 60.8.0esr |
    | Burp Suite      | 2.1.02    |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running LAMPP

  Scenario: Normal use case
    Given The upload site
    When I access it
    Then I see a file upload field
    And I can upload images ("PNG" or "JPG")

  Scenario: Static detection
    When I look at the code "fileupload2.php"
    Then I can see it is only checking the file extension
    """
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $type = $_FILES["file"]["type"];
    if($type != "image/png" && $type != "image/jpeg" ){
        echo "JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    if($uploadOk == 1){
        move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
        echo "File uploaded /uploads/".$_FILES["file"]["name"];
    }
    """
    Then I can conclude that I can change the file type with Burp Suite
    And Upload a ".php" file disguised as a ".jpg"

  Scenario: Dynamic detection
    Given The upload field
    And The previous static analysis
    When I upload a "shell.jpg" file
    Then I capture the request with Burp Suite
    And I change the file extension in the request back to ".php"
    Then I can conclude that I can upload a ".php" file as a ".jpg"

  Scenario: Exploitation
    Given The "fileupload2.php" file
    When I upload an image I get the full path to where it was uploaded
    Then I search for PHP shells
    And I find a really good one that only needs one file
    """
    https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php
    """
    Then I upload the shell to the server as a "JPG"
    And I capture the request with Burp Suite
    Then I change the file name [evidence](img1.png)
    Then I get a shell in the server [evidence](img2.png)
    And I have a fully working shell on the system

  Scenario: Remediation
    Given The source code
    When I add code to the upload code
    """
    $type = $_FILES["file"]["type"];
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check["mime"] == "image/png" || $check["mime"] == "image/gif"){
      $uploadOk = 1;
    }else{
      $uploadOk = 0;
      echo "Mime?";
      echo $check["mime"];
    }
    if($uploadOk == 1){
      move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
      echo "File uploaded /uploads/".$_FILES["file"]["name"];
    }
    """
    When I try to upload a ".jpg" file
    And change it with Burp Suite back to ".php"
    Then I get a message saying [evidence](img3.png)
    """
    Mime?
    """
    Then I can confirm that the vulnerability was patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:H/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.8/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - MAV:L/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date {2019-09-30}
