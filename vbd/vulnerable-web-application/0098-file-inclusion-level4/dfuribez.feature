## Version 1.4.1
## language: en

Feature:
  TOE:
    vwa
  Category:
    Local File Inclusion
  Location:
    https://172.17.0.2/www/FileInclusion/pages/lvl4.php
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement in
    PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Read the file /etc/passwd
  Recommendation:
    Sanitize the input parameters and implement a white list of files to include

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | ArchLinux       | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site
    And it runs on a container with XAMPP 5.6.21

  Scenario: Normal use case
    Given I access https://172.17.0.2/www/FileInclusion/pages/lvl4.php
    Then I can see two buttons "Button" and  "The other button"
    And if I click on "Button" I can see that the page displays the text:
    """
    Why Dont You Click the Other Button??
    """
    And if I click on "The other button" I can see that the page displays:
    """
    Did you notice anything changed? Browse the site.
    """

  Scenario: Static detection
    Given the application's code
    When I look at the code of the file lvl4.php
    Then I can see some php code from lines 21 to 35
    """
    21:<?php
    22: echo "</br></br>";
    23:   if (isset( $_GET[ 'file' ]))
    24:     {
    25:       $secure4 = $_GET[ 'file' ];
    26:        if ($secure4!="1.php" && $secure4!="2.php")
    27:        {
    28:          $secure4=substr($secure4, 0,-4);
    29:        }
    30:        if (isset($secure4))
    31:        {
    32:          include($secure4);
    33:        }
    34:    }
    35:?>
    """
    And I see lines 26 to 28 checks if file's parameter matches 1.php or 2.php
    When line 26 "$secure4" matches either 1.php or 2.php
    Then the code does nothing
    And line 32 includes "$secure4"
    When in line 26 "$secure4" does not match either 1.php or 2.php
    Then line 28 removes the last four character from "$secure4"
    And in line 32 "$secure4" gets included
    And I can conclude that lvl4.php is vulnerable to LFI

  Scenario: Dynamic detection
    Given the url of the site
    And knowing the vulnerability to exploit
    When I go to the following url to see the page's response
    """
    http://172.17.0.2/www/FileInclusion/pages/lvl4.php?file=anythinghere
    """
    Then I get the following warnings:
    """
    Warning: include(anything): failed to open stream: No such file or
    directory in /www/FileInclusion/pages/lvl4.php on line 35
    Warning: include(): Failed opening 'anything' for inclusion
    (include_path='.:/opt/lampp/lib/php') in /www/FileInclusion/pages/lvl4.php
    on line 35
    """
    And I can conclude that the page is vulnerable to LFI

  Scenario: Exploitation
    Given that the code only removes the last 4 characters of the parameter
    When I try the following url
    """
    http://172.17.0.2/www/FileInclusion/pages/lvl4.php?file=/etc/passwd1234
    """
    Then I get the /etc/passwd file [evidence](inclusion.png):
    And I can conclude that the system can be exploited

  Scenario: Remediation
    Given I have patched the code replacing it with
    """
    21: <?php
    22:  echo "</br></br>";
    23:  if (isset( $_GET[ 'file' ])) {
    24:    $secure4 = trim($_GET[ 'file' ]);
    25:    if ($secure4 == "1.php" || $secure4 =="2.php") {
    26:      @include($secure4);
    27:    }
    28:  }
    29: ?>
    """
    When line 24 strips whitespaces from the begining and end of "$secure4"
    Then line 25 makes sure "$secure4" is equal to either 1.php or 2.php
    When I go back to the page
    """
    http://172.17.0.2/www/FileInclusion/pages/lvl4.php?file=/etc/passwd1234
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.4 (High) - RC:C/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-07-01}
