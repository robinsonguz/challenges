## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    XSS-Cross Site Scripting
  Location:
    http://localhost/vwa/XSS/XSS_level1.php - username (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Reflect html code
  Recommendation:
    Use prepared statements with variable bindings

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given I am accesing the site
    """
    http://localhost/vwa/XSS/XSS_level1.php
    """
    And A field to type any name
    And The app is running on XAMPP v3.2.4

  Scenario: Normal use case
    Given A field to type any name and then get the typed name in the screen
    When I type 'John' i get as response 'Your name is John'
    Then I try to type different names or any other text

  Scenario: Static detection
    When I watch the source code
    Then I can see this html code and a php if condition
    """
    <div align="center">
    <form method="GET" action="" name="form">
    <p>Your name:<input type="text" name="username"></p>
    <input type="submit" name="submit" value="Submit">
    </form>
    </div>
    <?php
    if(isset($_GET["username"]))
    echo("Your name is ".$_GET["username"])?>
    """
    And I can see that anything a user types in the field will be displayed

  Scenario: Dynamic detection
    When I try to type this text inside html tags '<h1>John</h1>'
    Then I can see it got reflected as a text title
    And the input got reflected exactly as i typed it [evidence](image1.png)
    And I realize it could recognize script tags or php code

  Scenario: Exploitation
    When I type in the input field this code
    """
    <script>alert("This action requires login confirmation");
    var user = prompt("Username: "); var pwd = prompt("Password: ");
    </script><h1>Username is:
    <script type="text/javascript">document.write(user);</script>
    </h1><h2>Password is:
    <script type="text/javascript">document.write(pwd)</script></h2>
    """
    Then I reflected a 'login confirmation' alert where user have to log in
    And I replaced the name and displayed the username and password instead

  Scenario: Remediation
    When I was looking at the code
    Then I modified the php code and used this
    """
    <?php
    if(isset($_GET["username"])) {
    $user = str_replace("<", "", $_GET["username"]);
    echo("Your name is "."$user");
    }
    ?>
    """
    And It replaces the '<' character with a blank space
    And The response is 'h1>John/h1>' instead of a text title with 'John' on it
    And Any html tag that a user types won't be taken as code, just plain text
    And The vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.9/10 (Low) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    2.5/10 (Low) - IR:L/MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:N/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-06
