## Version 1.4.1
## language: en

Feature: XSS LEVEL 4
  TOE:
    Vulnerable Web Application
  Category:
    XSS-Cross Site Scripting
  Location:
    http://localhost:9991/www/XSS/XSS_level4.php
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Insert javascript code
  Recommendation:
    Whitelisting allowed inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
  TOE information:
    Given The site "http://localhost:9991/www/XSS/XSS_level4.php"
    When I enter into it
    Then The server is running PHP 4.2.3

  Scenario: Normal use case
    Given A text box
    When I enter a name
    Then I get the string "Your name is " and the value that I enter

  Scenario: Static detection
    Given The source code
    When I search for "echo" function that displays the name
    Then I can see that the code replace some words by an empty space
    """
    $values = array("script", "prompt", "alert", "h1");
    $user = str_replace($values, " ",$_GET["username"]);
    """
    And It replaces the string "<script>" in case insensitive
    """
    $user = preg_replace("/<(.*)[S,s](.*)[C,c](.*)[R,r](.*)[I,i]
    (.*)[P,p](.*)[T,t]>/i","", $_GET["username"]);
    """
    When I see that the code only show the result of the second substitution
    Then I can continue using javascript code
    And The code is vulnerable to XSS

  Scenario: Dynamic detection
    Given A text box
    When I type "alert"
    Then The page shows "Your name is alert"
    And The first filter is not working
    When I type "<script>a</script>" in the text box
    Then The code escapes that string
    And The only filter that works is the second one [evidence](image1.png)

  Scenario: Exploitation
    Given That I can use the string "<script>" because of the filter
    When I think about other way to execute javascript code
    Then I find that I can use the "img" html tag
    When I put the code inside the onerror attribute
    """
    onerror="alert('XSS')"
    """
    Then I use a image name that creates an error
    """
    src="not_an_image"
    """
    When I have the payload complete
    """
    <img src="not_an_image" onerror="alert('XSS')">
    """
    Then I copy it in the text box
    And I have a reflected XSS [evidence](image2.png)

  Scenario: Remediation
    Given The source code
    When I modify the line to use the already escape var "$user"
    """
    $user = str_replace($values, " ",$_GET["username"]);
    $user = preg_replace("/<(.*)[S,s](.*)[C,c](.*)[R,r](.*)[I,i]
    (.*)[P,p](.*)[T,t]>/i", "", $user);
    """
    Then I also add the "<" and ">" chars to the blacklisted array
    """
    $values = array("script", "prompt", "alert", "h1","<",">");
    """
    And The XSS is patched [evidence](image3.png)

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.8/10 (High) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:L/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.1/10 (Medium) - E:X/RL:T/RC:U/CR:H/IR:L/AR
    Environmental: Unique and relevant attributes to a specific user environment
      7.6/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-12
