## Version 2.0
## language: en

Feature: ReDoS
  TOE:
    Node-goat
  Category:
    ReDoS Regular expressions DoS
  Location:
    http://localhost:4000/profile
  CWE:
    CWE-0700: Improper Input Validation
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Make a Denial of Service in the web
  Recommendation:
    Use Node.js's validator.js package to validate expected data format

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 18.04         |
    | Chromium      | 80.0.3987.122 |
    | Burp          | 2020.2        |
  TOE information:
    Given I am accessing the site http://localhost:4000/
    And server is running Node.js

  Scenario: Normal use case
    Given I access the route "/profile"
    Then I can update my profile

  Scenario: Static detection
    Given I see source code in "nodegoat/app/routes/profile.js"
    """
    // Allow only numbers with a suffix of #, for example: 'XXXXXX#'
    var regexPattern = /([0-9]+)+\#/;
    var testComplyWithRequirements = regexPattern.test(bankRouting)
    """
    Then I see that this lines take a lot of CPU resources to validate

  Scenario: Dynamic detection
    Given I'm on the route "/profile"
    When I try to see the HTML source code
    Then I can't find anything
    When I try to use every field with a lot of characters
    Then I see that the field "Bank Routing #" has a lot of CPU usage
    And The field have a vulnerability

  Scenario: Exploitation
    Given I'm on the route "/profile"
    When I try to use all CPU validating the field "Bank Routing #"
    Then I try to use a long number without "#"
    """
    987165215648920123196482054894206519840231654
    """
    Then I submit the form
    And The web can't load anything for a few seconds
    When I use burp to send a lot of requests
    Then The web can't load anymore
    And I exploit the vulnerability

  Scenario: Remediation
    Given The source code in "nodegoat/app/routes/profile.js"
    Then I optimize the regular expression
    """
    var regexPattern = /([0-9]+)+\#/;
    var testComplyWithRequirements = regexPattern.test(bankRouting)
    """
    Then I delete the extra "+" and limit the regex for 8 numbers and "#"
    """
    var regexPattern = /^([0-9]{1,8})\#/;;
    var testComplyWithRequirements = regexPattern.test(bankRouting)
    """
    And I limit the max length of the field to 9 in case of the regex fail
    """
    var maxLength = 9;
    if (req.body.bankRouting.length <= maxLenght){
      var bankRouting = req.body.bankRouting;
    }
    """
    When Use a second length validation if the admin want to delete the regex
    And I use the var "maxLenth" if the admin want to change the max length
    Then I try to use attack this field again
    And I see that the field was patched
    And I fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:X/IR:X/AR:H

  Scenario: Correlations
  No correlations have been found to this date 2020-03-09
