## Version 2.0
## language: en

Feature: Unvalidated Redirects and Forwards
  TOE:
    Node-goat
  Category:
    A10 - Unvalidated Redirects and Forwards
  Location:
    http://localhost:4000/ - page (href)
  CWE:
    CWE-0601: URL Redirection to Untrusted Site ('Open Redirect')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Redirect the user to an illegitimate site
  Recommendation:
    Avoid the use of redirect or whitelist allowed URLs.
    Make sure they belong to the same domain before redirecting.

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
  TOE information:
    Given I am accessing the site http://localhost:4000/
    And server is running Node.js

  Scenario: Normal use case
    Given I access the route "/"
    Then I can make actions that are allowed only for that user

  Scenario: Static detection
    Given I see source code in "nodegoat/app/routes/index.js"
    """
    app.get("/learn", function (req, res, next) {
        return res.redirect(req.query.url);
    });
    """
    Then I see it redirects to any url passed in the parameter at learn link

  Scenario: Dynamic detection
    Given Firefox Developer Tools
    When I inspect element
    """
    <a id="learn-menu-link" target="_blank"
    href="/learn?url=https://www.khanacademy.org/">
    <i class="fa fa-edit"></i> Learning Resources</a>
    """
    Then I try changing the URL parameter to an external URL
    """
    http://localhost:4000/learn?url=http://youtube.com
    """
    Then I get redirected to "http://youtube.com"

  Scenario: Exploitation
    Given I can redirect users to whatever URL I want
    Then I send a user a link that redirects to a similar phishing page
    When the user tries to log in
    Then I get their credentials

  Scenario: Remediation
    Given I add a whitelist
    And Patch the "/learn" GET function
    """
    ...
    const redirectWhitelist = new Set([
      'https://www.khanacademy.org'
    ])
    ...
    app.get("/learn", function (req, res, next) {
      const {url} = req.query;
      if (redirectWhitelist.has(url)) {
        return res.redirect(req.query.url)
      } else {
        return res.send('invalid redirect url')
      }
    });
    ...
    """
    Then the user will only be redirected if the URL is in the whitelist

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
  No correlations have been found to this date 2020-02-28
