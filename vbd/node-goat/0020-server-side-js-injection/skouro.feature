## Version 1.4.1
## language: en

Feature:
  TOE:
    Node Goat
  Category:
    SSJS Injection attacks
  Location:
    http://localhost:4000/contributions
  CWE:
    CWE-150: https://cwe.mitre.org/data/definitions/150.html
    CWE-20: https://cwe.mitre.org/data/definitions/20.html
    CWE-80: https://cwe.mitre.org/data/definitions/80.html
    CWE-95: https://cwe.mitre.org/data/definitions/95.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Stop the service web.
  Recommendation:
    Do not use eval()function to parse user inputs.

  Background:
    Hacker's software:
        | <Software name>                  | <Version>     |
        | Ubuntu                           | 19.04         |
        | Mozilla Firefox                  | 68.04         |
  TOE information:
    Given I am accessing the site http://localhost:4000/
    And entered /contributions
    And the server is running Node.js

  Scenario: Normal use case
    When I enter in the contributions module
    Then I can change the percent of contribution

  Scenario: Static detection
    When I look at the code of file NodeGoat/app/routes/contributions.js:
    """
    24 var preTax = eval(req.body.preTax);
    """
    Then I see that the user input is not validated
    And use the eval()function
    Then I conclude that the site is susceptible to server side injection

  Scenario: Dynamic detection
    When I enter the following string in the Pre-Tax input:
    """
    3 + 5
    """
    Then the server interprets the expression
    Then I concluded that Employee Pre-Tax input is a possible attack vector

  Scenario: Exploitation
    When I enter the following string in the Pre-Tax input:
    """
    process.exit()
    """
    Then I close the service of the web server
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When I use an alternate method to eval:
    """
    24 var preTax = parseInt(req.body.preTax);
    """
    Then I enter the following string in the Pre-Tax input:
    """
    process.exit()
    """
    And the site returns:
    """
    Invalid contribution percentages
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.7/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-08-05
