## Version 2.0
## language: en

Feature: Cross Site Scripting
  TOE:
    Node-goat
  Category:
    Cross Site Scripting
  Location:
    http://localhost:4000/profile
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Insert javascript code
  Recommendation:
    Always validate users inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | Python          | 2.7.14+     |
  TOE information:
    Given The site "http://localhost:4000/profile"
    When I enter into it
    Then The server is running on NodeJs "v8.10.0"

  Scenario: Normal use case
    Given The route "/profile"
    When I change the values on it
    Then I can update my profile

  Scenario: Static detection
    Given The source code
    When I go to the "views/layout.html"
    Then I can see the html code where the username is displayed
    """
    <li class="dropdown user-dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-user"></i> {{firstName}} {{lastName}}
      <b class="caret"></b></a>
    """
    When I see the code in "/routes/profile.js"
    """
    doc.firstNameSafeString = ESAPI.encoder().encodeForHTML(doc.firstName)
    """
    Then The "firstName" is encoded in the wrong scope
    And I have an XSS vulnerability

  Scenario: Dynamic detection
    Given A form to update the profile
    When I type  "<br> <h1> test </h1>" after my first name
    Then I update my profile
    When I see my name in the right corner
    Then I have an html injected code [evidence](image1.png)
    And An XSS vulnerability

  Scenario: Exploitation
    Given An XSS vulnerability
    When I create a Python script to update the profile
    Then I can inject my javascript code
    When I run the Python script
    """
    python xss.py <password>
    """
    Then I get an alert with my text [evidence](image2.png)
    And I have exploited the XSS vulnerability

  Scenario: Remediation
    Given The source code
    When I change the code to
    """
    doc.doc.firstNameSafeURLString = ESAPI.encoder().encodeForURL(urlInput)
    """
    Then The vulnerabilty is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.8/10 (High) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:L/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.1/10 (Medium) - E:X/RL:T/RC:U/CR:H/IR:L/AR
    Environmental: Unique and relevant attributes to a specific user environment
      7.6/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-09-17
