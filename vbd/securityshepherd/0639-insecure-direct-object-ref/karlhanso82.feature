## Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Location:
    http://localhost/
    fdb94122d0f032821019c7edf09dc62ea21e25ca619ed9107bcc50e4a8dbc100.jsp
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.176: Restrict system objects
  Goal:
    Get the result key from request given the IDOR
  Recommendation:
    For every data request ensure the user has enough
    privileges to access the record

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | OWASP ZAP            | 2.7.0          |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access a site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials
   And I enter the site and click on the link Get the next Challenge

 Scenario: Static detection
  Given I enter enter https://localhost/login.jsp
  And I click to the next challenge link
  Then I saw that the site is build on java and I have access
  And I just look for webapp directory where tomcat deploy the apps
  Then I find ROOT.war and unzip it
  And I see the folder challenges
  Then I saw link with the id of challenge
  """
  fdb94122d0f032821019c7edf09dc62ea21e25ca619ed9107bcc50e4a8dbc100.jsp
  """
  Then I saw that it does some verifications csfr and session
  """
   ShepherdLogManager.logEvent(request.getRemoteAddr(),
   request.getHeader("X-Forwarded-For"), levelName + " Accessed");
   if (request.getSession() != null)
   {
        HttpSession ses = request.getSession();
        //Getting CSRF Token from client
        Cookie tokenCookie = null;
        try
        {
         tokenCookie = Validate.getToken(request.getCookies());
  """
  But since is a jsp page there is not much backend code
  Then I search for post request SolutionSubmit
  And I search this in web.xml
  And I see that it leads to SolutionSubmit.class
  Then the parameter is not handle properly in this class
  And I finish the inspection

 Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I saw the description
   """
   Imagine a web page that allows you to view your personal information.
   The web page that shows the user their information is generated based
   on a user ID. If this page was vulnerable to insecure Direct Object
   References an attacker would be able to modify the user identifier
   parameter to reference any user object in the system.
   """
   And I saw an input where i must send a key
   Then I refresh the page and I open the brower console
   And I see post request that sends username=guest
   Then I see that parameter is not handled properly

 Scenario: Explotation
   Given the challenge link we send data to the input
   Then we use OWASP zap tool to record the post request
   And I see this post request
   Then I saw username=guest
   And we modify this parameter to username=admin
   Then we recieve the administrator key
   And gain access to any data given the flaw

 Scenario: Remediation
   When the page has been developed
   Then it should be ensure that only one user can use it or
   Then use indirect object references
   And that solves the issue

 Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
    7.9/10 (Low) - AV:L/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:
 Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
    5.4/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-14