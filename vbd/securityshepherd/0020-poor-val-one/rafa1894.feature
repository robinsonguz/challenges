## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Security Shepherd
  Category:
    Poor Data Validation
  Location:
    https://localhost/index.jsp - Quantity (field)
  CWE:
    CWE-0020: Improper Input Validation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Manipulate the pages inputs to get items for free
  Recommendation:
    Validate all data inputs depending on the context

  Background:
  Hacker's software:
    | Ubuntu          | 18.04.2 LTS |
    | Mozilla Firefox | 67.0        |
    | VirtualBox      | 6.0.8       |
  TOE information:
    Given I am accessing the site https://localhost/index.jsp
    And entered one of its pages where I can shop some items
    And the site's database environment is MySQL
    And the overall website is built with JSP technology

  Scenario: Normal use case
    When I access https://localhost/index.jsp
    Then I can see several items that I can buy
    And I can enter how many I want of each item
    And then click on the "Place Order" button
    And I get the items for the correct price

  Scenario: Static detection
    When I inspect the page looking for the code source
    Then I recognize the html code that handles item quantity input
    """
    38 <input type="text" style="width: 40px" value="0"id="numberOfRage"
       autocomplete="off"/>
    44 <input type="text" style="width: 40px" value="0" id="numberOfNotBad"
       autocomplete="off"/>
    50 <input type="text" style="width: 40px" value="0" id="numberOfTroll"
       autocomplete="off"/>
    56 <input type="text" style="width: 40px" value="0" id="numberOfMegusta"
       autocomplete="off"/>
    """
    And also the related javascript code
    """
    74 var theMegustaAmount = $("#numberOfMegusta").val();
    75 var theTrollAmount = $("#numberOfTroll").val();
    76 var theRageAmount = $("#numberOfRage").val();
    77 var theNotBadAmount = $("#numberOfNotBad").val();
    84 data: {
    85   megustaAmount: theMegustaAmount,
    86   trollAmount: theTrollAmount,
    87   rageAmount: theRageAmount,
    88   notBadAmount: theNotBadAmount
    89  },
    """
    And I check for the backend code but it isn't accesible
    Then I can see that at least the client side needs input validation

  Scenario: Dynamic detection
    When I access https://localhost/index.jsp
    Then I can enter some input that should be invalid like a negative number
    And I see the cost can result in a negative as seen in [evidence](img1.png)
    Then I realize I can use this to buy items for free

  Scenario: Exploitation
    When I access https://localhost/index.jsp
    Then I enter a "1" on the troll item field
    And also a "-200" on the megusta item field
    Then I click on "Place Order"
    And I can see the order was completed successfully
    And I also the price was -3000 dollars as seen in [evidence](img1.png)

  Scenario: Remediation
    When the website is in its development stage
    Then the user input should be validated so it stays within a given range
    And also so it doesn't accept anything besides numbers
    And this can be done by writing in the javascript code something like:
    """
    function checkinput(){
        if (enteredValue !== (range based on context)) {
            alert('Invalid input');
            return false;
        }
    }
    """
    And the input should also be validated on server side e.g like this:
    """
    if(is_numeric(userInput) == false) {
        $errorMsg=  "error : Not a number";
    }
    else{
    echo "No errors."
    }
    """
    And this way the exploit can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.2/10 (Medium) - CR:L/MAC:L/MUI:R/MC:N

  Scenario: Correlations
    No correlations have been found to this date 2019/06/11
