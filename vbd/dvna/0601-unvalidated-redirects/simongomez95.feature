## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Information Leakage
  Location:
    http://localhost:8000/app/redirect?url= - url (parameter)
  CWE:
    CWE-0601: URL Redirection to Untrusted Site ('Open Redirect') -variant-
      https://cwe.mitre.org/data/definitions/601.html
    CWE-0610: Externally Controlled Reference to a Resource in Another
    Sphere -class-
      https://cwe.mitre.org/data/definitions/610.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Redirect user to an unsafe location via a safe-looking URL
  Recommendation:
    Use a whitelist of approved URLs or domains to be used for redirection

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/core/appHandler.js"
    """
    186 module.exports.redirect = function (req, res) {
    187   if (req.query.url) {
    188     res.redirect(req.query.url)
    189   } else {
    190     res.send('invalid redirect url')
    191   }
    192 }
    """
    Then I see it redirects to any url passed in the parameter

  Scenario: Dynamic detection
  Changing redirect URL
    Given I go to "http://localhost:8000/app/redirect?url=http://localhost:8000"
    Then I get redirected to the homepage
    Then I try changin the URL parameter to an external URL
    """
    http://localhost:8000/app/redirect?url=http://google.com
    """
    Then I get redirected to "http://google.com"

  Scenario: Exploitation
  Using version info
    Given I can redirect users to whatever URL I want
    Then I send a user a link that redirects to a phishing page
    Then when the user tries to log in
    Then I get their credentials

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    186 module.exports.redirect = function (req, res) {
    187   if (req.query.url.startswith("http://localhost:8000/")) {
    188     res.redirect(req.query.url)
    189   } else {
    190     res.send('invalid redirect url')
    191   }
    192 }
    """
    Then the user will only be redirected if the URL points to a page in the app

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
