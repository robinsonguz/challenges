## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Cookie Management
  Location:
    / - connect.sid, continueCode, cookieconsent_status (Cookies)
  CWE:
    CWE-1004: Sensitive Cookie Without 'HttpOnly' Flag -variant-
      https://cwe.mitre.org/data/definitions/1004.html
    CWE-0732: Incorrect Permission Assignment for Critical Resource -class-
      https://cwe.mitre.org/data/definitions/732.html
    CWE-1011: Authorize Actors -category-
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-031: Accessing/Intercepting/Modifying HTTP Cookies -detailed-
      https://capec.mitre.org/data/definitions/31.html
    CAPEC-157: Sniffing Attacks -standard-
      https://capec.mitre.org/data/definitions/157.html
    CAPEC-117: Interception -meta-
      https://capec.mitre.org/data/definitions/117.html
  Rule:
    REQ.029: https://fluidattacks.com/web/es/rules/029/
  Goal:
    Steal cookies
  Recommendation:
    Set 'HttpOnly' and 'Secure' flag in sensitive cookies

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
    Given I check the code at "/dvna/server.js"
    """
    ...
    23  app.use(session({
    24    secret: 'keyboard cat',
    25    resave: true,
    26    saveUninitialized: true,
    27    cookie: { secure: false }
    28  }))
    ...
    """
    Then I see session cookies aren't being flagged as Secure

  Scenario: Dynamic detection
  Checking Secure flag in cookies
    Given I log in to the site
    And open Firefox Developer tools
    Then I check the cookies
    And find that none of them have the Secure attribute set
    Then I know they are sent unencrypted

  Scenario: Exploitation
  Sniffing non-secure cookies
    Given I know the cookies on the site aren't Secure
    Then I can sniff a user's traffic to the application
    And get their cookies
    Then I can hijack their session

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I patch the code like this
    """
    ...
    23  app.use(session({
    24    secret: 'keyboard cat',
    25    resave: true,
    26    saveUninitialized: true,
    27    cookie: { secure: true }
    28  }))
    ...
    """
    Then the session cookie is marked Secure and won't travel unencrypted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
