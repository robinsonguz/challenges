## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Information Leakage
  Location:
    /index.html - Page Information
  CWE:
    CWE-0285: Improper Authorization -class-
      https://cwe.mitre.org/data/definitions/285.html
    CWE-1011: Authorize Actors
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-001: Accessing Functionality Not Properly Constrained by
    ACLs -standard-
      http://capec.mitre.org/data/definitions/1.html
    CAPEC-122: Privilege Abuse -meta-
      http://capec.mitre.org/data/definitions/122.html
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    See a section of the app that shouldn't be accessible by me
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    44  router.get('/admin/users', authHandler.isAuthenticated, function(req,
    res){
    45      res.render('app/adminusers')
    46  })
    """
    Then I see it allows access just by being logged in

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/admin/users"
    Then I see a user list with their names and emails [evidence](users.png)

  Scenario: Exploitation
  Using version info
    Given I can see a list of users with their personal data
    Then I can use this data to attack them with social engineering

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    44  router.get('/admin/users', authHandler.isAuthenticated,
    authHandler.isAdmin, function(req, res){
    45      res.render('app/adminusers')
    46  })
    """
    Then I can't access admin pages without admin privilege anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-24
