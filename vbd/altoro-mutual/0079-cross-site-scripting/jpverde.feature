## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Category:
    Basic XSS
  Location:
    https://demo.testfire.net/search.jsp?query - search (field)
  CWE:
    CWE-80: Improper Neutralization of Script-Related
    HTML Tags in a Web Page (Basic XSS)
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject a script to pop up a javascript prompt() to get
    username and password.
  Recommendation:
    Encode the user form specifying Accept-Charset=UTF-8 to form elements

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0        |
  Machine information:
    Given A link for a webpage
    """
    http://demo.testfire.net/index.jsp
    """
    And A web app simulating a bank app

  Scenario: Normal use case
    Given A common search bar
    When User types any text and press enter or clicks search
    Then User gets redirected to the results page

  Scenario: Static detection
    When Looking at the source code where the search bar is located
    Then I can find in line 23 that this form is not encoded
    """
    <form id="frmSearch" method="get" action="/search.jsp">
    """
    And It uses GET method

  Scenario: Dynamic detection
    When I search something using search bar
    Then The text written is reflected in the results
    And The parameters are printed in URL using GET method
    When I try to use some html tags to show text in the body
    Then the result page is replaced shows
    """
    No results were found for the query:
    """
    And Reflect the html code that i sent
    And Print search parameters in url
    And [evidence](image1.png)

  Scenario: Exploitation
    When I input the following text simulating a login confirmation
    """
    <script>alert("Search needs login confirmation");
    var user = prompt("Username: "); var pwd = prompt("Password: ");
    </script><h1>Username is:
    <script type="text/javascript">document.write(user);</script>
    </h1><h2>Password is:
    <script type="text/javascript">document.write(pwd)</script></h2>
    """
    Then The site execute the code simulating a login from the prompt
    And It prints the text written in the results page
    And [evidence](image2.png)

  Scenario: Remediation
    When User input text in any field
    Then The user input must be encoded
    And Adding the Accept-Charset or Accept header to the specific field
    """
    Accept: text/html;charset=UTF-8
    Accept-Charset: ISO-8859-1
    """
    When This header is specified
    Then The text sent by the form is encoded so it can't manage html code
    And The vulnerability could be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.9/10 (Low) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    2.5/10 (Low) - IR:L/MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:N/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-23
