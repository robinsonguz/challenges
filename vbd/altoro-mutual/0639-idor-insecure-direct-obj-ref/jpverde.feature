## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Category:
    Insecure Direct Object Reference (IDOR)
  Location:
    https://demo.testfire.net/bank/doTransfer - amount (field)
    from account (field)
    to account (field)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.156 Source code without sensitive information
  Goal:
    Modify the amount of money to transfer during the request
  Recommendation:
    Use secure patterns to name objects and encrypt its values

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
    | Burp Suite CE   | 2.1.02      |
  Machine information:
    Given A link for a webpage
    """
    https://demo.testfire.net/bank/doTransfer
    """
    And A form to transfer money to other accounts

  Scenario: Normal use case
    Given A user transfers money from one to other accounts
    When User inputs the amount to transfer
    Then User presses the 'Transfer Money' button

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When Logged in as a common user
    Then I transfer 1 dollar from Savings account to Checking account
    And Using Burp Suite to intercept the transfer request
    And I can see in the request, the object names and its values
    And The names are "fromAccount", "toAccount" and "transferAmount"

  Scenario: Exploitation
    When Using Burp Suite to intercept the request
    Then I modify the value identified in the "transferAmount" object to 1000
    And I press forward to the request
    And The app identifies the transfer with a value of 1000 instead of 1
    And [evidence](image3.png)
    And This can also be used to change "fromAccount" and "toAccount" values

  Scenario: Remediation
    When A developer specifies objects
    Then He can use a secure pattern for the objects names
    And Can use a hash to encrypt the values sent in the API to the server
    When A hacker intercepts the request
    Then He won't see any familiar name nor strings or int as values
    And the vulnerability could be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.1/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:C/C:N/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:P/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    6.9/10 (Medium) - IR:H/MAV:N/MAC:H/MPR:N/MUI:R/MS:C/MC:N/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-29
