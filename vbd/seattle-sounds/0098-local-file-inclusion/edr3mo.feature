## Version 1.4.1
## language: en

Feature:
  TOE:
   seattle-sounds
  Category:
   local-file-inclusion
  Location:
    http://192.168.56.6/download.php?item=Brochure.pdf
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement in PHP
  Rule:
    REQ.037: https://fluidattacks.com/web/en/rules/037/
  Goal:
    Detect and Exploit a LFI vulnerability
  Recommendation:
    Avoid the inclusion of directory names and file paths in input fields

  Background:
  Hacker's software:
    | <Software name>             | <Version>   |
    | Kali Linux                  | 2019.1a     |
    | Firefox Quantum             | 60.6.2esr   |
    | BurpSuite Community Edition | 1.7.36      |
  TOE information:
    Given I am accessing the site
    """
    http://192.168.56.6/index.php
    """
    And I see an eCommerce site
    Then navigate through the site
    And I see five sections Home, Vinyl, Clothing, Blog and My Account
    When I visit the Vinyl section I see four products
    Then I visit the Clothing section and I can see four products
    And I see in the blog section four post from three users
    When I visit My Account section I see a login form

  Scenario: Normal use case
    Given I access the URL
    """
    http://192.168.56.6/download.php?item=Brochure.pdf
    """
    Then I can see in the home page the option to download a Brochure
    """
    192.168.56.6/download.php?item=Brochure.pdf
    """
    Then I see that I can select Vinyls and T-shirt products
    When I visit the Blog I can select the differnt posts
    And I see each post show sensitive information about the user
    Then I see in My account section a Login form and a Register Option

  Scenario: Static detection
    When I run the vulnerability scanner Nikto
    """
    nikto -h http://192.168.56.6
    """
    And I can see directories that shows possible sensitive data
    Then I conclude that I can possibly perform an LFI Attack

  Scenario: Dynamic detection
    Given I know that the page allow file inclusion
    And I read that this vulnerability is use with the GET request
    Then I search a GET request in the site
    And I can see that the Brochure download is through a GET request
    Then I use BurpSuite to capture this request
    And I send the request to the Repeater tool
    Then I use the repeater to write the path in the request
    And include the file etc/passwd
    Then I get in the response the etc/passwd file[evidence](lfi.png)

  Scenario: Exploitation
    Given I there is a GET request
    And I can use the GET request to include the etc/passwd file
    Then I use the URL with the path in Mozilla
    Then I conclude I use LFI to get the etc/passwd from the server

  Scenario: Remediation
    Given I have the web server credentials
    And I can access the etc/php.ini
    Then I look for the allow_url_fopen directive
    And I change it's value to Off
    When the include function is use it's necessary to validate
    And sanitize the input escaping specual characters as slash

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:H/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-05-16
