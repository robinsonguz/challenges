## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    URL Redirection to Untrusted Site
  Location:
    http://testphp.vulnweb.com/redir.php - r (field)
  CWE:
    CWE-601: URL Redirection to Untrusted Site ('Open Redirect')
  Rule:
    REQ.050: https://fluidattacks.com/web/rules/050/
    REQ.158: https://fluidattacks.com/web/rules/158/
    REQ.241: https://fluidattacks.com/web/rules/241/
  Goal:
    Get script to open redirect to another website.
  Recommendation:
    Avoid having scripts that can redirect to any website passing the url.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | google search   | n/a         |
  TOE information:
    Given I have access to the url http://testphp.vulnweb.com/redir.php
    And the server is running nginx version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/redir.php?r=categories.php
    Then I am redirected to the categories page.

  Scenario: Static detection
    When I look at the code redir.php.
    Then I am able to see that this script redirects without validation.
    """
    $redir = str_replace(array("\r", "\n"), " ", $_GET['r']);
    header("Location: ".$_GET['r']);
    """
    And I conclude that the script is vulnerable to open redirect

  Scenario: Dynamic detection
    When I use google to search for directories in the testphp.vulnweb.com
    Then I get the folder where the script is located
    When I open the following url
    """
    http://testphp.vulnweb.com/redir.php?r=categories.php
    """
    Then I am redirected to a local page of the site.
    When I change the parameter to a external URL
    """
    redir.php?r=https://fluidattacks.com/
    """
    Then I am redirected to the external site
    And I conclude that the site allows open redirect

  Scenario: Exploitation
    When I open the url
    And I add any external site in the r parameter
    """
    http://testphp.vulnweb.com/redir.php?r=http://externalsite
    """
    Then I am redirected to the externalsite
    Then I conclude that open redirect vulnerability can be exploited.

  Scenario: Remediation
    When file redir.php validates redirects
    And the script forces redirect only to trusted domains
    Then I open the URL: http://testphp.vulnweb.com/redir.php
    Then I pass https://fluidattacks.com/web/ as parameter
    Then I should not be redirected to the other site.
    And I conclude that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.1/10 (Medium) - CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.1/10 (Medium) - E:H/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-29
