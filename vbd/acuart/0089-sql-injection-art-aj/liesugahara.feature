## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Category:
    SQL Injection
  Location:
    http://testphp.vulnweb.com/AJAX/infoartist.php - id
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get DB information
  Recommendation:
    Use parameterized queries

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 60.7.2      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/
    And enter a php site that uses SQL queries

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/AJAX/infoartist.php?id=3
    When I look at the page
    Then I can see all the information about artist 3 [evidence](artist.png)

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    Given I access http://testphp.vulnweb.com/AJAX/infoartist.php?id=3
    When I add a "'" at the end
    Then I get a SQL error [evidence](error.png)
    And I can conclude that it is vulnerable to SQL injections.

  Scenario: Exploitation
    Given I access http://testphp.vulnweb.com/AJAX/infoartist.php?id=3
    And I know the site is vulnerable to SQLi
    When I add a "group by" to the URL
    And an aditional number until I get an error
    """
    group by 1,2,3--+-
    """
    Then I get that the column number is 3
    When I add the following to get information about the DB.
    """
    http://testphp.vulnweb.com/AJAX/infoartist.php?id=3 AND
    3=2 UNION ALL SELECT 1,version(),user() --+-
    """
    Then I can see the the parameters I requested [evidence](sqli.png)
    And I can conclude that I can get DB parameters through SQL Injection.

  Scenario: Remediation
    When the website is being developed
    Then parameterized queries should be implemented
    And the inputs should be whitelisted
    Then the website would no longer be vulnerable.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.6/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:M/IR:L/MAV:N/MAC:L/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-25
