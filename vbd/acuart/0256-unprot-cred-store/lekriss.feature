## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0256: Unprotected Storage of Credentials
  Rule:
    REQ.127 Store hashed passwords
  Goal:
    Access the database and recover the stored passwords
  Recommendation:
    Hashing the passwords before storing them

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
    |sqlmap                | 1.3.4-37       |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered /search.php

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/search.php
    And I click on "search"
    Then I can see this http://testphp.vulnweb.com/searh.php?test=query

  Scenario: Static detection:
    Given I try to open the source code of the page
    Then it is not possible to access to the source code

  Scenario: Dynamic detection:
    Given I open sqlmap with parameter "-u" as
    """
    http://testphp.vulnweb.com/seacrh.php?test=query
    """
    Then it throws the name of the database which is "acuart"
    Then I conclude that it is possible to access the stored data

  Scenario: Exploitation:
    Given I open sqlmap with parameter "-u" as
    """
    http://testphp.vulnweb.com/seacrh.php?test=query
    """
    Then it throws the name of the database which is "acuart"
    Then I can add parameter "-D" as "acuart"
    And I can see the tables of that database
    And I see the interesting table "users"
    Then I execute sqlmap adding "-T users" parameters
    And "-C uname,pass" and "--dump" parameters
    And I can see the only entry on the table
    """"
    +-------+--------+
    | uname | pass   |
    +-------+--------+
    |test   |test    |
    +-------+--------+
    """"
    Then I try to get access with the stolen credentials
    And I gain access to the web site
    Then I conclude that the password is being stored without protection

  Scenario: Remediation:
    When the passwords are stored in the database
    Then they should be hashed before being stored
    And ensure that even if the DB is accessed, it is not possible to gain them

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    8.2 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (Critical) - /CR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:N/MA:N/

  Scenario: Correlations
    Given I do an SQL injection in search.php
    Then I can use that gap to access to the database
    And obtain the stored passwords without having legitimate access
