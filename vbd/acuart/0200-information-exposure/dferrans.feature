## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Information exposure
  Location:
    http://testphp.vulnweb.com/secured/phpinfo.php
  CWE:
    CWE-200: Information exposure
  Rule:
    REQ.006: https://fluidattacks.com/web/rules/037/
  Goal:
    Get unprotected information in the server protected area
  Recommendation:
    Disable phpinfo() function in php.ini file

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | dirbuster       | 0.9.12      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And the server is running Ngxix version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/
    Then I can see the page

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection

  Scenario: Dynamic detection
    When I use dirbuster to try detect unprotected files.
    Then I get the  result url
    Then I get a detailed information about the server with phpinfo()
    Then I can see file[evidence](phpinfosite.png)
    Then I can conclude that this file should not be there.
    And I can conclude that detailed server information is being disclosed

  Scenario: Exploitation
    When I open  the url
    Then I can see all the parameters in the server.
    Then I can conclude that this urs is displaying confidential information

  Scenario Outline: Extraction
  Exposed files
    When I open URL detailed information of the server is displayed

  Scenario: Remediation
    When phpinfo() function is disabled
    Then no deatailed information of the server is displayed.
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/secured/phpinfo.php
    """
    Then I get blank page without displaying the server deailed configuration.
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.8.0/10 (Low) - E:U/RL:W/CR:L
  Environmental: Unique and relevant attributes to a specific user environment
    3.2/10 (Low) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date {2019-06-10}
