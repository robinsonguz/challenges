#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name gruyere -p "$1:8008" -t karthequian/gruyere
fi
