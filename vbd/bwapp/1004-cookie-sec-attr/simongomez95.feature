## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cookie Management
  Location:
    bwapp/ - security_level, PHPSESSID, movie_genre (Cookies)
  CWE:
    CWE-1004: Sensitive Cookie Without 'HttpOnly' Flag
  Rule:
    REQ.029: https://fluidattacks.com/web/en/rules/029/
  Goal:
    Steal cookies
  Recommendation:
    Set 'HttpOnly' flag in sensitive cookies

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running bwapp on http://bwapp

  Scenario: Normal use case
    Given I go to "http://bwapp/"
    Then I can log in and navigate the site

  Scenario: Static detection
  Given I check the code at "/bwapp/security.php"
    """
    ...
    21  session_start();
    22
    23  $addresses = array();
    24  @list($ip, $len) = explode('/', $AIM_subnet);
    ...
    """
    Then I see session cookies aren't being flagged as HttpOnly

  Scenario: Dynamic detection
  Checking HttpOnly flag in cookies
    Given I log in to the site
    And open Firefox Developer tools
    Then I check the cookies
    And find that none of them have HttpOnly set
    Then I know they are accessible via JavaScript

  Scenario: Exploitation
  Steal non-HttpOnly cookies
    Given I know the cookies on the site aren't HttpOnly
    Given I figure out a way to inject an XSS
    Then I can steal user cookies and hijack their sessions

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I patch the code as follows
    """
    ...
    20  ini_set( 'session.cookie_httponly', 1 );
    21  session_start();
    22
    23  $addresses = array();
    24  @list($ip, $len) = explode('/', $AIM_subnet);
    ...
    """
    Then the session cookies are flagged as HttpOnly
    Then they can't be stolen anymore via XSS

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I leave a stored xss attack that steals the user cookies
      And a user goes to the comments section, giving me their cookies
      Then I can use them and spoof the victims identity on the site
  Scenario: Correlations
    systems/webgoat/79-dom-xss
    systems/webgoat/80-reflected-xss
      Given I trick a user into clicking on a xss-injected link for the site
      Then I steal their cookies
      Then I can use them and spoof the victims identity on the site
