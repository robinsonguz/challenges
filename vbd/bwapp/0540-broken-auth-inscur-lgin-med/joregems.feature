## Version 1.4.1
## language: en

Feature: Detect and exploit vuln Insecure Login Forms
  From the bWAPP application
  From the A2 - Broken auth & Sessiong mgmt
  With Medium security level

  TOE:
    bWAPP
  Category:
    0540
  Location:
   http://localhost/bwapp/ba_insecure_login_2.php - passphrase (field)

  CWE: V
  CWE-540: Information Exposure Through Source Code
  Rule:
    REQ.156 Source code without sensitive information
  Goal:
    Detect and exploit vuln Insecure Login Forms
  Recommendation:
    Use external database connection for credentials

  Background:

  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu 18.1             | x64 |
    | firefix quantum      | 65.0.1 (64-bit) |

  TOE information:
    Given the site http://localhost/bWAPP/ba_insecure_login_2.php
    And I am trying to access the page througth the login form
    When I'm not authorized for that

  Scenario: Normal use case
  I access given I know the passphrase and name
    Given I access http://localhost/bwapp/ba_insecure_login_2.php
    And put the correct passphrase and hit the unlock button
    Then appears in green text "The secret was unlocked: HULK SMASH!"

  Scenario: Alternative use case
  Not grant access given I don't know the name and passphrase
    Given I access http://localhost/bwapp/ba_insecure_login_2.php
    And put the any password aside the correct one and hit the unlock button
    Then appears in red text "Still locked... Don't lose your temper Bruce! "

  Scenario: Static detection
  I check the php code seeking for clues that allow me find the passphrase
    Given I have priviligied access to the code
    When I'm reading the code I find
    """
    137 if(isset($_REQUEST["secret"]))
    138 {
    139
    140     if($_REQUEST["secret"] == "hulk smash!")
    141     {
    142
    143         $message = "<font color=\"green\">The secret was unlocked:
                 HULK SMASH!</font>";
    144
    145     }
    146
    147     else
    148     {
    149
    150         $message = "<font color=\"red\">Still locked...
                Don't lose your temper Bruce!</font>";
    151
    152     }
    153
    154 }
    """
    Then I continue searching for more vulnerabilities
    Then I discover that button have associated a js function
    Then I search the function unlock_secret()
    And Find the code that is exposing information
    """
    175 function unlock_secret()
    176 {
    177
    178     var bWAPP = "bash update killed my shells!"
    179
    180     var a = bWAPP.charAt(0);  var d = bWAPP.charAt(3);
      var r = bWAPP.charAt(16);
    181     var b = bWAPP.charAt(1);  var e = bWAPP.charAt(4);
      var j = bWAPP.charAt(9);
    182     var c = bWAPP.charAt(2);  var f = bWAPP.charAt(5);
      var g = bWAPP.charAt(4);
    183     var j = bWAPP.charAt(9);  var h = bWAPP.charAt(6);
      var l = bWAPP.charAt(11);
    184     var g = bWAPP.charAt(4);  var i = bWAPP.charAt(7);
      var x = bWAPP.charAt(4);
    185     var l = bWAPP.charAt(11); var p = bWAPP.charAt(23);
     var m = bWAPP.charAt(4);
    186     var s = bWAPP.charAt(17); var k = bWAPP.charAt(10);
     var d = bWAPP.charAt(23);
    187     var t = bWAPP.charAt(2);  var n = bWAPP.charAt(12);
     var e = bWAPP.charAt(4);
    188     var a = bWAPP.charAt(1);  var o = bWAPP.charAt(13);
     var f = bWAPP.charAt(5);
    189     var b = bWAPP.charAt(1);  var q = bWAPP.charAt(15);
     var h = bWAPP.charAt(9);
    190     var c = bWAPP.charAt(2);  var h = bWAPP.charAt(2);
      var i = bWAPP.charAt(7);
    191     var j = bWAPP.charAt(5);  var i = bWAPP.charAt(7);
      var y = bWAPP.charAt(22);
    192     var g = bWAPP.charAt(1);  var p = bWAPP.charAt(4);
      var p = bWAPP.charAt(28);
    193     var l = bWAPP.charAt(11); var k = bWAPP.charAt(14);
    194     var q = bWAPP.charAt(12); var n = bWAPP.charAt(12);
    195     var m = bWAPP.charAt(4);  var o = bWAPP.charAt(19);
    196
    197     var secret = (d + "" + j + "" + k + "" + q + "" + x +
             "" + t + "" +o + "" + g + "" + h + "" + d + "" + p);
    198
    199     if(document.forms[0].passphrase.value == secret)
    200     {
    201
    202         // Unlocked
    203         location.href="<?php echo($_SERVER["SCRIPT_NAME"]);
     ?>?secret=" + secret;
    204
    205     }
    206
    207     else
    208     {
    209
    210         // Locked
    211         location.href="<?php echo($_SERVER["SCRIPT_NAME"]); ?>?secret=";
    212
    213     }
    214
    """
    And find that the user name is exposed too
    """
    261        <p><label for="name">Name:</label>
    <font color="white">brucebanner</font><br />
    262<input type="text" id="name" name="name" size="20" value="brucebanner"/>
    </p>
    """
    Then I can conclude the code is exposing sensible information

  Scenario: Dynamic detection
  I check the html code seeking for clues that allow me find the password
    Given the login form from the page and the user name is setted
    When I put any random text and hit the unlock button
    Then appears in red text
    """
    Still locked... Don't lose your temper Bruce!
    """
    Then I hit right click on the unlock button and open the element inspector
    Then I discover that button have associated a js function
    Then I search that function unlock_secret()
    And get
    """
    function unlock_secret()
    {

      var bWAPP = "bash update killed my shells!"

      var a = bWAPP.charAt(0);  var d = bWAPP.charAt(3);
        var r = bWAPP.charAt(16);
      var b = bWAPP.charAt(1);  var e = bWAPP.charAt(4);
        var j = bWAPP.charAt(9);
      var c = bWAPP.charAt(2);  var f = bWAPP.charAt(5);
        var g = bWAPP.charAt(4);
      var j = bWAPP.charAt(9);  var h = bWAPP.charAt(6);
        var l = bWAPP.charAt(11);
      var g = bWAPP.charAt(4);  var i = bWAPP.charAt(7);
        var x = bWAPP.charAt(4);
      var l = bWAPP.charAt(11); var p = bWAPP.charAt(23);
       var m = bWAPP.charAt(4);
      var s = bWAPP.charAt(17); var k = bWAPP.charAt(10);
       var d = bWAPP.charAt(23);
      var t = bWAPP.charAt(2);  var n = bWAPP.charAt(12);
       var e = bWAPP.charAt(4);
      var a = bWAPP.charAt(1);  var o = bWAPP.charAt(13);
       var f = bWAPP.charAt(5);
      var b = bWAPP.charAt(1);  var q = bWAPP.charAt(15);
       var h = bWAPP.charAt(9);
      var c = bWAPP.charAt(2);  var h = bWAPP.charAt(2);
        var i = bWAPP.charAt(7);
      var j = bWAPP.charAt(5);  var i = bWAPP.charAt(7);
        var y = bWAPP.charAt(22);
      var g = bWAPP.charAt(1);  var p = bWAPP.charAt(4);
        var p = bWAPP.charAt(28);
      var l = bWAPP.charAt(11); var k = bWAPP.charAt(14);
      var q = bWAPP.charAt(12); var n = bWAPP.charAt(12);
      var m = bWAPP.charAt(4);  var o = bWAPP.charAt(19);

      var secret = (d + "" + j + "" + k + "" + q + "" + x
        + "" + t + "" +o + "" + g + "" + h + "" + d + "" + p);

      if(document.forms[0].passphrase.value == secret)
      {
          // Unlocked
          location.href="/bWAPP/ba_insecure_login_2.php?secret=" + secret;

      }
      else
      {

          // Locked
          location.href="/bWAPP/ba_insecure_login_2.php?secret=";
      }

    }
    """
    Then I right click anywhere on the page
    And select inspect element then select the console tab
    Then copy part of the code from the -unlock_secret- function I found
    """
    var bWAPP = "bash update killed my shells!"

    var a = bWAPP.charAt(0);  var d = bWAPP.charAt(3);
      var r = bWAPP.charAt(16);
    var b = bWAPP.charAt(1);  var e = bWAPP.charAt(4);
      var j = bWAPP.charAt(9);
    var c = bWAPP.charAt(2);  var f = bWAPP.charAt(5);
      var g = bWAPP.charAt(4);
    var j = bWAPP.charAt(9);  var h = bWAPP.charAt(6);
      var l = bWAPP.charAt(11);
    var g = bWAPP.charAt(4);  var i = bWAPP.charAt(7);
      var x = bWAPP.charAt(4);
    var l = bWAPP.charAt(11); var p = bWAPP.charAt(23);
     var m = bWAPP.charAt(4);
    var s = bWAPP.charAt(17); var k = bWAPP.charAt(10);
     var d = bWAPP.charAt(23);
    var t = bWAPP.charAt(2);  var n = bWAPP.charAt(12);
     var e = bWAPP.charAt(4);
    var a = bWAPP.charAt(1);  var o = bWAPP.charAt(13);
     var f = bWAPP.charAt(5);
    var b = bWAPP.charAt(1);  var q = bWAPP.charAt(15);
     var h = bWAPP.charAt(9);
    var c = bWAPP.charAt(2);  var h = bWAPP.charAt(2);
      var i = bWAPP.charAt(7);
    var j = bWAPP.charAt(5);  var i = bWAPP.charAt(7);
      var y = bWAPP.charAt(22);
    var g = bWAPP.charAt(1);  var p = bWAPP.charAt(4);
      var p = bWAPP.charAt(28);
    var l = bWAPP.charAt(11); var k = bWAPP.charAt(14);
    var q = bWAPP.charAt(12); var n = bWAPP.charAt(12);
    var m = bWAPP.charAt(4);  var o = bWAPP.charAt(19);

    var secret = (d + "" + j + "" + k + "" + q
    + "" + x + "" + t + "" +o + "" + g + "" + h + "" + d + "" + p);
    """
    And paste it in the console
    And write in the console "secret"
    Then I get the password
    """
    "hulk smash!"
    """
    Then I can conclude
    And the vulnerability is being caused by unlock_secret function
    And is very likely I could use it for exploit

  Scenario: Dynamic exploitation
    Given I got the password and the name field is already filled
    Then I put that password in the login Passphrase field and get
    """
     The secret was unlocked: HULK SMASH!
    """

  Scenario: Remediation
    Given Mi patched the code by adding new user with msql server doing
  """
  INSERT INTO users (login, password, email, secret, activated)
  VALUES ('brucebanner',SHA1("hulk smash!"),'brucebanner@marvel.example','a',1);
  """
  And adding the nex line at line 22
  """
  include("connect_i.php");
  """
  And replacing part of the code at lines 137 TO 154 for
  """
  if(isset($_POST["form"]))
    {
      $name = $_POST["name"];
      $name = mysqli_real_escape_string($link, $name);

      $passphrase = $_POST["passphrase"];
      $passphrase = mysqli_real_escape_string($link, $passphrase);
      $passphrase = hash("sha1", $passphrase, false);

      $sql = "SELECT * FROM users WHERE login = '" . $name;
      $sql.= "' AND BINARY password = '" . $passphrase . "'";
      $sql.= " AND login ='brucebanner'";
      $sql.= " AND activated = 1;";


      $recordset = $link->query($sql);

      if(!$recordset)
      {

          die("Error: " . $link->error);

      }

      else
      {
          $row = $recordset->fetch_object();

          if($row)
          {
          $message = "<font color=\"green\">
          The secret was unlocked: HULK SMASH!</font>";
          }

          else
          {

            $message = "<font color=\"red\">
            Still locked... Don't lose your temper Bruce!</font>";

          }

      }
      $link->close();

    }
  """
  And replacing lines 259 TO 269 for
  """
  <form action="<?php echo($_SERVER["SCRIPT_NAME"]);
  ?>" method="POST" id= "form-login">

    <p><label for="name">Name:</label><font color="white"></font><br />
    <input type="text" id="name" name="name" size="20"></p>

    <p><label for="passphrase">Passphrase:</label><br />
    <input type="password" id="passphrase" name="passphrase" size="20" /></p>
    <button type="submit" name="form" value="submit">Unlock</button>

  </form>
  """
  And finally deleting lines 173 to 217
  Then I tried searching the passphrase again and I can't
  Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
    7.5/10 AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 E:H/RL:W/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 CR:M/IR:M/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MI:N/MA:N

  Scenario: Correlations
    Given No correlations have been found to this date 2019-04-23
