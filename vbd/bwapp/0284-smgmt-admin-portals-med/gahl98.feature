## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    A2 - Broken Auth. & Session Mgmt.
    Session Management - Administrative Portals
  Location:
    http://localhost/smgmt_admin_portal.php
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.096: https://fluidattacks.com/web/rules/096/
    REQ.097: https://fluidattacks.com/web/rules/097/
    REQ.269: https://fluidattacks.com/web/rules/300/
  Goal:
    Gain full access to Administrative Portals page
  Recommendation:
    Restrict access by validating if the current user has admin privileges.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Chromium        | 75.0.3770   |
    | Burp            | 2.1         |
  TOE information:
    Given I am accessing the site http://bwapp.test/login.php
    And bWAPP is running from the "raesene/bwapp" container
    And it runs Ubuntu 4.14
    And an Apache 2.4.7 server
    And the PHP version is 5.5.9
    And the DB is mysql version 5.5.47
    And docker engine is 18.09.8
    And I log in with default user credentials
    """
    Login: bee
    Password: bug
    """
    And I set the security level on medium
    And I hit the "Login" button

  Scenario: Normal use case
    Given I access http://bwapp.test/login.php
    And at the "Choose your bug:" drop-down list I choose
    """
    Session Mgmt. - Administrative Portals
    """
    And redirects me http://bwapp.test/smgmt_admin_portal.php
    And I click the "Hack" button
    And it loads a page and a red sign appears [evidence](locked.png)
    """
    This page is locked
    """
    And after that a line when it reads "HINT: check the cookies..."

  Scenario: Static detection
    When I look at the php file of the page "smgmt_admin_portal.php"
    Then I see it validates the admin COOKIE value
    And it gives access if it equals 1
    """
    61  if((isset($_COOKIE["admin"])))
    62  {
    63    if($_COOKIE["admin"] == "1")
    """
    Then I can conclude that the admin validation only checks that cookie value

  Scenario: Dynamic detection
    Given I access http://bwapp.test/smgmt_admin_portal.php
    And I check the GET request at Burp
    Then I see the cookies and I find that there are three of them
    And they are "PHPSESSID", "security_level" and "admin"
    And I see for "admin" their value equal 0
    Then I can conclude that modifying that value must give admin access.

  Scenario: Exploitation
    Given the app validates admin status through a cookie
    Then I access http://bwapp.test/smgmt_admin_portal.php
    And I open the developer's tools in Chromium
    And I go to the Application tab
    And I check the Cookies in the Storage section
    Then I see the same cookies as before
    Then I set the admin cookie value to 1
    And I reload the page
    And the text at the page's body changes
    And it says in green letters at last line [evidence](Cowabunga.png)
    """
    Cowabunga...
    You unlocked this page using a cookie manipulation.
    """

  Scenario: Remediation
    Given I have opened the "login.php" file
    And I see the session variable has an admin key
    Then I patch validation to only allow the admin check throw session values
    """
    61  if((isset($_SESSION["admin"])))
    62  {
    63    if($_SESSION["admin"] == "1")
    """
    When I load the http://bwapp.test/smgmt_admin_portal.php page
    And I find the default user has admin permissions
    Then for testing, I create another user
    And I log in with that one
    And it only shows the rejection message
    """
    This page is locked.
    HINT: contact your dba...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.4/10 (Medium) - CR:L/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-19
