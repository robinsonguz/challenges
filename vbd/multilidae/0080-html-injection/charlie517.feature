## Version 1.4.1
## language: en

Feature:
  TOE:
    Multillidae
  Location:
    http://127.0.0.1/index.php?page=add-to-your-blog.php
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags
    in a Web Page (Basic XSS)
  Rule:
    REQ.173 Discard unsafe inputs.
  Goal:
    Get HTML code to be executed on the server.
  Recommendation:
    Discard all potentially harmfull HTML code.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | 4.15.0-45   |
    | Firefox         | 65.0        |
  TOE information:
    Given I am accessing the site 127.0.0.1/index.php?page=add-to-your-blog.php
    And enter a php site that allows me add post on a blog.
    And PHP version 5.5.9
    And is running on Ubuntu 4.25

  Scenario: Normal use case
  The web page allows you to add a blog post.
    Given I access the page
    And open add my text in the text box
    Then I can see [evidence](evidence1.png)

  Scenario: Static detection
  The unsanitized input in the text box can be used to execute code (HTMLi)
    When I look at the code add-to-your-blog.php
    Then I can see that the vulnerability is in line 126 to line 126
    """
    $lBlogEntry = $_REQUEST["blog_entry"];
    $SQLQueryHandler->insertBlogRecord($lLoggedInUser, $lBlogEntry);
    """
    Then I can conclude that I can inject code in the text box.

  Scenario: Dynamic detection
  I can use HTML tags to make an injection of code.
    Given I wrote on the text box
    And I saw I could use HTML tags
    Then I can execute the following:
    """
    <html>
    <body>
    <script>
    alert('Hello, world!');
    </script>
    </body>
    </html>
    """
    Then I get the output:
    """
    [evidence2](evidence2.png)
    """
    Then I can conclude that I can exploit this to get sensitive information.

  Scenario: Exploitation
  The web page allow me to use HTML tag inside the text box
    Given I can use this kind of tags I can exploit the web page.
    Then I can execute the following:
    """
    <html>
    <body>
    <script>
    alert('Hello, world!');
    </script>
    </body>
    </html>
    """
    Then I get the output:
    """
    [evidence2](evidence2.png)
    """
    Then I can conclude that combining this with sqli I can get sensitive info.

  Scenario: Remediation
  Sanitize input on every field to prevent the use of special characters.
    Given I have patched the code by doing a sanitation
    """
    $lBlogEntry = $_REQUEST["blog_entry"];
    $SQLQueryHandler->insertBlogRecord($lLoggedInUser, $lBlogEntry);
    $clean=strip_tag=("SQLQueryHandler);
    """
    Then If I re-run my exploit:
    """
    <html>
    <body>
    <script>
    alert('Hello, world!');
    </script>
    </body>
    </html>
    """
    Then I get:
    """
    [evidence3](evidence3)
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.9/10 (low) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    0/10 (low) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    0/10 (low) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-02-12