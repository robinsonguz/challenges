## Version 1.4.1
## language: en

Feature:
  TOE:
    Mutillidae 2.1.19
  Category:
    Cross Site Scripting
  Location:
    http://mutillidae/index.php?page=show-log.php
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  CAPEC:
    CAPEC-592: Stored XSS -detailed-
      http://capec.mitre.org/data/definitions/592.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Store JavaScript code in the database and execute it
  Recommendation:
    Encode output logs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  TOE information:
    Given The site saves each log record in the database
    And The the log page displays each search of the "DNS Lookup" page
    And We already know the "DNS Lookup" page is sensitive to Reflected XSS
    And This happens In several site pages
    But "DNS Lookup" page is going to be used to expose the vulnerability

  Scenario: Normal use case
    When I go to "DNS Lookup" page and I enter the next string [evidence](evidence.png)
    """
    Hello this is a DNS
    """
    Then The log page displays that search [evidence](evidence2.png)

  Scenario: Static detection
    When I check out the code "logHandle.php"
    """
    switch ($this->mSecurityLevel){
      case "0": // This code is insecure, we are not encoding output
      case "1": // This code is insecure, we are not encoding output
        $this->encodeOutput = FALSE;
        /* stopSQLInjection is set to true even in
        * insecure configuration because trying to log
        * sql injections breaks the log handler which then
        * breaks the calling page. Since SQL injections are
        * allowed, we dont want the logger to break and stop
        * the SQL injection.
        */
        $this->stopSQLInjection = TRUE;
        break;

      case "2":
      case "3":
      case "4":
      case "5": // This code is fairly secure
        // If we are secure, then we encode all output.
        $this->encodeOutput = TRUE;
        $this->stopSQLInjection = TRUE;
      break;
    }// end switchx
    """
    Then I realize, in that switch-case block the output is not encoded until case 5, where the variable is "true"
    And The variable who decide if the output is encoded is a boolean and its value is "false" until case 5
    When I see the code again, I found a "if statement"
    """
    if (!$this->encodeOutput){
      $lUserAgent = $_SERVER['HTTP_USER_AGENT'];
    }else{
      /* Cross site scripting defense */
         // encode the entire message following OWASP standards
         // this is HTML encoding because we are outputting data into HTML
      $lUserAgent = $this->Encoder->encodeForHTML($_SERVER['HTTP_USER_AGENT']);
    }// end if
    """
    Then This "if statement" checks if the variable "encodeOutput" is true
    And If it is true, encode the output
    And "If" the output is not encoded, a Stored XSS is allowed
  Scenario: Dynamic detection
  Detecting Stored XSS
    When I enter a value in the "DNS Lookup" page [evidence](evidence3.png)
    Then The value is displayed in the log page
    When I input HTML tags in the "DNS Lookup" input
    Then I can conclude the site is vulnerable to Cross-site scripting [evidence](evidence4.png)

  Scenario: Exploitation
    When I enter the following string in the "DNS Lookup" input
    """
    <script>
      for(var i = 1; i < 10; i++) {
        alert("Wow, this is gonna hurt b0i \n
        you recieve: "+ i * 102231 + " of damage");
      }
    </script>
    """
    Then The "DNS Lookup" page executes the JavaScript code [evidence](evidence5.png)
    And The log page executes the JavaScript too [evidence](evidence6.png)

  Scenario: Remediation
    Given The next block code in the file "logHandle.php"
    """
    switch ($this->mSecurityLevel){
      case "0": // This code is insecure, we are not encoding output
      case "1": // This code is insecure, we are not encoding output
        $this->encodeOutput = FALSE;
        /* stopSQLInjection is set to true even in
        * insecure configuration because trying to log
        * sql injections breaks the log handler which then
        * breaks the calling page. Since SQL injections are
        * allowed, we dont want the logger to break and stop
        * the SQL injection.
        */
        $this->stopSQLInjection = TRUE;
        break;

      case "2":
      case "3":
      case "4":
      case "5": // This code is fairly secure
        // If we are secure, then we encode all output.
        $this->encodeOutput = TRUE;
        $this->stopSQLInjection = TRUE;
      break;
    }// end switch
    """
    When I set the variable "$this->encodeOutput = TRUE;" for all cases
    """
    switch ($this->mSecurityLevel){
      case "0": $this->encodeOutput = TRUE
      case "1": $this->encodeOutput = TRUE
        $this->encodeOutput = FALSE;
        /* stopSQLInjection is set to true even in
        * insecure configuration because trying to log
        * sql injections breaks the log handler which then
        * breaks the calling page. Since SQL injections are
        * allowed, we dont want the logger to break and stop
        * the SQL injection.
        */
        $this->stopSQLInjection = TRUE;
        break;

      case "2": $this->encodeOutput = TRUE
      case "3": $this->encodeOutput = TRUE
      case "4": $this->encodeOutput = TRUE
      case "5": // This code is fairly secure
        // If we are secure, then we encode all output.
        $this->encodeOutput = TRUE;
        $this->stopSQLInjection = TRUE;
      break;
    }// end switch
    """
    Then Inside the "if statement" would encode the output
    And It would be enough

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:N/I:N/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:H/RL:T/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.6/10 (Medium) - CR:L/IR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-02-07
