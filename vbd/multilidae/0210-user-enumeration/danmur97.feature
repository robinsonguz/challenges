## Version 1.4.1
## language: en

Feature:
  TOE:
    Mutillidae
  Category:
    SFP Secondary Cluster: Exposed Data
  Location:
    http://localhost/mutillidae/index.php?page=login.php
  CWE:
    CWE-210: Information Exposure Through Self-generated Error Message
  Rule:
    REQ.105 Avoid assets leakage
  Goal:
    Enumerate valid usernames
  Recommendation:
    Use generic error message for failed authentication attempts

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Windows         | 10                |
    | Firefox         | 67.0.3            |
  TOE information:
    Given I am accessing the site "http://localhost/mutillidae"
    And it is built with php and mysql

  Scenario: Normal use case
    Given I access "http://localhost/mutillidae/index.php?page=login.php"
    And a form for user authentication
    When I supply a user
    And a password
    Then the application gives access if data is correct
    But if not, the application shows one of these messages
    """
    Account does not exist
    Password incorrect
    """

  Scenario: Static detection
    When I look at the code in "login.php"
    Then I see the variable that is determining the state of the error message
    """
    38 echo "var lAuthenticationAttemptResultFlag =
          {$lAuthenticationAttemptResult};" . PHP_EOL;
    """
    Then php variable "$lAuthenticationAttemptResult" is set by server
    When I look at "includes/process-login-attempt.php"
    Then I can see the vulnerability
    And is being caused by lines 58 to line 62
    """
    58 if ($lConfidentialityRequired){
    59   $lAuthenticationAttemptResult = $cUSERNAME_OR_PASSWORD_INCORRECT;
    60 }else{
    61   $lAuthenticationAttemptResult = $cACCOUNT_DOES_NOT_EXIST;
    62 }
    """
    And lines 69 to line 73
    """
    69 if ($lConfidentialityRequired){
    70   $lAuthenticationAttemptResult = $cUSERNAME_OR_PASSWORD_INCORRECT;
    71 }else{
    72   $lAuthenticationAttemptResult = $cPASSWORD_INCORRECT;
    73 }// end if
    """
    Then user existence can be inferred from the client side
    When checking javascript code
    Then user existence is determined specifically at line 919
    """
    919 var lAuthenticationAttemptResultFlag
    """

  Scenario: Dynamic detection
    Given the authentication fields
    And the existence flag in javascript
    When testing with random user/password combination
    Then inexistent user error appears
    And flag is set to 0
    When testing with a valid username
    But wrong password
    Then wrong password error appears
    And flag is set to 1
    Then I notice that sending a username
    And a random password
    And checking this flag
    Then user existence is determined

  Scenario: Exploitation
    Given the leakage of information about users existence
    When I build a program for testing the responses
    """
    See 'dictionary-username' file
    """
    And use curl library for sending requests
    And a custom dictionary of common users
    """
    Subset of the big dictionary found at
    https://github.com/duyetdev/bruteforce-database/blob/master/usernames.txt

    admin, adrian, Aggie, Aggy, Agna, Agnella, Agnes, Agnese, john, jeremy,
    kevin, tim, Agnesse, Agneta
    """
    Then I can determine valid user names as in [evidence](success.png)

  Scenario: Remediation
    Given I have patched the code by replacing lines 58 to 62
    And lines 69 to 73 with
    """
    $lAuthenticationAttemptResult = $cUSERNAME_OR_PASSWORD_INCORRECT;
    """
    Then error do not differentiate between incorrect user or password
    When I try the same vector attack
    Then I get [evidence](fail.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (medium) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.4/10 (medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    vbd/multilidae/0307
      Given there is no limit on the number of authentication attempts
      Then user enumeration can be done faster
      And password cracking can be done more efficiently
