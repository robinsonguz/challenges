## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Error Handling
  Location:
    / - Error Handling
  CWE:
    CWE-200: Information Exposure
  Rule:
    REQ.176: https://fluidattacks.com/web/es/rules/176/
  Goal:
    Use a deprecated endpoint
  Recommendation:
    Remove deprecated functions completely

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "fileUpload.js"
    """
    ...
    48  if (utils.endsWith(file.originalname.toLowerCase(), '.xml')) {
    49      if (utils.notSolved(challenges.deprecatedInterfaceChallenge)) {
    50        utils.solve(challenges.deprecatedInterfaceChallenge)
    51      }
    52      if (file.buffer && !utils.disableOnContainerEnv()) {
    53        const data = file.buffer.toString()
    54        try {
    55          const sandbox = { libxml, data }
    56          vm.createContext(sandbox)
    57          const xmlDoc = vm.runInContext('libxml.parseXml(data, { noblanks
    : true, noent: true, nocdata: true })', sandbox, { timeout: 2000 })
    58          const xmlString = xmlDoc.toString(false)
    59          if (utils.notSolved(challenges.xxeFileDisclosureChallenge) && (m
    atchesSystemIniFile(xmlString) || matchesEtcPasswdFile(xmlString))) {
    60            utils.solve(challenges.xxeFileDisclosureChallenge)
    61          }
    62          res.status(410)
    63          next(new Error('B2B customer complaints via file upload have bee
    n deprecated for security reasons: ' + utils.trunc(xmlString, 200) + ' (' +
    file.originalname + ')'))
    ...
    """
    Then I see there is code from a deprecated function

  Scenario: Dynamic detection
  Causing a system error
    Given I go to "http://localhost:8000/#/complain"
    Then I click on the button to upload a file
    Then I notice it accepts zip and pdf files
    But you can also change the selector to 'All Files'
    Then I do it and select a node shell JS file
    But it doesn't let me upload it
    Then I select a random XML file
    And the application accepts it, returning
    """
    <title>Error: B2B customer complaints via file upload have been deprecated
    for security reasons (uh.xml)</title>
    """

  Scenario: Exploitation
  Looking for a vulnerability
    Given I have access to a deprecated function
    Then it's probably not been patched recently
    And I know it parses XML
    Then I can play with it and look for a vulnerability

  Scenario: Remediation
  Access control
    Given I remove the deprecated function from the codebase
    Then it's not accessible anymore and not a threat

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-31
