## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Error Handling
  Location:
    / - Error Handling
  CWE:
    CWE-200: Information Exposure
  Rule:
    REQ.176: https://fluidattacks.com/web/es/rules/176/
  Goal:
    Expose error stacktrace
  Recommendation:
    Never show system errors to users

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "package.json"
    """
    ...
    137 "scripts": {
    138   "postinstall": "cd frontend && npm install && cd .. && npm run build",
    139   "serve": "concurrently --kill-others \"node app\" \"cd frontend && ng
    serve\"",
    140   "build": "cd frontend && node --max-old-space-size=8192 ./node_modules
    /@angular/cli/bin/ng build --aot --prod --output-hashing=none --vendor-chunk
    =true --source-map=false",
    141   "start": "node app",
    ...
    """
    Then I see it doesn't define "NODE_ENV" so it defaults to "development"

  Scenario: Dynamic detection
  Causing a system error
    Given I go to "http://localhost:8000/ftp/coupons_2013.md.bak"
    Then I see a stack trace for the forbidden file error [evidence](error.png)
    Then I know the server is configured to show full system errors

  Scenario: Exploitation
  Using leaked info
    Given I can see full stack traces on error
    Then I can use this information to plan my attacks

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    137 "scripts": {
    138   "postinstall": "cd frontend && npm install && cd .. && npm run build",
    139   "serve": "concurrently --kill-others \"node app\" \"cd frontend && ng
    serve\"",
    140   "build": "cd frontend && node --max-old-space-size=8192 ./node_modules
    /@angular/cli/bin/ng build --aot --prod --output-hashing=none --vendor-chunk
    =true --source-map=false",
    141   "start": "NODE_ENV=production node app",
    ...
    """
    Then the NODE_ENV is set to production and stacktraces aren't displayed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
