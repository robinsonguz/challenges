## Version 1.4.1
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Sensitive Data Exposure
  Location:
    /metrics - Information Exposure
  CWE:
    CWE-200: Exposure of Sensitive Information
  Rule:
    REQ.096: https://fluidattacks.com/web/rules/096/
  Goal:
    Access unauthorized data
  Recommendation:
    Set users' access permissions

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 19.10           |
    | Google Chrome   | 81.0.4044.138   |

  TOE information:
    Given I am running Juice-Shop on heroku at
    """
    https://localhost:3000/#/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I access https://localhost:3000/#/
    Then I see the site
    And I can navigate

  Scenario: Static detection
  No access control
    Given I access the site
    Then I tried to access to https://localhost:3000/#/metrics
    And It redirects to the home page of the store
    Then I tried to access to https://localhost:3000/metrics
    And I can see the metrics

  Scenario: Dynamic detection
  Accessing unprotected endpoints
    Given I know the app uses Prometheus as monitoring system
    Then Go to read the documentation available at:
    """
    https://prometheus.io/docs/introduction/first_steps/
    """
    Then Go the section Using the expression browser
    And Here explain how to access the metrics via the browser
    """
    Prometheus expects metrics to be available on targets on a path
    of /metrics. So this default job is scraping via the URL:
    http://localhost:9090/metrics.
    """
    Then I tried to access to https://localhost:3000/metrics
    And I can see the metrics
    Then I realize that this pages allow access to any loged in or not user

  Scenario: Exploitation
  Access unauthorized data
    Given I can access to metrics
    Then I can read important information about the app:
    """
    # HELP juiceshop_users_registered Number of registered users grouped
    by customer type.
    # TYPE juiceshop_users_registered gauge
    juiceshop_users_registered{type="standard",app="juiceshop"} 8
    juiceshop_users_registered{type="deluxe",app="juiceshop"} 2

    # HELP juiceshop_users_registered_total Total number of registered users.
    # TYPE juiceshop_users_registered_total gauge
    juiceshop_users_registered_total{app="juiceshop"} 17

    # HELP juiceshop_wallet_balance_total Total balance of all users' digital
    wallets.
    # TYPE juiceshop_wallet_balance_total gauge
    juiceshop_wallet_balance_total{app="juiceshop"} 500
    """

  Scenario: Remediation
  Access control
    Given I see the code at "server.js"
    """
    /* Serve metrics */
    const Metrics = metrics.observeMetrics()
    const metricsUpdateLoop = Metrics.updateLoop
    app.get('/metrics', metrics.serveMetrics())
    """
    Then I see it makes a GET request to /metrics endpoint
    And The info is public to anyone from the URL
    """
    https://localhost:3000/metrics
    """
    Then I modified the code like this
    """
    /* Serve metrics */
    const Metrics = metrics.observeMetrics()
    const metricsUpdateLoop = Metrics.updateLoop
    """
    And The endpoint is not accesible


  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - /AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - /E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - /CR:M/IR:L/AR:L


  Scenario: Correlations
    No correlations have been found to this date {2020-07-23}
