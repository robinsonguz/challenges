## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    Privacy Violation
  Location:
    http://localhost:3000/users/7/work_info - SSN
  CWE:
    CWE-359: Exposure of Private Information
  Rule:
    REQ.185: https://fluidattacks.com/web/rules/185/
    REQ.186: https://fluidattacks.com/web/rules/186/
    REQ.300: https://fluidattacks.com/web/rules/300/
  Goal:
    Get stored sensitive information
  Recommendation:
    Sensitive data should be encrypted everywhere.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 52.9.0      |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I login
    Then I can click on Work Info
    And I can see the last 4 digits of my Social Security Number (SSN).

  Scenario: Static detection
    When I look at the files in the views folder
    Then I can see that in "work_info/index.html.erb" all the info is rendered
    And it can be found in this file from line 26 to line 30.
    """
    26 <td><%= "#{@user.first_name} #{@user.last_name}" %></td>
    27 <td><%= @user.work_info.income %></td>
    28 <td><%= @user.work_info.bonuses %></td>
    29 <td><%= @user.work_info.years_worked %></td>
    30 <td class="ssn"><%= @user.work_info.SSN %></td>
    """
    Then I can see that no encrypt nor decrypt functions are being called.
    When I keep going through that file
    Then I can see that the SSN is being called directly from the DB
    And the script is replacing the first digits with "*"
    Then I can conclude that it is being stored in plain text.

  Scenario: Dynamic detection
    Given I access the rails console
    When I execute the following command to access work info:
    """
    $ WorkInfo.all
    """
    Then I get the output:
    """
    WorkInfo Load (0.2ms)  SELECT "work_infos".* FROM "work_infos"
    => [#<WorkInfo:0x000055ea6cda1d10
    id: 1,
    user_id: 1,
    income: "$30,000",
    bonuses: "7,000",
    years_worked: 1,
    SSN: "999-99-9999",
    DoB: Fri, 01 Jan 1982,
    created_at: Wed, 10 Jul 2019 15:27:38 UTC +00:00,
    updated_at: Wed, 10 Jul 2019 15:27:38 UTC +00:00,
    encrypted_ssn: nil>,
    """
    Then I can conclude that it is being stored in plain text.

  Scenario: Exploitation
    Given I access the rails console
    When I see all the work information after the dynamic detection
    Then I can see the SSN and that the encrypted SSN doesn't exist
    Then I can conclude that the SSN is being stored in plain text.

  Scenario: Remediation
    Given I have patched the code by encrypting the SSN upon signup
    And added the following to the user.rb model
    """
    work_info.build_key_management(:iv => SecureRandom.hex(32))
    """
    Then I added some seed data for new users in seeds.rb from line 310 to 317.
    """
    work_info.each do |wi|
      list = [:user_id, :SSN]
      info = WorkInfo.new(wi.reject {|k| list.include?(k)})
      info.user_id = wi[:user_id]
      info.build_key_management({:user_id => wi[:user_id],
      :iv => SecureRandom.hex(32) })
      info.SSN = wi[:SSN]
      info.save
    end
    """
    Then I changed the attribute being called when rendering the SSN
    And it can be found in "work_info/index.html.erb" from line 26 to line 30.
    """
    26 <td><%= "#{@user.first_name} #{@user.last_name}" %></td>
    27 <td><%= @user.work_info.income %></td>
    28 <td><%= @user.work_info.bonuses %></td>
    29 <td><%= @user.work_info.years_worked %></td>
    30 <td class="ssn"><%= @user.work_info.last_four %></td>
    """
    When I create a new user
    And access the rails console
    And execute the following command:
    """
    $ WorkInfo.last
    """
    Then I get the output:
    """
    WorkInfo Load (0.9ms) SELECT "work_infos".* FROM "work_infos"
    ORDER BY "work_infos"."id" DESC LIMIT ? [["LIMIT", 1]]
    => #<WorkInfo:0x000055ea6f5a6298
    id: 14,
    user_id: 10,
    income: "$30,000",
    bonuses: "7,000",
    years_worked: 1,
    SSN: nil,
    DoB: Fri, 01 Jan 1982,
    created_at: Fri, 12 Jul 2019 15:15:43 UTC +00:00,
    updated_at: Fri, 12 Jul 2019 15:15:43 UTC +00:00,
    encrypted_ssn: "5\x01\x17\xA3or_\x81z\x13\xDD\xAF\x12\x10V\xA8">
    """
    And I can see that the SSN is encrypted
    Then I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.1/10 (Medium) - CVSS:3.0/AV:L/AC:L/PR:H/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.3/10 (Medium) - CR:H/IR:L/AR:L/MAV:L/MAC:L/MPR:H/MS:U/MC:H/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
