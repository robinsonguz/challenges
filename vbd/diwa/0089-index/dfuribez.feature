## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Category:
    SQL injection
  Location:
    http://172.17.0.2/index.php - email (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL
    Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Log in as administrator
  Recommendation:
    Sanitize the inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux       | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site on http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I access at the index of the page
    And there is a login form at the top of the page
    And it asks for an email and password
    And it allows to either login or sign up

  Scenario: Static detection
    When I look at the code of model.php
    """
    19 $sql = 'SELECT * FROM ' . $this->prefix . 'users WHERE email = \''
        . $pEmail . '\' AND password = \'' .
        hash($pHashingAlgorithm, $pPassword) . '\';';
    20 return $this->db->query($sql)->fetchAll();
    """
    Then I can see that the DB query is done by query() instead of prepared()
    And no input validation it's done
    Then I can conclude that the code is vulnerable to SQLi

  Scenario: Dynamic detection
    Given the login form
    And I try to use "'" both as email as well as the password
    And I see the error [evidence](error.png)
    When I use "'" as the email only
    Then I get the same error as before
    When I use "'" as the password only
    Then I see the message
    """
    Wrong E-Mail-Address or Password
    """
    And I can conclude that email might be vulnerabl to SQL injection

  Scenario: Exploitation
    Given the login area
    And the vuilnerability to exploit
    When I try the following email
    """
    ' or 1=1 -- -
    """
    Then I get administrative access [evidence](admin.png)
    And I can conclude that I can successfully inject arbitray SQL code

  Scenario: Remediation
    Given I have patched the code by using prepare() instead of query()
    """
    20 return $this->db->prepare($sql)->fetchAll();
    """
    Then If I try again
    """
    ' or 1=1 -- -'
    """
    Then I get the following message:
    """
    Wrong E-Mail-Address or Password
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0 (critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5 (critial) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-07-06}
