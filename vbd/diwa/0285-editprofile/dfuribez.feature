## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=editprofile - id - (field)
  CWE:
    CWE-285: Improper Authorization
  Rule:
    REQ.176: R176. Restrict system objects
  Goal:
    Change any account's admin status
  Recommendation:
    Check user's privileges before performing an action

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And is runs on a docker container

  Scenario: Normal use case
    Given I am logged as an administrator
    And I want to edit a user's informarion (password, admin status)
    When I go to
    """
    http://172.17.0.2/?page=editprofile&id=1
    """
    Then I can save the changes [evidence](adminchanges.png)

  Scenario: Static detection
    When I look at the code at "editprofile.php"
    Then I can see that the vulnerability is being caused by line 11
    """
    ...
    08  $adminMode = false;
    09
    10  $userId = $_SESSION['user_id'];
    11  if(isset($_GET['id'])) {
    12    $adminMode = true;
    13    $userId = $_GET['id'];
    14  }
    ...
    47  if($model->editUser($userId, $_POST['email'], $_POST['country'],
          $changePassword, ($adminMode && $userId !== $_SESSION['user_id']  ?
            (1 == $_POST['is_admin'] ? 1 : 0) : null))) {
    ...
    """
    And line 11 grants admin access if "id" field is set
    And line 47 allows to change admin status only for another user
    Then I can conclude any user can get admin access

  Scenario: Dynamic detection
    Given I am logged as a regular user
    When I go to the current admin profile
    """
    http://172.17.0.2/?page=editprofile&id=1
    """
    Then I can either change his password or his admin status [evidence](ed.png)
    When I log again as "admin"
    Then I can see that user not longer has admin access [evidence](noadmin.png)
    And I can conclude that I can modify any user's profile

  Scenario: Exploitation
    Given I am logged as any regular user
    When I go to
    """
    http://172.17.0.2/?page=editprofile&id=<ID>
    """
    Then I replace "<ID>" for the victims "id"
    And I can modify his profile, admin status or password
    Then I can conclude that I can change any user profile

  Scenario: Remediation
    Given I have patched the code by adding the code
    """
    ...
    08  $adminMode = false;
    09
    10  $userId = $_SESSION['user_id'];
    11  if ($_SESSION['user']['is_admin'] == 1) {
    12    $adminMode = true;
    13  }
    14  if(isset($_GET['id'])) {
    15    $userId = $_GET['id'];
    16    if ($userId != $_SESSION['user_id'] && $adminMode == false) {
    17      die("Forbidden");
    18    }
    19  }
    ...
    38  if('post' === strtolower($_SERVER['REQUEST_METHOD']) &&
        isset($_POST) && $adminMode) {
    ...
    """
    And line 11 makes sure only an admin can get admin access
    And line 16 makes sure a non admin user can only see his own profile
    And line 38 makes sure only an admin can edit another user profile
    When I go as a regular user to the admin's profile
    """
    http://172.17.0.2/?page=editprofile&id=1
    """
    Then I get [evidence](forbidden.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5 (Critical) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (Critical) - MAV:N/MAC:L/MPR:L/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    vbd/diwa/0799-register
      Given that the invitation code can be bruteforced as in "0799-register"
      And any unauthorized user can register two accounts
      And with the method aforementioned
      And he can use one account to give admin access to the other account
      And gain full access to the site
