## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=register - invitation-code (field)
  CWE:
    CWE-799: Improper Control of Interaction Frequency
  Rule:
    REQ.237: Ascertain human interaction
  Goal:
    Bruteforce the invitation code
  Recommendation:
    Implement a captcha system

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I am in the "Register" section
    Then I can register a new user only if I have the invitation code

  Scenario: Static detection
    When I look at the code at "register.php"
    Then I can see that the code only checks if the user is already registered
    """
    03  if(isset($_SESSION['user_id'])) {
    04    echo getForbiddenMessage('You already are a registered User.');
    05    return;
    06  }
    """
    Then I can conclude the site is vulnerable to a bruteforce attack

  Scenario: Dynamic detection
    Given I am on the "Rgister" section
    When I try several times to register with an invalid code
    Then I get the error [evidences](wrong.png)
    """
    The Invitation Code was wrong.
    """
    Then I can conclude that I can bruteforce the invitation code

  Scenario: Exploitation
    Given than the site is vulnerable to a bruteforce attack
    And I write the script "bruteforce.py" wich tries all 10k possible codes
    When I run "bruteforce.py"
    """
    $ python bruteforce.py
    """
    Then I get the invitation code:
    """
    [+] found code: 3702
    """
    And the user "test@test.test" with password "test" gets created
    And I can conclude that I successfully bruteforce the code

  Scenario: Remediation
    Given I have patched the code by doing adding
    """
    08  $errors = array();
    09  $url = 'https://www.google.com/recaptcha/api/siteverify';
    10  $response = $_POST["g-recaptcha-response"];
    11  $data = array(
    12    "secret" => "private-key",
    13    "response" => $response);
    14
    15  $options = array (
    16  "http" => array(
    17    "method" => "POST",
    18    "content" => http_build_query($data))
    19  );
    20
    21  $context = stream_context_create($options);
    22  $verify = file_get_contents($url, false, $context);
    23  $success = json_decode($verify);
    24
    25  if ($success->success == true) {
    26    $register = true;
    27  } else {
    28    $register = false;
    29    $errors[] = "wrong captcha";
    39  }
    40
    41  if('post' === strtolower($_SERVER['REQUEST_METHOD']) &&
            isset($_POST) &&
            $register === true) {
    ...
    """
    And adding in the form
    """
    157 <center>
    158   <div class="g-recaptcha" data-sitekey="public-key"></div>
    159 </center>
    """
    And the above code adds a reCAPTCHA to the form [evidences](captcha.png)
    When I run again "bruteforce.py" I get nothing
    """
    $ python bruteforce.py
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.6 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    vbd/diwa/0284-board
      Given now I can get the invitation code
      And register a new user
      When I do the steps describen on "0284-board"
      Then I get access to private messages on "Board" section
