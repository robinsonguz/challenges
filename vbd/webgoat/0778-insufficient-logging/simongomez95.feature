## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Improper Error Handling
  Location:
    /WebGoat/challenge/5 - exception, message, trace (Return values)
  CWE:
    CWE-778: Insufficient Logging
  Rule:
    REQ.075: https://fluidattacks.com/web/es/rules/075/
  Goal:
    Provoke an error in the system without leaving traces
  Recommendation:
    Don't return system errors to the user

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge5.lesson"
    Then I get a login page where I can submit credentials

  Scenario: Static detection
  No origin validation
    Given I check the configuration at
    """
    /webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/challenge5
    /challenge6/Assignment5.java
    """
    And I see
    """
    ...
    if (resultSet.next()) {
            return success().feedback("challenge.solved").feedbackArgs(Flag.FLAG
            S.get(5)).build();
        } else {
            return failed().feedback("challenge.close").build();
        }
    }
    ...
    """
    Then I notice it's not logging failed login attempts

  Scenario: Dynamic detection
  No dynamic detection posible for this vulnerability

  Scenario: Exploitation
  No logging no traces
    Given I perform a brute force attack against the login system
    Given there are no logs
    Then it's not possible to investigate my attack afterwards

  Scenario: Remediation
  Implement logging on security events
    Given I implement a logging system to the application
    And make it log anytime something potentially dangerous happens
    Then it's easier to conduct forensics after an attack attempt

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-21
