## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Session Management
  Location:
    /WebGoat/ - JSESSIONID (Cookie)
  CWE:
    CWE-613: Insufficient Session Expiration
  Rule:
    REQ.023: https://fluidattacks.com/web/en/rules/023/
  Goal:
    Hijack another user's session while they're gone
  Recommendation:
    Expire inactive sessions after a prudential time

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/" and navigate the site
    Then I go away for an hour or two
    Then when I come back I'm still logged in

  Scenario: Static detection
  No session expiry time
    Given I check the code at
    """
    webgoat-container/src/main/java/org/owasp/webgoat/WebSecurityConfig.java
    """
    And I see
    """
    56  @Override
    57  protected void configure(HttpSecurity http) throws Exception {
    58      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterce
    ptUrlRegistry security = http
    59              .authorizeRequests()
    60              .antMatchers("/css/**", "/images/**", "/js/**", "fonts/**",
    "/plugins/**", "/registration", "/register.mvc").permitAll()
    61              .antMatchers("/servlet/AdminServlet/**").hasAnyRole("WEBGOAT
    _ADMIN", "SERVER_ADMIN") //
    62              .antMatchers("/JavaSource/**").hasRole("SERVER_ADMIN") //
    63              .anyRequest().authenticated();
    64      security.and()
    65              .formLogin()
    66              .loginPage("/login")
    67              .defaultSuccessUrl("/welcome.mvc", true)
    68              .usernameParameter("username")
    69              .passwordParameter("password")
    70              .permitAll();
    71      security.and()
    72              .logout().deleteCookies("JSESSIONID").invalidateHttpSession(
      true);
    73      security.and().csrf().disable();
    74
    75      http.headers().cacheControl().disable();
    76      http.exceptionHandling().authenticationEntryPoint(new AjaxAuthentica
    tionEntryPoint("/login"));
    77  }
    """
    Then I notice it doesn't have any session expiration configured
    Then I know sessions are living forever

  Scenario: Dynamic detection
  Wait and see
    Given I login to the platform with my own account
    And I idle for a long time not interacting with the site
    Then I refresh the site
    And I'm still logged in
    Then I know sessions are not timing out

  Scenario: Exploitation
  Hijack another user's session
    Given I know sessions don't expire
    Then I go to a space where my potential victim might be working
    And wait until they go to the bathroom leaving their PC unattended
    Then I go to their PC and look for the site cookies
    Then I email myself the session cookie
    Then I have access to the site as the victim until they log out

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I patch the code like this
    """
    56  @Override
    57  protected void configure(HttpSecurity http) throws Exception {
    58      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterce
    ptUrlRegistry security = http
    59              .authorizeRequests()
    60              .antMatchers("/css/**", "/images/**", "/js/**", "fonts/**",
    "/plugins/**", "/registration", "/register.mvc").permitAll()
    61              .antMatchers("/servlet/AdminServlet/**").hasAnyRole("WEBGOAT
    _ADMIN", "SERVER_ADMIN") //
    62              .antMatchers("/JavaSource/**").hasRole("SERVER_ADMIN") //
    63              .anyRequest().authenticated();
    64      security.and()
    65              .formLogin()
    66              .loginPage("/login")
    67              .defaultSuccessUrl("/welcome.mvc", true)
    68              .usernameParameter("username")
    69              .passwordParameter("password")
    70              .permitAll();
    71      security.and()
    72              .logout().deleteCookies("JSESSIONID").invalidateHttpSession(
      true);
    73      security.and().csrf().disable();
    74
    75      http.headers().cacheControl().disable();
    76      http.exceptionHandling().authenticationEntryPoint(new AjaxAuthentica
    tionEntryPoint("/login"));
    77      http.sessionManagement()
    78            .expiredUrl("/sessionExpired.html")
    79            .invalidSessionUrl("/invalidSession.html");
    80  }
    """
    And in "/webapp/WEB-INF/web.xml" I add
    """
    ...
    15  <session-config>
    16    <session-timeout>5</session-timeout>
    17  </session-config>
    ...
    """
    Then when a user idles for more than 5 minutes
    Then their session expires
    And can't be abused later

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.5/10 (Low) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.3/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I leave a stored xss attack that steals the user cookies
      And a user goes to the comments section, giving me their cookies
      Then I can use them and spoof the victims identity on the site
  Scenario: Correlations
    systems/webgoat/79-dom-xss
    systems/webgoat/80-reflected-xss
      Given I trick a user into clicking on a xss-injected link for the site
      Then I steal their cookies
      Then I can use them and spoof the victims identity on the site