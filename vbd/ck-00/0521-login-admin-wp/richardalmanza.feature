## Version 1.4.1
## language: en

Feature: login admin in wordpress
  TOE:
    CK-00
  Category:
    Weak Password Requirements
  Location:
    http://ck/wp-admin/
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.133: https://fluidattacks.com/web/rules/133/
  Goal:
    Privilege escalation to collect the flags
  Recommendation:
    Use longer passwords and less predictable

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
    | GNU bash        | 4.4.20(1)-release |
    | OpenSSl         | 1.1.1             |
    | Nmap            | 7.60              |
  TOE information:
    Given I'm starting the CK~00 vm
    And it gives me a goal
    """
    Your goal is to get root shell and flag as well
    """
    And it shows me its ip given by my router dhcp service
    And it shows a field for user login

  Scenario: Normal use case
    Given the wordpress [page](wp2.png) in http://ck/
    Then I saw a simple looking and relative empty blog

  Scenario: Static detection
    Given I see a normal wordpress page
    And no code exposed

  Scenario: Dynamic detection
    Given the author name of the unique article here
    Then I tried to log in with the author name, admin
    And a random password
    And it returned me
    """
    ERROR: The password you entered for the username admin is incorrect. Lost your password?
    """
    When I tried again with random username
    And it returned me
    """
    ERROR: Invalid username. Lost your password?
    """
    Then I could confirm username admin is a real account

  Scenario: Exploitation
    Given the information from dynamic detection
    When I tried admin as password
    Then I got access to admin account
    And with it all admin privileges for wordpress

  Scenario: Remediation
  WordPress ask you to confirm if you really want a
  [weak password](user-wp.png) for your account,
  so the problem was made by the person who create
  the admin account.
    Given the analysis of this wordpress
    Then I installed iThemes Security plugin
    And I setted up the password requirements module to force a strong password
    And I checked if now it allow you to use [strong password only](user-wp2.png)
    And I updated admin password

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.7/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-02
