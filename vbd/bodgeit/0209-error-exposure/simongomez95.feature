## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Improper Error Handling
  Location:
    bodgeit/advanced.jsp - Response
  CWE:
    CWE-0209: Information Exposure Through an Error Message -base-
      https://cwe.mitre.org/data/definitions/209.html
    CWE-0200: Information Exposure -class-
      https://cwe.mitre.org/data/definitions/200.html
    CWE-0963: SFP Secondary Cluster: Exposed Data -category-
      https://cwe.mitre.org/data/definitions/963.html
  CAPEC:
    CAPEC-215: Fuzzing and observing application log data/errors for application
    mapping -detailed-
      http://capec.mitre.org/data/definitions/215.html
    CAPEC-054: Query System for Information -standard-
      http://capec.mitre.org/data/definitions/54.html
    CAPEC-116: Excavation -meta-
      http://capec.mitre.org/data/definitions/116.html
  Rule:
    REQ.156: https://fluidattacks.com/web/es/rules/156/
  Goal:
    Provoke an error in the system to analyze output for potential vulns
  Recommendation:
    Don't return system errors to the user

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/advanced.jsp
    Then I can search for a product with various different criteria

  Scenario: Static detection
  Print stacktrace
    Given I see the code at "/thebodgeitstore/search/AdvancedSearch.java"
    """
    ...
    157 } catch (Exception e){
    158         StringWriter sw = new StringWriter();
    159         e.printStackTrace(new PrintWriter(sw));
    160         this.debugOutput.add(sw.toString());
    161    }
    ...
    """
    Then the stack trace is being output on error

  Scenario: Dynamic detection
  Forcing an error
    Given I intercept a search request
    """
    POST /bodgeit/advanced.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/advanced.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 40
    Connection: close
    Cookie: JSESSIONID=C3F83CCA13FD64A7FC766C1D0E67F4B3
    Upgrade-Insecure-Requests: 1

    q=0xcc02097d423a425caeff1c10766b34eb867b
    """
    Then I change the "q" parameter
    """
    POST /bodgeit/advanced.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/advanced.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 40
    Connection: close
    Cookie: JSESSIONID=C3F83CCA13FD64A7FC766C1D0E67F4B3
    Upgrade-Insecure-Requests: 1

    q=1
    """
    Then I get an error 500 response with the stacktrace (evidence)[trace.png]

  Scenario: Exploitation
  Using stack trace
    Given I have the full stacktrace on error
    Then I know what is happening in the backend
    And can plan my attacks better

  Scenario: Remediation
  Remove stacktrace
    Given I patch the code like this
    """
    ...
    157 } catch (Exception e){
    158         this.debugOutput.add("System Error");
    159    }
    ...
    """
    Then the stacktrace isn't output anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0089-*
    0327-conq-aes-enc-and-apnd-lst-rslt
      Given I can get errors that show me how the underlying components behave
      Then I can plan my attacks much better and easier
