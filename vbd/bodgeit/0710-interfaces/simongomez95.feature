## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Communication
  Location:
    bodgeit/login.jsp - username, password (Fields)
  CWE:
    CWE-0710: Improper Adherence to Coding Standards -class-
      https://cwe.mitre.org/data/definitions/710.html
    CWE-0978: SFP Secondary Cluster: Implementation -category-
      https://cwe.mitre.org/data/definitions/978.html
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.049: https://fluidattacks.com/web/es/rules/049/
  Goal:
    Code quality
  Recommendation:
    Adhere to coding standards

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
    Given I go through the prject codebase
    Then I see almost all the business logic is implemented in the views

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  Bad code quality
    Given the code quality is poor
    Then there will be more bugs
    And therefore more vulnerabilities for attackers to exploit

  Scenario: Remediation
  Improve programming practices
    Given I use a well designed architecture
    And extend classes from well defined interfaces
    Then the application will be less error prone

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    0.0/10 (None) - AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    0.0/10 (None) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    0.0/10 (None) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-22
