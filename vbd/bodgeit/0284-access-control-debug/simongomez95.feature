## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Broken Access Control
  Location:
    bodgeit/*.jsp?debug=true - Access
  CWE:
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0284: Improper Access Control -class-
      https://cwe.mitre.org/data/definitions/284.html
    CWE-0264: Permissions, Privileges, and Access Controls -category-
      https://cwe.mitre.org/data/definitions/264.html
  CAPEC:
    CAPEC-077: Manipulating User-Controlled Variables -standard-
      http://capec.mitre.org/data/definitions/77.html
    CAPEC-022: Exploiting Trust in Client -meta-
      http://capec.mitre.org/data/definitions/22.html
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Access diagnostic data I'm not supposed to be able to see
  Recommendation:
    Always check if the user trying to access a resource is privileged enough

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No user validation in admin section
    When I look at the code at "/bodgeit/root/advanced.jsp"
    """
    ...
    61  <!-- Debug Output
    62  <%= (as.isDebug()) ? as.getDebugOutput() : "" %>
    63  -->
    ...
    """
    And in "/bodgeit/src/com/thebodgeitstore/search/AdvancedSearch.java"
    """
    63  public boolean isDebug(){
    64  return "true".equals(this.request.getParameter("debug"));
    65  }
    """
    Then I see it doesn't check for user permissions to display debug data

  Scenario: Dynamic detection
  Add debug flag in URL
    Given I am navigating the site
    And add a "debug" parameter in the URL to see if it makes any difference
    """
    http://localhost:8000/bodgeit/advanced.jsp?debug=true
    """
    Then I make a search and see extra useful data displayed
    """
    You searched for:
    product:aaa

    SELECT PRODUCT, DESC, TYPE, TYPEID, PRICE FROM PRODUCTS AS a JOIN PRODUCTTYP
    ES AS b ON a.TYPEID = b.TYPEID WHERE PRODUCT LIKE '%aaa%' AND DESC LIKE '%'
    AND PRICE LIKE '%' AND TYPE LIKE '%'
    No Results Found
    """
    Then I know I can have debug data to help me break stuff

  Scenario: Exploitation
  Using debug data to easily esploit a SQLi
    Given I know there's a "/bodgeit/advanced.jsp?debug=true"
    And it shows me the internal SQL query of my search
    Then I can craft my SQLi payload on a white box

  Scenario: Remediation
  Only rendering debug data if user is admin
    Given I patch the code like this
    """
    ...
    63  <!-- Debug Output
    64  <%= (as.isDebug() && session.getAttribute("usertype")) ?
    as.getDebugOutput() : "" %>
    65  -->
    ...
    """
    Then the page only renders diagnostics if the user is ADMIN

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-16
