## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Communication
  Location:
    bodgeit/login.jsp - username, password (Fields)
  CWE:
    CWE-0521: Weak Password Requirements -base-
      https://cwe.mitre.org/data/definitions/521.html
    CWE-0287: Improper Authentication -class-
      https://cwe.mitre.org/data/definitions/287.html
    CWE-0947: SFP Secondary Cluster: Authentication Bypass -category-
      https://cwe.mitre.org/data/definitions/947.html
  CAPEC:
    CAPEC-016: Dictionary-based Password Attack -detailed-
      http://capec.mitre.org/data/definitions/16.html
    CAPEC-049: Password Brute Forcing -standard-
      http://capec.mitre.org/data/definitions/49.html
    CAPEC-112: Brute Force -meta-
      http://capec.mitre.org/data/definitions/112.html
  Rule:
    REQ.129: https://fluidattacks.com/web/es/rules/129/
  Goal:
    Use old password to access user account
  Recommendation:
    Always encrypt sensitive data

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
  No precedent password validation
    Given I see the code at "bodgeit/root/password.jsp"
    """
    ...
    27  ResultSet rs = null;
        28  try {
        29      stmt.executeQuery("UPDATE Users set password= '" + password1 +
        "' where name = '" + username + "'");
        ...
    """
    Then I see it just updates the password without checking past passwords

  Scenario: Dynamic detection
  Setting old password
    Given I go to http://localhost:8000/bodgeit/password.jsp
    And change my password to the last password I had
    Then the application lets me do it

  Scenario: Exploitation
  Finding functioning old passwords
    Given I have a combolist for users of the application
    And some user has changed their password to one in the list
    Then I can access that user's account

  Scenario: Remediation
  Validate past passwords
    Given I store the hash of every password the user has had
    And I compare them to the new password when they try to change it
    And don't allow reusing old passwords
    Then they can't recycle old passwords

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-22
