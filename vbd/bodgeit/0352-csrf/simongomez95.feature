## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/contact.jsp - Access
  CWE:
    CWE-0352: Cross-Site Request Forgery (CSRF) -compound-
      https://cwe.mitre.org/data/definitions/352.html
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0345: Insufficient Verification of Data Authenticity -class-
      https://cwe.mitre.org/data/definitions/345.html
    CWE-0949: SFP Secondary Cluster: Faulty Endpoint Authentication -category-
      https://cwe.mitre.org/data/definitions/949.html
  CAPEC:
    CAPEC-467: Cross Site Identification -detailed-
      http://capec.mitre.org/data/definitions/467.html
    CAPEC-062: Cross Site Request Forgery -standard-
      http://capec.mitre.org/data/definitions/62.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.225: https://fluidattacks.com/web/es/rules/255/
  Goal:
    Change a user's password via external GET request
  Recommendation:
    Use CSRF protection

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/password.jsp"
    """
    ...
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String okresult = null;
    13  String failresult = null;
    14
    15  if (password1 != null && password1.length() > 0) {
    16    if ( ! password1.equals(password2)) {
    17      failresult = "The passwords you have supplied are different.";
    18    }  else if (password1 == null || password1.length() < 5) {
    19      failresult = "You must supply a password of at least 5 characters.";
    20    } else {
    21      Statement stmt = conn.createStatement();
    22      ResultSet rs = null;
    23      try {
    24        stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    25        where name = '" + username + "'");
    26
    27        okresult = "Your password has been changed";
    ...
    """
    Then I see it doesn't check for request method or origin in any way

  Scenario: Dynamic detection
  Verb and Host tampering
    Given I am logged in to the site
    And intercept a password change request with Burp
    """
    POST /bodgeit/password.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/password.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 23
    Connection: close
    Cookie: b_id=""; JSESSIONID=F1B5FFCAFC944853AFD96F38CF86ACE0
    Upgrade-Insecure-Requests: 1

    password1=12345&password2=12345
    """
    Then I try changing the Host header to see if it accepts external requests
    """
    POST /bodgeit/password.jsp HTTP/1.1
    Host: test
    ...
    password1=a&password2=a
    """
    Then I get the response
    """
    Your password has been changed
    """
    Then I know it's vulnerable to CSRF

  Scenario: Exploitation
  Changing a victim's password
    Given I know the password change function is vulnerable to CSRF
    Then I make a page that POSTs a password change (sgomezatfluid.html)
    And send the link to a victim
    Then when they click on it, their password gets changed to "p0wnd"
    Then I can login to their account with their new password

  Scenario: Remediation
  Implementing anti-CSRF token
    Given I patch the code as follows
    """
    ...
    09  csrf = session.getAttribute("csrf");
    10  String password1 = (String) request.getParameter("password1");
    11  String password2 = (String) request.getParameter("password2");
    12  String csrftoken = (String) request.getParameter("csrf");
    12  String okresult = null;
    13  String failresult = null;
    14  byte[] array = new byte[7];
    15  new Random().nextBytes(array);
    16  String newcsrf = new String(array, Charset.forName("UTF-8"));
    17  session.setAttribute("csrf", newcsrf);
    18  if (password1 != null && password1.length() > 0 ) {
    19    if ( ! password1.equals(password2)) {
    20      failresult = "The passwords you have supplied are different.";
    21    }  else if (password1 == null || password1.length() < 5) {
    22      failresult = "You must supply a password of at least 5 characters.";
    23    } else {
    24      Statement stmt = conn.createStatement();
    25      ResultSet rs = null;
    26      try {
    27        stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    28        where name = '" + username + "'");
    29
    30        okresult = "Your password has been changed";
    ...
    57  <form method="POST">
    58    <center>
    59    <table>
    60    <tr>
    61      <td>Name</td>
    62      <td><%=username%></td>
    63    </tr>
    64    <tr>
    65      <td>New Password:</td>
    66      <td><input id="password1" name="password1" type="password"/></td>
    67    </tr>
    68    <tr>
    69      <td>Repeat Password:</td>
    70      <td><input id="password2" name="password2" type="password"/></td>
    71    </tr>
    72    <input id="csrf" name="csrf" type="hidden" value="<%=newcsrf%>"/></td>
    73    <tr>
    74      <td></td>
    75      <td><input id="submit" type="submit" value="Submit"/></td>
    76    </tr>
    77    </table>
    78    </center>
    79  </form>
    """
    Then CSRF isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.7/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0079-lv-2-xss-stored
      Given I post a feedback with an XSS payload that redirects to my CSRF page
      Then when the admin goes to their dashboard, their password gets cahnged
      And I get access to an admin account
