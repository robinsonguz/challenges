## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Information Leakage
  Location:
    /index.html - Page Information
  CWE:
    CWE-0497: Exposure of System Data to an Unauthorized Control Sphere -base-
      https://cwe.mitre.org/data/definitions/497.html
    CWE-0200: Information Exposure -class-
      https://cwe.mitre.org/data/definitions/200.html
    CWE-0063: SFP Secondary Cluster: Exposed Data -category-
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.262: https://fluidattacks.com/web/es/rules/262/
  Goal:
    Determine server software
  Recommendation:
    Disable default server pages and favicon

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No code available

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/"
    Then I see the default Tomcat page with server info (evidence)[tomcat.png]
    Then I know which server and version the site is running on

  Scenario: Exploitation
  Using version info
    Given I know the server software and version
    Then I can search for known vulnerabilities in that version
    Then I can exploit them to gain access to the system

  Scenario: Remediation
  Configuration
    Given disable all default server pages in the server
    Then that info isn't leaked anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium ) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium ) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium ) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-18
