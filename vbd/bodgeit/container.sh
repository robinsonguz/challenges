#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --name bodgeit -p "$1:8080" -t psiinon/bodgeit
fi

