## Version 1.4.1
## language: en
Feature:
  TOE:
    Badstorage Linux Debian prerelease # 16 2003
  Category:
    Unprotected Storage of Credentials
  Location:
    Port 80 TCP whith Apache httpd 1.3.28, service Web
  CWE:
    CWE-256: Unprotected Storage of Credentials
  Rule:
    REQ.002 Identify dependencies or components
    REQ.0176 The system must restrict access to system objects
             that have sensitive content.
             It will only allow its access to authorized users.
  Goal:
    Access to file with user credentials Storage
  Recommendation:
    Exclude file with sensitive information from the server.

  Background: Hacker's software:
    |    <Software name>          |    <Version>          |
    |    VMWare                   | 15.0.2 build 10952284 |
    |    Firefox Quantum          | 60.5.1 (64-bit)       |
    |    Kali GNU/Linux Rolling   | 4.19.0-kali3-amd64    |
    |    Nmap                     | Nmap 7.70             |
  TOE information:
    Given Apache  version 1.3.28
    And MySQL version 4.1.7-standard
    And is running on Linux 2.4.21 Debian prerelease # 16 2003

  Scenario: Normal use case
    Given I access http://localhost/
    Then I can navigate for multiple options
    And make create user and login to diferent options

  Scenario: Detection
    Given ip by Debian server
    Then verify if server has enable robots.txt
    Then I get the output:
    """
    # /robots.txt file for http://www.badstore.net/
    # mail webmaster@badstore.net for constructive criticism

    User-agent: badstore_webcrawler
    Disallow:
    User-agent: googlebot
    Disallow: /cgi-bin
    Disallow: /scanbot # We like Google

    User-agent: *
    Disallow: /backup
    Disallow: /cgi-bin
    Disallow: /supplier
    Disallow: /upload
    """
    Then I review http://localhost/supplier/
    And I view the file accounts:
    """
    1001:am9ldXNlci9wYXNzd29yZC9wbGF0bnVtLzE5Mi4xNjguMTAwLjU2DQo=
    1002:a3JvZW1lci9zM0NyM3QvZ29sZC8xMC4xMDAuMTAwLjE=
    1003:amFuZXVzZXIvd2FpdGluZzRGcmlkYXkvMTcyLjIyLjEyLjE5
    1004:a2Jvb2tvdXQvc2VuZG1lYXBvLzEwLjEwMC4xMDAuMjA=
    """

  Scenario: Exploitation
    Given Whith the accounts file information
    Then I decode the base64 lines with teh command base64 -d:
    Then i get the output
    """
    joeuser/password/platnum/192.168.100.56
    kroemer/s3Cr3t/gold/10.100.100.1
    janeuser/waiting4Friday/172.22.12.19
    kbookout/sendmeapo/10.100.100.20
    """
    Then I conclude that is data are valid user from the aplication

  Scenario: Remediation
    Given I use "DENY" directive to block access to supplier directory
    Then I get:
       """
       Forbidden
       You don't have permission to access /supplier/ on this server.
       """
    Then I confirm that the vulnerability was successfully patche

  Scenario: Scoring
    Given Severity scoring according to CVSSv3 standard
    """
    Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:F/RL:O/RC:X
    Environmental: Unique and relevant attributes to a normal users
    3.4/10 (Medium) - CR:L/IR:X/AR:X/MAV:N/MAC:L/MPR:N/MUI:R/MS:X/MC:L/MI:N/MA:N
    """

  Scenario: Correlations
    No correlations have been found to this date 2019-03-12
