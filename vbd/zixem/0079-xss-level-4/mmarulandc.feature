## Version 1.4.1
## language: en

Feature:
  TOE:
    zixem
  Category:
    Cross Site Scripting
  Location:
    http://zixem.altervista.org/XSS/4.php - Source (field)
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags
    in a Web Page (Basic XSS)
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute an alert with message 1337
  Recommendation:
    Remove user intervention

  Background:
  Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE information:
    Given I am accessing the site
    And The page is made in PHP
    And The page displays a image
    And I realize the URL has a parameter
    And I realize that parameter corresponds to the image source
    And The image is found with a GET request

  Scenario: Normal use case
    Given The page just displays a image
    When I check out all the page
    Then I realize displaying a image is its only functionality

  Scenario: Static detection
    Given There is not access to the source code
    When I try a static detection
    Then I realize this is not possible

  Scenario: Dynamic detection
  Detecting DOM based XSS
    When The img source is passed by parameter in the URL
    Then The image is displayed
    When I input another image source in the URL
    Then The image disapearead
    When I inspect the HTML code over the page
    Then I see the HTML code has changed [evidence](evidence.png)
    Then I can conclude the site is vulnerable to Cross-site scripting

  Scenario: Exploitation
    When I input the following string
    """
    http://zixem.altervista.org/XSS/4.php?img=test'onerror='alert(1337)
    """
    Then the site executes the JavaScript code [evidence](evidence1.png)

  Scenario: Remediation
    Given The image is being found through a query string
    When The user changes that query string, the image is not found
    Then Change the way of getting the image source would fix the vulnerability
    Then I would put a img tag of HTML with its respective source, with no user intervention
    """
    <img src='test'/>
    """
    Then It would be enough

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:P/RL:U/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    4.3/10 (High) - CR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-01-13