## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper neutralization of input during web page generation
  Location:
    https://testasp.vulnweb.com/showforum.asp?id=0 - title (field)
  CWE:
    CWE-79: Improper neutralization of input during
    web page generation ('cross-site scripting')
    https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross side scripting in the forum form
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com
    And I enter a "ASP.NET" site that allows me to login
    And I can create a post through a form
    And the server is running Microsoft SQL server version 8.5
    And "ASP.NET"

  Scenario: Normal use case
    Given I access https://testasp.vulnweb.com
    When I login into the web site
    Then I can create a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=0
    """
    And I can post something

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    Given I access https://testasp.vulnweb.com/
    When I login into the web site
    Then I can create a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=0
    """
    And I can use cross site scripting commands in the title field
    """
    <h1>Hello, xss html</h1>
    """
    And I click on "post it" button
    And I can see my post in "h1" tag [evidence](img1.png)
    And I can conclude that it can be affected by cross side scripting

  Scenario: Exploitation
    Given I access https://testasp.vulnweb.com/
    When I login into the web site
    Then I can create a post in the page form
    """
    https://testasp.vulnweb.com/showforum.asp?id=0
    """
    And I can use cross site scripting commands in the title field
    """
    <script>alert('Injected! with XSS');</script>
    """
    And I click on "post it" button
    And I can see my alert every time i reload the page [evidence](img2.png)
    And I can conclude that it can be affected by cross-site scripting

  Scenario: Remediation
    Given the site is using an insecure form against cross site scripting
    When I input a value it must be inserted only if it is trusted Data
    And It must escape all special characters
    And It must escape all untrusted data based on the body,js,CSS and url
    And It must use White-List input validation
    And It must use OWASP auto-sanitization library
    And It must use OWASP content security policy
    And It must use output validation

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-09-15
