pkgs:

rec {
  srcJavaCheckstyle = pkgs.fetchurl {
    url = "https://github.com/checkstyle/checkstyle/releases/download/checkstyle-8.29/checkstyle-8.29-all.jar";
    sha256 = "1rbipf4031inv34ci0rczz7dipi3b12cpn45h949i095gdh37pgh";
  };
}
