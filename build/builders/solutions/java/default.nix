{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  builders.pythonPackage = import ../../../builders/python-package pkgs;
  inputs = [
    pkgs.openjdk
    pkgs.nodejs
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          srcJavaCheckstyleCofig = ../../../configs/checkstyle.xml;
          srcJavaCheckstyle = pkgs.fetchurl {
            url = "https://github.com/checkstyle/checkstyle/releases/download/checkstyle-8.29/checkstyle-8.29-all.jar";
            sha256 = "1rbipf4031inv34ci0rczz7dipi3b12cpn45h949i095gdh37pgh";
          };
          srcPrettierConfig = ../../../configs/prettier-generic.yaml;

          builder = ./builder.sh;

          nodeJsModulePrettier = builders.nodePackage "prettier@2.0.5";
          nodeJsModulePrettierjava = builders.nodePackage "prettier-plugin-java@0.8.0";

          pyPkgLizard = builders.pythonPackage "lizard==1.17.3";
        })
  )
