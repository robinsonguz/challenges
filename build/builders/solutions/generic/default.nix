{ solutionPath, pkgs, inputs }:


let
  stringToDerivationName = import ../../../lambdas/string-to-derivation-name pkgs;
  baseInputs = [
    pkgs.glibcLocales
  ];
in
  rec {
    inherit solutionPath;

    srcShellOptions = ../../../include/generic/shell-options.sh;
    srcDirStructure = ../../../include/generic/dir-structure.sh;
    srcEnv = ../../../include/env.sh;
    srcGeneric = ./builder.sh;
    srcSolution = ../../../.. + solutionPath;
    solutionFileName = baseNameOf solutionPath;

    name = stringToDerivationName solutionPath;

    buildInputs = baseInputs ++ inputs;
  }
