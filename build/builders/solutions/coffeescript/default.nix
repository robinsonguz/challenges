{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  inputs = [
    pkgs.nodejs
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            srcCoffeelintConfig = ../../../configs/coffeelint.json;

            builder = ./builder.sh;

            nodeJsModuleCoffeescript = builders.nodePackage "coffeescript@2.5.1";
            nodeJsModuleCofeelint = builders.nodePackage "coffeelint@2.1.0";
          })
    )
