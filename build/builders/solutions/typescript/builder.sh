source "${srcGeneric}"

function prepare_types {
  cp -a "${nodeJsModuleTypes}/node_modules" .
}

function helper_lint_prettier {
  local solution="${1}"

  prettier \
    --check \
    --config "${srcPrettierConfig}" \
    "${solution}"
}

function lint {
  local solution="${1}"

      eslint \
        -c "${srcEslintConfig}" \
        "${solution}" \
  &&  if helper_lint_prettier "${solution}"
      then
        return 0
      else
            echo '[ERROR] Please run prettier on your solution with config:' \
        &&  echo "${srcPrettierConfig}" \
        &&  return 1
      fi
}

function compile {
  local solution="${1}"

      prepare_types \
  &&  tsc \
        --strict \
        --noImplicitAny \
        --noImplicitThis \
        --noUnusedLocals \
        --noUnusedParameters \
        --noImplicitReturns \
        --noFallthroughCasesInSwitch \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_node_modules \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
