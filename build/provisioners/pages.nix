let
  pkgs = import ../pkgs/stable.nix;
  builders.rubyGem = import ../builders/ruby-gem pkgs;
in
  pkgs.stdenv.mkDerivation (
       (import ../src/basic.nix)
    // (rec {
      name = "builder";

      buildInputs = [
        pkgs.git
        pkgs.gitstats
        pkgs.gitinspector
        pkgs.cacert
        pkgs.glibcLocales
        pkgs.ruby
        pkgs.rubyPackages.nokogiri
      ];

      rubyGemNanoc = builders.rubyGem "nanoc:4.7.4";
      rubyGemSlim = builders.rubyGem "slim:4.0.1";
      rubyGemAdsf = builders.rubyGem "adsf:1.2.1";
      rubyGemParallel = builders.rubyGem "parallel:1.13.0";
    })
  )
