let
  pkgs = import ../pkgs/stable.nix;

  srcProductGit = pkgs.fetchgit {
    url = "https://gitlab.com/fluidattacks/integrates";
    rev = "c9689a77ba02be6193917ec47b17b707c4660d19";
    sha256 = "03jkkxs9a8jr4m2vlz2mg6zr2mvc0ilawfyvrv2w0wlaigm7hc9y";
  };
in
  pkgs.stdenv.mkDerivation (
       (import ../src/basic.nix)
    // (import ../src/external.nix pkgs)
    // (rec {
      name = "builder";

      buildInputs = [
        pkgs.git
        pkgs.nix
      ];

      srcProduct = import srcProductGit;
    })
  )
