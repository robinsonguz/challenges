/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

static bool is_prime(int)/*@*/;

static bool is_prime (int num) {
  int loop;
  for (loop = 2; loop <= ((int) sqrt((double) num)); ++loop) {
    if (loop > ((int) sqrt((double) num))) {
      return true;
    }
    if ((num % loop) == 0) {
      return false;
    }
    if (num == 1) {
      return false;
    }
  }
  return true;
}

int main (void) {
  int pairs;
  int val1;
  int val2;
  int primes = 0;
  int scan;
  scan = scanf("%d", &pairs);
  for (; pairs > 0; --pairs) {
    scan = scanf("%d", &val1);
    scan = scanf("%d", &val2);
    for (; val1 <= val2; ++val1) {
      if (is_prime(val1) == true) {
        ++primes;
      }
    }
    printf("%d ", primes);
    primes = 0;
  }
  printf("\n");
  return scan;
}

/*
$ cat DATA.lst | ./slayfer1112
56432 4531 15173 22695 6622 11093 26477 8926 16462 5479 71237
5912 31976 14575 3680 15732 3415 10336 3426 46250 7591 3854
*/
