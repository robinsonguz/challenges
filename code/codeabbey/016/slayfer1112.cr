#! /usr/bin/crystal

# $ ameba \
#   --all \
#   --fail-level Convention \
#   slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 3.54 milliseconds
# $ crystal build slayfer1112.cr



def solution(array)

  sum = 0

  array.each do |x|
    x = x.is_a?(String) ? x.try &.to_f : x
    sum += x
  end

  avg = Int32.new((sum/(array.size-1)).round(0))

  print "#{avg} "

end

cases = gets

if cases
  cases = cases.is_a?(String) ? cases.try &.to_i : cases
  0.step(to: cases,by:1) do |_|
    args = gets
    if args
      args = args.split
      solution(args)
    end
  end
end

puts

# $ cat DATA.lst | crystal slayfer1112.cr
# 277 611 10977 517 1098 5051 135 138 947 4499 7933 4607 471
