# rubocop oscardjuribe.rb
# 1 file inspected, no offenses detected
# frozen_string_literal: true

# Code to solve the problem Say 100 from codeabbey

require 'httparty'

# read token from file
token_value = File.read('DATA.lst')

# request first value from server using token
response = HTTParty.post('http://open-abbey.appspot.com/interactive/say-100',
                         body: { token: token_value })

# calculate answer value substracting 100
answer_value = 100 - Integer(response.split(': ')[1])

# send answer and token to get final response
end_token = HTTParty.post('http://open-abbey.appspot.com/interactive/say-100',
                          body: { token: token_value,
                                  answer: answer_value })

# split response to get only the token
print end_token.split(': ')[1]

# ruby oscardjuribe.rb
# ttQ0HAD/pA4jB8NsUn1e7jg5
