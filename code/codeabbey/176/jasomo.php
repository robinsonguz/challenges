/*
$ prettier jasomo.php -c
Checking formatting...
All matched files use Prettier code style!
*/


<?php
echo 'Insert token: ';
$token = fgets(STDIN, 1024);

$url = 'http://open-abbey.appspot.com/interactive/say-100';
$data = ['token' => "$token"];

$options = [
  'http' => [
    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
    'method' => 'POST',
    'content' => http_build_query($data),
  ],
];
$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

if (strpos($result, 'error') !== false) {
  echo "$result";
  exit();
}

$value = substr($result, 8);
$answer = 100 - intval($value);

echo "Received value: $value";
echo "Calculated answer: $answer\n";

$data = ['token' => "$token", 'answer' => "$answer"];
$options = [
  'http' => [
    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
    'method' => 'POST',
    'content' => http_build_query($data),
  ],
];

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

if (strpos($result, 'error') !== false) {
  echo "$result";
  exit();
}

echo "Received victory token: $result";
?>

/*
$ php jasomo.php
Insert token: 4ZF3eFWcigPqyIXBpvKQEjM0
Received value: 88
Calculated answer: 12
Received victory token: end: 6ghiu2U21ZrwSs1KmlxtDzU4
*/
