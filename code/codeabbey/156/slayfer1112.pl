# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

use Readonly;
use POSIX;

my $EMPTY = q{};
Readonly my $nine => 9;
Readonly my $ten  => 10;

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub split_four {
  my @args = @_;
  my $four = $EMPTY;
  my @card;
  for my $x (@args) {
    $four = "$four" . "$x";
    if ( length $four == ( 2 * 2 ) ) {
      push @card, $four;
      $four = $EMPTY;
    }
  }
  return @card;
}

sub checksum {
  my @args = @_;
  my $sums = 0;
  for my $vals (@args) {
    my @sum  = split $EMPTY, $vals;
    my $val1 = $sum[0] * 2;
    if ( $sum[0] * 2 > $nine ) { $val1 = ( $sum[0] * 2 ) - $nine }
    my $b = $sum[1];
    my $c = $sum[2] * 2;
    if ( $sum[2] * 2 > $nine ) { $c = ( $sum[2] * 2 ) - $nine }
    my $d = $sum[ ( 2 + 1 ) ];
    $sums += $d + $c + $b + $val1;
  }
  return $sums;
}

sub swap_most_left {
  my @args = @_;
  my $bool = 1;
  my $ind  = join $EMPTY, @args;
  my @ind2 = split $EMPTY, $ind;
  my $result;
  for ( 0 .. ( $#ind2 - 1 ) ) {
    if ($bool) {
      my $vals2 = join $EMPTY, @args;
      my @vals  = split $EMPTY, $vals2;
      my $inter = $vals[$_];
      $vals[$_] = $vals[ ( $_ + 1 ) ];
      $vals[ ( $_ + 1 ) ] = $inter;
      my @card = split_four @vals;
      $result = join $EMPTY, @card;
      if ( checksum(@card) % $ten == 0 ) { $bool = 0 }
      $vals[ ( $_ + 1 ) ] = $vals[$_];
      $vals[$_] = $inter;
    }
  }
  exit 1 if !print "$result ";
  return 'Here you find the result for the missing number';
}

sub number {
  my @args    = @_;
  my @card    = @{ $args[0] };
  my $index1  = $args[1];
  my $index2  = $args[2];
  my $counter = 0;
  my $sums    = 0;
  my $sums2   = 1;
  my @repair;

  for my $x (@card) {
    my @z = split $EMPTY, $x;
    if ( $counter == $index1 ) {
      @repair = @z;
    }
    else {
      my @sum;
      for my $y (@z) {
        push @sum, $y;
      }
      my $val1 = $sum[0] * 2;
      if ( $sum[0] * 2 > $nine ) { $val1 = ( $sum[0] * 2 ) - $nine }
      my $b = $sum[1];
      my $c = $sum[2] * 2;
      if ( $sum[2] * 2 > $nine ) { $c = ( $sum[2] * 2 ) - $nine }
      my $d = $sum[ ( 2 + 1 ) ];
      $sums += $d + $c + $b + $val1;
    }
    $counter++;
  }
  my $replace = '1';
  for ( 0 .. $nine ) {
    if ($replace) {
      $repair[$index2] = $_;
      my $val1 = $repair[0] * 2;
      if ( $repair[0] * 2 > $nine ) { $val1 = ( $repair[0] * 2 ) - $nine }
      my $b = $repair[1];
      my $c = $repair[2] * 2;
      if ( $repair[2] * 2 > $nine ) { $c = ( $repair[2] * 2 ) - $nine }
      my $d = $repair[ ( 2 + 1 ) ];
      $sums2 = $d + $c + $b + $val1 + $sums;
    }
    if ( $sums2 % $ten == 0 ) { $replace = 0 }
  }
  $card[$index1] = join $EMPTY, @repair;
  my $result = join $EMPTY, @card;
  exit 1 if !print "$result ";
  return 'Here you find the result for the missing number';
}

sub solution {
  my @arg     = @_;
  my $args    = $arg[0];
  my $four    = $EMPTY;
  my @array   = split $EMPTY, $args;
  my $counter = 0;
  my $indexes = 0;
  my @card;
  for my $x (@array) {
    if ( $x eq q{?} ) { $indexes = $counter }
    $four = "$four" . "$x";
    if ( length $four == ( 2 * 2 ) ) {
      push @card, $four;
      $four = $EMPTY;
    }
    $counter++;
  }
  my $index1 = floor $indexes / ( 2 * 2 );
  my $index2 = $indexes % ( 2 * 2 );
  if ( $args =~ /[?]/xms ) {
    number( \@card, $index1, $index2 );
  }
  else {
    swap_most_left(@card);
  }
  return 'I give it a try with my card number but fails';
}

sub main {
  my @vals = data_in();
  for ( 1 .. $#vals ) {
    solution $vals[$_];
  }
  exit 1 if !print "\n";
  return 'I remember that Luhn algorithm saves me one time';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 8969144023813056 6772582689506541 7309966460741023 9229374427085795
# 5862253652508145 6586746067527942 1389132636151086 2191241905189242
# 1289408938530560 2475140437917301 8892104896844432 9128524409398929
# 7187850672618434 1426534455193807 9624027214738056 3880033780673125
# 6700641624739729 7389167579118475 7672346679297464 8066614071400172
# 6293016468378673 1743029704118800 1577980079745281 2514664152995344
# 4144857862362177 4652586940711774 2631089891485589 8667657206620332
# 7728334990533522 7869415011979783 6757997146628465 4688100251546262
# 2561453738371184 9664895346103501 6373502359946721 6296569219278276
# 7675149968273973 6678613087169148 5193376452276017 5233442682055050
# 1214581809153041 9871360278267344
