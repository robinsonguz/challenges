#! /usr/bin/env clojure
;; To lint using eastwood, a project is required
; $ lein eastwood
; == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns richardalmanza.core
  (:require [clojure.string :as str]))

(defn prepare-args [s] (str/split s #"[ \n]"))

(defn to-ten [i] (mod (- 10 (mod i 10)) 10))

(defn digit-to-int [c] (- (int c) 48))

(defn int-to-digit [i] (char (+ i 48)))

(defn shield-subs [i e s]
  (if (< i (count s))
    (if (> e i) (subs s i e) "")
    ""))

(defn sum-d-sub [i] (+ (mod i 10) (int (/ i 10))))

(defn sum-d [i d]
  (let [n (digit-to-int d)]
    (if (even? i)
      (sum-d-sub (* 2 n))
      n)))

(defn check-sum [s]
  (loop [t 0 i 0]
    (if (< i (count s))
      (recur (+ t (sum-d i (get s i))) (inc i))
      t)))

(defn missing-digit-sub [subt i]
  (if (even? i)
    (if (even? subt) (/ subt 2) (/ (+ subt 9) 2))
    subt))

(defn missing-digit [i s]
  (str/replace s \? (int-to-digit
                     (->
                      (str/replace s \? \0)
                      (check-sum)
                      (to-ten)
                      (missing-digit-sub i)))))

(defn swap-digits-sub [s i]
  (str
   (shield-subs 0 (- i 1) s)
   (get s i)
   (get s (- i 1))
   (shield-subs (+ i 1) (count s) s)))

(defn swap-digits [s]
  (loop [ss s i 1]
    (if (not (= (mod (check-sum ss) 10) 0))
      (recur (swap-digits-sub s i) (inc i))
      ss)))

(defn do-task [s]
  (let [index (str/index-of s \?)]
    (if index
      (missing-digit index s)
      (swap-digits s))))

(defn get-args []
  (->
   (if *command-line-args*
     (->
      (first *command-line-args*)
      (prepare-args))
     (->
      (slurp "DATA.lst")
      (prepare-args)))
   (subvec 1)))

(->>
  (get-args)
  (map do-task)
  (apply println))

; $ ./richardalmanza.clj
; 5758906139044615 2875846744300686 6811258967791237
; 8234992885570340 4262228835515380 8298778175833404
; 1589855506831944 2341933709558866 1587592341392301
; 6167627576553107 6869294619664879 7553977553329199
; 2177269790869006 4674740772170435 4405838786528691
; 6909640030235321 7755700604921772 9165875032808617
; 5730627900127800 9000495773041044 2347080787847227
; 1900845589091559 7065254051004992 5438591988007299
; 8218612711805138 6456940843473565 2771378224128713
; 1053794810080020 7285680029008653 3411304373613531
; 9255789263066731 9972872192089966 1228355316651241
; 9204789813040378 5243651022043893 4019751263617309
; 8239729970704482 5163553228090346
