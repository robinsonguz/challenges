{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO ()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = map read $ words line :: [Int]
        let n1 = head vector_ints
        let n2 = vector_ints!!1
        let result = n1 `mod` 6 + n2 `mod` 6 + 2
        print result
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 5 7 10 4 9 7 6 4 6 8 3 11 10 9 4 9 5 8 4 8 5 5
-}
