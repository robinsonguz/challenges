/*
$ eslint slayfer1112.ts
*/
function selectionSort(arr: number[], res: number[]): number[] {
  const size = arr.length;
  if (size > 1) {
    const max: number = Math.max(...arr);
    const body: number = arr[size - 1];
    const index: number = arr.indexOf(max);
    const start: number[] = arr.slice(0, index);
    const last: number[] = arr.slice(index + 1, size - 1);
    const newArr: number[] = start.concat(body, last);
    if (newArr.indexOf(max) === 0 - 1) {
      return selectionSort(newArr, res.concat(index));
    }
    return selectionSort(newArr.slice(0, size - 1), res.concat(index));
  }
  return res;
}

function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const result: number[] = selectionSort(entryF, []);
  process.stdout.write(result.join(' '));
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const arr = entryArray[1].split(' ');
    solution(arr);
    return 0;
  });
  return 0;
}

main();
/*
$ tsc slayfer1112.ts && cat DATA.lst | node slayfer1112.js
27 110 19 42 44 57 83 106 43 1 3 46 64 35 76 39 73 101 44 66 60 92
54 77 58 52 32 11 41 23 12 87 31 4 85 74 77 44 20 38 20 48 47 20 37
3 74 44 3 37 52 41 12 52 21 24 37 10 36 33 17 38 20 30 51 5 52 40
38 21 7 2 29 22 39 33 29 39 10 40 30 5 25 21 29 2 11 4 0 17 10 17
17 10 5 20 2 15 0 20 0 18 1 4 11 0 8 13 12 5 0 6 2 5 2 5 1 3 1 0
*/
