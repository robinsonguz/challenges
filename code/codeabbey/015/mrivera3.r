# $ lintr::lint('mrivera3.r')


maxmin <- function() {
  data <- scan("DATA.lst")
  max <- max(data)
  min <- min(data)
  cat(max, min)
}
maxmin()

# $ Rscript mrivera3.r
#79241 -78703
