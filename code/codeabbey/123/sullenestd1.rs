/*
$ cargo clippy
Checking code_123 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.11s
$ cargo build
Compiling code_123 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.28s
$ rustc sullenestd1.rs
*/

use std::cmp;
use std::io::{self, BufRead};

fn knapsack(values: Vec<i64>, weights: Vec<i64>, w_max: i64, n: i64) -> i64 {
  let mut k: Vec<Vec<i64>> = Vec::new();
  for _ in 0..n + 1 {
    k.push((0i64..w_max + 1).collect::<Vec<_>>());
  }
  for i in 0i64..n + 1 {
    for j in 0i64..w_max + 1 {
      if i == 0 || j == 0 {
        k[i as usize][j as usize] = 0
      } else if weights[(i - 1) as usize] <= j {
        k[i as usize][j as usize] = cmp::max(
          values[(i - 1) as usize]
            + k[(i - 1) as usize][(j - weights[(i - 1) as usize]) as usize],
          k[(i - 1) as usize][j as usize],
        )
      } else {
        k[i as usize][j as usize] = k[(i - 1) as usize][j as usize]
      }
    }
  }
  k[n as usize][w_max as usize]
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 0;
  let mut w_max = 0;
  let mut values: Vec<i64> = Vec::new();
  let mut weights: Vec<i64> = Vec::new();
  for w in lines {
    let line = w.unwrap();
    let num: Vec<i64> = line
      .trim()
      .split(' ')
      .map(|w| w.parse().expect("Not a float!"))
      .collect();
    if n == 0 {
      n = num[0];
    } else if num.len() == 1 {
      w_max = num[0];
    } else {
      weights.push(num[0]);
      values.push(num[1]);
    }
    if weights.len() == n as usize {
      break;
    }
  }
  println!("{}", knapsack(values, weights, w_max, n))
}

/*
$ cat DATA.lst | ./sullenestd1
4732
*/
