--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

require 'io'

local data ;
data = io.lines("DATA.lst")
local nTestCases;
local timePrinter1
local timePrinter2
local nPages

local dataArr = {}

nTestCases=0


local function split(s, delimiter)
    local splitted = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(splitted, match);
    end
    return splitted;
end

local function getMinimumTimeIntByUnit(time1,time2)

    return (time1*time2)/(time1+time2)
end


for line in data do
    if nTestCases==0  then
        nTestCases=line
    else

        local lineSplitted;
        lineSplitted=split(line," ")
         table.insert(dataArr, lineSplitted)

    end
end

for i=1,nTestCases do
    timePrinter1,timePrinter2,nPages=unpack(dataArr[i])

    local timeMinimunByUnit;

    local timeMinimunTotal;

    local nPagesInTime1;
    local nPagesInTime2;

    local time1Total;
    local time2Total;

    local limSupDifferenceTime1;
    local limSupDifferenceTime2;

    local optimizedTime;

    timeMinimunByUnit=getMinimumTimeIntByUnit(timePrinter1,timePrinter2)
    timeMinimunTotal= timeMinimunByUnit*nPages

    nPagesInTime1=math.ceil(timeMinimunTotal/timePrinter1)
    nPagesInTime2=math.ceil(timeMinimunTotal/timePrinter2)

    time1Total = nPagesInTime1*timePrinter1
    time2Total = nPagesInTime2*timePrinter2

    limSupDifferenceTime1=time1Total-nPages;
    limSupDifferenceTime2=time2Total-nPages;

    if(limSupDifferenceTime1<limSupDifferenceTime2) then
        optimizedTime=time1Total;
    else
        optimizedTime=time2Total;
    end
    io.write(optimizedTime .. " ");
end
--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
309055713 33295378 322580664 359769725 332892224 162592318 175123025
]]
