# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use List::Util qw( min max reduce );
use List::MoreUtils qw(first_index);
use POSIX;
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data[ 1 .. $#data ];
}

sub printers {
  my @data     = @_;
  my $printerA = $data[0];
  my $printerB = $data[1];
  my $pages    = $data[2];
  my $interA   = ceil( ( $printerA / ( $printerA + $printerB ) ) * $pages );
  my $printsA  = $interA * $printerB;
  my $interB   = ceil( ( $printerB / ( $printerA + $printerB ) ) * $pages );
  my $printsB  = $interB * $printerA;

  if ( $printsA < $printsB ) {
    return $printsA;
  }
  else {
    return $printsB;
  }
}

sub solution {
  my @data     = @_;
  my $vals     = $data[0];
  my @dat      = split $SPACE, $vals;
  my $printerA = $dat[0];
  my $printerB = $dat[1];
  my $pages    = $dat[2];
  my $res      = printers $printerA, $printerB, $pages;
  exit 1 if !printf '%.0f ', $res;
  return 'This is time of probability!';
}

sub main {
  my @vals = data_in();
  for my $test (@vals) {
    solution($test);
  }
  exit 1 if !print "\n";
  return 'You make it in the first attemp?';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 263758430 237548096 379877502 197944 211802745 193536936
# 349935036 252645018 290500263 39628224 355061139 275607571
# 356903610 25905942 248863520 173586819 140773700 335585812
# 236054268 377929433
