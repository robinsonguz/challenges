/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn two_printers(arr: &Vec<f64>) {
  let x = arr[0];
  let y = arr[1];
  let n = arr[2];
  let maxi = max(x, y);
  let mini = min(x, y);
  let ex = (n * mini / (x + y)).ceil() * maxi;
  let ey = (n * maxi / (x + y)).ceil() * mini;
  let min_time = min(ex, ey);
  print!("{} ", min_time);
}

fn max(n1: f64, n2: f64) -> f64 {
  if n1 >= n2 {
    return n1;
  }
  n2
}

fn min(n1: f64, n2: f64) -> f64 {
  if n1 >= n2 {
    return n2;
  }
  n1
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut inputs = vec![];
  let mut lines = contents.lines();
  lines.next();
  for l in lines {
    for c in l.split_whitespace() {
      inputs.push(c.trim().parse::<f64>().unwrap());
    }
    two_printers(&inputs);
    inputs.clear()
  }
  Ok(())
}
/*
  $ ./adrianfcn
  318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
  309055713 33295378 322580664 359769725 332892224 162592318 175123025
*/
