/*
$ cargo clippy
Checking sullenestd1 v0.1.0 (/.../sullenestd1/)
Finished dev [unoptimized + debuginfo] target(s) in 0.57s
$ cargo build
Compiling sullenestd1 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.32s
$ rustc sullenestd1.rs
*/

use std::io;
use std::io::Read;

fn reverse_string(s: &str) -> String {
  let st: String = s.chars().rev().collect();
  st
}

fn process(f_text: String, s2: String) -> Vec<i64> {
  let rev_text = reverse_string(&f_text);
  let mut counter: Vec<Vec<i64>> = Vec::new();
  for (mut i, _) in s2.split("").enumerate() {
    let len: i64 = i as i64 + 1;
    let subs: String = reverse_string(&s2[0..len as usize].to_string());
    let index = rev_text.find(&subs);
    if index == None {
      counter.push([-1, -1].to_vec());
    } else {
      counter.push([index.unwrap() as i64 + len - 1, len].to_vec());
    }
    i += 1;
    if i == 15 {
      break;
    }
  }
  counter.sort_by(|a, b| {
    let mut a_check = a[0];
    let mut b_check = b[0];
    if a_check == b_check {
      a_check = a[1];
      b_check = b[1];
    }
    b_check.cmp(&a_check)
  });
  counter[0].clone()
}

fn to_hex(n1: i64, n2: i64) {
  let n = n1 * 4096 + n2;
  println!("{:X}", n);
}

fn main() {
  let mut full_text = String::new();
  let input = io::stdin();
  let mut lines = input.lock();
  lines
    .read_to_string(&mut full_text)
    .expect("Error parsing stdin");
  let data = full_text.split_off(47434);
  let mut nums: Vec<i64> = data
    .trim()
    .split_ascii_whitespace()
    .map(|w| w.parse().expect("Not an Integer!"))
    .collect();
  nums.remove(0);
  for x in nums {
    let text = &full_text[(x - 4096) as usize..x as usize];
    let refe = &full_text[x as usize..(x + 15) as usize];
    let array: Vec<i64> = process(text.to_string(), refe.to_string());
    to_hex(array[1], array[0]);
  }
}

/*
$ cat doyle.txt DATA.lst | ./sullenestd1
60A5
31DB
2012
45FB
231E
60F5
728D
F168
5264
D03B
3185
2995
584E
41B0
3430
4830
61C0
70CA
31C2
6DC3
32FF
4A64
5BAD
26CE
5D99
30B7
24B0
3143
31FC
3012
4083
687A
4041
385F
5C6C
585A
4875
3004
4101
4B2A
47FB
D23A
602C
46B2
*/
