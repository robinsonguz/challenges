;; $ clj-kondo --lint slayfer1112.clj
;; linting took 21ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))]
    [data]))

(defn val-a [r e phi-n sprev scur tprev tcur]
  (if (= r 0N)
    scur
    (let [q (bigint (/ e phi-n))
          snext (- sprev (* q scur))
          tnext (- tprev (* q tcur))
          x-n phi-n
          phi-n-n (mod e phi-n)
          sprev-n scur
          tprev-n tcur
          scur-n snext
          tcur-n tnext
          r-n (mod x-n phi-n-n)]
      (val-a r-n x-n phi-n-n sprev-n scur-n tprev-n tcur-n))))

(defn val-d [d phi-n]
  (if (< d 0)
    (let [d-n (mod d phi-n)]
      d-n)
    d))

(defn exteuclidian [e phi-n]
  (let [sprev (bigint 1)
        scur (bigint 0)
        tprev (bigint 0)
        tcur (bigint 1)
        r (mod e phi-n)
        a (val-a r e phi-n sprev scur tprev tcur)]
    a))

(defn isqrt [n x y]
  (if (<= x y)
    x
    (let [xn y
          yn (bigint (/ (+ xn (bigint (/ n xn))) 2))]
      (isqrt n xn yn))))

(defn fermat [n t s]
  (if (= (bigint (* (+ t s) (- t s))) n)
    [(+ t s) (- t s)]
    (let [tn (+ 1 t)
          nn (- (* tn tn) n)
          sn (isqrt nn nn (/ (inc nn) 2))]
      (fermat n tn sn))))

(defn message [m a index msg]
  (if (= (compare a "00") 0)
    msg
    (let [a-n (str (str/join (vec (m index))))
          msg-n (conj (a))]
      (println a)
      (message m a-n (inc index) msg-n))))

(defn rsa-decryptor [cipher d n]
  (let [m (bigint (.modPow (biginteger cipher) (biginteger d) (biginteger n)))
        m1 (str/split (str m) #"00")
        m2 (vec (partition 2 (m1 0)))]
    (doseq [x m2]
      (print (char (read-string (str/join (vec x))))))))

(defn solution [data]
  (let [cipher (read-string (data 1))
        e 65537N
        n (read-string (data 0))
        t0 (isqrt n n (bigint (/ (+ n 1) 2)))
        [p q] (fermat n t0 0N)
        phi-n (* (- p 1) (- q 1))
        d (bigint (val-d (exteuclidian e phi-n) phi-n))
        ]
    (rsa-decryptor cipher d n)))

(defn main []
  (let [[data] (get-data)]
    (solution data)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; EAR DOG ACE MAT SHY RAT CAT SKY
