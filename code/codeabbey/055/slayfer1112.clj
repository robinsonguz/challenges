;; $ clj-kondo --lint slayfer1112.clj
;; linting took 14ms, errors: 0, warnings: 0

(ns slayfer1112
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datb (str/split-lines dat)
        datv (str/split (datb 0) #" ")]
    [(vec (drop-last datv))]))

(defn get-map [data res counter]
  (if (< counter (alength (to-array-2d data)))
    (let [val (data counter)]
      (if (contains? res (data counter))
        (get-map data (assoc res val (inc (get res val))) (inc counter))
        (get-map data (assoc res val 1) (inc counter))))
    res))

(defn solution [data]
  (let [mapp (into (sorted-map) (get-map data {} 0))]
    (doseq [x mapp]
      (if (< 1 (x 1))
        (print (x 0) "")
        ()))))

(defn main []
  (let [[data] (get-data "DATA.lst")]
    (solution data)))

(main)

;; bac baq bas bat bek boh bok boq byf byx dek dep dit dix doh dyh goh gox
;; guh gus gyk jah jih jip jit jus jyc jyf loq luc lut mex mik nat nuc nyf
;; nyk rac rat ret ric roc ruf rus ryc ryf vaf veh veq vix voh vot vox vut
;; vyk zef zes zox zuk
