;; $ clj-kondo --lint podany270895.cljs
;; linting took 31ms, errors: 0, warnings: 0

(println (+
  (Integer. (first *command-line-args*))
  (Integer. (second *command-line-args*))
))

;; $ clj code/codeabbey/001/podany270895.cljs 123 321
;; 444
