#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 45.11 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def bfs_cap(n, edges, s_node, f_node)
  path =
    [] of Tuple(Int32, Tuple(Int32, Int32), Tuple(Int32, Int32), Int32, Int32)
  nodes = [] of Tuple(Int32, Bool, Int32)
  n.each { |x| nodes << {x, true, -1} }

  queue = [s_node]
  nodes[s_node] = {s_node, false, -1}

  until queue.empty?
    current = queue.shift

    if current == f_node
      return path + [{-1, {-1, -1}, {-1, -1}, -1, nodes[current][2]}]
    end

    edges.select { |x| current.in?(x[1]) && x[2].sum < x[3] }
      .each do |x|
        next_node = x[1].reject { |y| y == current }[0]

        if nodes[next_node][1]
          queue << next_node
          nodes[next_node] = {next_node, false, x[0]}
          path << x + {nodes[current][2]}
        end
      end
  end

  [] of Tuple(Int32, Tuple(Int32, Int32), Tuple(Int32, Int32), Int32, Int32)
end

def clean_path(path)
  parent = path[-1][-1]

  path.reverse.each do |x|
    path.delete x unless x[0] == parent
    parent = x[-1] if x[0] == parent
  end
end

def change_flow(path, edges)
  t = edges
  flow_min = min_cap(path)
  flow_min_p = min_cap_p(path)
  flow_max = max_cap(path)

  path.each do |x| # .map { |x| {x[0], x[-1]} }
    node_p = get_nodep_path(path, x)

    index_p = x[1].index(node_p) || 5
    flow = x[2].to_a

    if flow_min == flow_min_p
      flow[index_p] += flow_min
    elsif flow_max > flow.sum
      flow[index_p] = flow_max
      flow[index_p - 1] -= (x[3] - flow.sum).abs if flow.sum > x[3]
    end

    t[x[0]] = {x[0], x[1], {Int32, Int32}.from(flow), x[3]}
  end
end

def balance_nodes(path, edges)
  queue = [] of Int32
  ignore = [get_nodep_path(path, path[0]), get_noded_path(path, path[-1])]

  path.each do |x|
    node = get_nodep_path(path, x)
    queue << node unless node.in? ignore
  end

  queue.each do |node|
    sum_in = node_flow_in(edges, node)
    sum_out = node_flow_out(edges, node)

    next if sum_in == sum_out

    node_edges = get_edges(edges, node)

    node_edges.each do |each_edge|
      from_path = path.select { |x| x[0] == each_edge[0] }
      next unless from_path.empty?

      index = each_edge[1].index(node) || 999
      diff = sum_out - sum_in
      pipe = each_edge[2].to_a

      if diff > 0
        minimun = [diff, each_edge[2][index]].min
        pipe[index] -= minimun
        sum_out -= minimun
      elsif diff < 0
        minimun = [diff.abs, each_edge[2][index - 1]].min
        pipe[index - 1] -= minimun
        sum_in -= minimun
      end

      node_d = each_edge[1][index - 1]
      queue << node_d unless node_d.in?(queue) || node_d.in? ignore

      edges[each_edge[0]] = {each_edge[0]} +
                            {each_edge[1]} +
                            {({Int32, Int32}.from(pipe))} +
                            {each_edge[3]}
    end
  end
end

def get_edges(edges, node)
  edges.select { |x| node.in?(x[1]) }
end

def node_flow_out(edges, node)
  get_edges(edges, node).map do |x|
    index = x[1].index(node) || 5
    x[2][index]
  end.sum
end

def node_flow_in(edges, node)
  get_edges(edges, node).map do |x|
    index = x[1].index(node) || 5
    x[2][index - 1]
  end.sum
end

def get_nodep_path(path, edge)
  return path[0][1].reject { |x| x.in?(path[1][1]) }[0] if edge[-1] == -1

  edge_p = path.select { |y| y[0] == edge[-1] }[0]
  edge[1].select { |y| y.in?(edge_p[1]) }[0]
end

def get_noded_path(path, edge)
  node_p = get_nodep_path(path, edge)
  index = edge[1].index(node_p) || 5

  edge[1][index - 1]
end

def min_cap_p(path)
  path.map do |x|
    node_p = get_nodep_path(path, x)

    x[3] - x[2][x[1].index(node_p) || 5]
  end.min
end

def min_cap(path)
  path.map { |x| x[3] - x[2].sum }.min
end

def max_cap(path)
  path.map { |x| x[3] }.min
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args.map { |x| x.to_i }

header = args[...4]
args = args[4..]

nodes = (0...header[0]).to_a
edges = [] of Tuple(Int32, Tuple(Int32, Int32), Tuple(Int32, Int32), Int32)
# [{id, {node1, node2}, {current flow from node1, c_f from n2}, max flow}]

(0...args.size).step(3).each do |x|
  a = args[x]
  b = args[x + 1]
  edges << {x // 3, {a, b}, {0, 0}, args[x + 2]}
end

path = bfs_cap(nodes, edges, header[2], header[3])
until path.empty?
  clean_path(path)
  change_flow(path, edges)
  balance_nodes(path, edges)

  path = bfs_cap(nodes, edges, header[2], header[3])
end

total = edges.select { |x| header[3].in?(x[1]) }

puts total.sum { |x| x[2].sum }

# $ ./richardalmanza.cr
# 20
