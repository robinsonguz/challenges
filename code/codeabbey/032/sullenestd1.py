# mypy sullenestd1.py
# Success: no issues found in 1 source file


def kill_kths(cont: int, k: int) -> int:
    if cont == 1:
        return 1
    return (kill_kths(cont - 1, k) + k - 1) % cont + 1


def main() -> None:
    input_data = [int(x) for x in input().split()]
    result = kill_kths(input_data[0], input_data[1])
    print(result)


main()

# cat DATA.lst | python sullenestd1.py
# 54
