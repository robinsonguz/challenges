% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0,read_file/1]).

-compile(andresclm).

start() ->
  [_|Data] = read_file("DATA.lst"),
  Matrix = [string:split(Line," ") || Line<-Data],
  Result = [sum_list(Row) || Row<-Matrix],
  [io:format("~B ",[Number]) ||Number<-Result].

sum_list(List) ->
  lists:foldl(fun (Item, Accumulator) ->
    {Number, _} = string:to_integer(Item),
    Number + Accumulator end,
    0, List).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\n").

% $ erl -noshell -s andresclm -s init stop
% 602139 543855 1409727 707784 745831 738766 1493476 792403 881291 811859
% 732132 1458154 1016464 1525208 666583
