(*
$ ocamlc -w @0..1000 slayfer1112.ml
*)
open Printf

let () =
  let cases = Scanf.scanf "%d " (fun x -> x) in
  for _ = 1 to cases do
    let n1 = Scanf.scanf "%d " (fun x -> x) in
    let n2 = Scanf.scanf "%d " (fun x -> x) in
    let n3 = n1 + n2 in
    printf "%d " n3
  done;
  printf "\n"

(*
$ cat DATA.lst | ./slayfer1112
108 260 1999
*)
