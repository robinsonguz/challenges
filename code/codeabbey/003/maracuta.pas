program maracutasumsloop; {Nombre del programa}

Uses sysutils;

var
  Num2, control, x,y, res: longint; {Declaro variables como enteras}

  vector: array [1..100] of longint;

begin

  {Inicializo variables para prevenir error de compilacion}
  Num2 :=0;
  res := 0;
  control := 0;

  {Solicito variable de control}
  writeLn('Introduzca la variable de control: ');
  readLn(control);



  writeLn('Escriba los pares de numeros separados por espacio:');
  for x :=1 to control do
  begin
      y:=2;
  repeat

    read (Num2);
    res:=res+Num2;
    y:=y-1;
  until y=0;

  vector[x]:=res;
  res:=0;

  end;

  for x :=1 to control do
  begin
      write(vector[x]);
      write(' ');
  end;

  readLn();
  readLn();

end.
