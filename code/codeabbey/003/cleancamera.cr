# !/usr/bin/crystal

# $ ameba cleancamera.cr
# Inspecting 1 file.


def read_data_file()
    data = File.read("DATA.lst")
    values = [] of Array(UInt64)
    data.each_line do |x|
        inter = [] of UInt64
        (x.split).each do |y|
            y = y.to_u64
            inter << y
        end
        values << inter
    end
    values[1..]
end

def sum_loop(array)
    menor = array[0] + array[1]
    puts menor
end

get_data = read_data_file()
get_data.each do |x|
    sum_loop(x)
end

# $ crystal cleancamera.cr
# 602139 543855 1409727 707784 745831
# 738766 1493476 792403 881291 811859
# 732132 1458154 1016464 1525208 666583
