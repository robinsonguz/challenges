/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;

  for line in lines {
    let l = line.unwrap();
    if n != 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      let mut sum: i32 = 0;
      for i in params.iter() {
        sum += (i.parse().unwrap_or(0) as i32).pow(2);
      }
      println!("{}", sum);
    } else {
      n = 1;
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
369
945
449
45
17
1139
598
1236
842
125
753
125
1027
179
*/
