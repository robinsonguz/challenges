/*
# dscanner --styleCheck juansierra12.d
# dscanner --syntaxCheck juansierra12.d
# dmd juansierra12.d
*/

import std.stdio;
import std.math : pow;
import std.conv : to;
import std.conv : parse;
import std.array : split;
import std.string;

void main() {
  int res;
  string line;
  string result = "";
  File file = File("DATA.lst", "r");
  int TEST_CASES;
  string[] numbers;

  while ((line = file.readln()) !is null) {

    line = chomp(line);
    TEST_CASES = to!int(line);

    for (int i = 0; i < TEST_CASES; i++) {

      line = chomp(file.readln());
      numbers = line.split(" ");

      foreach (item; numbers) {
        res += pow(parse!int(item), 2);
      }

      result = result ~ to!string(to!int(res)) ~ " ";
      res = 0;
    }
    writeln(result);
  }
}
/*
# ./juansierra12
993 350 298 254 26 217 26 308 1615 1039 668 80 10
*/
