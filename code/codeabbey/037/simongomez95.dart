/*
➜  058 git:(sgomezatfluid) ✗ dartanalyzer sgomezatfluid.dart
Analyzing sgomezatfluid.dart...
No issues found!
*/

import 'dart:io';
import 'dart:math';

void main() async {
  List<String> data = await new File('DATA.lst').readAsLines();
  List<String> numberstring = data[0].split(' ');
  List<int> numberlist = getNumbers(numberstring);
  int loanSize = numberlist[0];
  double rate = numberlist[1]/100/12;
  int months = numberlist[2];
  double monthly = loanSize * (
    (
      rate *
      pow((1 + rate), months)
    )/
      (pow((1 + rate), months) - 1)
      );
  print(monthly.round());

}

List<int> getNumbers(List<String> numberstring) {
  List<int> numberlist = [];
  numberstring.forEach(
    (number) {
      numberlist.add(int.parse(number));
    }
  );
  return numberlist;
}

/*
➜  058 git:(sgomezatfluid) ✗ dart sgomezatfluid.dart
King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
*/
