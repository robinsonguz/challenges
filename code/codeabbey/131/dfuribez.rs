/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::fs::File;
use std::io::{self, BufRead, BufReader};

fn search(letters: &[&str], words: Vec<String>, len: u32) {
  let letters = letters.to_vec();
  let mut total = 0;
  for word in words {
    let mut letters_copy = letters.clone();
    let mut counter: u32 = 0;
    for word_letter in word.chars() {
      let char_str: &str = &word_letter.to_string();
      if letters_copy.contains(&char_str) {
        counter += 1;
        let letter_index = letters_copy.iter().position(|&x| x == char_str);
        letters_copy.remove(letter_index.unwrap_or(0));
      }
    }
    if counter == len {
      total += 1;
    }
  }
  print!("{} ", total);
}

fn read_file(len: u32) -> Vec<String> {
  let file = File::open("words.txt").expect("failed");
  let mut content = Vec::new();

  let buffer = BufReader::new(file).lines();

  for line in buffer {
    let line_content = line.unwrap();
    if line_content.len() == len as usize {
      content.push(line_content);
    }
  }

  content
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;

  for line in lines {
    let l = line.unwrap();
    if n != 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      let len = params[0].parse().unwrap_or(0);
      let letters: &[&str] = &params[1..];
      let words = read_file(len);
      search(letters, words, len);
    } else {
      n = 1;
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
12 8 2 1 3 4 16 11 2 9 3 4 3 18 6 1
*/
