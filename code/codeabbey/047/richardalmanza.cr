#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.74 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

k = args[1].to_i
args = args[2..]
alphabet = ("A".."Z").to_a.join

args.each do |x|
  x.each_char do |chr|
    if !chr.in?(alphabet)
      print chr
      next
    end
    id = ((alphabet.index(chr) || 0) - k) % alphabet.size
    print "#{alphabet[id]}"
  end
  print " "
end

puts

# $ ./richardalmanza.cr
# CALLED IT THE RISING SUN FOUR SCORE AND SEVEN YEARS AGO THE ONCE AND
# FUTURE KING. A DAY AT THE RACES. MET A WOMAN AT THE WELL. THE DEAD BURY
# THEIR OWN DEAD THAT ALL MEN ARE CREATED EQUAL. AS EASY AS LYING. AND SO
# YOU TOO BRUTUS. THE SECRET OF HEATHER ALE WHO WANTS TO LIVE FOREVER.
