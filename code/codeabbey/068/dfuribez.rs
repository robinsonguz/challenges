// $ rustfmt --check --config max_width=80,tab_spaces=2 dfuribez.rs
// $ rustc dfuribez.rs

use std::io::{self, BufRead};

fn calc_dist(distance: f64, v1: f64, v2: f64) -> f64 {
  return (distance * v1) / (v1 + v2);
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;
  for line in lines {
    if n != 0 {
      let l = line.unwrap();
      let params: Vec<&str> = l.split_whitespace().collect();
      let s: f64 = params[0].parse().unwrap_or(0.0);
      let v1: f64 = params[1].parse().unwrap_or(0.0);
      let v2: f64 = params[2].parse().unwrap_or(0.0);

      let dist = calc_dist(s, v1, v2);
      println!("{}", dist);
    } else {
      n = 1;
    }
  }
}
/*
$ cat DATA.lst | ./dfuribez
223.47619047619048
7.659574468085107
32.25
21.666666666666668
9.375
29.454545454545453
20.563636363636363
9.68
15.75
5.260869565217392
4.2
27.711111111111112
7.755102040816326
72
128.8
5.135135135135135
11.2
12.567567567567568
5.5813953488372094
30
117.09433962264151
168.72727272727272
11.428571428571429
*/
