open System

[<EntryPoint>]
let main argv =
    let Operations (myfrac: float) =
        let result = (myfrac- Math.Floor(myfrac))*10.0
        if (result = 0.0) then (int myfrac)
        elif (myfrac>0.0) then (
            if(result>=5.0) then (int (Math.Ceiling(myfrac)))
            else (Convert.ToInt32(myfrac))
            )
        elif(result>=5.0) then (int (Math.Ceiling(myfrac))  )
            else (Convert.ToInt32(myfrac))
    printfn "ingrese los datos:"
    let dataArray = stdin.ReadLine().Split ' ' //array de string
    let numbersArray = Array.zeroCreate (dataArray.Length - 1)
    for i in 1 .. (dataArray.Length-1) do
        numbersArray.[i-1] <- int dataArray.[i]
    for i in 0..numbersArray.Length-1 do
        let mutable c = (float (numbersArray.[i] - 32) ) / 1.8
        let d = Operations c
        printf "%i " d
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
