;; $ clj-kondo --lint andresclm.clj
;; linting took 18ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (vec (rest data'))
        data''' (conj data'' (first data''))]
    (vec (map (fn [line] (str/split line #" ")) data'''))))

(defn polygon-area [points]
  (let [matrix (partition 2 1 points)
        result (reduce (fn [accum points]
                         (let [[[x1 y1] [x2 y2]] points]
                           (+ accum
                              (- (* (Integer. x1) (Integer. y2))
                                 (* (Integer. y1) (Integer. x2))))))
                       0
                       matrix)]
    (format "%.1f" (double (/ (Math/abs result) 2)))))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (polygon-area data)]
    (print  result)))

;; $ clj -m andresclm
;; 62358620.0
