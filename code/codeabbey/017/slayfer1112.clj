;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (str/split (data 1) #" ")]
    [head
     body]))

(defn checksum [array counter len res]
  (if (< counter len)
    (let [resu (* (+ res (array counter)) 113)]
      (if (>= resu 10000007)
        (let [a (mod resu 10000007)]
          (checksum array (inc counter) len a))
        (checksum array (inc counter) len resu)))
    res))

(defn solution [_ body]
  (let [vals (vec(map read-string body))
        check (checksum vals 0 (count vals) 0)]
    (print (str check " "))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 6389830
