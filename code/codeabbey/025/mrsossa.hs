{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let numbers = map read $ words line :: [Int]
        let a = head numbers
        let c = numbers!!1
        let m = numbers!!2
        let x = numbers!!3
        let n = numbers!!4
        let result i x = if i > 0
            then do
                let xx = (a * x + c) `mod` m
                result (i-1) xx
            else show x
        let output = result n x
        print output
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 286 0 2 401907 35 30 149439 10 57
 647 38566 2129 83549 13339 6
-}
