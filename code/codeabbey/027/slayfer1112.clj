;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (str/split (data 1) #" ")]
    [head
     body]))

(defn count-p-s [values p-counter s-counter counter c f]
  (if (< c (dec (count values)))
    (if (< counter (- (dec (count values)) c))
      (if (> (values counter) (values (inc counter)))
        (let [s-countern (inc s-counter)
              temp (assoc values counter (values (inc counter)))
              temp2 (assoc temp (inc counter) (values counter))]
          (if f
            (count-p-s temp2 (inc p-counter) s-countern (inc counter) c false)
            (count-p-s temp2 p-counter s-countern (inc counter) c f)))
        (count-p-s values p-counter s-counter (inc counter) c f))
      (if f
        (count-p-s values p-counter s-counter 0 (inc c) f)
        (count-p-s values p-counter s-counter 0 (inc c) true)))
    [p-counter s-counter]))

(defn solution [_ body]
  (let [values (vec (map read-string body))
        [p-counter s-counter] (count-p-s values 1 0 0 0 true)]
    (print (str p-counter " " s-counter))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 15 70
