# $ lintr::lint('mrivera3.r')


bubsort <- function() {
  data <- scan("DATA.lst")
  data <- data[- 1]
  counter <- 0
  swaps <- 0
  swapvec <- c()
  repeat {
    prov <- data
    counter <- counter + 1
    for (i in seq_len(length(data) - 1)) {
      a <- data[i]
      b <- data[i + 1]
      if (data[i] > data[i + 1]) {
        data[i] <- b
        data[i + 1] <- a
        swaps <- swaps + 1
        swapvec <- c(swapvec, swaps)
      }
    }
    if (all(data == prov)) {
      break
    }
  }
  cat(counter, swaps)
}
bubsort()

# $ Rscript mrivera3.r
#16 97
