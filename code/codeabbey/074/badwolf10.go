/*
$ golint badwolf10.go #linting
$ go tool compile -e badwolf10.go #compilation
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "math"
  "os"
  "strconv"
  "strings"
)

func main() {

  input, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }

  timeScanner := bufio.NewScanner(input)
  timeScanner.Scan()
  timeScanner.Scan()
  times := strings.Split(timeScanner.Text(), " ")

  for t := 0; t < len(times); t++ {
    time := strings.Split(times[t], ":")
    H, _ := strconv.Atoi(time[0])
    m, _ := strconv.Atoi(time[1])
    aH := float64((H%12)*30 + m/2)
    am := float64(m * 6)
    xH := 6.0*math.Sin(math.Pi*aH/180.0) + 10.0
    yH := 6.0*math.Cos(math.Pi*aH/180.0) + 10.0
    xm := 9.0*math.Sin(math.Pi*am/180.0) + 10.0
    ym := 9.0*math.Cos(math.Pi*am/180.0) + 10.0
    fmt.Printf("%f %f %f %f ", xH, yH, xm, ym)
  }
}

/*
$ go run badwolf10.go
4.803848 13.000000 10.940756 18.950697 4.022832 10.522934 18.221909 13.660630
5.208187 13.610890 18.950697 10.940756 9.268784 4.044723 19.000000 10.000000
15.999086 10.104714 8.128795 18.803328
*/
