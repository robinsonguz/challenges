/*
$ cppcheck --enable=all --suppress=missingIncludeSystem alb4tr02.c #linting
Checking alb4tr02.c ...
$ splint -preproc alb4tr02.c
Splint 3.1.2 --- 25 Aug 2010

Finished checking --- no warnings

$ gcc -Wall -Wextra -Wstrict-aliasing -pedantic -fmax-errors=5 -Werror \
-Wunreachable-code -Wcast-align -Wcast-qual -Wdisabled-optimization \
-Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wredundant-decls \
-Wstrict-overflow=5 -Wshadow -Wswitch-default -Wundef -Wconversion \
alb4tr02.c -o alb4tr02 #compilation
*/

#include <stdio.h>
#include <stdlib.h>

static char is_palindrome(char const *, int) /*@*/;

int main(int /*@unused@*/ argc, char /*@unused@*/ *argv[]) {

  int cnt, indx, n_read, i;
  char c;
  char buff[400];
  argc++;
  argv++;
  n_read = scanf("%d", &cnt);
  for (i = 0; i < 400; i++) {
    buff[i] = '\0';
  }
  n_read = scanf("%c", &c);
  for (indx = 0; cnt >= 1 && n_read != 0; cnt--) {
    n_read = scanf("%c", &c);
    while (c != '\n') {
      c = (c >= 'A' && c <= 'Z') ? (char)((int)c + 32): c;
      if (c >= 'a' && c <= 'z' && indx <= 399) {
        buff[indx] = c;
        indx++;
      }
      n_read = scanf("%c", &c);
    }
    printf("%c ", is_palindrome(buff, indx));
    indx = 0;
  }
  return 0;
}

static char is_palindrome(char const *buff, int indx) {

  int i = 0;
  indx--;
  while (indx >= 0) {
    if (buff[i] != buff[indx]) {
      return 'N';
    }
    indx--;
    i++;
  }
  return 'Y';
}

/*
$ cat DATA.lst | ./alb4tr02
N Y Y Y Y Y N Y N Y N N N N N
*/
