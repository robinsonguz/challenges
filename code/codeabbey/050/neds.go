/*
$ gofmt -w neds.go
$ go vet neds.go
vet.exe: errors: 0; warnings: 0
$ go build -work neds.go
WORK=/tmp/go-build085615486
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "os"
  "strconv"
  "strings"
)

func convString(el string) int {
  number, e := strconv.Atoi(el)
  if e != nil {
    fmt.Println(e)
  }
  return number
}

func readData() []string {
  var dat []string
  file, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  lines := bufio.NewScanner(file)
  i := 0
  for lines.Scan() {
    if i != 0 {
      dat = append(dat, strings.ToLower(lines.Text()))
    }
    i = 1
  }

  if err := lines.Err(); err != nil {
    log.Fatal(err)
  }
  return dat
}

func main() {
  var out string
  var palindrome string
  var phrase []string
  data := readData()

  for i := 0; i < len(data); i++ {
    phrase = nil
    palindrome = "Y"
    for j := 0; j < len(data[i]); j++ {
      if data[i][j] > 96 && data[i][j] < 123 {
        phrase = append(phrase, string(data[i][j]))
      }
    }

    length := len(phrase)
    for k := 0; k < length; k++ {
      if phrase[k] != phrase[length-1-k] {
        palindrome = "N"
        break
      }
    }
    out += palindrome + " "
  }
  fmt.Println(out)
}

/*
./neds
N Y Y
*/
