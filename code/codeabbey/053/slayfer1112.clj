;; $ clj-kondo --lint slayfer1112.clj
;; linting took 22ms, errors: 0, warnings: 0

(ns slayfer1112
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")
        body (subvec datv 1 (alength (to-array-2d datv)))]
    [head
     body]))

(defn get-map [data res counter]
  (if (< counter (alength (to-array-2d data)))
    (let [val (data counter)]
      (get-map data (assoc res val counter) (inc counter)))
    res))

(defn ul [i-q j-q res]
  (if (and (> i-q 0) (> j-q 0))
    (ul (dec i-q) (dec j-q) (conj res [(dec i-q) (dec j-q)]))
    res))

(defn ur [i-q j-q res]
  (if (and (> i-q 0) (< j-q 7))
    (ur (dec i-q) (inc j-q) (conj res [(dec i-q) (inc j-q)]))
    res))

(defn dl [i-q j-q res]
  (if (and (< i-q 7) (> j-q 0))
    (dl (inc i-q) (dec j-q) (conj res [(inc i-q) (dec j-q)]))
    res))

(defn dr [i-q j-q res]
  (if (and (< i-q 7) (< j-q 7))
    (dr (inc i-q) (inc j-q) (conj res [(inc i-q) (inc j-q)]))
    res))

(defn -eval [vals king counter]
  (if (< counter (count vals))
    (if (= (vals counter) king)
      true
      (-eval vals king (inc counter)))
    false))

(defn diags [i-q j-q i-k j-k]
  (let [u-l (ul i-q j-q [])
        u-r (ur i-q j-q [])
        d-l (dl i-q j-q [])
        d-r (dr i-q j-q [])
        vals (vec (concat [[i-q j-q]] u-l u-r d-l d-r))]
    (-eval vals [i-k j-k] 0)))

(defn check? [i-k j-k i-q j-q]
  (if (or (= i-k i-q) (= j-k j-q))
    true
    (diags i-q j-q i-k j-k)))

(defn check [line]
  (let [vals (str/split line #" ")
        rows (get-map ["8" "7" "6" "5" "4" "3" "2" "1"] {} 0)
        columns (get-map ["a" "b" "c" "d" "e" "f" "g" "h"] {} 0)
        king (str/split (vals 0) #"")
        [j-k i-k] [(get columns (king 0)) (get rows (king 1))]
        queen (str/split (vals 1) #"")
        [j-q i-q] [(get columns (queen 0)) (get rows (queen 1))]]
    (if (check? i-k j-k i-q j-q)
      (print "Y" "")
      (print "N" ""))))

(defn solution [_ body]
  (doseq [c body]
    (check c)))

(defn main []
  (let [[head body] (get-data "DATA.lst")]
    (solution head body)))

(main)

;; Y N Y N N N N N Y N N Y Y Y N N N N N N N
