# $ lintr::lint('lfcbluis.r')

datain <- read.table("stdin", fill = TRUE,
                     header = FALSE)
counter <- datain[1, 1]
currentline <- 2

while (counter > 0) {
  x1 <- datain[currentline, 1]
  y1 <- datain[currentline, 2]
  x2 <- datain[currentline, 3]
  y2 <- datain[currentline, 4]
  x3 <- datain[currentline, 5]
  y3 <- datain[currentline, 6]

  a <- sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2)
  b <- sqrt((x3 - x2) ^ 2 + (y3 - y2) ^ 2)
  c <- sqrt((x1 - x3) ^ 2 + (y1 - y3) ^ 2)

  s <- (a + b + c) / 2

  ar <- sqrt(a * (s - a) * (s - b) * (s - c))

  counter <- counter - 1
  currentline <- currentline + 1

  cat(ar, " ")
}
#cat DATA.lst|Rscript lfcbluis.r
#1281918  152015.1  1488566  7283377  3057148  6930417 9241255
#4209809  1444902  9681582  4175849  2166587  5811396  8317565
