{-
 $ ghc  jdiegoc.hs
   [1 of 1] Compiling Main ( jdiegoc.hs )
   Linking code ...
 $ hlint jdiegoc.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO ()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = map read $ words line :: [Double]
        let x1 = head vector_ints
        let y1 = vector_ints!!1
        let x2 = vector_ints!!2
        let y2 = vector_ints!!3
        let x3 = vector_ints!!4
        let y3 = vector_ints!!5
        let ab = sqrt((x1-x2)^2+(y1-y2)^2)
        let ac = sqrt((x1-x3)^2+(y1-y3)^2)
        let bc = sqrt((x2-x3)^2+(y2-y3)^2)
        let area = 1/4*sqrt(4*(bc^2*ac^2)-(bc^2+ac^2-ab^2)^2)
        print area
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ runhaskell jdiegoc.hs
 8008545.5 9597811.000000006 1.1915667000000002e7 1.1450171e7 1.5066084e7
 2427172.5000000005 3265797.0000000047 3.102292e7 492795.0
 5095620.999999998 4526801.999999997 8112862.5 1371712.9999999972
 1036641.0 7015461.5 200912885e7
-}
