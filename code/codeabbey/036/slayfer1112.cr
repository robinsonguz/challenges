#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 5.27 milliseconds
# $ crystal build slayfer1112.cr
def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(String)
  data.each_line do |x|
    inter = [] of String
    (x.split).each do |y|
      inter << y
    end
    values << inter
  end
  values[1..]
end

def match(number, guess, corrects)
  match = 0
  i = 0
  while i < number.size
    if number[i] == guess[i]
      match += 1
    end
    i += 1
  end
  match == corrects
end

def solution(array)
  time1 = Time.local
  tests = array.size
  digits = array[0][0].size
  final = "9"
  if digits > 1
    1.step(to: digits-1, by:1) do |_|
      final += "9"
    end
  end
  final = final.to_i
  0.step(to: final, by: 1) do |x|
    x = x.to_s
    while x.size < digits
      x = "0" + x
    end
    pass = 0
    array.each do |y|
      pass += 1 if match(x, y[0], y[1].to_i)
    end
    if pass == tests
      print "#{x} "
      print "\nfinish in #{(Time.local - time1).nanoseconds/1000000} "
      print "milliseconds"
    end
  end
end

data = data_entry()
solution(data)
puts

# $ ./slayfer1112
# 1895
# finish in 6.143 milliseconds
