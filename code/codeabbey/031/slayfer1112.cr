#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 79.96 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(String)
  data.each_line do |x|
    values << x.split
  end
  values[1..]
end

def solution(array)
  k = Int32.new(array[0])-1
  s = array[1]
  if k<0
    k = s.size+k
  end
  sol = s[k+1..]+s[..k]
  print "#{sol} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# itqwoaaabhcmpgas tzgvpazsoecfzdytemv igisqmifaymvhmpaowel xgayafgqwuobfuy
# oiaufrkckmowcwdsriyoig vdfeaiaealozulyyy rhyrthfygijyaee apzjhuyueazygvz
# odvoaloeebmdbsby ffakqpporursoslzppspilz mdxlwauqudyhtes
