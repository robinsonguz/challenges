/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_SIZE 1024

static int diagonal_win (int board[])
  /*@modifies nothing@*/ {
  if (board[0] == board[4] && board[0] == board[8]) { return 1; }
  if (board[2] == board[4] && board[2] == board[6]) { return 1; }
  return 0;
}

static int vertical_win (int board[])
  /*@modifies nothing@*/ {
  int i;
  for (i = 0; i < 3; i++) {
    if (board[i] == board[i+3] && board[i] == board[i+6]) { return 1; }
  }
  return 0;
}

static int horizontal_win (int board[])
  /*@modifies nothing@*/ {
  int i = 0;
  while (i < 9) {
    if (board[i] == board[i+1] && board[i] == board[i+2]) { return 1; }
    i += 3;
  }
  return 0;
}

int main(void) {
  int board[9];
  int games, i;
  int pointer = 0;
  int j = 0;
  int seq[9];
  int return_value = 0;
  char temp[MAX_SIZE];
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  games = atoi(temp);
  for (i = 0; i < games; i++) {
    if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
    return_value = sscanf(temp,
    "%d %d %d %d %d %d %d %d %d",
    &seq[0], &seq[1], &seq[2],
    &seq[3], &seq[4], &seq[5],
    &seq[6], &seq[7], &seq[8]);
    for (j = 0; j < 9; j++) { board[j] = 10 + j; }
    for (j = 0; j < 9; j++){
      if (seq[j] <= 9) { pointer = seq[j] - 1; }
      board[pointer] = (j % 2) + 1;
      if (horizontal_win(board) == 1) { /*@innerbreak@*/ break; }
      if (vertical_win(board) == 1) { /*@innerbreak@*/ break; }
      if (diagonal_win(board) == 1) { /*@innerbreak@*/ break; }
    }
    printf("%d ", (j + 1) % 10), j = 0;
  }
  printf("\n");
  return return_value;
}

/*
$ cat DATA.lst | ./slayfer1112
5 6 8 8 8 0 8 6 6 8 7 9 6 8 7
*/
