#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 6.34 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

dicc = Hash(String, Int32).new

args[2..].each do |x|
  dicc[x] = 0 if !dicc.has_key?(x)
  dicc[x] += 1
end

(1..args[1].to_i).each do |x|
  print "#{dicc[x.to_s]} "
end
puts

# $ ./richardalmanza.cr
# 21 24 32 24 23 25 29 27 25 28 16 19 30
