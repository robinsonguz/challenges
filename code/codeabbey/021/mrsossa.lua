--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local d = -1
local result = {}
local case2

for line in data do
    if d == -1 then
        local a = 0
        for token in string.gmatch(line, "[^%s]+") do
            if a == 1 then
                case2 = tonumber(token)
            end
            a = 1
        end
        for i = 0, case2 do
            result[i] = 0
        end
    else
        for token in string.gmatch(line, "[^%s]+") do
            for i = 1, case2 do
                if tonumber(token) == i  then
                    result[i] = result[i]+1
                end
            end
        end
    end
    d = d+1
end
for i = 1, case2 do
    print(result[i])
end

--[[
$ lua mrsossa.lua
21
24
32
24
23
25
29
27
25
28
16
19
30
]]
