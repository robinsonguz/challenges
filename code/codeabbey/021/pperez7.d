/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;

void main(){
  File file = File("DATA.lst", "r");
  string input = file.readln();
  string [] parameters = input.split(" ");
  const int M = parse!int(parameters[0]);
  const int N = parse!int(parameters[1]);
  int [] answers = new int[](N);
  string test = file.readln();
  string [] inputNumbers = test.split(" ");
  int[] numbers = new int[](M);

  for(int i = 0; i < M; i++){
    numbers[i] = parse!int(inputNumbers[i]);
    for(int j = 0; j < N; j++){
      if(numbers[i]==j+1){
        answers[j] = answers[j] + 1;
      }
    }
  }
  writefln("%(%s %)", answers[]);
}

/*
dmd -run pperez7.d
21 24 32 24 23 25 29 27 25 28 16 19 30
*/
