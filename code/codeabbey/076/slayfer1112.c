/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#define MAX_SIZE 1024


/*Function that clear an used string with size of MAX_SIZE*/
static void clean_string (/*@reldef@*/ char *str)
  /*@modifies str@*/ /*@requires maxSet(str) >= 0@*/ {
  int i = 0;
  for (i = 0; i < MAX_SIZE; i++) {
    str[i] = '\0';
  }
}

/*Function that clear an used moves array with size of MAX_SIZE*/
static void clean_moves(/*@reldef@*/ int moves[][4])
  /*@modifies moves[][4]@*/ {
  int i = 0, j = 0;
  for (i = 0; i < MAX_SIZE; i++) {
    for (j = 0; j < 4; j++) {
      moves[i][j] = 0;
    }
  }
}

/*Function that say if the selected piece is pawn*/
static bool is_pawn (/*@in@*/ int board[][8],
  /*@reldef@*/ int i, /*@reldef@*/ int j)
  /*@modifies nothing@*/ {
  if (board[i][j] == 1 || board[i][j] == 2) {
    return true;
  } else {
    return false;
  }
}

/*Function that say if the selected piece is white*/
static bool is_white (/*@in@*/ int board[][8],
  /*@reldef@*/ int i, /*@reldef@*/ int j)
  /*@modifies nothing@*/ {
  if (board[i][j] % 2 == 0) {
    return true;
  } else {
    return false;
  }
}

/*Function that say if the selected piece is black*/
static bool is_black (/*@in@*/ int board[][8],
  /*@reldef@*/ int i, /*@reldef@*/ int j)
  /*@modifies nothing@*/ {
  if (board[i][j] % 2 == 1) {
    return true;
  } else {
    return false;
  }
}

/*Function that say if the selected piece is at the begining*/
static bool is_start (/*@reldef@*/ int i, /*@reldef@*/ int piece)
  /*@modifies nothing@*/ {
  if (piece == 119) {
    return i == 6;
  } else if (piece == 98) {
    return i == 1;
  } else {
    return false;
  }
}

/*Function that say if the move is ahead*/
static bool move_ahead (/*@reldef@*/ int i, /*@reldef@*/ int j)
  /*@modifies nothing@*/ {
  if (i >= 1 && i <= 2 && j == 0) {
    return true;
  } else {
    return false;
  }
}

/*Function that say if the move is diag*/
static bool mov_diag (/*@reldef@*/ int i, /*@reldef@*/ int j)
  /*@modifies nothing@*/ {
  if (i == 1 && (j == -1 || j == 1)) {
    return true;
  } else {
    return false;
  }
}

/*Function that say if a black piece can move*/
static bool move_black (/*@in@*/ int board[][8], /*@in@*/ int mov[])
  /*@modifies nothing@*/ {
  int diff_i, diff_j;
  diff_j = mov[3] - mov[1];
  diff_i = mov[2] - mov[0];
  if (move_ahead(diff_i, diff_j)) {
    if (board[mov[0]+1][mov[1]] == 0) {
      if (diff_i == 2 && is_start(mov[0], 98)) { return true; }
      if (diff_i == 1) { return true; }
    }
  }
  if (mov_diag(diff_i, diff_j)) {
    if (board[mov[2]][mov[3]] % 2 == 0) { return true; }
  }
  return false;
}

/*Function that say if a white piece can move*/
static bool move_white (/*@in@*/ int board[][8], /*@in@*/ int mov[])
  /*@modifies nothing@*/ {
  int diff_i, diff_j;
  diff_j = mov[3] - mov[1];
  diff_i = mov[0] - mov[2];
  if (move_ahead(diff_i, diff_j)) {
    if (board[mov[0]-1][mov[1]] == 0) {
      if (diff_i == 2 && is_start(mov[0], 119)) { return true; }
      if (diff_i == 1) { return true; }
    }
  }
  if (mov_diag(diff_i, diff_j)) {
    if (board[mov[2]][mov[3]] % 2 == 1) { return true; }
  }
  return false;
}

/*Function that say if any move is possible*/
static bool mov_is_possible (/*@in@*/ int board[][8], /*@in@*/ int mov[])
  /*@modifies nothing@*/ {
  if (is_pawn(board, mov[0], mov[1])) {
    if (is_white(board, mov[0], mov[1])) {
      return move_white(board, mov);
    }
    else if (is_black(board, mov[0], mov[1])) {
      return move_black(board, mov);
    }
    else { return false; }
  }
  return true;
}

/*Function that say where is the incorrect move, 0 = all moves are ok*/
static int has_incorrect_mov (/*@in@*/ int moves[][4], /*@reldef@*/ int steps)
  /*@modifies nothing@*/ {
  int board[8][8] = {{3,3,3,3,3,3,3,3},
                     {1,1,1,1,1,1,1,1},
                     {0,0,0,0,0,0,0,0},
                     {0,0,0,0,0,0,0,0},
                     {0,0,0,0,0,0,0,0},
                     {0,0,0,0,0,0,0,0},
                     {2,2,2,2,2,2,2,2},
                     {4,4,4,4,4,4,4,4}};
  int i = 0, j = 0;
  for (i = 0; i <= steps; i++) {
    if (mov_is_possible(board, moves[i])) {
      if (moves[i][3] <= 7 && moves[i][1] <= 7) {
        board[moves[i][2]][moves[i][3]] = board[moves[i][0]][moves[i][1]];
        board[moves[i][0]][moves[i][1]] = 0;
      }
    } else {
      j = i + 1;
      break;
    }
  }
  printf("%d ", j);
  return 1;
}

/*Function that get the entry data and eval the challenge*/
int main(void) {
  int cases, i = 0, j = 0, len = 0, steps = 0, k = 0, return_val = 0;
  char input[MAX_SIZE];
  int moves[MAX_SIZE][4];
  if (fgets(input, MAX_SIZE, stdin) == 0) { return 0; }
  cases = atoi(input);
  for (i = 0; i < cases; i++) {
    if (fgets(input, MAX_SIZE, stdin) == 0) { return 0; }
    len = (int) strlen(input) - 1;
    steps = (len - 4) / 5;
    for (j = 0; j <= steps; j++) {
      k = 5 * j;
      moves[j][1] = (int) input[k] - 97;
      moves[j][0] = 7 - ((int) input[k+1] - 49);
      moves[j][3] = (int) input[k+2] - 97;
      moves[j][2] = 7 - ((int) input[k+3] - 49);
    }
    return_val = has_incorrect_mov(moves, steps);
    steps = 0, k = 0;
    clean_moves(moves);
    clean_string(input);
  }
  printf("\n");
  return return_val;
}

/*
$ cat DATA.lst | ./slayfer1112
3 4 4 0 6 2 0 4 6 0 6 3 0
*/
