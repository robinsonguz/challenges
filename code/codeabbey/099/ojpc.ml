let slope = [|0;0;0;0;0;0;0;1;1;2;2;3;4;4;4;4;5;6;7;8;9;10;10;10;11;12;12;12;13;13;13;14;14;14;15;15;16;16;17;18|];;
let xslop= [|0;4;8;12;16;20;24;28;32;36;40;44;48;52;56;60;64;68;72;76;80;84;88;92;96;100;104;108;112;116;120;124;128;132;136;140;144;148;152;156|];;
let intmult (l,m)= Array.map (fun x -> x * m) l;;
let yslop= intmult (slope,4);;
let veli= 32;;
let angi = 34;;
let index = ref 0;;
let t= ref 0.1;;
let res= ref 0.0;;
let rec find a x n =
if a.(n) = x then n 
else find a x (n+1);;
let disty= ref 0.0;;
let distx= ref 0.0;;

while !t<100.0 do
    let pi = 4.0 *. atan 1.0 in
    distx := (float_of_int veli)*. !t*.cos((float_of_int angi)*.(pi/.180.0));
  disty := (float_of_int veli)*. !t*.sin((float_of_int angi)*.(pi/.180.0)) -. 0.5*.(9.81*.(!t**2.0));
  for i=1 to Array.length xslop -1 do
        if (!distx > float_of_int (xslop.(i-1)) && !distx < float_of_int (xslop.(i))) then (index:= i-1 );
  done;
    if (!disty <= float_of_int(yslop.(!index))) then (res:= !distx; print_float !res; t:=100.0;) else (t:= !t+.0.0001);
done;


     
