% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Matrix = [string:tokens(Line," ") || Line<-Data],
  Result = [get_results(Row) || Row<-Matrix],
  [io:format("(~B ~B) ",[A,B]) ||[A,B]<-Result].

get_results([A,B,C,D]) ->
  {X1, _} = string:to_integer(A),
  {Y1, _} = string:to_integer(B),
  {X2, _} = string:to_integer(C),
  {Y2, _} = string:to_integer(D),
  Slope = calculate_slope(X1,Y1,X2,Y2),
  Intercept = calculate_intercept(Y1,X1,Slope),
  [Slope, Intercept].

calculate_slope(X1,Y1,X2,Y2) ->
  (Y1-Y2) div (X1-X2).

calculate_intercept(Y,X,M) ->
  (Y - (X*M)).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [_|Data] = string:tokens(erlang:binary_to_list(Binary), "\n"),
  Data.

% $ erl -noshell -s andresclm -s init stop
% (-78 -834) (-48 -353) (95 621) (58 -495) (91 -681) (-60 254) (-10 430)
% (-32 -645) (-48 -855) (-21 -279) (-66 520) (-97 -212) (11 268) (-48 -699)
% (73 -977)
