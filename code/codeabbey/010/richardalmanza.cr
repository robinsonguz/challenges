#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 3.5 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

class Point
  property x : Int32
  property y : Int32

  def initialize(@x, @y)
  end
end

def m(p1 : Point, p2 : Point)
  (p2.y - p1.y) // (p2.x - p1.x)
end

def b(p1 : Point, p2 : Point)
  (p1.y * p2.x - p2.y * p1.x) // (p2.x - p1.x)
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

point_list = [] of Tuple(Point, Point)

(1...args[1..].size).step(4) do |x|
  point_list << {Point.new(args[x].to_i, args[x + 1].to_i),
                    Point.new(args[x + 2].to_i, args[x + 3].to_i)}
end

point_list.each do |x|
  print "(#{m(x[0], x[1])} #{b(x[0], x[1])}) "
end

puts

# $ ./richardalmanza.cr
# (-78 -834) (-48 -353) (95 621) (58 -495) (91 -681) (-60 254) (-10 430)
# (-32 -645) (-48 -855) (-21 -279) (-66 520) (-97 -212) (11 268) (-48 -699)
# (73 -977)
