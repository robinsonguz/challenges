/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;
import std.math;
import std.format;

void main(){
  File file = File("DATA.lst", "r");
  string testcases = file.readln();
  const int pairs = parse!int(testcases);
  string [] answers = new string[](pairs);

  for(int i = 0; i < pairs; i++){
    string input = file.readln();
    string [] points = input.split(" ");
    int [] X;
    int [] Y;
    for(int j = 0; j < 4; j++){
      if(j % 2 == 0){
        X ~= parse!int(points[j]);
      }
      else{
        Y ~= parse!int(points[j]);
      }
    }
    double a = 0;
    double b = 0;
    a = ((Y[1]-Y[0])/(X[1]-X[0]));
    b = (Y[0]-a*X[0]);
    answers[i]="("~to!string(a)~" "~to!string(b)~")";
  }
  writefln("%-(%s %)",answers[]);
}

/*
dmd -run pperez7.d
(-78 -834) (-48 -353) (95 621) (58 -495) (91 -681) (-60 254) (-10 430)
(-32 -645) (-48 -855) (-21 -279) (-66 520) (-97 -212) (11 268) (-48 -699)
(73 -977)
*/
