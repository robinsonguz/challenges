#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

CARDS = " A23456789T"

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split("\n") if fluid
args = args[0].split("\n") if !fluid

args = args[1..]

args.each do |h|
  hand = h.split
  ace_counter = 0
  points_counter = 0

  hand.each do |card|
    points_card = CARDS.index(card) || 10

    ace_counter += 1 if points_card == 1
    next if points_card == 1

    points_counter += points_card
  end

  (0...ace_counter).each do |x|
    possible_score = (ace_counter - x) * 11 + points_counter
    points_counter += 1 if possible_score > 21
    points_counter += 11 unless possible_score > 21
  end

  print "Bust " if points_counter > 21
  print "#{points_counter} " unless points_counter > 21 || points_counter == 0
end

puts

# $ ./richardalmanza.cr
# 17 19 17 18 Bust 17 20 Bust 18 16 18 21 16 21 20 18 17 18 20 20 16 21 21 21
