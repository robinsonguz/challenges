-- $ ghc -o kedavamaru kedavamaru.hs
--   [1 of 1] Compiling Main ( kedavamaru.hs, kedavamaru.o )
--   Linking code ...
-- $ hlint kedavamaru.hs
--   No hints

module Main where

import System.IO
import Control.Monad

process :: Handle -> IO ()
process ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = map read $ words line :: [Int]
        print (minimum vector_ints)
        process ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    process ifile
    hClose ifile

-- $ ./kedavamaru
--   107814
--   386086
--   -5721210
--   -3265497
--   -9006366
--   124682
--   -455274
--   -4695046
--   -2027624
--   -2726599
--   6289004
--   -2232700
--   -5564619
--   -9844812
--   -3110310
--   -2335335
--   -9253696
--   -9053831
--   -3981282
--   -2784460
--   -1406274
--   -8164135
--   3309434
