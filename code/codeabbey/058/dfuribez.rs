/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn decode_card(card_number: i32) {
  let suit = ["Clubs", "Spades", "Diamonds", "Hearts"];
  let rank = [
    "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King",
    "Ace",
  ];
  let suit_index: i32 = card_number / 13;
  let rank_index: i32 = card_number % 13;
  println!(
    "{}-of-{}",
    rank[rank_index as usize], suit[suit_index as usize],
  );
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;

  for line in lines {
    let l = line.unwrap();
    if n != 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      for i in params.iter() {
        decode_card(i.parse().unwrap_or(0));
      }
    } else {
      n = 1;
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
Jack-of-Diamonds
7-of-Diamonds
4-of-Clubs
6-of-Hearts
5-of-Clubs
3-of-Hearts
6-of-Clubs
2-of-Clubs
2-of-Spades
2-of-Diamonds
10-of-Hearts
8-of-Diamonds
8-of-Clubs
7-of-Hearts
3-of-Clubs
6-of-Diamonds
8-of-Hearts
5-of-Diamonds
Ace-of-Hearts
4-of-Spades
4-of-Diamonds
7-of-Clubs
9-of-Hearts
4-of-Hearts
*/
