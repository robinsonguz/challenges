{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = reverse( map read $ words line :: [Int])
        let suits = ["Clubs", "Spades", "Diamonds", "Hearts"]
        let ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10",
                    "Jack", "Queen", "King", "Ace"]
        let l = length vector_ints-1
        let iter i = if i>=0
            then ranks!!(vector_ints!!i `mod` 13)++"-of-"++
                suits!!(vector_ints!!i `div` 13) ++ " " ++ iter(i-1)
            else ""
        let exit = iter l
        print exit
        processfile ifile


main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
 Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
 Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
 3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
 2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
-}
