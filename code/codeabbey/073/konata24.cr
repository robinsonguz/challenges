#! /usr/bin/crystal

# $ ameba konata24.cr
# Inspecting 1 file.
# .
# Finished in 4.19 milliseconds
# $ crystal build konata24.cr
def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(String)
  data.each_line do |x|
    inter = [] of String
    (x.split("")).each do |y|
      inter << y
    end
    values << inter
  end
  values[1..]
end

# This dictionary store the movements in relation of the angle
# of the movement
def dicc()
  pi = 3.141_592_653_589_793_238_46
  dicc = Hash(String, Float64).new
  dicc["A"] = 0
  dicc["B"] = pi/3
  dicc["C"] = 2*pi/3
  dicc["D"] = pi
  dicc["E"] = -2*pi/3
  dicc["F"] = -1*pi/3
  dicc
end

def solution(array)
  mov_x = 0
  mov_y = 0
  array.each do |x|
    # With "cos" and "sin" I can discriminate the values of X and Y
    mov_x += Math.cos(dicc[x])
    mov_y += Math.sin(dicc[x])
  end
  distance = (((mov_x**2)+(mov_y**2))**0.5).round(8)
  print "#{distance} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./konata24
# 4.3588989400000004 5.19615242 8.88819442 5.29150262 7.0 1.0 2.64575131 5
#.19615242 6.08276253 3.60555128 3.60555128 1.0 1.0 6.08276253 8.71779789
# 3.60555128 4.58257569 6.0 4.0 7.0 2.64575131
