open System

[<EntryPoint>]
let main argv =
    let operations n =
        let mutable num = n
        let mutable newNum = n
        let mutable numDigits = 0
        while (num > 0 ) do //primero saber cuantos digitos tiene el numero
            num <- int (num/10)
            numDigits <- numDigits + 1
        let mutable j = numDigits
        let mutable sum = 0
        while (j > 0 ) do
            sum <- sum + (newNum % 10)*j
            newNum <- int (newNum/10)
            j <- j - 1
        sum
    printfn "ingrese la cantidad de pruebas"
    let c = stdin.ReadLine() |> int
    printfn "ingrese %i numeros separados por un espacio" c
    let dataArray = stdin.ReadLine().Split ' '
    for i in 0..(c-1) do
        printf "%i " (operations (int dataArray.[i]))
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
