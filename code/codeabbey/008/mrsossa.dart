/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    for (var i = 1; i < data.length; i++) {
      var line = data[i].split(" ");
      var a = int.parse(line[0]);
      var b = int.parse(line[1]);
      var n = int.parse(line[2]);
      var sum = 0;
      for (var i = 0; i < n; i++) {
        sum = sum + (a + i * b);
      }
      print(sum);
    }
  }
  );
}

/* $ dart mrsossa.dart
 * 91180 49039 29762 9860 3552 867 97119 3225 779 3654
*/
