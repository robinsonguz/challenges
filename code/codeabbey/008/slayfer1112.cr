#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.01 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_i : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  a = array[0]
  b = array[1]
  n = array[2]

  sum = 0
  counter = 0

  while counter < n
    if counter == 0
      sum = a
    else
      a += b
      sum += a
    end
    counter += 1
  end

  print "#{sum} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 26000 82110 21575 1320 5226 8576 3306 2020 15106
