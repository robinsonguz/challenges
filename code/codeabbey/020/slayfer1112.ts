/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function solution(entry: string): number {
  const noVowels: string = entry.replace(/[aeiouy]/g, '');
  const vowels: number = entry.length - noVowels.length;
  process.stdout.write(`${vowels} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
10 4 8 18 8 10 10 15 11 11 11 8 12 10 11 9 17 9 13 6
*/
