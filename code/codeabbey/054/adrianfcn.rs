/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn solution(value: i64) {
  let mut i: i64 = 1;
  let mut j: i64 = 1;
  let input: i64 = value / 2;
  loop {
    while j.pow(2) + (j * i) < input {
      j += 1;
    }
    if j.pow(2) + (j * i) == input {
      break;
    } else {
      i += 1;
      j = 1;
    }
  }
  let sol = (i.pow(2) + j.pow(2)).pow(2);
  print!("{} ", sol);
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  lines.next();
  for l in lines {
    solution(l.parse::<i64>().unwrap());
  }
  println!();
  Ok(())
}

/*
  $ ./adrianfcn
  88390026150409 41051455793881 42074487655225 107016728422321 100589526301489
  65692078654225 31933179393025
*/
