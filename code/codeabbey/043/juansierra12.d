/*
$ dscanner --imports juansierra12.d #linting
  std.conv
  std.stdio
$ dscanner --syntaxCheck juansierra12.d #linting
$ dscanner --styleCheck juansierra12.d #linting
$ dmd juansierra12.d -of=juansierra12 #compilation
$
*/
import std.stdio;
import std.conv;

void main() {
  //Initializing variables
  int test_cases;
  real num = 0.0;
  string input = " ";
  string res = "";

  //Read input from stdin
  readf("%s", &test_cases);
  readln();

  for (int i=0; i < test_cases; i++) {
    //Read each o the numbers
    input = readln();
    //Parse string red to real
    num = parse!real(input);
    num = num*6+1;
    //Concat the string that has the output
    res = res ~ to!string(to!int(num)) ~ " ";
  }
  writeln(res);
}

/*
./juansierra12
61 73 25 67 19 109 55 13 91 43
*/
