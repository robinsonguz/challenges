--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local res = {}
local i = 0
local a
for line in data do
    if i==0 then
        a = tonumber(line)
    else
        res[i-1] = tonumber(line) * 6 + 1
    end
    i = i + 1
end
for c = 0, a-1 do
    print(math.floor(res[c]))
end

--[[
$ lua mrsossa.lua
6
3
1
6
6
5
4
1
5
4
4
6
3
4
1
6
6
2
5
4
3
4
5
2
]]
