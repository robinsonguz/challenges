/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <string.h>
#define MAX_SIZE 1024
#define M_PI acos(-1.0)

/*struct of the stars*/
typedef struct Star {
  char name[MAX_SIZE];
  double x;
  double y;
} Star;

/*
function that rotate the star from the origin with
the given rotation degrees.
*/
static Star rotate_star (/*@in@*/ Star star, /*@reldef@*/ double rotation)
  /*@modifies nothing@*/ {
  /*θ = theta*/
  double theta = rotation * (M_PI/180);
  Star new_star;
  memcpy(new_star.name, star.name, (size_t) 255);
  new_star.x = (star.x * cos(theta)) - (star.y * sin(theta));
  new_star.y = (star.y * cos(theta)) + (star.x * sin(theta));
  return new_star;
}

/*function that print the array of stars*/
static int show_stars (/*@in@*/ Star stars[], /*@reldef@*/ int size)
  /*@modifies nothing@*/ {
  int i = 0;
  for (i = 0; i < size; i++) {
    printf("%s ", stars[i].name);
    /*printf("%s %.2f %.2f\n", stars[i].name, stars[i].x, stars[i].y);*/
  }
  return 1;
}

/*
function to sort the stars by axis Y ascendend and if Y is equal, sort
by X axis ascending too.
*/
static void sort_stars (/*@reldef@*/ Star stars[], /*@reldef@*/ int size)
  /*@modifies stars[]@*/ /*@requires maxSet(stars) >= 1@*/ {
  bool swapped;
  Star temp;
  int i = 0;
  do {
    swapped = false;
    for (i = 0; i < size - 1; i++) {
      if ((stars[i].y - stars[i+1].y) > DBL_EPSILON) {
        temp = stars[i];
        stars[i] = stars[i+1];
        stars[i+1] = temp;
        swapped = true;
      }
      if (fabs(stars[i].y - stars[i+1].y) < DBL_EPSILON) {
        if ((stars[i].x - stars[i+1].x) > DBL_EPSILON) {
          temp = stars[i];
          stars[i] = stars[i+1];
          stars[i+1] = temp;
          swapped = true;
        }
      }
    }
  } while (swapped);
}

/*
When can rotate the star by 2 ways, the first one transform it to polar
coordinate system, sum the angle and convert again to cartesian, the
second ways is to multiply the rotation matrix with the vector X Y,
and we get x' = x cos Θ - y sin Θ; and y' = x sin Θ + y cos Θ;
in this case we try to use the second way.
*/
int main(void) {
  char temp[MAX_SIZE];
  double cases, rotation;
  int i = 0;
  static Star stars[MAX_SIZE], stars_r[MAX_SIZE];
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  if (sscanf(temp, "%le %le", &cases, &rotation) == 0) { return 0; }
  for (i = 0; i < (int) cases; i++) {
    if (fgets(temp, MAX_SIZE, stdin) == 0){
      return 0;
    }
    if (sscanf(temp,"%s %le %le",stars[i].name,&stars[i].x,&stars[i].y) == 0) {
      return 0;
    }
    stars_r[i] = rotate_star(stars[i], rotation);
  }
  sort_stars(stars_r, (int) cases);
  if(show_stars(stars_r, (int) cases) != 1) { return 0; }
  printf("\n");
  return 1;
}

/*
$ cat DATA.lst | ./slayfer1112
Mizar Nembus Media Diadem Kastra Alcyone Altair Vega Procyon Deneb Kochab
Gemma Algol Capella Zosma Rigel Aldebaran Sirius Thabit Jabbah Bellatrix
Yildun Unuk Electra Alcor Betelgeuse Pherkad
*/
