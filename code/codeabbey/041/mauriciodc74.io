#!/usr/local/bin/io
/*
 Scripts are executed without compilation.
 No linter available for IO
*/

data := File with("DATA.lst")
data openForReading
values := data readLine asNumber
while (line := data readLine,
  list := line split
  d := List clone append(list at(0) asNumber,
  list at(1) asNumber,
  list at(2) asNumber)
  d := d sort
  write(d at(1)," ")
)

data close

/*
 $ io mauriciodc74.io
 4 1087 22 57 503 80 161 811 620 112 952 52 15 847
 13 90 38 36 308 14 971 117 250 284 11 929 151 358
*/
