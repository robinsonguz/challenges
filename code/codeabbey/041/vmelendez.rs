/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn check() -> i32 {
  let input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let x: i32 = input[0] - input[1];
  let y: i32 = input[1] - input[2];
  let z: i32 = input[0] - input[2];

  if (x * y) > 0 {
    return input[1];
  }

  if (x * z) > 0 {
    return input[2];
  }

  return input[0];
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  std::io::stdin().read_line(&mut n).unwrap();

  let mut vec = Vec::new();
  for _i in 0..28 {
    vec.push(check());
  }

  let mut out_string = String::new();
  let vec_iter = vec.iter();

  for i in vec_iter {
    out_string += &i.to_string();
    out_string += &" ".to_owned();
  }

  println!("{}", out_string);
  Ok(())
}

/*
  $ cat DATA.lst | ./vmelendez
  4 1087 22 57 503 80 161 811 620 112 952 52 15 847
  13 90 38 36 308 14 971 117 250 284 11 929 151 358
*/
