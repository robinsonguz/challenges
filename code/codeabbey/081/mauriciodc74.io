#!/usr/local/bin/io
/*
 Scripts are executed without compilation.
 No linter available for IO
*/

data := File with("DATA.lst")
data openForReading
values := data readLine asNumber
vector := Sequence clone setItemType("uint32") setEncoding("number")
while (line := data readLine, list = line split)
for(i, 0, values-1,
    b := list at(i) asNumber
    write(vector set(b) bitCount," ")
)
data close

/*
 $ io mauriciodc74.io
 3 20 4 19 12 6 18 12 20 29 30 28 20 3 20 16 6 3 5 10 10 30
 5 29 28 31 20 15 6 27 29 13 7 22 10 0 14 17 22 1 27 19 12
*/
