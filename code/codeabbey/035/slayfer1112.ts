/*
$ eslint slayfer1112.ts && prettier --check slayfer1112.ts
$ tsc \
  --strict \
  --noImplicitAny \
  --noImplicitThis \
  --noUnusedLocals \
  --noUnusedParameters \
  --noImplicitReturns \
  --noFallthroughCasesInSwitch \
  slayfer1112.ts
*/

function savings(
  valS: number,
  valR: number,
  valY: number,
  valP: number
): number {
  if (valS <= valR) {
    const newS = Number((valS * valP).toFixed(2));
    return savings(newS, valR, valY + 1, valP);
  }
  return valY;
}

function solution(entry: number[]): number {
  const [valS, valR, valI]: number[] = entry;
  const hundred = 100;
  const valP: number = valI / hundred + 1;
  const valY = 0;
  process.stdout.write(`${savings(valS, valR, valY, valP)} `);
  return 1;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value.split(' ').map((val) => Number(val))));
    process.stdout.write('\n');
    return 1;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
109 109 273 302 163 137 163 21 92 324 163 163 221 163 7
*/
