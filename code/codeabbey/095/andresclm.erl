% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Matrix = [get_values_as_integers(Line) || Line<-Data],
  {XiYi, Xi, Yi, XiSquare} = lists:foldl( fun({Xi, Yi},{A, B, C, D}) ->
    calculate_accumulators({Xi, Yi},{A, B, C, D}) end,
    {0, 0, 0, 0},
    Matrix),
  N = erlang:length(Matrix),
  K = (XiYi-(Xi*Yi/N))/(XiSquare-(math:pow(Xi,2)/N)),
  B = (Yi/N) - K*(Xi/N),
  io:format("~.11.~f ~.11.~f~n", [K, B]).

calculate_accumulators({Xi, Yi},{A, B, C, D}) ->
  Result1 = A + (Xi*Yi),
  Result2 = B + Xi,
  Result3 = C + Yi,
  Result4 = D + math:pow(Xi,2),
  {Result1, Result2, Result3, Result4}.

get_values_as_integers(List) ->
  [_,A,B] = string:lexemes(List," "),
  {Xi,_} = string:to_integer(A),
  {Yi,_} = string:to_integer(B),
  {Xi, Yi}.

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  [_|Data] = string:lexemes(erlang:binary_to_list(Binary), "\n"),
  Data.

% $ erl -noshell -s andresclm -s init stop
% 2.10020654881 91.90653583023
