/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function solution(entry: string[]): number {
  const dat: string[] = entry;
  const x: number[] = dat.map((value) => Number(value.split(' ')[1]));
  const y: number[] = dat.map((value) => Number(value.split(' ')[2]));
  const xpro: number = x.reduce((acc, cur) => acc + cur) / x.length;
  const ypro: number = y.reduce((acc, cur) => acc + cur) / y.length;
  const xXpro: number[] = x.map((value) => value - xpro);
  const yYpro: number[] = y.map((value) => value - ypro);
  const XxYy: number[] = xXpro.map((value, index) => value * yYpro[index]);
  const interX: number[] = xXpro.map((value) => value * value);
  const xXproSqrt: number = interX.reduce((acc, cur) => acc + cur);
  const interY: number[] = yYpro.map((value) => value * value);
  const yYproSqrt: number = interY.reduce((acc, cur) => acc + cur);
  const sxy: number = XxYy.reduce((acc, cur) => acc + cur) / x.length;
  const sxx: number = Math.sqrt(xXproSqrt / x.length);
  const syy: number = Math.sqrt(yYproSqrt / y.length);
  const coe: number = sxy / (sxx * syy);
  const resK: number = coe * (syy / sxx);
  const resB: number = ypro - resK * xpro;
  process.stdout.write(`${resK} ${resB}`);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    solution(dat);
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat dat.lst | node slayfer1112.js
0 0 1 1 0 1 1 1 1 1 1 0 0 0 0 1 1 1 1 0
*/
