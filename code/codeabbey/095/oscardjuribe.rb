# frozen_string_literal: true

# rubocop oscardjuribe.rb
# Inspecting 1 file
# 1 file inspected, no offenses detected

# Code to solve the proble linear regresion from codeabbey

# !/usr/bin/ruby

# array to save days
days = []
# array to save prices
prices = []

# count to ignore first line
line_num = 0
# open DATA.lst file
text = File.open('DATA.lst').read
# sub \r\n to only \n
text.gsub!(/\r\n?/, "\n")
# iterate over lines
text.each_line do |line|
  if line_num != 0
    # get current day and price after split
    _, day, price = line.split(' ')
    # push day to array as float
    days.push(day.to_f)
    # push price to array as float
    prices.push(price.to_f)
    # stop if we are in the final year
  end
  line_num += 1
end

# calculate mean of x
x_mean = days.reduce(:+).to_f / days.size
# calculate mean of y
y_mean = prices.reduce(:+).to_f / prices.size

# acumulate (x-x_mean)*(y-y_mean)
sum_diff_x_y = 0

# acumulate (x-x_mean)^2
sum_diff_x = 0

# acumulate (y-y_mean)^2
sum_diff_y = 0

# size of array
lenght = days.size

(1...lenght).each do |index|
  # get the current x value
  x = days[index]
  # get the current y value
  y = prices[index]
  # calculate (x - x_mean) * (y - y_mean)
  sum_diff_x_y += (x - x_mean) * (y - y_mean)
  # calculate (x-x_mean)^2
  sum_diff_x += (x - x_mean) * (x - x_mean)
  # calculate (y-y_mean)^2
  sum_diff_y += (y - y_mean) * (y - y_mean)
end

# calculate pearson correlation coeficient
r = sum_diff_x_y / Math.sqrt(sum_diff_x * sum_diff_y)

# standard desviation for x
std_x = Math.sqrt(sum_diff_x / (days.size - 1))
# standard desviation for y
std_y = Math.sqrt(sum_diff_y / (days.size - 1))

# calculate b coeficient
b = r * (std_y / std_x)
# # calculate a coeficient
a = y_mean - b * x_mean

# print solution
puts "#{b} #{a}"

# ruby oscardjuribe.rb
# 2.1026343967840027 91.66132318508971
