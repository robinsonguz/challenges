;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn solution [data]
  (let [x (vec (map (fn [[_ valx _]] (read-string valx)) data))
        y (vec (map (fn [[_ _ valy]] (read-string valy)) data))
        xpro (double (/ (reduce + x) (count x)))
        ypro (double (/ (reduce + y) (count y)))
        x-xpro (vec (map #(- % xpro) x))
        y-ypro (vec (map #(- % ypro) y))
        xx-yy (vec (map * x-xpro y-ypro))
        x-xpro-sqrt (reduce + (vec (map #(* % %) x-xpro)))
        y-ypro-sqrt (reduce + (vec (map #(* % %) y-ypro)))
        sxy (/ (reduce + xx-yy) (count xx-yy))
        sx (Math/sqrt (double (/ x-xpro-sqrt (count x))))
        sy (Math/sqrt (double (/ y-ypro-sqrt (count y))))
        p (/ sxy (* sx sy))
        b (* p (/ sy sx))
        a (- ypro (* b xpro))]
    (print (str b " " a " "))))

(defn main []
  (let [[_ body] (get-data)]
    (solution (map #(str/split % #" ") body))
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 1.8567559342754807 84.64146186099433
