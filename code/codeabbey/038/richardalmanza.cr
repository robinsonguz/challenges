#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(3).each do |x|
  i = false
  a = args[x]
  b = args[x + 1]
  c = args[x + 2]

  discriminat = b ** 2 - 4 * a * c
  i = true if discriminat < 0

  if i
    real = (-b / (2 * a)).to_i
    imaginary = (Math.sqrt(- discriminat) / (2 * a)).to_i
    print "#{real}+#{imaginary}i "
    print "#{real}-#{imaginary}i"
  else
    print "#{((-b + Math.sqrt(discriminat)) / (2 * a)).to_i} "
    print "#{((-b - Math.sqrt(discriminat)) / (2 * a)).to_i}"
  end

  print "; " unless x + 3 == args.size
end

puts

# $ ./richardalmanza.cr
# 1 -10; 6 3; 6 -6; 3 -5; 10 7; 4+9i 4-9i; -3+8i -3-8i; -9+7i -9-7i; -7+1i
# -7-1i; 3 2; 10 -2; -5+7i -5-7i; -1+1i -1-1i
