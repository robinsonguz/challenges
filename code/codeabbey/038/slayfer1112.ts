/*
$ eslint slayfer1112.ts && prettier --check slayfer1112.ts
$ tsc \
  --strict \
  --noImplicitAny \
  --noImplicitThis \
  --noUnusedLocals \
  --noUnusedParameters \
  --noImplicitReturns \
  --noFallthroughCasesInSwitch \
  slayfer1112.ts
*/

function isComplex(valBS: number, valAC: number): boolean {
  if (valBS < valAC) {
    return true;
  }
  return false;
}

function result(
  valB: number,
  valBS: number,
  valAC: number,
  valA: number,
  complex: boolean
): number {
  if (complex) {
    const valueA: number = Math.round(-valB / valA);
    const valueB: number = Math.round((valAC - valBS) ** (1 / 2) / valA);
    process.stdout.write(`${valueA}+${valueB}i `);
    process.stdout.write(`${valueA}-${valueB}i; `);
    return 1;
  }
  const valueA: number = Math.round(-valB + (valBS - valAC) ** (1 / 2) / valA);
  const valueB: number = Math.round(-valB - (valBS - valAC) ** (1 / 2) / valA);
  process.stdout.write(`${valueA} ${valueB}; `);
  return 1;
}

function solution(entry: number[]): number {
  const [valA, valB, valC]: number[] = entry;
  const valBS: number = valB * valB;
  const valAC: number = 2 * 2 * valA * valC;
  const valAS: number = 2 * valA;
  const complex: boolean = isComplex(valBS, valAC);
  return result(valB, valBS, valAC, valAS, complex);
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value.split(' ').map((val) => Number(val))));
    process.stdout.write('\n');
    return 1;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
-15 -20; -40 -49; 34 26; 4 -9; 5+4i 5-4i; 1+6i 1-6i;
-9+3i -9-3i; -8 -9; 66 62; 10+1i 10-1i; 73 67; 10+2i 10-2i;
*/
