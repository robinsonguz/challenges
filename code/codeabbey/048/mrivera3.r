# $ lintr::lint('mrivera3.r')


collatzseq <- function() {
  "finding the number of steps needed to come to 1"
  input <-  scan("DATA.lst")
  results <- c()
  for (i in 2:(input[1] + 1)) {
    counter <- 0
    x <-  input[i]
    while (x != 1) {
      if (x %% 2 == 0) {
        x <- x / 2; counter <- counter + 1;
      }
      else {
        x <- x * 3 + 1; counter <- counter + 1
      }
    }
    results <- c(results, counter)
  }
  return(results)
}

# $ Rscript mrivera3.R
#106   9  24  44  11 104  22  73  64  29  10  26  45  40  20 111 187
#126  23  70  11  45  21
