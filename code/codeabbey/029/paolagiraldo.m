%>> mlint('paolagiraldo.m')
%>>

data = textscan (stdin, '%s');
data = str2double (data{1}(2:end));
[sorted, indexes] = sort (data);

% cat DATA.lst | octave paolagiraldo.m
% indexes =
%
%   20   17    8    9   13   11   15    6   16    1    2   18
%   10    4    5   19   21   14    7   12    3
