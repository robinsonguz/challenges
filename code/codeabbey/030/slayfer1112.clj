;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (data 0)]
    [head]))

(defn solution [data]
  (let [text data
        val (str/reverse text)]
    (print val)))

(defn main []
  (let [[head] (get-data)]
    (solution head)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; aococ erehw ffo yhw flehs nwolc eraf
