/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var rev = data[0];
    print(rev.split('').reversed.join());
  }
  );
}

/* $ dart mrsossa.dart
 * sutcac tuoba ydrapoej flehs ffo reppus eraf no
*/
