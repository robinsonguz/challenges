/*
$ rustc alb4tr02.rs #compilation
$ cargo fmt   --all     -- --check alb4tr02.rs #linting
*/

use std::io;
fn main() {
  let mut word = String::new();
  io::stdin().read_line(&mut word).unwrap();
  let mut n = word.len() - 1;
  let mut done = true;
  while done {
    n = n - 1;
    print!("{}", word.remove(n));
    if n == 0 {
      done = false;
    }
  }
  println!("");
}

/*
$ cat DATA.lst | ./alb4tr02
$ sutcac tuoba ydrapoej flehs ffo reppus eraf no
*/
