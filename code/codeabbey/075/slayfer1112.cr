#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 5.74 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(String)
  data.each_line do |x|
    inter = [] of String
    (x.split).each do |y|
      inter << y
    end
    values << inter
  end
  values[1..]
end

def straight(array)
  result = "none"
  if array[0] == "1" && array[4] == "5"
    result = "small-straight"
  elsif array[0] == "2"
    result = "big-straight"
  end
  result
end

def dice_poker(pair, three, four, yacht, array)
  result = "none"
  if pair == 2
    result = "two-pairs"
  elsif pair == 1 && three == 1
    result = "full-house"
  else
    if pair == 1
      result = "pair"
    elsif three == 1
      result = "three"
    elsif four == 1
      result = "four"
    elsif yacht == 1
      result = "yacht"
    else
      result = straight(array)
    end
  end
  result
end

def solution(array)
  array = array.sort
  pair = 0
  three = 0
  four = 0
  yacht = 0
  dices = Hash(String,Int32).new
  array.each do |x|
    dices["#{x}"] = 0 if !dices.has_key?(x)
    dices[x] += 1
  end
  dices.each do |x|
    if x[1] == 2
      pair += 1
    elsif x[1] == 3
      three += 1
    elsif x[1] == 4
      four += 1
    elsif x[1] == 5
      yacht += 1
    end
  end
  result = dice_poker(pair, three, four, yacht, array)
  result = "none" if result == ""
  print "#{result} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# pair big-straight three big-straight two-pairs two-pairs
# two-pairs big-straight yacht pair two-pairs pair three
# full-house two-pairs yacht full-house small-straight
# big-straight yacht big-straight small-straight three
# big-straight none pair
