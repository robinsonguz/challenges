;; $ clj-kondo --lint andresclm.clj
;; linting took 23ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn get-range [init end]
  (->> end inc (range init) vec))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (vec (rest data'))]
    (vec (map (fn [line] (str/split line #" ")) data''))))

(defn fac
  ([n] (fac (bigint n) 1))
  ([n f]
   (if (<= n 1)
     f
     (recur (dec n) (* f n)))))

(defn calculate-an [S N B]
  (let [K-max (quot S B)
        summatory (get-range 0 K-max)]
    (reduce (fn [accumulator Ki] (let [c1 (Math/round (Math/pow -1 Ki))
                                       c2 (fac (+ (- S (* Ki B)) (- N 1)))
                                       c3 (fac (- S (* Ki B)))
                                       c4 (* (fac (- N Ki)) (fac Ki))
                                       c (quot (* c1 c2 N)
                                               (* (bigint c3) (bigint c4)))]
                                   (+ accumulator c)))
            0
            summatory)))

(defn count-luck [N B]
  (let [N-half (quot N 2)
        summatory (get-range 1 (- (* N-half (- B 1)) 1))]
    (reduce (fn [accumulator Si] (let [result (calculate-an Si N-half B)]
                                   (+ accumulator (* result result))))
            2
            summatory)))

(defn lucky-tickets [N B]
  (cond
    (= N 1) B
    (even? N) (count-luck N B)
    :else (* (count-luck N B) B)))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (map (fn [pair] (let [[N B] pair
                                     result (lucky-tickets
                                             (edn/read-string N)
                                             (edn/read-string B))]
                                 (str result)))
                    data)]
    (apply print result)))

;; $ clj -m andresclm
;; 25288120 184756 128912240 344 252 22899818176 58199208 40 8 8092 1231099425
;; 1132138107 44152809
