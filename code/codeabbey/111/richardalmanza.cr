#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.44 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def gcd_eucl(a : Int, b : Int)
  a, b = b, a if b > a

  until b == 0
    a, b = b, a % b
  end

  a
end

def e_phi(a : Int) : Int32
  ephi = 1

  (2...a).each { |x| ephi += 1 if gcd_eucl(a, x) == 1 }

  ephi
end

def polya_necklace_colored(n_beads : Int, n_colours : Int) : UInt64
  total = 0_u64

  d = Set(Int32).new
  (1..n_beads).each { |x| d << x if n_beads % x == 0 }

  d.each { |x| total += e_phi(n_beads // x) * n_colours.to_u64 ** x }

  total // n_beads
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map { |x| x.to_i }

(0...args.size).step(2).each do |x|
  print "#{polya_necklace_colored(args[x + 1], args[x])} "
end

puts

# $ ./richardalmanza.cr
# 834 956635 8230 45 2195 1119796 11
