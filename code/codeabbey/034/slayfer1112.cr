#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.48 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  a = array[0]
  b = array[1]
  c = array[2]
  d = array[3]

  min = 0
  max = 100

  sol = 1
  bool = true

  while bool
    xmiddle = (max+min)/2
    x = (a*xmiddle) + (b*(xmiddle**1.5)) - (c*Math.exp((-xmiddle)/50)) -d
    if x < 0
      min = xmiddle
    elsif (max-min).round(8) == 0
      sol = xmiddle
      bool = false
    else
      max = xmiddle
    end
  end

  print "#{sol} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 27.10789650591323 73.75654172719806 94.22122457399382 7.129944399639498
# 93.97400993839256 70.49472766084364 71.40361419733381 60.940205822407734
# 24.17851854552282
