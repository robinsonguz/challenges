#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args.delete_at 1

args[1..].each do |guess|
  point_spa_c = 0
  fame_spa_c = 0

  guess.each_char do |digit|
    if digit.in?(args[0])
      point_spa_c += 1
      fame_spa_c += 1 if guess.index(digit) == args[0].index(digit)
    end
  end

  point_spa_c -= fame_spa_c
  print "#{fame_spa_c}-#{point_spa_c} "
end

puts

# $ ./richardalmanza.cr
# 0-2 1-2 0-1 1-1 1-1 0-2 0-1 1-0 0-1 0-1
