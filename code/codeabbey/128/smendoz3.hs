-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (map (function . checkString) inputs))

checkString :: String -> [Integer]
checkString str = map read $ words str :: [Integer]

function :: [Integer] -> String
function arg = do
  let n = head arg
  let k = arg !! 1
  functionAux n k

functionAux :: Integer -> Integer -> String
functionAux n k = show (factorial n `div` (factorial k * factorial (n - k)))

factorial :: Integer -> Integer
factorial n = if n < 2 then 1 else n * factorial (n-1)

-- $ ./smendoz3
--   27883218168 20286591270 64276915527 6348337336 27883218168 23446881315
--   8996462475 31821795720
