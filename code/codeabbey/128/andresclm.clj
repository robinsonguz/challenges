;; $ clj-kondo --lint andresclm.clj
;; linting took 17ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-range [n]
  (->> n inc (range 1) vec))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")
        data'' (vec (rest data'))]
    (vec (map (fn [line] (str/split line #" ")) data''))))

(defn combination [n k]
  (let [temp (get-range k)]
    (reduce (fn [accumulator ki] (* accumulator (/ (- n (- ki 1))
                                                   ki)))
            1
            temp)))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (map (fn [n-k-pair] (let [[n k] n-k-pair
                                         result (combination (biginteger n)
                                                             (biginteger k))]
                                     (str result)))
                    data)]
    (apply print result)))

;; $ clj -m andresclm
;; 53060358690 11050084695 10639125640 28987537150 10235867928 35607051480
;; 22760723700 65033528560
