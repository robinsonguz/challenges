# frozen_string_literal: true

# $ rubocop neds.rb
# Inspecting 1 file
# 1 file inspected, no offenses detected

def read_data
  @data = []
  File.open('DATA.lst').each do |line|
    aux = line.split(' ')
    aux.each do |x|
      @data.push(x.to_i)
    end
  end
  @data
end

def find_factorial(number)
  @fact = 1
  i = 1
  while i < number
    @fact += i * @fact
    i += 1
  end
  @fact
end

data = read_data
out = ''
i = 1
while i < data.length
  nf = find_factorial(data[i])
  kf = find_factorial(data[i + 1])
  n_kf = find_factorial(data[i] - data[i + 1])
  comb = nf / (kf * n_kf)
  out += comb.to_s + ' '
  i += 2
end
puts out

# $ ruby neds.rb
# 1 6 10
