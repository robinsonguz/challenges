#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 5.15 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(String)
  data.each_line do |x|
    inter = [] of String
    (x.split).each do |y|
      inter << y
    end
    values << inter
  end
  values[1..]
end

def base36(j)
  array = [] of String
  0.step(to: 35, by: 1) do |x|
    array << (x.to_s(base: 36))
  end
  array[j]
end

def comb(n, k)
  ret = 1.0
  if k > n/2
    k = n - k
  end
  0.step(to: k-1, by: 1) do |i|
    ret *= (n-i)/(k-i)
  end
  ret.round(0)
end

def solution(array)
  n = Int64.new(array[0])
  k = Int64.new(array[1])
  i = Int64.new(array[2])

  c = ""
  counter = 0

  0.step(to: k-1, by: 1) do |x|
    while comb(n-counter-1, k-x-1) <= i
      i -= comb(n-counter-1, k-x-1)
      counter += 1
    end
    c += base36(counter)
    counter += 1
  end

  print "#{c.upcase} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 135ACEFHJKLMNOPQRV 369AEKOSTUVXZ 123457ABCDFGHJKLMN 034567ABCDE 1345689ACE
# 356 34678BCHPQRUVY 123456789ABDE 15789 245679HILNSVWXY 13479DEGHLMNQS 5
# 25CFJMORUVWYZ 13456789BCFGHKPQTUVXY 1349AFGLNRVWXY 2469ACDEHJLNO 367AEGHLMNOX
# 13567ACDEFGHIJMNOQ 0145689ACEFGHIJ 19ADFGHIJLOQRUV
