;; $ clj-kondo --lint slayfer1112.clj
;; linting took 40ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-vector [data res i]
  (if (< i (count data))
    (get-vector data (conj res (str/split (data i) #"")) (inc i))
    res))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        data-n (get-vector data [] 0)]
    [data-n]))

(defn get-x-y [data counter-i counter-j xy]
  (if (< counter-i 5)
    (if (< counter-j 7)
      (if (= ((data counter-i) counter-j) "X")
        (get-x-y data counter-i (inc counter-j)
                 (conj xy [(inc counter-j) (inc counter-i)]))
        (get-x-y data counter-i (inc counter-j) xy))
      (get-x-y data (inc counter-i) 0 xy))
    xy))

(defn neighboors [[x y]]
  (for [dx [-1 0 1]
        dy [-1 0 1]
        :when (not (= dx dy 0))]
    [(+ x dx) (+ y dy)]))

(defn should-be-alive [frq is-alive]
  (or (= frq 3)
      (and (= frq 2) is-alive)))

(defn step [board]
  (->> board
       (mapcat neighboors)
       frequencies
       (filter (fn [[cell frq]] (should-be-alive frq (board cell))))
       (map first)
       set))

(defn m-board [board]
  (->> board
       (iterate step)
       (take 6)
       (map vec)
       vec))

(defn solution [data]
  (let [x-y (get-x-y data 0 0 [])
        board (set x-y)
        steps (drop 1 (m-board board))]
    (doseq [x steps]
      (print (str (count x) " ")))))

(defn main []
  (let [[data] (get-data)]
    (solution data)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 14 11 12 13 13
