open System

[<EntryPoint>]
let main argv =
    let mutable sum = 0
    printfn "ingrese la cantidad de numeros a sumar:"
    let c = stdin.ReadLine()
    printfn "ingrese %s numeros separados por un espacio:" c
    let numbers = stdin.ReadLine().Split ' ' //array de string
    let arraynumbers = [| for i in 0 .. (int c)-1 -> int numbers.[i] |]
    for i in 0 .. arraynumbers.Length - 1 do
      sum <- sum + arraynumbers.[i]
    printfn "%i" sum
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
