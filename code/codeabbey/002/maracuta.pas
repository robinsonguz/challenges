program maracutasumloop; {Nombre del programa}


var
  Num, control, x, res: integer; {Declaro variables como enteras}

begin

  {Inicializo variables para prevenir error de compilacion}
  Num := 0;
  res := 0;
  control := 0;

  {Solicito variable de control}
  writeLn('Introduzca la variable de control: ');
  readLn(control);

  for x := 1 to control do
  begin
    {Solicito valor}
    writeLn('Introduzca el valor: ');
    readLn(Num);
    res:=res+Num;
  end;


  {Imprimo}
  writeLn(res);
  readLn();

end.
