/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <stdio.h>
#define MAX_SIZE 1024
#define M_PI acos(-1.0)
#define R pow(2.0,(1.0/12.0))
#define FREQ 261.63

/*struct of Notes*/
typedef struct Note {
  char name[MAX_SIZE];
  int d;
  int octave;
  double hz;
} Note;

/*this functions works similar to strncpy but adding '\0' in the end*/
static void my_own_strncpy (/*@reldef@*/ char *array,
  /*@reldef@*/ char *replace, /*@reldef@*/ int size)
  /*@modifies *array@*/ /*@requires maxSet(array) >= size@*/ {
  int i = 0;
  for (i = 0; i < size; i++) {
    array[i] = replace[i];
  }
  array[size] = '\0';
}

/*function to set all notes in order*/
static void set_notes (/*@reldef@*/ char notes_arr[][3])
  /*@modifies notes_arr[][3]@*/ {
  my_own_strncpy(notes_arr[0], "C\0", 2);
  my_own_strncpy(notes_arr[1], "C#", 2);
  my_own_strncpy(notes_arr[2], "D\0", 2);
  my_own_strncpy(notes_arr[3], "D#", 2);
  my_own_strncpy(notes_arr[4], "E\0", 2);
  my_own_strncpy(notes_arr[5], "F\0", 2);
  my_own_strncpy(notes_arr[6], "F#", 2);
  my_own_strncpy(notes_arr[7], "G\0", 2);
  my_own_strncpy(notes_arr[8], "G#", 2);
  my_own_strncpy(notes_arr[9], "A\0", 2);
  my_own_strncpy(notes_arr[10], "A#", 2);
  my_own_strncpy(notes_arr[11], "B\0", 2);
}

/*function to set the frequencies in hz for each test case*/
static void set_frequencies(/*@in@*/ Note notes[], /*@reldef@*/ int size)
  /*@modifies notes[]@*/ /*@requires maxSet(notes) >= 0@*/ {
  int i = 0;
  double d;
  for (i = 0; i < size; i++) {
    d = (double) notes[i].d + (12.0 * (notes[i].octave - 4.0));
    notes[i].hz = FREQ * pow(R, d);
  }
}

/*function that return the distance beetwen any element and the first*/
static int index_of(/*@in@*/ char comparator[], /*@in@*/ char array[][3])
  /*@modifies nothing@*/ {
  int i = 0;
  for (i = 0; i < 12; i++) {
    if (strcmp(comparator,array[i]) == 0) {
      return i;
    }
  }
  return 0;
}

/*function to print frequencies of notes*/
static int show_notes (/*@in@*/ Note notes[], /*@reldef@*/ int size)
  /*@modifies nothing@*/ {
  int i = 0;
  for (i = 0; i < size; i++) {
    printf("%.0f ", notes[i].hz);
  }
  printf("\n");
  return 1;
}

int main(void) {
  static char notes_arr[12][3];
  char temp[MAX_SIZE], temp2[255], tmp = ' ';
  int i = 0, j = 0, k = 0, cases = 0, octave = 0, len = 0;
  static Note notes[MAX_SIZE];
  set_notes(notes_arr);
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  cases = atoi(temp);
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  while ((int) tmp != 10) {
    tmp = temp[i];
    if ((int) tmp <= 32) {
      temp2[j+1] = '\0';
      if ((int) strlen(temp2) <= 1024) { len = (int) strlen(temp2); }
      my_own_strncpy(notes[k].name, temp2, len);
      notes[k].octave = octave;
      notes[k].d = index_of(notes[k].name, notes_arr);
      temp2[0] = '\0', temp2[1] = '\0', temp2[2] = '\0';
      k++, j = 0;
    }
    else if ((int) tmp > 48 && (int) tmp < 57) {
      octave = (int) tmp - 48;
    }
    else { temp2[j] = tmp, j++; }
    i++;
  }
  set_frequencies(notes, cases);
  return show_notes(notes, cases);
}

/*
$ cat DATA.lst | ./slayfer1112
46 740 52 33 65 880 523 139 622 554 62 123 35 233 698 262 208 82 494 466 659
*/
