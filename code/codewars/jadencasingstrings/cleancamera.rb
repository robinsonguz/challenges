# frozen_string_literal: true

# rubocop cleancamera.rb
# 1 file inspected, no offenses detected

def titleize(str)
  str.split.map(&:capitalize).join(' ')
end

File.open('DATA.lst', 'r') do |lines|
  while (line = lines.gets)
    puts(titleize(line))
  end
end

# ruby cleancamera.rb
# OutPut:
# How Can Mirrors Be Real If Our Eyes Aren't Real
