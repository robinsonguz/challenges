## Version 2.0
## language: en

Feature: dont-use-client-side - web - picoCTF
  Code:
    dont-use-client-side
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04           |
    | Firefox         | 70.0.1 (64-bit) |
  Machine information:
    Given I access to the challenge via browser
    When I enter to the page I see little form
    And I see the title "Enter a valid credentials to proceed"
    Then I can assume the challenge consist in validation in the client side

  Scenario: Success:Inspect the Source Code
    When I press ctrl + U
    Then I see a field named as "md5.js"
    When I click on it
    Then It redirects me to a screen that shows "Not found"
    When I go back to the source page
    Then I see below a function called "verify"
    And that function it takes the value of an element with the id "pass"
    And save it in a string variable called "checkpass"
    When I see the validation method:
    """
    if (checkpass.substring(split, split*2) == 'CTF{') {
      if (checkpass.substring(split*4, split*5) == 'ts_p') {
          if (checkpass.substring(split*3, split*4) == 'lien') {
            if (checkpass.substring(split*5, split*6) == 'lz_e') {
              if (checkpass.substring(split*2, split*3) == 'no_c') {
                if (checkpass.substring(split*7, split*8) == '4})'{
                  if (checkpass.substring(0, split) == 'pico') {
                    if (checkpass.substring(split*6, split*7) == 'e2f2') {
                      }
                    }
                  }
                }
              }
            }
          }
        }
    """
    Then I see the first statement, I notice it starts by "0,split"
    And I notice the variable "split" appears to be multiplying a number
    When I notice the number seems to follow an incremental sequence
    Then I see that it is possible sort the "if" statements in increase way
    And now I capture the flag and  solve the challenge
