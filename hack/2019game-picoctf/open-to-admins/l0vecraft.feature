## Version 2.0
## language: en

Feature: Open-to-admins - web - picoCTF
  Code:
    Open-to-admins
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>           |
    | Ubuntu             | 18.04               |
    | FireFox            | 72.0.1(64 bits)     |
    | BurpSuite Community| 2.1.07 (64 bits)    |
  Machine information:
    Given I access to the challenge via browser
    When I read the description of the challenge
    And It tells me some conditions the cookie needs to have
    Then I can assume the challenge consist in modification of the cookie params

  Scenario: Failed: Add wrong params
    When I press the button Flag
    Then I intercept the request to the directory "flag" with BurpSuite
    And now I Can modify and add extra params to the cookies
    When I write this params
    """
    cookie: admin=1;time=1400
    """
    Then I get a file with some default configurations but no flag

  Scenario: Success: Add correct params
    When I press the button Flag
    Then I intercept the request to the directory "flag" with BurpSuite
    And now I Can modify and add extra params to the cookies
    When I write this params
    """
    cookie: admin=True;time=1400
    """
    Then then flag appear to me in the screen
    And now I can resolve the challenge
