## Version 2.0
## language: en

Feature: JWT-Scratchpad - web - picoCTF
  Code:
    JWT-Scratchpad
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>              |
    | Ubuntu             | 18.04                  |
    | FireFox            | 72.0.1(64 bits)        |
    | BurpSuite Community| 2.1.07 (64 bits)       |
    | jwt2jrt            | 0.0.1                  |
    | John The Ripper    | 1.9.0                  |
  Machine information:
    When I enter to the challenge I see the title with "jwt" and a form field
    Then I read the description, It says I can enter any username
    And It recommends me to use "john" as username
    And I assume the challenge consist in something in realtion with JWT

  Scenario: Fail: Using 'John' as username
    When I enter the username "john"
    Then I intercept the request and notice a param "jwt" with a token
    And I arrive to the home page with the scratchpad

  Scenario: Fail: Using 'admin' as username
    When I enter the username "admin"
    Then I intercept the request and notice a param "jwt" with a token
    And I arrive to the home page but I get a message
    And It says that I can login as special user

  Scenario: Fail: Decode the JWT
    When I login with a normal username, I can intercept the request
    Then I take the token and use a online JWT decoder
    And I notice the user name that I use to login
    And I change for "admin"
    When I try to login with a normal username
    Then I change the JWT
    And the application show me a error

  Scenario: Succes: Decode and cracking the JWT
    When I take the the JWT
    Then I use the script "jwt2jtr" to generate a token for "John the ripper"
    """
    python jwt2jtr.py <ACTUAL-TOKEN> >>hash.txt
    """
    When I use the new token with "Jhon the ripper"
    """
    ./run/john --wordlist=rockyou.txt hash.txt
    """
    Then It crack the "secret-string" of the JWT
    """
    ilovepico
    """
    When I change the username in the decoder
    Then I use the "secret" for the correct encode
    When I enter a new username in the webapplication
    Then I intercept the request and change the token for the new encoded
    And the webapplication shows me the flag
    And now I can solve the challenge
