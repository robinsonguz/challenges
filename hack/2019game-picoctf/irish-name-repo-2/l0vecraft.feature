## Version 2.0
## language: en

Feature: Irish-Name-Repo-2 - web - picoCTF
  Code:
    Irish-Name-Repo-2
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>              |
    | Ubuntu             | 18.04                  |
    | FireFox            | 72.0.1(64 bits)        |
    | BurpSuite Community| 2.1.07 (64 bits)       |
  Machine information:
    When I enter to the challenge I see the same page from another challenge
    Then I see the same menu
    And I enter to the admin login option, I see a little form
    And I assume the challenge consist in bypass this login

  Scenario: Fail: Trying a simple Bypass
    When I enter in the platform, I go to the support page
    Then I see a few usernames like "Anna" and "admin"
    When I enter in the login page
    Then I try a simple bypass with the username "admin"
    """
    admin'OR 1=1 --
    """
    And I hit enter
    And the webpage shows me the message "SQLI detected"

  Scenario: Success: Intercepting the requests
    When I write the credentials
    """
    username=admin&password=1234
    """
    Then I intercept the request with BurpSuite
    When I inspect the body of the request I see another param
    """
    username=admin&password=1234&debug=0
    """
    Then I change the value of the "debug" param to one
    And the webpage shows me the SQL query
    """
    username: admin'--
    password: 1234
    SQL query: SELECT * FROM users WHERE name='admin' AND password='1234'
    """
    When I analyze the query I notice that I can break the query adding "'--'"
    Then I modify the "username" param to
    """
    username=admin'--
    """
    And the webpage shows me the flag
    And now I can solve the challenge
