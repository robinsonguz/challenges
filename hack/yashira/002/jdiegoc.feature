## Version 2.0
## language: en

Feature: 002 Hash-1 yashira
  Site:
    yashira
  User:
    Jdiegoc (yashira)
  Goal:
    Decrypting a MD4 hash with hashcat

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2019.4      |
    | Chrome          | 79.0.3945.88|
    | hashcat         | 4.0.1       |
  Machine information:
    Given an MD4 hash
    When I identify what a MD4 hash
    Then I learn to decipher it
    And decrypting

  Scenario: Success: hashcat
    Given the MD4 hash
    When I used hashcat
    Then I saw that it was with MD4
    And I run it
    Then I took the exit
    """
    Found : <flag>
    """
    And I solve the challenge
