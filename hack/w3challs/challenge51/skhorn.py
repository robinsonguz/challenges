#!/usr/bin/env python
# pylint: disable=no-else-return
# pylint: disable=unused-variable
"""
$ pylint skhorn.py
No config file found, using default configuration
Your code has been rated at 10.00/10 (previous run: 8.49/10, +1.51)
$ chmod +x skhorn.py
$
"""
import time
from bs4 import BeautifulSoup
import requests


URL = [
    "https://w3challs.com/challs/Crypto/asy",
    "https://w3challs.com/challs/Crypto/asy/index.php?p=solution"
]

COOKIES = {
    '__utma': '265617161.806190679.1526174915.1526174915.1526174915.1',
    '__utmb': '265617161.2.10.1526174915',
    '__utmc': '265617161',
    '__utmt': '1',
    'lang': 'en',
    'nick': 'Skhorn',
    'session': 'c6cc39b1f098e36b76a890e7729edd4922048229',
    'PHPSESSID': 'rre5djcm8j3d7nh4euc13nhhr1',
}


def egcd(aval, bval):
    """
    Extended Euclidean greatest
    common divisor
    """
    if aval == 0:
        return (bval, 0, 1)
    else:
        gval, yval, xval = egcd(bval % aval, aval)
        return (gval, xval - (bval // aval) * yval, yval)


def modinv(aval, mval):
    """"
    ModInv function
    """
    gval, xval, yval = egcd(aval, mval)
    if gval != 1:
        raise Exception('modular inverse does not exist')
    else:
        return xval % mval


def search_in_html(response, tag):
    """
    Search in the html sent as response, via param
    Just localize by a tag
    """
    soup_parser = BeautifulSoup(response, 'html.parser')

    return soup_parser.findAll(tag)


def main():
    """
    Solving Asymmetric Encryption
    PHP Cipher implementation
    https://w3challs.com/challs/Crypto/asy/implementation.php
    Init values
    ----- No clue, no easy way to obtain them
    $ONE = gmp_init(1,10);

    $a = gmp_random(3);
    $b = gmp_random(3);
    $c = gmp_random(3);
    $d = gmp_random(3);

    To summarize
    e = a*b - 1
    f = c*e + a;                // Public key
    g = d*e + b;                // Private key
    h = c*d*e + a*d + c*b + 1;  // Public key

    Encryption
    ciphertext = (plaintext * f) % h

    Decryption
    plaintext = (ciphertext * g) % h

    We need to know how to calculate G, so, we
    can decrypt the ciphertext and get the plaintext
    How? Modular Arithmetic, pure fucking black magic
    By using modular properties, there seems to be
    no way to get something, but by using
    the modular inverse, eg

    modInv(h, f), we can get G! that's all!
    """
    time0 = time.time()
    session = requests.Session()

    response = session.\
        get(URL[0], cookies=COOKIES)

    response = response.text

    html_search = search_in_html(response, 'textarea')
    textarea_1 = html_search[0].text.split('\n')
    textarea_2 = html_search[1].text.split('\n')

    print "\n[+] Extracting Public Keys & Ciphertext from URL: {0}".format(
        URL[0])
    public_key_h = textarea_1[3].split(' ')[2]
    print "[+] -- Public Key (H): \n{0}".format(public_key_h)
    public_key_f = textarea_1[5].split(' ')[2]
    print "[+] -- Public Key (F): \n{0}".format(public_key_f)

    ciphertext = textarea_2[3]
    print "[+] -- Ciphertext (C): \n{0}".format(ciphertext)
    private_key = modinv(int(public_key_f), int(public_key_h))

    print "[+] Recovering Plaintext: G = inverse_mod(F, H)"
    print "[+] -- Private Key (G): \n{0}".format(private_key)

    print "[+] Recovering Plaintext: (cip * priv) % H"

    plaintext = (int(ciphertext) * int(private_key)) % int(public_key_h)

    print "[+] -- Plaintext (P): \n{0}".format(plaintext)

    print "[+] Verifying..."
    print"[+] Ciphering: (msg * F) % H\n"

    cipher = (int(plaintext) * int(public_key_f)) % int(public_key_h)

    print "[+] Ciphertext Extracted (C0): \n{0}\n".format(ciphertext)
    print "[+] Ciphertext Generated (C1): \n{0}\n".format(cipher)
    print "[+] ++ Comparisson C0 == C1: \n{0}\n".format(
        int(ciphertext) == int(cipher))

    response = requests.post(URL[1],
                             cookies=COOKIES,
                             data={"mess": plaintext})

    time1 = time.time()
    print "[+] ++ Total time: {0}".format(time1-time0)
    print response.content


main()
