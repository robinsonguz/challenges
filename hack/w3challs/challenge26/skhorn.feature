# language: en

Feature: Solve challenge 26
  From site W3Challs
  From Hacking Category
  With my username Skhorn
 
Background:
  Given the fact i have an account on W3Challs site
  And i have Debian 9 as Operating System
  And i have internet access
  And i have opened the resources 
  
Scenario: Succesful attempt
  Given the link to the challenge
  And a statement that the password is longer than 7 characters
  And a web page that simulates a windows xp interface
  And the only available section to insert text is the folder lookup C:\
  And a text stating that the challenge is divided in two parts
  And the first part is about finding a directory which contains logs
  And the second part is about getting the password from those logs
  And another clue stating it is a case sensitive answer
  And i look on the forums for a clue
  And i see i have to find SAM
  And SAM states for Security Account Manager
  And it has an specific path on windows
  And i write the specific path on the search bar
  And a congratulations message appear
  And a zip file to be downloaded
  When i extract the content of the file
  And i see it is MS Windows registry file
  And by knowing that SAM files contains user information such as passwords hashes
  And i used a tool called samdump2 to extract that sensitive information
  And i search about how it is the structure of that registry
  And i see it is <user:user-code:lm-hash:nt-hash>
  And i pick the lm-hash 
  And use an online tool to decrypt it
  And i get a mix of characters with numbers in a readable word all in uppercase
  And i pick the nt-hash
  And use the same online tool to decrypt it
  And i get the same answer as before but not all in uppercase
  When i use the last one as solution
  Then i solve the challenge
