## Version 2.0
## language: en

Feature: CHALLENGE-Ringzer0ctf
  Site:
    Ringzer0ctf
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag decoding the message.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
    | NodeJS          | 13.6.0        |
  Machine information:
    Given I am accessing to the website
    Then I choose a challenge
    And I was redirect to a login form

  Scenario: Fail:decode-base64
    Given I'm on the challenge page
    And I see a message with the instructions
    """
    You have 3 seconds to get the secret word.
    Send the answer back using
    https://ringzer0ctf.com/challenges/15/[your_string]

    ----- BEGIN Elf Message -----
    <A very very long encode string>
    ----- End Elf Message -----

    ----- BEGIN Checksum -----
    <A MD5 chechsum>
    ----- END Checksum -----
    """
    When I see the challenge
    Then I copy the crypted message
    And I try to use "atob()" in the console
    When I type "atob('<A very very long encode string>')"
    Then I receive the output "<Antoher very long string>"
    And I think that this can't be the secret word
    When I see that I try to decode until I get the final decoding
    Then I try to make with Javascript in console
    """
    elf = (document.getElementsByClassName("message")[0].innerText);
    var regex = /-+\s*(begin|end)\s+(elf\s+message|checksum)\s*-+/ig;
    elf = elf.replace(regex, '').trim();

    while (/^[a-z\d+/=]+$/i.test(elf)) {
      elf = atob(elf).toString();
    }
    """
    And I receive a very long string [evidence](img1)
    When I think that string can be a Binary file
    Then I think that "Binary" can be the secret word
    And I try to send "Binary" in the URL
    """
    https://ringzer0ctf.com/challenges/15/Binary
    """
    When I see an alert message that says
    """
    Wrong answer or too slow!
    """
    And I fail to get the password

  Scenario: Success:Make-a-binary-file-and-execute
    Given I'm on the challenge page
    Then I need to make the decode faster
    And Put in the URL before 3 seconds
    When I think a way to make this I think that I can use javascript in node
    Then I think that I can use the module "puppeteer" for this challenge
    And I make a script in NodeJS that solves the challenge
    """
    const puppeteer = require('puppeteer');
    const fisys = require('fs');
    const exec = require('child_process').execSync;

    async function challenge() {
      const browser = await puppeteer.launch();

      const page = await browser.newPage();
      await page.goto(
        'https://ringzer0ctf.com/challenges/15/', { waitUntil: 'load' });

      page.evaluate(() => {
        document.getElementsByName('username')[0].value = '<username>';
        document.getElementsByName('password')[0].value = '<password>';
        document.getElementsByTagName('form')[0].submit();
      });

      await page.waitFor('.message');

      let [ elf ] = await page.evaluate(() =>
        [ (document.getElementsByClassName('message')[0].innerText) ]);

      let [ checksum ] = await page.evaluate(() =>
        [ (document.getElementsByClassName('message')[1].innerText) ]);

      const regex = /-+\s*(begin|end)\s+(elf\s+message|checksum)\s*-+/ig;

      // The exercise say that the key have 10 chars
      //  and the flag was encoded in base64
      elf = elf.replace(regex, '').trim();
      checksum = checksum.replace(regex, '').trim();

      while (/^[a-z\d+/=]+$/i.test(elf)) {
        elf = Buffer.from(elf, 'base64').toString('binary');
      }

      elf = Buffer.from(elf, 'binary').reverse();
      fisys.writeFileSync('./payload.bin', elf);

      let resultubuntu = exec('ubuntu run ./payload.bin');
      resultubuntu = resultubuntu.toString().trim();

      let checksumubuntu = exec('ubuntu run md5sum ./payload.bin');
      checksumubuntu =
        checksumubuntu.toString().trim().replace('  ./payload.bin', '');

      const url = `https://ringzer0ctf.com/challenges/15/${ resultubuntu }`;

      await page.goto(url);
      const flag = await page.evaluate(() =>
        document.getElementsByClassName('alert alert-info')[0].innerText);

      // Validate that the file have the same checksum that the page say
      if (checksumubuntu === checksum) {
        process.stdout.write(`${ '\nThe secret word is: ' }${ resultubuntu }
        \nThe URL is: ${ url }\n\nThe FLAG is: ${ flag }`);
        exec('del /f payload.bin');
      } else {
        process.stdout.write(`File is corrupted. \n${ checksumubuntu }
        \n${ checksum }`);
        exec('del /f payload.bin');
      }
    }

    challenge();

    /*
    $ node slayfer1112.js

    The secret word is: 7scoW9

    The URL is: https://ringzer0ctf.com/challenges/15/7scoW9

    The FLAG is: <flag>
    */
    """
    And My script makes the following
    """
    The script login in the website, next take the encoded string and the
    checksum and decode the string with base64 and later create a binary file,
    then execute the binary and get the result, next the script eval the
    checksum of the file created with the checksum in the challenge and
    print the result if the checksums match, then use the result in the URL
    and eval the response to get the flag.

    You can run it with:

    node slayfer1112.js
    """
    When I get the flag
    Then I copy the flag and paste it in the submit flag form
    When I submit the form
    Then I receive a message that says "Good job! You got +4 points"
    And I found the flag
