## Version 2.0
## language: en

Feature: Crypto-Is-Not-As-Good-As-You-Think
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>         |
  | Kali Linux      | 5.5.0-kali1-amd64 |
  | Firefox         | 68.7.0esr         |
  | Python3         | 3.8.2             |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/105
    """
    When I open it in a browser
    Then I see a message
    """
    Your dont have admin privileges. Sorry no flag for you.
    """
    And a button to download a ZIP file
    When I extract the files
    Then I get a TXT file with the source code of the page in PHP

  Scenario: Success: Break-OFB-Encryption
    Given that the URL has a "token" parameter
    Then I realize that I have to supply the correct token to get the flag
    When I analyze the PHP code of the page
    """
    CryptToken::GetInstance()->Crypt('user=anonymous|ts=' . (time() + 10)));
    ...
    ...
    ...
    public function AdminCheck() {
      if($this->context->user == 'admin') {
        return true;
      }
      return false;
    }
    """
    Then I see the structure of the token in plaintext
    And I realize I have to forge my own using the user "admin"
    And a valid timestamp
    When I analyze the encryption process
    """
    public function Crypt($buffer) {
      return
      base64_encode(
          mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
              pack('H*', substr($this->crypt->key, 0, 64)),
              $buffer,
              MCRYPT_MODE_OFB,
              pack('H*', substr($this->crypt->key, -64))));
    }
    """
    And investigate what those settings imply
    """
    https://www.php.net/manual/es/function.mcrypt-encrypt.php
    https://www.php.net/manual/en/mcrypt.constants.php
    https://crypto.stackexchange.com/questions/75100/how-does-reusing-iv-
    for-ofb-mode-breaks-the-scheme
    """
    Then I notice that the initialization vector is the same each time
    Given the IV is constant
    And a basic understanding of OFB (Output Feedback Block)
    Then I realize the key used to encrypt is always the same
    And the process just does a bitwise XOR of the key with the plaintext
    And having a pair plaintext-ciphertext allows me to get the key
    When I build a script to obtain the key [exploit.py]
    Then I can encrypt the message that I want
    When I input the obtained token in the URL
    Then I am greeted as admin
    And I get the flag
