## Version 2.0
## language: en

Feature: ringzeroctf
  Site:
    ringzeroctf
  User:
    RichSphinx (wechall)
  Goal:
    get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Parrot OS       | 5.1.0       |
    | Firefox         | 66.0.5      |
    | SSH             | OpenSSH_7.9p|
  Machine information:
    Given I am accessing ringzer0ctf machine
    And it's running Ubuntu-4.15.0-45
    And it's running OpenSSH_7.2p2
    And I can see the challenge

  Scenario: Fail:cat passwd file
    Given I'm logged in the machine
    And I tried to cat the passwd file
    Then I can see that the file display trinity user
    Then I try to open the file
    And I got a permission error
    And I could not capture the flag

  Scenario: Success: grep trinity in the /etc/ folder
    Given I'm logged in the machine
    And I checked the user 'trinity' to be in the system
    Then I can grep the username in the etc folder
    Then I see the user password was hardcoded in a backup script
    Then I got the password (flag)
    And I found the flag
    And I solved the challenge
