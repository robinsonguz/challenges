## Version 2.0
## language: en

Feature: scooby-doo -web -ctftime
  Site:
    ctflearn.com
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: modify div container
    Given I am in the challenge page
    When I inspect the elements of the page
    Then I can see the "div" container with "id" equal to "animationContainer"
    """
    <div id="animationContainer">
        <button onmouseover="mouseOver()" onclick="mouseClick()"
         id="button" style="top: 93px; left: 154px;">
            Click Me!
        </button>
    </div>
    """
    And I change "onmouseover='mouseOver()'" to "onmouseover=''"
    And I can click the button
    And I can not get the flag

  Scenario: Success: modify the opacity variable
    When I see the "div" container with "id" equal to "flagContainer"
    """
    <div id="flagContainer">
      <img class="letter" src="a.png" style="opacity: 0; left:860px;">
      <img class="letter" src="b.png" style="opacity: 0.2; top: 5px;
       left:240px;">
      <img class="letter" src="c.png" style="opacity: 0; top: 5px;
       left:820px;">
      <img class="letter" src="d.png" style="opacity: 0; top: 5px;
      left:740px;">
      <img class="letter" src="e.png" style="opacity: 0; left:780px;">
      <img class="letter" src="f.png" style="opacity: 0; top: 10px;
      left:530px;">
      <img class="letter" src="g.png" style="top: 10px; left:20px;">
      <img class="letter" src="h.png" style="opacity: 0; left:570px;">
      <img class="letter" src="i.png" style="opacity: 0; left:440px;">
      <img class="letter" src="j.png" style="opacity: 0.3; top: 5px;
       left:200px;">
      <img class="letter" src="k.png" style="opacity: 0; left:700px;">
      <img class="letter" src="l.png" style="opacity: 0.4; top: 10px;
      left:160px;">
      <img class="letter" src="m.png" style="opacity: 0.8; left:50px;">
      <img class="letter" src="n.png" style="opacity: 0.05; top: 10px;
      left:320px;">
      <img class="letter" src="o.png" style="opacity: 0.6; top: 20px;
       left:90px;">
      <img class="letter" src="p.png" style="opacity: 0; top: 10px;
      left:660px;">
      <img class="letter" src="q.png" style="opacity: 0; top: -5px;
       left:610px;">
      <img class="letter" src="r.png" style="opacity: 0; top: 5px;
       left:400px;">
      <img class="letter" src="s.png" style="opacity: 0.5; top: 5px;
       left:120px;">
      <img class="letter" src="t.png" style="opacity: 0.1; left:280px;">
      <img class="letter" src="u.png" style="opacity: 0; left:360px;">
      <img class="letter" src="v.png" style="opacity: 0; top: 5px;
       left:480px;">
    </div>
    """
    Then I can delete the "opacity" values
    And I get the flag [evidence](img.png)
