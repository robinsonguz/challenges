# language: en

Feature: Solve the challenge Pong!
  From the happy-security.de website
  From the Steganographie category
  With my username raballestas

  Background:
    Given a GIF image

  Scenario: Successful solution
    When I download the GIF
    And open it in the hex editor bless
    Then I find the password in the image
    And I solve the challenge
