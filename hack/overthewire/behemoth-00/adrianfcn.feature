## Version 2.0
## language: en

Feature: behemoth00 - reverse-engineering - overthewire
  Site:
    http://overthewire.org/wargames/behemoth/
  User:
    adrianC (wechall)
  Goal:
    Get the flag to level 0 of behemoth

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | ssh             | openssh7.9p1|
    | gdb             | 7.12.0      |
  Machine information:
    Given I am accessing the server
    And SSH with ssh behemoth0@behemoth.labs.overthewire.org -p 2221
    And enter a console as the user behemoth0
    And SSH Version openssh7.9p1
    And server is running on Linux behemoth with kernel 4.18.12

  Scenario: Fail: Executable file with password
    Given the data for the level can they be found in "/behemoth/"
    And I use "cd /behemoth" to move in the directory
    And I use the command "ls -lash" in the current directory
    And I see that the current directory has 7 files
    And I know that I have only permissions for the file "behemoth0"
    And I try to know what kind of file it is
    When I use the command "file behemoth0"
    Then I discover that "behemoth0" is a executable file
    When I execute the file "behemoth0"
    Then it asks me for a password
    When I write a random password
    Then the program response me with access denied

  Scenario: Success: Debugging a file with gdb
    Given that executable file "behemoth0" has a password
    And I run gdb on the console to debug the file "behemoth0"
    And I can disassemble a frame of data that I selected
    When I use the command "disassemble main"
    Then I can look the Dump of assembler code for function main
    And I search the sentences that contain "puts@plt" and "strcmp@plt"
    And I save the previous memory location for each one of sentences
    And I created a break-point for each memory location saved previously
    When I use "break memory-location"
    Then break-point is created in these
    And I use "run" to execute the file "behemoth0"
    And stops the flow of the program at each break-point it finds
    And I use "n" for continue of flow until the next break-point
    And I can watch all register value in each break-point
    When I use "info registers"
    Then I can watch all registers
    And I can print register value of each one
    When I use "x/1s register-name"
    Then I can watch register value
    And I search which can be the password value correct
    And I copy the values in a separate file
    And I executed "./behemoth0" until I enter the correct password value
    And I get and shell as "behemoth1"
    When I use "cat /etc/behemoth_pass/behemoth1"
    Then I caught the flag for the level 0
