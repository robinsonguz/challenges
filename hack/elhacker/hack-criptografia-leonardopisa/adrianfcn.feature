## Version 2.0
## language: en

Feature: LeonardoPisa - criptografia - elhacker
  Site:
    http://warzone.elhacker.net
  User:
    adrianC (Wechall)
  Goal:
    Decrypt the message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have access to "http://warzone.elhacker.net/cripto/f.php"
    """
    matienymustianbastentiqueabmusolrirstuvnqueilscontresccamorquesnnuncaabulso
    ksqeucnslsiaa
    """
    And I know that Leonardo Pisa discovered the succession Fibonacci
    When I only use letters in the position of a number in the succession
    """
      1 : m
      2 : a
      3 : t
      5 : e
      8 : m
      13 : a
      21 : t
      34 : i
      55 : c
      89 : a
    """
    Then the message was decrypted
    """
    matematica
    """
