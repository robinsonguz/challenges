Feature: Solve EKRPAT challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Understanding the challenge
  Given Two images of diferent keyboard distribution
  And A cyphered message
  When I search in wikipedia for keyboard distribution
  And I find "DVORAK" and "HCESAR" distribution
  When I compare the image of the keyboards
  And I realize they match the images of the challenge
  Then I realize I have to decode from DVORAK and HCESAR

Scenario: Decoding DVORAK message
  Given A message cyphered in DVORAK distribution
  When I search for aditional information about DVORAK keyboard
  And I find a mapping tool from DVORAK to QWERTY
  Then I copy the DVORAK message
  And I find the first keyword

Scenario: Challenge Solved
  Given A message cyphered in HCESAR distribution
  When I search in google for HCESAR keyboard
  And I realize there is no mapping tool for this keyboard
  Then I code a small java code for mapping
  And I Solve the challenge

