## Version 2.0
## language: en

Feature: Practice-flag
  Site:
    ctflearn
  User:
    jdiegoc (ctflearn)
  Goal:
    enter the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows         | 10          |
    | Chrome          | 79.0.3945.88|
  Machine information:
    Given a text
    When I read the text
    Then I realized that it is the other way around

  Scenario: Success: read the text
    Given a text
    When I read the text
    Then I realized that it is the other way around
    And I turned it
    """
    Found : <flag>
    """
    And I solve the challenge
