## Version 2.0
## language: en

Feature: Lazy Game Challenge - Binary - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Binary
  User:
    smendoz3
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A netcat command in challenge page: "nc thekidofarcrania.com 10001"
    And challenge comments about a game that was made by a guy named "John_123"
    And command display a game of bets with 10 rules
    And Key is given after winning $1000000 in the game

  Scenario: Fail: Lose the game
    Given The netcat command
    When I play the game
    """
    Rules of the Game :
    (1) You will be Given 500$
    (2) Place a Bet
    (3) Guess the number what computer thinks of !
    (4) computer's number changes every new time !.
    (5) You have to guess a number between 1-10
    (6) You have only 10 tries !.
    (7) If you guess a number > 10, it still counts as a Try !
    (8) Put your mind, Win the game !..
    (9) If you guess within the number of tries, you win money !
    (10) Good Luck !..

    Money you have : 500$
    Place a Bet :
    """
    Then I realize I can intetional lose thanks to rule (7)
    And I decide to bet a negative amount "1000$" since is lower than my wallet
    When I try to fail the number guess
    Then I accidentally have the correct guess
    When I win a negative amount
    Then my wallet is lower than 0
    And I lose the game
    Then I couldn't get the flag

  Scenario: Success: Win $1000000 in the netcat command game
    Given The name of the original creator of the code
    And netcat command to play the game
    When I research about "John_123" in Google
    Then I found a link with original source code of the game
    And it contains "CTFlearn{REDACTED}"
    When I read the amount to spend per bet in the source code
    """
    spent = int(input('Place a Bet : '))
    """
    Then I realize I can bet any amount of money
    But It can't exceed my wallet amount
    When I start to play the game
    """
    Rules of the Game :
    (1) You will be Given 500$
    (2) Place a Bet
    (3) Guess the number what computer thinks of !
    (4) computer's number changes every new time !.
    (5) You have to guess a number between 1-10
    (6) You have only 10 tries !.
    (7) If you guess a number > 10, it still counts as a Try !
    (8) Put your mind, Win the game !..
    (9) If you guess within the number of tries, you win money !
    (10) Good Luck !..

    Money you have : 500$
    Place a Bet :
    """
    Then I realize I can intetional lose thanks to rule (7)
    And I decide to bet a negative amount "1000$" since is lower than my wallet
    When I intentional lose the game guessing with a number greater than 10
    """
    Sorry you didn't made it !
    Play Again !...
    Better Luck next Time !.
    Sorry you lost some money !..
    Your balance has been updated !.
    Current balance :  :
    1500$
    """
    Then I get my balance increased due to logic calculation
    And I bet again the amount asked by the challenge
    When I lose but get my wallet increased in that amount
    """
    Sorry you lost some money !..
    Your balance has been updated !.
    Current balance :  :
    1001500$
    What the... how did you get that money (even when I tried to stop you)!?
    I guess you beat me!

    The flag is CTFlearn{FLAG}

    Thank you for playing !
    """
    Then I solved the challenge [evidence](evidence.png)
    And I caught the flag
