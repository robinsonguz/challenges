## Version 2.0
## language: en

Feature: p-d-v-two -web -ctf365
  Site:
    ctf365.com
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
    | Burp Suite      | 2.1.04      |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: negative value with Burp Suite
    When I catch the request with Burp Suit
    Then I can modify the value of "userdata" field
    And I can enter a negative value
    And I can not get the flag

  Scenario: Success: buffer overflow with Burp Suite
    When I catch the "POST" request with Burp Suit
    Then I can send it to "Intruder"
    And I use "trollAmount" field as the target
    And I use a "Payload" of "Numbers" from 1 to 9999999 [evidence](img.png)
    And I use "step" equal to 1000
    And I use "Number format" equal to "Decimal" and integer
    And I get the flag after the "Payload" 716000 [evidence](img2.png)
