Feature: Solve Python 102 challenge
  from site Rankk
  logged as Edprado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have Sublime Text software

Scenario: First failed attempt
  Given A script in phyton
  When I try to run the script in Sublime Text
  And The script doesn't compile
  Then I realize there are missing lines in the script
  And I understand I need to modify the script

Scenario: Challenge solved
  Given A script in python with missing lines
  And a clue about lists in python
  When I learn about lists operations in python
  Then I add the missing line of the script to compile it
  And I use the missing line as password to solve the challenge
