## Version 2.0
## language: en

Feature: nice-try
  Site:
    https://www.rankk.org/challenges/nice-try.py
  User:
    Sierris (wechall)
  Goal:
    Find the password on the given message

  Background:
  Hacker's software:
    | Ubuntu      | 18.04.3      |
    | Chrome      | 77.0.3865.90 |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see an encrypted message
    """
    begin 0666 restricted
    M2&4@9&EE9"!A="!T:&4@8V]N<V]L92P-"D]F(&AU;F=E<B!A;F0@=&AI<G-T
    M+@T*3F5X="!D87D@:&4@=V%S(&)U<FEE9"P-"D9A8V4@9&]W;BP@;FEN92UE
    *9&=E(&9I<G-T+GD@
    `
    end
    """

  Scenario: Fail: Searching the message in Google
    Given The encrypted message
    When I search it in google
    Then I don't find anything useful

  Scenario: Success: Decrypting the hidden message
    Given The encrypted message
    When I search the header in Google
    """
    begin 0666 restricted
    """
    Then I find the type of encryption is "UUencoding"
    When I search for "UUencode decode" in Google
    Then I find a useful page to decode the message
    """
    https://www.dcode.fr/uu-encoding
    """
    And I put the message on it to decrypt it
    When I decrypt it
    Then It converts the character to decimal
    And Subtracts 32
    Then It splits them in 8 bits parts
    And Converts them back to decimal and to characters
    Then The message is decoded
    And I get the message
    """
    He died at the console,
    Of hunger and thirst.
    Next day he was buried,
    Face down, nine-edge first.
    """

  Scenario: Success: Entering the correct flag
    Given The decrypted message
    When I use it as the flag
    Then I get wrong answer
    When I search for the string in Google
    Then I find a GNU poem called "The last bug"
    And That's the flag
