# language: en

Feature: Challenge tube
  From site rankk.org
  From Level 1 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/challenge-tube.py
  Message: The solution is...?
  Objetive: Find the answer of the challenge
  Evidence: Video from the Youtube platform
  """

Scenario: Video
The page shows a video and a field for the solution
  Then I play the video of the challenge
  And it shows a text for intervals
  """
  Challenge 2277
  ?
  Solution
  =
  """
  Then after a few seconds I see a text in the lower right
  """
  ostrich plume
  """
  Then I enter the text into the field
  And I solve the challenge
