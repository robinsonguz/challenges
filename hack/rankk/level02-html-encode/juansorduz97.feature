## Version 2.0
## language: en

Feature: HTML encode - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/html-encode.py
  User:
    juansorduz97
  Goal:
    Enter the passcode

Background:
Hacker's software:
  | <Software name> | <Version>   |
  | Ubuntu          | 16.04.6     |
  | Chrome          | 79.0.3945   |

Machine information:
  Given a link in the challenge page
  When I access it
  Then I can see the following message
  """
  Enter the passcode to pass this challenge.
  """

Scenario:  Fail: Put the first value seen
  Given The challenge page does not show more information
  When I inspect the challenge page source code
  Then I find a conditional that "passcode == p"
  And I introduce "p"
  Then "p" was not the solution

Scenario:  Fail: Modify challenge page code
  Given "p" does not work
  When I analyze the challenge page source code carefully
  Then I identify that "p" is a variable and not a value
  And The challenge page is deployed decoding a large variable
  Then I use the following tool https://coderstoolbox.net/ to encoded-decoded
  And I rewrite the challenge page code to print the "p" variable in an alert
  And I encoded the challenge page code modified
  When I open the developer tools
  Then I try to introduce the modified code
  Then I failed because the challenge page always refresh its initial code

Scenario:  Success: Run specific javascript code
  Given I can not modify the encoded challenge page as I can modify html code
  When I analyze the javascript code
  Then I identify that "p" is a set of modifications of the url
  And I use the following tool https://js.do/ to run js code
  When I run a little part of the js code using as input the challenge url
  """
  var url=unescape("https://www.rankk.org/challenges/html-encode.py");
  var start=url.lastIndexOf("/")+1;
  var script=url.substring(start,url.length);
  var p=script.substring(9,11)+script.substring(7,11)+script.substring(9,10);
  alert(p);
  console.log("HelloWorld")
  """
  Then I obtain a value for "p"
  And I introduce the value in the challenge page
  Then I solved the challenge
