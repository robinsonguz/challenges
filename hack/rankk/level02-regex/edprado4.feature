Feature: Solve Regex challenge
  from site Rankk
  logged as Edprado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given A regular expression
  And A desired string to match
  When I learn about regex
  Then I understand the syntax of the desired string
  When I use a web service to validate my string with the regex
  Then I discover that my string matches the regex
  And I use my string as password to solve the challenge
