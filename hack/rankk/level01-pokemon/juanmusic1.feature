## Version 2.0
## language: en

Feature: Pokemon! - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/pokemon.py
  User:
    JuanMusic1 (wechall)
  Goal:
    Find the key in the page

  Background:
  Hacker's software:
    | Ubuntu      | 18.04.3 |
    | Firefox     | 70.0.1  |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see two inputs and a table
    """
      [evidence](start.png)
    """

  Scenario: Fail: Playing the game
    Given The table with the game
    When I am playing the game
    Then I don't find anything useful

  Scenario: Success: Analysing the content of the page
    Given The page with the game
    When I search in the source code of the page
    Then I find a route with the source of the images
    """
      <img src="images/z1.gif" border="0">
    """
    When I try to access the path /images
    Then I find a page with the inputs but not the table
    """
      [evidence](middle.png)
    """
    And I return to the previous page
    Then I found a new javascript code
    When I analyse the javascript code
    Then I found a function with a particular href location
    """
      self.location.href = 'pokemon.py?c=<FLAG>';
    """

  Scenario: Success: Entering the flag
    Given The location with the parameter
    When I try to access with this parameter
    Then I solve the challenge
