## Version 2.0
## language: en

Feature: Access denied - miscellaneous - rankk
  Site:
    https://www.rankk.org/
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see a picture which is the hint of the challenge
    """
    [evidence](brokenqrcode.png)
    """

  Scenario: Fail: Wrong procedure
    Given The picture of the challenge
    When I try to scan the picture with my cellphone
    Then I see that nothing happens

  Scenario: Success: Solving challenge
    Given The picture of the challenge
    When I realize that the picture looks like a QR code
    Then I see that it has two main squares of the QR code broken
    Then I use the Paint tool on my computer
    And I crop the bottom left main square of the picture and copy it
    And I paste it over the top right main square and over top left main square
    Then I scan the new picture with my cellphone
    Then I get a new URL
    And I open it
    Then I see a new challenge's page with the text "The solution is <FLAG>"
    Given The text
    When I try sending <FLAG> in the original challenge's page
    Then I solve the challenge
