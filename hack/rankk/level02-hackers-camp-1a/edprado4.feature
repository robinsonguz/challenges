Feature: Solve Hackers Camp 1A challenge
  from site Rankk
  logged as Edprado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have Firefox Quantum 57.0 browser

Scenario: First failed attempt
  Given A webpage with no submitting option
  When I click the submit button
  And I can't send the answer
  Then I try to enable the submit button from the sourcecode
  But The submit button is already enabled
  And I understand the modification I need to apply is elsewhere

Scenario: Challenge solved
  Given My previous failed attempt
  When I analyze the sourcecode of the page
  And I compare it with another challenge page from the same domain
  Then I realize there's a web form missing
  And I proceed to manually add it by using Firefox web designer tool
  When I retry to send the password using the submit button
  Then I can finally send the password
  And I solve the challenge

