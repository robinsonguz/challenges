## Version 1.0
## language: en

Feature: transposition cipher-mysterytwisterc3
  Site:
    https://www.mysterytwisterc3.org
  Category:
    transposition cipher
  User:
    kevinc
  Goal:
    Decrypt the english sentence that has been encrypted using the
    beaver code and the instructions

  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | google chrome        | 75.0  (64 bit) |
  Machine information:
    Given I opened the following url
    """
    https://www.mysterytwisterc3.org/en/challenges/level-1/beaver-code
    """

  Scenario: fail: Web search
    Given I read the problem
    Then I open a new tab in chrome
    And I try to search a tool to decrypt the sentence
    And I can't find any tool that help me
    Then I think to try other posibilities

  Scenario: Success: Using paper and pen to draw the situation in a b tree
    Given I read the problem and instructions
    Then I take the encrypted sentence "TTRNEHEORAVNEWESEAABBTREOT"
    And I take paper and pen and start to draw
    Then I start to analize the sentence and follow the rules in the link
    And I draw a b tree to position the letters in correct order
    Then I put even letters on the right and odd on the left
    Then I follow the structure and start to organize the sentence
    Then I got the sentence "TWO BEAVERS ARE BETTER THAN ONE"
    And I put this sentence in page to verify the solution
    And I solve the challenge
