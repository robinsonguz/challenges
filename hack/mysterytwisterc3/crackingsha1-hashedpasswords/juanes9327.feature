## Version 1.0
## language: en

Feature: chacking-sha1
  Site:
      https://www.mysterytwisterc3.org
  User:
      juanes9327
  Goal:
    Reveal the plaintext password given its SHA1 hash value
  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | google chrome        | 75.0  (64 bit) |
  Machine information:
    Given I am accessing the web page using google chrome browser
    And Windows 10 as OS
    And ip 192.168.1.5

  Scenario: fail: Opening a Sha1-Decoder in the Web
    Given I opened te following url
    """
    https://www.dcode.fr/sha1-hash#0
    """
    Then I put the hash value
    And It did not have any string related to that hash

  Scenario: Success: Searching in the Web
    Given I find a SHA1 decyrpt tool
    """
    https://md5decrypt.net/en/Sha1/
    """
    And I put the hash value there
    Then I find the password to solve the challenge
