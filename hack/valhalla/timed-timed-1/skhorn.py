#!/usr/bin/env python
# pylint: disable=F0401
"""
$ pylint skhorn.py
No config file found, using default configuration
Your code has been rated at 10.00/10 (previous run: 8.49/10, +1.51)
$ chmod +x skhorn.py
$
"""

from bs4 import BeautifulSoup
import requests

URL = [
    'https://halls-of-valhalla.org/challenges/timed/timed1.php'
]

COOKIES = {
    '_cfduid': 'd3f825cd4e4b68fef83616959c795c58a1522670435',
    '_ga': 'GA1.2.1045806654.1522670600',
    '_gid': 'GA1.2.1217147496.1523906380',
    'PHPSESSID': '58ihcp91idi17f1ifs0494fti7',
}

PAYLOAD = {}


def connect(url, cookie, payload):
    """
    Send POST req, with cookie, payload + tell
    requests not to verify SSL(CA_certificates)
    """
    response = requests.post(url,
                             cookies=cookie,
                             data=payload,
                             verify=False).text
    return response


def search_in_html(response, tag):
    """
    Search in response, any tag given and return
    all tag encountered
    """
    soup_parser = BeautifulSoup(response, 'html.parser')

    return soup_parser.findAll(tag)


def bubble_sort(data_array):
    """
    Non optimized Bubble sort function
    """
    move = 0
    comp = 0
    for i in range(len(data_array)):
        for index in range(len(data_array)-i-1):
            comp = comp + 1
            if int(data_array[index+1]) < int(data_array[index]):
                tmp = data_array[index+1]
                data_array[index+1] = data_array[index]
                data_array[index] = tmp
                move = move + 1
    return [comp, move]


def main():
    """
    Timing challenge Valhalla
    """
    response = connect(URL[0],
                       COOKIES,
                       payload="")
    html_search = search_in_html(response, "tr")
    for item in html_search:
        td_array = item.find_all('td')

    chall_array = []
    for item in td_array:
        chall_array.append(item.get_text().encode('ascii'))

    print chall_array
    solution = bubble_sort(chall_array)
    print "My solutione", solution
    payload = {
        'comparisons': solution[0],
        'moves': solution[1],
        'submit': 'Check',
    }
    response = connect(URL[0],
                       COOKIES,
                       payload=payload)
    print response


main()
