Feature: Solve Visuel et Basique challenge
  from site Newbiechallenge
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have VMWare player software
  And I have a Windows XP virtual machine
  And I have OllyDbg software

Scenario: Challenge solved
  Given an executable file with password
  When I load the file into OllyDbg
  And I analyze the assembler code
  Then I search for user defined strings
  And I find a word that does not match with the rest of the code
  When I use that word as password
  Then I solve the challenge
