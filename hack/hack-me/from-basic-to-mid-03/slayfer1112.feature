## Version 2.0
## language: en

Feature: CHALLENGE-Hac.me
  Site:
    Hac.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag to pass to the next stage

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
    | Photoshop       | CS6           |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a password to continue

  Scenario: Fail:Type-a-generic-password
    Given I complete the second stage
    Given I'm on the field that ask me the name of the doctor [evidence](login)
    When I try to put some generic names like "Carlos Restrepo"
    Then I receive a popup with a error message [evidence](img1)
    And I fail to access

  Scenario: Fail:Inspect-code-to-see-the-script
    Given I complete the second stage
    Given I'm on the field that ask me the name of the doctor
    When I Inspect the page to see the HTML
    Then I see a function in the submit button "checkForm();"
    And I try to find the script to see the function
    When I found the script I see the function "checkForm()"
    """
    function checkForm() {
      var login = document.getElementById('login');
      if(login.value.length < 15) {
        alert('Error: Full name must contain at least fifteen characters!');
        login.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(login.value)) {
        alert('Error: Full name must contain at least one lowercase letter
              (a-z)!');
        login.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(login.value)) {
        alert('Error: Full name must contain at least one uppercase letter
              (A-Z)!');
        login.focus();
        return false;
      }
      form = document.getElementById('form');
      form.action = login.value.replace(/ /g, "_") + '.htm'
      form.submit();
      return true;
    }
    """
    Then I inspect that to find any clue for the doctor name
    And I find that the script take the full name and redirects
    When I think on it I see that I don't have any clue with the script
    And I fail to found the flag

  Scenario: Success:Inspect-code-to-see-the-script
    Given I complete the second stage
    Given I have the script "checkForm"
    Given I found that the script don't have any clue
    When I see the HTML again I found a curious line
    """
    <tr><td style="text-align:center"><a title=
    "http://www.wikihow.com/Create-Hidden-Watermarks-in-GIMP"
    target="_blank">hint</a></td></tr>
    """
    Then I see that this line say "hint" and I was curious
    And I go to the link in the "title" attribute
    When I'm redicrect to the link I see a tutorial [evidence](img2)
    Then I think the relation with the Watermarks and this stage
    And I think that the background image have other clue in the watermark
    When I go to source panel I found 2 images "original" and "dubious"
    Then I compare both and I find that dubioues have a little difference
    And I download dubious [evidence](img2)
    When I try to see the watermark but was very hard
    Then I think that I can use an image editor to make it more visible
    And I open Photoshop CS6 and paste the image
    When I was playing with some filters in Photoshop
    Then I can make the watermark more visible [evidence](img3)
    And I found another hint "http://www.imdb.com/title/tt0104692/"
    When I go to those link I found a calification of one movie
    Then I see that the name of the movie is "The Lawnmower Man"
    And I need to seek any name to use in the doctor name field
    When I go to the cast section I see a lot of names [evidence](img4)
    Then I see that "Pierce Brosnan" was "Dr. <Name>"
    And I think that "Dr." can make a reference to a doctor
    When I back to the login form I put the name of the doctor
    Then I click on submit button
    And I'm redirect to the final of the challenge
    And I found the flag [evidence](img5)
