## Version 2.0
## language: en

Feature: CHALLENGE-Hac.me
  Site:
    Hac.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag to pass to the next stage

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a password to continue

  Scenario: Fail:Type-a-generic-password
    Given I'm on the field that ask me the password
    When I try some generics passwords like "admin", "Admin", "pssword", etc
    Then I receive a popup with a error message [evidence](image1)
    And I fail to access

  Scenario: Success:Inspect-code-to-see-the-script
    Given I'm on the field that ask me the password
    When I Inspect the page to see the HTML
    Then I see a function in the submit button "onclick='checkForm();'"
    And i try to find the script to see the function
    When I found the script I see the function "checkForm()"
    """
    function checkForm() {
      var password = document.getElementById('password');
      if(password.value.length < 8) {
        alert('Error: Password must contain at least eight characters!');
        password.focus();
        return false;
      }
      if(password.value.length >= 10) {
        alert('Error: Password must contain less than ten characters!');
        password.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one number (0-9)!');
        password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one lowercase
              letter (a-z)!');
        password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one uppercase
              letter (A-Z)!');
        password.focus();
        return false;
      }
      if(password.value == <flag>) {
        form = document.getElementById('form');
        form.action = password.value + '.html'
        form.submit();
      } else {
        alert('You entered a valid password, but not the correct one!
              You entered: ' + password.value);
        password.focus();
        return false;
      }
    }
    """
    Then I inspect that to find any clue for the password
    And I found the password "<flag>"
    When I input the "<Flag>" on the password field
    Then I was redirect to the step 2
    And I found the flag for the step 1
