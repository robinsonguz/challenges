## Version 2.0
## language: en

Feature: CHALLENGE-Hack.me
  Site:
    Hack.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag to pass to the next stage

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a username and password to continue

  Scenario: Fail:Type-a-generic-username-and-password
    Given I complete the first stage
    Then I'm redirected to the second stage
    And I see a login form
    When I try with a generic username and password like "admin", "admin123"
    Then I see a message that says "Error: Username is incorrect!"
    And I fail to get the flag

  Scenario: Fail:Inspect-code-to-see-the-HTML
    Given I complete the first stage
    Then I'm redirected to the second stage
    And I see a login form
    When I inspect the code
    And I see a function on the login button [evidence](img1)
    Then I try to find the script in the code
    And I only see some scripts with the atribute "src"
    Then I can't find the script in the HTML code
    And I fail to found the flag

  Scenario: Success:Inspect-sources-to-see-the-script
    Given I complete the first stage
    And I know that the scripts are in
    """
    js/aes.js
    js/auth.js
    """
    When I try to inspect the sources of the website
    Then I see the html file and 2 folders [evidence](img2)
    And I'm interested in the folder "js"
    When I open the folder I see 2 files "aes.js" and "auth.js"
    Then I open "auth.js"
    And I see the following script
    """
    $(function(){
      document.getElementById('username').focus();
    });


    function createErrorMessage(mssg, item) {
      $("#errorMessage").text(mssg);
      document.getElementById("errorMessage").style.color = "#ff0000";
      item.focus();
      return false;
    }

    function checkForm() {
      // Username Validation
      var username = document.getElementById('username');
      var decryptedUser = CryptoJS.AES.decrypt(
        "U2FsdGVkX18z/rFsid+zrm8+rqAU91X1dlb7QtorB0g=", "Secret Passphrase"
        );

      var decryptedUserString = decryptedUser.toString(CryptoJS.enc.Utf8);

      if( username.value != decryptedUserString ){
        return createErrorMessage(
          'Error: Username is incorrect!', username
          );
      }

      // Password Validation
      var password = document.getElementById('password');
      if(password.value.length < 8) {

        return createErrorMessage(
          'Error: Password must contain at least eight characters!', password
          );

      }
      if(password.value.length >= 10) {

        return createErrorMessage(
          'Error: Password must contain less than ten characters!', password
          );

      }
      re = /[0-9]/;
      if(!re.test(password.value)) {

        return createErrorMessage(
          'Error: password must contain at least one number (0-9)!', password
          );

      }
      re = /[a-z]/;
      if(!re.test(password.value)) {

        return createErrorMessage(
          'Error: password must contain at least one lowercase letter (a-z)!',
          password
          );

      }
      re = /[A-Z]/;
      if(!re.test(password.value)) {

        return createErrorMessage(
          'Error: password must contain at least one uppercase letter (A-Z)!',
          password
        );

      }

      var decryptedPW = CryptoJS.AES.decrypt(
        "U2FsdGVkX1/a5uuuQDyfKsnGdCaS0xk28schshQWGe8=","Secret Passphrase"
        );

      var decryptedPWString = decryptedPW.toString(CryptoJS.enc.Utf8);

      if( password.value == decryptedPWString ){

        $('#myModal').modal('show');
        return true;

      } else {

        return createErrorMessage(
          'Error: Invalid password : ' + password.value, password
          );

      }
    }
    """
    When I see the script I know that the username and password was encrypted
    Then I use the console to get the decrypted username and password
    And I run the following lines
    """
    var decryptedUser = CryptoJS.AES.decrypt(
      "U2FsdGVkX18z/rFsid+zrm8+rqAU91X1dlb7QtorB0g=", "Secret Passphrase"
      );
    var decryptedUserString = decryptedUser.toString(CryptoJS.enc.Utf8);
    var decryptedPW = CryptoJS.AES.decrypt(
      "U2FsdGVkX1/a5uuuQDyfKsnGdCaS0xk28schshQWGe8=","Secret Passphrase"
      );
    var decryptedPWString = decryptedPW.toString(CryptoJS.enc.Utf8);
    var loginform = ["user: "+decryptedUserString,"pass: "+decryptedPWString]
    console.log(loginform)
    """
    When I see the following result
    """
    ["user: <username>", "pass: <password>"]
    """
    And I try to use this username and password
    When I submit the form
    Then I receive a message that says "Good Job" [evidence](img3)
    And I get the flag
