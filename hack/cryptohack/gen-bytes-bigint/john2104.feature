## Version 2.0
## language: en

Feature: general-bytes-and-big-integers-cryptohack
  Site:
    https://cryptohack.org/challenges/general/
  Category:
    Crypto
  User:
    john2104
  Goal:
    Decrypt the text

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://cryptohack.org/challenges/general/
    """
    When I open the url with Firefox
    Then I see a string of numbers
    """
    1151519506386231889993168548881374739577551628728968263649996528271463725920
    6269
    """
  Scenario: Fail:decrypt
    Given the array
    Then I try to decypher it converting it using a built-in function
    When I put the string I get:
    """
      >>>  n.to_bytes(0,'big')
      Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      OverflowError: int too big to convert
    """
    And I didn't solve it

   Scenario: Success:Decrypt
    Given the bigint number
    Then I use the Python's PyCryptodome library
    And the function long_to_bytes [exploit.py]
    """
      print(number.long_to_bytes(VALUE))
    """
    Then I get the flag
    And I solve the challenge
