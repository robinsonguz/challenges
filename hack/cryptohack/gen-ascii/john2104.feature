## Version 2.0
## language: en

Feature: general-ascii-cryptohack
  Site:
    https://cryptohack.org/challenges/general/
  Category:
    Crypto
  User:
    john2104
  Goal:
    Decrypt the text

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://cryptohack.org/challenges/general/
    """
    When I open the url with Chrome
    Then I see an array of numbers
    """
    [99, 114, 121, 112, 116, 111, 123, 65, 83, 67, 73, 73, 95, 112, 114, 49,
    110, 116, 52, 98, 108, 51, 125]
    """
  Scenario: Fail:decrypt
    Given the array
    Then I go to the webpage
    """
      http://www.unit-conversion.info/texttools/ascii/
    """
    When I put the array there I get:
    """
      ߍ�y�o�G�u�y����Up
    """
    And I didn't solve it

   Scenario: Success:Decrypt
    Given the array
    Then I go to the webpage
    """
      https://www.browserling.com/tools/ascii-to-text
    """
    And put the array
    Then I get the flag
    Then I solve the challenge
