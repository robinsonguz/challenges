## Version 2.0
## language: en

Feature: 229-H4CK1NG-enigmes-a-thematiques
  Code:
    229
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the website

  Scenario: Fail:set-URL-parameter
    Given I am on the main page
    And I can see a form with a field to select values a submit button
    When I select whatever value from the form
    And I press the button
    Then A new message appears on the screen
    """
    Merci. J'apprécie beaucoup ton geste ! Mais tu dois me donner davantage
    si tu veux le mot de passe !
    """
    When I inspect the website
    Then I realize the input name is called "prix"
    When I make a new URL to set the "prix" value with "1234"
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep229.php/?prix=1234
    """
    And I refresh the website
    Then The website does not load correctly
    Then I can not capture the flag

  Scenario: Success:use-unescape-function
    Given I am on the main page
    When I inspect the website
    Then I realize the form method is set as POST
    When I search for the form data of the request
    Then I can see the related data
    """
    prix: 1
    send: Envoyer le cadeau
    """
    When I use the console to make the post request with the given data
    """
    $ curl https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep229.php
    -X POST --data 'prix=1234&send=Envoyer+le+cadeau'
    """
    Then I can see a message with the "<FLAG>"
    """
    Bien joué. :) Le mot de passe est <FLAG>.
    """
    And I capture the flag
