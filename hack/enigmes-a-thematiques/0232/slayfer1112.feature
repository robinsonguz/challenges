## Version 2.0
## language: en

Feature: 232-H4CK1NG-enigmes-a-thematiques
  Code:
    232
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    slayfer1112
  Goal:
    Found the flag with the answer

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    And the website show a form for authenticating

  Scenario: Fail:write-answer-directly
    Given I am on the challenge page
    When I click on "Accepter la mission"
    Then I was redirect to a question page
    And I can see a the question "Combien font 6+3*8-13 ?"
    When I try to write the answer
    Then I see that the answer fail

  Scenario: Succes:change-attribute-value
    Given I know that the field for the answer didn't work
    When I Think another way to solve the challenge
    And I try to inspect the page to read the HTML
    When I see an input with hidden type and name "answer"
    Then I try to change the value to "17"
    And I press the "Valider" button
    When I see the message "Le mot de passe est <FLAG>."
    And I try to validate the flag with the challenge page
    Then I receive the message "Tu as déjà résolu cette énigme !"
    And I capture the flag
