## Version 2.0
## language: en

Feature: 26-H4CK1NG-enigmes-a-thematiques
  Code:
    26
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Found the administrator password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing to the website
    And the website show a form for authenticating

  Scenario: Success:change-url-parameters
    Given I am at a login page
    And I can see a the title "Accès refusé !"
    When I analyze the url
    Then I see the parameter "password" witch has a value of false
    When I try to change the password value to true
    And I refresh the website with the new value
    Then The website show the message "Accès autorisé !"
    And The website show the password "pFz73a3u89"
    When I set the password to pass the challenge
    Then I see the message "Tu as déjà résolu cette énigme !"
    And I could capture the flag
