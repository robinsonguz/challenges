## Version 2.0
## language: en

Feature: 738-H4CK1NG-enigmes-a-thematiques
  Code:
    738
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the challenge page

  Scenario: Fail:use-ilovepdf-website
    Given I am on the main page
    And I can see the link called "80N 4NN1F 347"
    When I click on the link
    Then A ZIP file is downloaded
    When I decompress the file
    Then I can see a PDF file called "protected.pdf"
    When I attempt to open the PDF file
    Then I can see the message
    """
    Please enter the password
    """
    When I upload the file to the website
    """
    https://www.ilovepdf.com/unlock_pdf
    """
    And The website tries to remove the password
    Then The website can remove the password from the PDF file
    And I can not capture the flag

  Scenario: Success:use-pdfcrack-and-xlcrack
    Given I am on the main page
    When I use the pdfcrack tool to get the password
    """
    pdfcrack -f protected.pdf
    """
    Then The tool uses brute force to find the password
    And I can see the message on the console
    """
    found user-password
    """
    When I use the password with the file
    Then I can open the file
    And I can see a new track
    """
    ./[Nom de famille des deux sœurs qui ont composé la chanson dont l’air
    « Joyeux anniversaire » est inspiré].xls
    """
    When I search on internet about Joyeux anniversaire
    Then I can see that I need the keyword "hill"
    When I search the file "hill.xls" into the challenge website
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve738/hill.xls
    """
    Then The fill is downloaded
    When I attempt to open the file
    Then The file is protected with a password
    When I use the xlcrack tool to find the password
    """
    ./xlcrack hill.xls
    """
    Then I can see the message on the console
    """
    hills.xls: password id '<FLAG>'
    """
    When I open the hill.xls file
    Then I can see the message
    """
    Bien joué ;)
    """
    When I search for the "<FLAG>" into the file
    Then I can find the "<FLAG>" in the same sheet
    When I enter the "<FLAG>" to pass the challenge
    Then I capture the flag
