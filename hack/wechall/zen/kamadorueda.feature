# Abstract
#   - An automatic methodology is required
#   - The cipher algorithm is not listed in the books
#   - The ciphertext given and the answer is bound to the cookies
#     i.e. session or username

Feature: Solve Zen - The book
  From the WeChall site
  Of the category Special and Crypto
  With my username kedavamaru

Background:
  Given I am running Debian Stretch 9.5.0 (amd64)
  And I am using the C++17 standard
  And I am using Debian GNU/Linux 9 (stretch) (64-bit)
  And I am using GNU bash, version 4.4.12(1)-release (x86_64-pc-linux-gnu)
  And I am using Google Chrome v69.0.3497.100 (Official build) (64 bits)

Scenario: Normal use case
  Given the challenge url
  """
  https://www.wechall.net/en/challenge/Z/EN/index.php
  """
  Then I open it with Google Chrome
  And I see the problem statement
  """
  Hello Hacker,
  I got crazy again and published the anonymous zen master book :)
  Good luck finding it!
  """
  And I see an input field to submit the answer
  And I don't enter anything in the field
  Then I don't solve the challenge

Scenario: Failed attempt number 1
  Given the challenge url
  Then I open it with Google Chrome
  And I use the Developer Tools to see the HTML code of the served page
  And I inspect the entire code
  Then I see an HTML comment
  """
  <!-- youtube.com/watch?v=gjUYE21-uI0 -->
  """
  And I open the url inside the comment with Google Chrome
  And I enjoy the song because it is quite good
  But I don't found any useful information in the lyrics
  And I don't found any useful information in the comments
  Then I don't think it would yield the solution of the challenge
  And I keep inspecting the HTML code served
  And I see another HTML comment
  # some special characters has been removed for compatibility reasons
  """
  <!-- b252b135v131b115D161 P126K228a212cY251c144Z131. h1114h232L134
  D132G133V213h117o155p122Z142E166 Y112t132 L141j162u115W152 s211P122D161k
  cM118pn112o132V154 x221K126, Y122h412 E231A152H212h1113L135o132,
  h412W132m112Z136e145P126V1510 E132j162O132 c154F123gf212k181 g341j141y.
  K226C112t133 s212h1112A153E114 fz173R121b121 V158X162
  n122ph114y154K123h118H163L144j232B131cF183, H132e177A163 d192E165S122
  v121g145 B121R153c123 P142G133Y153X114X133 T117X152, yG133E454
  T141i134s144bV122T254P124m214 y142F124jd112y173 x162N122vU154b132g
  Y243z144X113L132W163O132X133 b112 o265E213T122m155Z131Q221 y183H142
  i134N113g121Z136 D154q112m144I136. - H121Q113X114U126 G243B135 I121
  bM113E121uT113p127? - F121H166e P135p145g113F128A121d192ch122 E165S112
  C131H131D122n154g i142O161I132R135 b123Z142 b133s132o121 t133o156Z132O144
  K521e C221E121C123n143, J211h118d R153j153y153V155 J154N113c122pll112
  cn152l112vm113o255 e133g134f125B123gp112 l271e133J122b132A414
  Y251n151K135K113 F121L122 H324r113h113i251 y182x146? p124q112q133u122
  W323D155v223G243M119D171A133 f125Y163 t134vM161a164K228A134y182d112g134,
  C235V122X152I112 v121v243K221 A351E112C153M156Z142A133c164hQ117,
  z233G133j132i2213C123'u113 v141K127? - R141x145p131e133vO132J151'
  dx145 m112d112D154 p146oN114g213h122h1114 I141p125 ky142Z131p S121q112I113
  h125l142Q134k133C133 i142M156B141t154s124 - ;) - f212K152cy144 Q114I112I113
  py153U122w133e133P112!
  N151s147zh112F152k121u135B251S135Q231S212w121f113N225dk154e161Z134t134R134
  -->
  """
  Then I inmediately recogize the comment as a ciphertext
  And I see that non-alphanumeric characters remain unaltered after the cipher
  And I see a pattern that consists of one letter, and some numbers after it
  And the pattern repeats through all the message
  And I see that some letters don't have numbers after them
  Then I try to match the ciphertext with all known ciphers listed in the books
  But I'm not able to decipher the message
  Then I dont solve the challenge

Scenario: Failed attempt number 2
  Given the challenge url
  Then I open it with Google Chrome
  And I use the Developer Tools to see the HTML code of the served page
  And I inspect the entire code
  Then I see an HTML comment
  """
  <!-- w221V124ln152C112 j153T133v251cq122km113. J127u114S135
  A133i132P124C113l143V158A152B142 z161c122 y183q112R162R133 bC112j122S194
  N321K122G442W112J113i134 Q112p123, k154X3108 V423r122U113M114K126q113,
  x112u115t281T347e145e177g R134V121i171 x273H121f222Q152a135 a144r137H134.
  Z134Z142d271 K321F152f112r135 i231W123M122g A143m166
  r137Y131g132I151R243A114U153W323F123o265w146C235, a151P126g243 y133t132y181
  e131x151 V155T132K228 fb122k245O143a131 W132P112, yM161u
  pI114s144N341k123K121e175O121 t134F124ju112v215 N121n122vy182u116g
  cN114y142W183H112L134k141 U122 d132m214q126Z115m113m121 V131I143
  v135Z131g121d a135o156j232n162. - O173s133K226i143 t252Z115 h113
  h141q113B151H212o155v215? - m155h232D155 p113b115B164gB151n154P352p113
  K133o112 F161I113Z121e177X142 e131C131q2111m113 F121E165 s133c132y
  M391T232M122t144 J164m113 j161A121G172o155,
  T148s146Q135 v132G172z173U141 N221z124f125X171q2111g113 J156g132q113vo132G151
  b135H155n132Z113gG132 Q141F143J122n154g F131blA113 E132M161 K132X113g121N153
  w171H112? E132O221k142l121 O131s124md131n154dd132 g151l161
  v134vf125lp212X152f224k154j153, t141g121I141h T148r138b116
  pO221y182g132l131c225x154y151U141, E264S123F143l121N117'I141 N373O144? -
  x162Q115s371v134vv134O131-  Q135r122 h118L122J127 b421c122O131gp113M116
  A13f125 D245e133Q134p P112K112g113 I136ci171N117i171 w121e175N373vg113 - ;) -
  d172x121P352t256 M116d172G152 g131r161y121I156k122g135!
  f222t252zY123s144i134U136wd155cs151at131N225b116x145C126O143w147c136 -->
  """
  And I inmediately recogize the comment as a ciphertext
  And I see that this ciphertext is different everytime I reload the website
  And I see that non-alphanumeric characters remain unaltered after the cipher
  And I see a pattern that consists of one letter, and some numbers after it
  And the pattern repeats through all the message
  And I see that some letters don't haven numbers after them
  Then I collect ciphertexts reloading the website
  And I analize just the first word
  | first word on the ciphertext |
  | w221V124llC112               |
  | T162n098P847k376o            |
  | heB3745T475V123              |
  And I see that the number of letters is exactly five in all the messages
  And I see that some letters don't have numbers after them
  Then I try to exploit that fact
  And I define an entropy-less character <l> as a set <a><n> of ciphertext
  And I define an entropy-full character <f> as a set <a> of ciphertext
  | <a>        | <n>                | <l>    | <f> |
  | Any letter | Any mix of numbers | <a><n> | <a> |
  Then I first replace <l> with _ leaving only <f> in the ciphertext
  | message |
  | __ll_   |
  | ____o   |
  | he___   |
  And merge the resulting messages into an entropy-full deciphered text
  | merged message |
  | hello          |
  Then I prepare a set of tools to massively collect and decipher ciphertexts
  And I create a bash script to collect HTML snapshots from the challenge page
  """
  for value in {1..10000}
  do
  wget 'https://www.wechall.net/en/challenge/Z/EN/index.php'
  done
  """
  And a C++ code to extract the comments from the HTML snapshots
  And a C++ code to decipher the extracted comments (a.k.a. ciphertexts)
  # see kedavamaru.cpp
  And I get a plain text deciphered message
  """
  <!-- hello nubcake. _he solu__on _o _h_s book c_phe_ _s, of cou_se,
  f_nd_ng _he __gh_ way. now you_ flag _s _mngg_ehnddl, and now as
  you found __, you p_obably enjoy hav_ng c_ea_ed a sys_em _o _ead _h_s.
  _sn_ __ a beau_y?  _he elegance of be_ng able _o say wha_ we wan_, and
  only people cleve_ enough be_ng able _o _ead __? _h_s _em_nds of
  evolu__on, ma_h and ph_losphy, doesn'_ __?  howeve_  do no_ fo_ge_ _o
  keep _he scene al_ve  ;)  hack _he plane_! g_zmo_ewechalldo_ne_ -->
  """
  And I see that only "t", "i", and "r" are missing from the message
  And I recognize that _mngg_ehnddl is the answer
  Then I replace _ with either "t", or "i", or "r"
  And I log into my account and try <answers>
  And I get <results> after submitting the <answers>
  | <answers>     |  <results>  |
  | tmnggtehnddl  |  INCORRECT  |
  | tmnggiehnddl  |  INCORRECT  |
  | tmnggrehnddl  |  INCORRECT  |
  | imnggtehnddl  |  INCORRECT  |
  | imnggiehnddl  |  INCORRECT  |
  | imnggrehnddl  |  INCORRECT  |
  | rmnggtehnddl  |  INCORRECT  |
  | rmnggiehnddl  |  INCORRECT  |
  | rmnggrehnddl  |  INCORRECT  |
  Then I don't solve the challenge

Scenario: Successful Solution
  # It took me a lot of analisys to realize the right
  # solution. In abstract I had two samples:
  # One set of ciphertexts collected manually
  # And one set collected automatically with bash.
  # When I performed the same analisys on both samples
  # I got two human readable messages, exactly the same
  # in every word except with different flags (the solution)
  # "a_lp_nm__scm"(manual) and "_mngg_ehnddl"(automatic).
  # The first one was impossible to crack by brute force
  # due to the lack of manual collected ciphertexts, and
  # the low entropy of each ciphertext itself, (each one
  # contributes around 1 char of entropy of the 1400
  # needed, sometimes they repeat and the ciphertext is wasted
  # and I had only 640 manual collected ciphertexts).
  # The second flag was the wrong solution as demostrated.
  # So I thought about the differences between manual and
  # automatic, and I realized that I was logged-in while
  # I collected the samples manually, but bash was logged off
  # when collecting the samples automatically.
  # So I configured my bash to be logged in, but it failed
  # too, and it was because wechall.net writes a prank cookie
  # to bash when wget tries to log-in.
  # So I inserted it manually, and I was finally in good path!
  Given I'm logged-in into the challenge site
  Then I inspect the cookies generated with Google Chrome
  And I get the values for my session
  # value has been replaced with 12345 because its sensible information
  | domain       | flag | path | https | expiration | name | value |
  | .wechall.net | TRUE |  /   | FALSE | 1598755423 |  WC  | 12345 |
  Then I create a text file called cookie.txt to simulate the cookie
  # tabulations have been replaced with spaces
  """
  .wechall.net TRUE / FALSE 1598755423 WC 12345
  """
  And I create a bash script to collect HTML snapshots from the challenge page
  """
  for value in {1..10000}
  do
  wget --load-cookies cookie.txt \
       'https://www.wechall.net/en/challenge/Z/EN/index.php'
  done
  """
  And a C++ code to extract the comments from the HTML snapshots
  And a C++ code to decipher the extracted comments (a.k.a. ciphertexts)
  # see kedavamaru.cpp
  And I get a plain text deciphered message
  """
  <!-- hello nubcake. _he solu__on _o _h_s book c_phe_ _s, of cou_se,
  f_nd_ng _he __gh_ way. now you_ flag _s aplpgnm__scm, and now as
  you found __, you p_obably enjoy hav_ng c_ea_ed a sys_em _o _ead
  _h_s.  _sn_ __ a beau_y?  _he elegance of be_ng able _o say wha_
  we wan_, and only people cleve_ enough be_ng able _o _ead __? _h_s
  _em_nds of evolu__on, ma_h and ph_losphy, doesn'_ __?  howeve_  do
  no_ fo_ge_ _o keep _he scene al_ve  ;)  hack _he plane_!
  g_zmo_ewechalldo_ne_ -->
  """
  And I recognize that aplpgnm__scm is the answer
  And I see that only "t", "i", and "r" are missing from the message
  And I log into my account and try <answers>
  And I get <results> after submitting the <answers>
  | <answers>     |  <results>  |
  | aplpgnmttscm  |  INCORRECT  |
  | aplpgnmtiscm  |  INCORRECT  |
  | aplpgnmtrscm  |  INCORRECT  |
  | aplpgnmitscm  |  INCORRECT  |
  | aplpgnmiiscm  |  INCORRECT  |
  | aplpgnmirscm  |  INCORRECT  |
  | aplpgnmrtscm  |  INCORRECT  |
  | aplpgnmriscm  |   CORRECT   |
  | aplpgnmrrscm  |  NOT TRIED  |
  Then I solve the challenge

  # Note: "aplpgnmriscm" only works for my username (i.e. session cookie).
  # if you are to try it, you have to do the entire procedure to generate
  # a valid answer for your username, unless, of course, a colission exists
  # between the hashing outputs
