#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''
import hashlib

FILE = open("DATA.lst", "r").readlines()
DICT = open("cain.txt", "r").readlines()
HASHES = []
HASHGAME_SALT_WC3 = 'zomgsalt'
HASHGAME_SALT_WC4 = 'zomgsalt4'


for l in FILE:
    HASHES.append(l.rstrip())

HASHES_WC3 = HASHES[0:17]
HASHES_WC4 = HASHES[17:]

SALTS = [x[-4:] for x in HASHES_WC4]


def wc3_hash(hash_word):
    """wc3 hashing"""
    inhash = hashlib.md5(hash_word.rstrip()).hexdigest()+HASHGAME_SALT_WC3
    hashed = hashlib.md5(inhash).hexdigest()
    if hashed in HASHES_WC3:
        print "wc3: " + hash_word


def wc4_hash(hash_word):
    """wc4 hashing"""
    for salt in SALTS:
        inhash = HASHGAME_SALT_WC4+hash_word.rstrip()+salt+HASHGAME_SALT_WC4
        hashed = hashlib.sha1(inhash).hexdigest()+salt
        if hashed in HASHES_WC4:
            print "wc4: " + hash_word


for word in DICT:
    wc3_hash(word)
    wc4_hash(word)


# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
wc4: a
wc3: abc
wc4: abc
wc3: attack
wc4: b
wc3: birthday
wc4: c
wc3: cake
wc4: code
wc3: coincidence
wc3: collision
wc4: elegance
wc4: fail
wc3: foo
wc4: foo
wc3: minimize
wc3: muffin
wc3: no
wc4: no
wc4: orthography
wc3: rainbow
wc4: sample
wc3: subversion
wc4: summertime
wc3: test
wc4: test
wc4: triangulation
wc3: turtle
wc4: virtual
wc4: win
wc4: wrong
'''
