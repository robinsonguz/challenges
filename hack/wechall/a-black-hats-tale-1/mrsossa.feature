## Version 2.0
## language: en

Feature: a-black-hats-tale-1
  Site:
    https://www.wechall.net/challenge/Z/blackhattale/index.php
  User:
    TheSossa (wechall)
  Goal:
    Crack the WPA password with some dictionary attack

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox Browser | 71.0(64.bit)|
    | aircrack-ng     | 1.5.2       |
  Machine information:
    Given a password encrypted
    Then learn what type of encryption it was
    And decrypting

  Scenario: Success: aircrack-ng dictionary
    Given a password encrypted
    Then i saw that it was with WPA
    When learned to use aircrack-ng
    And run it
    Then i took the output
    """
    KEY FOUND! [<flag>]
    """
    And I solve the challenge
