## Version 2.0
## language: en

Feature: hdc-web-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    web
  User:
     iral
  Goal:
    Find the flag on the given web

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | Linux           | 4.19.0-kali1-amd64  |
    | Firefox esr     | 60.8.0esr           |

  Scenario: Success: ctf
    Given I check the source code of the web
    And I find two files called myscripts.js and jquery-3.2.1.js
    When I check the jquery-3.2.1.js file I see the following
    """
    function doProcess() {
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "main/index.php");
        form.setAttribute("target", "view");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "name1");
        hiddenField.setAttribute("value", "TXlMaXR0bGU");
        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("type", "hidden");
        hiddenField2.setAttribute("name", "name2");
        hiddenField2.setAttribute("value", "cDB3bmll");
        form.appendChild(hiddenField2);
        form.appendChild(hiddenField);
        form.appendChild(hiddenField2);
        document.body.appendChild(form);
        window.open('', 'view');
        form.submit();
    }
    """
    Then analyzing the source code I get two values
    """
    Username: TXlMaXR0bGU
    Password: cDB3bmll
    """
    When I go to the access form and after putting the credentials
    Then I successfully connect to the panel
    When I clicked at Mailbox of Special Customers
    And I see a gif image and I decide to click
    And I found a path named secret_area_
    Then I opened the secret_area_
    And I found a text file named mails.txt [evidence](iral1.png)
    When I opened the file
    Then I got the list of names and emails
    """
    All good boys are here... hehehehehehe!
    ----------------------------------------
    Peter Punk CallMePink@newmail.com
    Nabuchodonosor BabyNavou@mailpost.gr
    Ilias Magkakos imagkakos@badmail.com
    Nick Pipshow NickTheGreek@mail.tr.gr
    Don Quixote Windmill@mail.gr
    Crazy Priest SeVaftise@hotmail.com
    Fishroe Salad fishroesalad@mail.com
    TaPanta Ola OlaMaziLeme@mail.gr
    Laertis George I8aki@mail.gr
    Thiseas Sparrow Pirates@mail.gr
    Black Dreamer SupaHacka@mail.com
    Callme Daddy FuckthemALL@mail.com
    Aggeliki Lykolouli FwsStoTounel@Traino.pourxetai
    Kompinadoros Yannnnis YannisWith4N@rolf.com
    Serafino Titamola Ombrax@mail.gr
    Joe Hard Soft@Butter.gr
    Bond James MyNameIsBond@JamesBond.com
    Endof Text EndOfLine@mail.com
    """
    When I enter the link to send emails
    And I start using every email in the list
    Then I find the mail fishroesalad@mail.com
    And I get the flag :)
