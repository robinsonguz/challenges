## Version 2.0
## language: en

Feature: Illumination - Forensics - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    OSINT
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text
    """
    A Junior Developer just switched to a new source control platform.
    Can you find the secret token?
    """
    And the flag format
    """
    HTB{flag}
    """
    And a zip file containing 2 files "bot.js" and "config.json"
    And challenge category Forensics

  Scenario: Fail: Wrong path
    Given The two files
    When I analyze both files
    Then I see "bot.js" is a JS file with useless information
    But I see "config.json" has some useful information
    When I see its content
    """
    {

    "token": "Replace me with token when in use! Security Risk!",
    "prefix": "~",
    "lightNum": "1337",
    "username": "UmVkIEhlcnJpbmcsIHJlYWQgdGhlIEpTIGNhcmVmdWxseQ==",
    "host": "127.0.0.1"

    }
    """
    Then I see username is encoded in Base64
    When I decode username
    Then I get "Red Herring, read the JS carefully"
    And I don't find anything useful

  Scenario: Success: Flag in Instagram photo
    Given The zip file
    And category Forensics
    When I unzip the file
    Then I use "ls -a"
    And I can see hidden ".git" file
    When I use "git log"
    Then I can see previous commits version exists [evidence](img1.png)
    When I move 3 commits behind before the token were removed
    Then I can see the token [evidence](img2.png)
    When I decode the token in Base64
    Then I can see the "HTB{flag}" format
    And I caught the flag
