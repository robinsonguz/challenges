## Version 2.0
## language: en

Feature: Image processing 101 - Stego - Hack the Box
  Site:
    https://www.wechall.net/
  Category:
    Stego
  User:
    arguoK
  Goal:
    Find secret word

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A image to download in category Steganography
    And image name is "cheeki_breeki.png"
    And sentence "Check all the domains. Don't be a script kiddie..."

  Scenario: Fail: Hex information
    Given The image "cheeki_breeki.png"
    And Category Stegano
    When I use hexdump command in teminal
    Then I get the image hex values
    And I see the headers looking for PK magic words
    When I find PK headers
    Then I try to form removable packages to unzip data
    But I can't find something useful

  Scenario: Success: Flag in FFT of the image
    Given The image "cheeki_breeki.png"
    And "Don't be a script kiddie" comment
    Then I asume there is a script to process the image
    When I run a Fast Fourier Transformation (FFT) Script
    Then I see some readable information [evidence](img1.png)
    And I caught the flag
