## Version 2.0
## language: en

Feature: Longbottoms_Locker-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Misc
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Python          | 3.6         |
    | pickle          | 3.8.4       |
    | Binwalk         | 2.1.1       |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given 3 files:
    """
    index.html
    neville.gif
    socute.jpg
    """
    And A basic login page in the HTML file
    And A flag format of
    """
    HTB{$FLAG}
    """
    And A serialize/deserialize library for python in pickle
    And Its running on Mint 19.3 with  kernel 18.04.3

  Scenario: Fail: Did not find the flag
    Given the file "index.html"
    When Read through the contents of the file
    Then I find the script that compares passwords
    """
    var passphrase = document.getElementById('passwd').value,
            encryptedMsg = '4cce4470203e10b395ab1787a22553a5b2503d42a965da81367\
            6d929cc16f76cU2FsdGVkX19FvUyhqWoQKHXNLBL64g8acK4UQoP6XZQ/n4MRL3rgQj\
            TJ/3r8Awtxte2V9s+RLfQHJOHGwYtctqRa/H2BetmxjwGG+LYKUWC8Z6WBoYbecwtAT\
            COuwewnp+VKBzsWLme+3BZyRgKEA==',
            encryptedHMAC = encryptedMsg.substring(0, 64),
            encryptedHTML = encryptedMsg.substring(64),
            decryptedHMAC = CryptoJS.HmacSHA256(encryptedHTML,
                            CryptoJS.SHA256(passphrase).toString()).toString();
    """
    And The password hash stored localy
    When I look at which algorithm is being used
    Then I realize they are using a SHA256 algorithm
    And I will not be able to crack the password directly

  Scenario: Success: Found the flag
    Given the file 'socute.jpg'
    When I try extracting hidden files in the image with the command:
    """
    binwalk -e socute.jpg
    """
    Then I get a hidden zip file and its contents extracted
    When I open the extracted file 'donotshare'
    Then I see some text with a structured pattern [evidence] (1.png)
    When I search for different snippets of the file
    Then I find this could be a serialized file generated using python
    When I write a python script to deserialize the file and print its contents
    """
    import pickle

      with open('donotshare', 'rb') as f:
        print(pickle.load(f))
    """
    Then I get a list of tuple arrays printed out [evidence] (2.png)
    When I run the script 'lbPickle.py' to print each array in one line
    Then I get the password printed as a banner [evidence] (3.png)
    When I enter this password in the login page
    Then I get a message with the flag already formatted
    When I enter the flag as the answer
    Then I solve the challenge
