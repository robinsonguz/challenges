# language: en

Feature: Solve the challenge Real level 3
  From www.hackthis.co.uk
  From Levels - Real
  With my username develalopez

  Background:
    Given that I am registered in the HackThis!! website
    Given I work in Windows 10 OS
    Given I have internet access

  Scenario: Succesful solution
    Given a link to the administration login page, I open it
    Then I check the page's source code
    And I find a file login.js
      But The credentials are encrypted
    Then I find the encription script
      And I see the following
    # Login version 4.2 june 2000 Copyright D10n
    When I Google that comment
      And find the documentation for Login Script Creator 4.2
    Then I see that this tool saves the credentials in the members.js file
      And I go back and open said file
    Then I copy the username and the password
      And I solve the challenge
