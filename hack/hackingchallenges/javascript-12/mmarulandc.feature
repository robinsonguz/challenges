## Version 2.0
## language: en

Feature: javascript-12 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    mmarulandc
  Goal:
    Enter the correct password

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.hacking-challenges.de/index.php?page=
    hackits&kategorie=javascript&id=12
    """
    And the site has one password field


  Scenario: Success:Finding values in javascript
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
    """
    function pwd(){
      var base= new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
      "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
      "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z","a", "b",
      "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
      "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
      var key=new Array(5)
      var i
      base.reverse()
      for (i=0; i<= 5;++i)
      {
        key[i] = new Array(2);
      }
      key[0][0]=base[7];
      key[0][1]=base[23];
      key[1][0]=base[18];
      key[1][1]=base[14];
      key[2][0]=base[5];
      key[2][1]=base[21];
      key[3][0]=base[7];
      key[3][1]=base[7];
      key[4][0]=base[21];
      key[4][1]=base[14];

      passab=document.hackit.eingabe.value;
      if (passab==key.join(",")){
        alert ("Passwort richtig!");
      }
      else {
        alert ("Passwot falsch!");
      }
    }
    """
    And I find out that there is a conditional blocking the access
    And I need to find the right value for the variable
    And I ran the script without the conditional code block
    Then I print the variable key with join function
    """
    var base= new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
    "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z","a", "b",
    "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
    "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
    var key=new Array(5)
    var i
    base.reverse()
    for (i=0; i<= 5;++i)
    {
      key[i] = new Array(2);
    }
    key[0][0]=base[7];
    key[0][1]=base[23];
    key[1][0]=base[18];
    key[1][1]=base[14];
    key[2][0]=base[5];
    key[2][1]=base[21];
    key[3][0]=base[7];
    key[3][1]=base[7];
    key[4][0]=base[21];
    key[4][1]=base[14];

    console.log(key.join(","))
    """
    Then I find the value for the password
    When I enter the password
    And the form is submitted
    Then a success message is displayed [evidence](img.png)
    """
    <F,L,A,G>
    """
    Then I solve the challenge
