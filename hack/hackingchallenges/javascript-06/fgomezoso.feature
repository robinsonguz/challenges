## Version 2.0
## language: en

Feature: javascript-06 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    fgomezoso
  Goal:
    Enter the correct password

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.hacking-challenges.de/index.php?page=
    hackits&kategorie=javascript&id=6
    """
    And the site has two field named "password" and "input"

  Scenario: Success:Value Assignment
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
    """
    function pwd(){
    var user=document.hackit.eingabe.value;
      if(user == "Hacking-") {
        pass=prompt("Passwort:","");
        if(pass == "Challenges") {
          alert ("Glückwunsch! Das Passwort ist richtig!\n
          Jetzt beide Passwörter zusammengeschrieben als Lösung eingeben.");
        }
        else {
          alert ("Das Passwort ist falsch!");
        }
      }
      else {
        alert ("Das Passwort ist falsch!");
      }
    }
    """
    And I can see that the script is activated with the "user" variable
    And the value for user must be "Hacking-"
    When I open the browser inspector
    And I change the piece of code assigned to "user" in the script
    """
    document.hackit.eingabe.value = "Hacking-"
    """
    Then I find out that the input field receives the user variable
    When I enter the previously assigned value
    Then a password prompt shows up
    And I enter the password from the source code [evidence](img.png)
    When an alert is displayed showing a success message [evidence](img2.png)
    Then I enter the previous values as the final password
    """
    Hacking-Challenges
    """
    And I solve the challenge
