## Version 2.0
## language: en

Feature: Network-247ctf
  Site:
    247ctf.com
  Category:
    Network
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 5.7.9-arch1-1 |
    | Chrome          | 85.0.4183.26  |
    | Wireshark       | 3.2.5         |
    | Python          | 3.8.3         |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    We are trying to decrypt a packet capture taken on our internal network.
    We know you can decrypt the data using the correct private key, but we
    simply have too many. Can you identify the correct key?
    """
    Then I click the link to start the challenge
    And downloads the following file
    """
    931deab0f3246f989bbb4500b06b3b8841f7beff.zip
    """

  Scenario: Fail: Check secrets in the files
    Given The downloaded file
    When I extract it, I get the following files
    """
    encrypted.pcap keys.tar.gz
    """
    Then I extract "keys.tar.gz" and get a folder with a lot of files
    """
    0a42b7242ca82143f230ae98982b25ca.key
    0b9b2ccb34f62e245df84c14614f2271.key
    0b55793cc0a3c3e9ba99640766ed4f30.key
    ...
    """
    And I open one of them and see a private key [evidence](key.png)
    And I conclude that all the files are private keys
    Given The file
    """
    encrypted.pcap
    """
    When I try to see in hex with
    """
    https://hexed.it/
    """
    Then I found nothing interesting
    And conclude that something must be done with the key files and
    """
    Wireshark
    """

    Scenario: Success: Decrypt with private key
    Given the file
    """
    encrypted.pcap
    """
    When I open it with
    """
    Wireshark
    """
    Then I notice 15 messages between two addresses
    """
    172.17.0.2 172.17.0.3
    """
    And Looking to the protocols, I only see the following 2
    """
    TCP TLS
    """
    Given The messages
    When I read the "info" column, I notice some with interesting messages
    """
    Client Hello
    Server Hello, Certificate, Server Hello Done
    Application Data
    """
    Then I Check the second one and found a Certificate
    And I Download it with the option
    """
    Export Packet Bytes
    """
    And I save it as
    """
    cert.crt
    """
    When I look the
    """
    Application Data message
    """
    Then I see an encypted message
    And I conclude that, this one should be the target message to decrypt
    When I notice that I have a certificate and the list of private keys
    Then I realize that I have a way to decrypt the message
    And That is, finding the correct key
    Given The list of keys and the certificate
    When I create a python script to find the correct key for the certificate
    """
    sullenestd1.py
    """
    Then I run it and get
    """
    $ python sullenestd1.py
    Found
    The correct key is 518dfdb269ef17a932a893a63630644c.key
    """
    Given The found key
    When I Insert it in Wireshark
    """
    Preferences -> Protocols -> TLS -> RSA Key list
    """
    Then I can see an extra tab for TLS messages
    """
    Decrypted TLS (x bytes)
    """
    And I check the package with the encrypted message
    Then I select the tab
    """
    Decrypted TLS (194 bytes)
    """
    And I found the flag
