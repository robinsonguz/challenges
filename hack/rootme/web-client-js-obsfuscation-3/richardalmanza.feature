## Version 2.0
## language: en

Feature: javascript-obfuscation3 - web-client - root-me

  Site:
    root-me.org
  Category:
    web-client
  User:
    richardalmanza
  Goal:
    Get the password from obfuscated javascript code

  Background:

      Hacker's software:
      | <Software name> | <Version>     |
      | Ubuntu (Bionic) | 18.04.4 LTS   |
      | Google Chrome   | 80.0.3987.132 |

    Machine information:
    Given I'm accesing the challenge page
    And the tittle is
      """
      Javascript - Obfuscation 3
      """
    And the subtitle is
      """
      Useful or Useless that is the question...
      """
    And in the statement section, there is a button
      """
      Start the challenge
      """
    And now I'm in the challenge page

  Scenario: Success:deobfuscate and analize the code
    Given source code of the challenge page
    Then I used JsNice, an online deobfuscating tool
    And I got an easier code to read
    Then I read the unique function in the code
    And deleted useless expressions and statements
    Then I analized the code again
    And I realized that the entire function is a distraction
    Then I checked the line that I ignored before
    And I found out the possible password as an byte array
    Then I used the console tool in google-chrome to get a String
    And I copied it to use it as the password
    Then I pass the challenge


  Scenario Outline: process' evidence
    Given A <step> of the process
    Then there is an image as <evidence>

      Examples:
      | step                | evidence        |
      | deobfuscate         | jsnice.png      |
      | console             | source-code.png |
      | challenge completed | challenge.png   |
