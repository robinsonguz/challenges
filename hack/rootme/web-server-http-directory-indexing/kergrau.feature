## Version 2.0
## language: en

Feature: Root Me - Web-Server - http-directory-indexing
  Site:
    Root Me
  Category:
    Web Server
  Challenge:
    Http directory indexing
  User:
    synapkg
  Goal:
    Find the password.

  Background:
  Hacker's software:
    | <Software name>  |   <version>       |
    | Ubuntu           | 16.04 LTS (64-bit)|
    | Mozilla Firefox  | 63.0.3 (64-bit)   |

  Scenario: Fail: Add text to URL
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Web-Server/HTTP-directory-indexing
    """
    Then I opened that URL with Mozilla Firefox
    And I opened the follow link to start challenge
    """
    http://challenge01.root-me.org/web-serveur/ch4/
    """
    Then I typed ctrl+u
    And it opened its source code in other tab
    And I saw a comment that says "<!-- include("admin/pass.html") -->"
    Then I added this to last of URL and it's look like that
    """
    http://challenge01.root-me.org/web-serveur/ch4/admin/pass.html
    """
    Then I opened the before link and I saw
    """
    J'ai bien l'impression que tu t'es fait avoir / Got rick rolled ? ;)
    T'inquiète tu n'es pas le dernier / You're not the last :p

    Cherche BIEN / Just search
    """

  Scenario: Success: Modify added text
    Given the composed link beforely I did not see the password
    Then I tried removing "pass.html" of the URL and it's look like that
    """
    http://challenge01.root-me.org/web-serveur/ch4/admin
    """
    Then I saw a directory where there were 2 folders and 1 file
    Then I opened the "backup" folder
    And I saw inside it I saw a file called admin.txt
    Then I opened it and I saw the follows
    """
    Password / Mot de passe : LINUX
    """
    And I put "LINUX" as the password and I solved the challenge
