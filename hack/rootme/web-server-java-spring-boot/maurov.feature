## Version 2.0
## language: en

Feature: java-spring-boot-web-server-rootme
  Site:
    rootme
  Category:
    web-server
  User:
    maurov
  Goal:
    Get the credentials to access a Spring Boot application

  Background:
  Hacker's software:
    | <Software name>         | <Version>    |
    | Ubuntu                  | 20.04        |
    | Chrome                  | 84.0.4147.89 |
    | Eclipse Memory Analyzer | 1.10.0       |
  Machine information:
    Given I am accesing the challenge website through its URL
    When I visit the link
    Then a login page loads in the browser

  Scenario: Fail:Check Actuator endpoints
    Given the Root-me website gives a tip on how to find the solution
    """
    Metrics, a dangerous feature
    """
    Then I deduce that I will need to gather some metrics for this application
    And I know Spring provides a module called Actuator, used for monitoring
    And this module allows users to access metrics using HTTP endpoints
    Then I visit the /mappings endpoint (made available by the module)
    And check which metrics endpoints I can access without authenticating
    Then the endpoint returns a JSON file with all the request mapping paths
    And there are 15 endpoints publicly available
    """
    /
    /login
    /beans
    /metrics
    /metrics/**
    /info
    /trace
    /autoconfig
    /heapdump
    /mappings
    /configprops
    /health
    /env
    /env/**
    /dump
    """
    Then I proceed to inspect each endpoint, looking for relevant information
    And I do not find anything that could lead me to the flag

  Scenario: Success:Check the heap dump file
    Given the /heapdump endpoint returns a file with a format other than JSON
    And it returns a GZip compressed hprof file
    Then I suspect that this dowloadable file may contain the flag
    And since the /mappings endpoint shows the methods that handle each request
    And this methods use their canonical names
    And these names contain the main package name
    """
    org.rootme.metrics
    """
    Then it occurs to me that I can check the heap dump file
    And search for credentials data leakage via references or objects in memory
    Then I use Eclipse Memory Analyzer to analyze this file
    And I search for class references with the pattern org.rootme.metrics.*
    Then I find 3 classes in this package
    """
    org.rootme.metrics.MetricsApplication$$EnhancerBySpringCGLIB$$adbec959
    org.rootme.metrics.validator.CredentialValidator
    org.rootme.metrics.controller.MetricsController
    """
    And I check the CredentialValidator class which contains 2 static fields
    """
    PASSWORD 51283da263faa96aac2bceaaf3af2e37
    LOGIN g4m3rZ
    """
    Then I login to the challenge website using this credentials
    And it redirects me to a page with the flag
    """
    You can use this flag to score : db23ac29ee9872d71a7532128d5c22b9
    """
    And I solve the challenge
