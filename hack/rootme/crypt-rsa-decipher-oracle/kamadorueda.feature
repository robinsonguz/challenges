## Version 2.0
## language: en

Feature: Root Me - Crypto - RSA - Decipher Oracle
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    RSA - Decipher Oracle
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>             |
    | Ubuntu          | 18.04.1 LTS (amd64)   |
    | Google Chrome   | 70.0.3538.77 (64-bit) |

  Machine information:
    Given I'm accesing the challenge page
    And the problem statement is provided
    """
    You have sucessfully intercepted a C text,
    encrypted with a RSA keypair which you know the (n, e) public component.

    Luckily, your victim has made a mistake by using a server that allows you
    to decrypt any ciphered text with his public key, except for those including
    a secret that must stay encrypted.

    Your job is to handle it in order to decrypt this weird message!
    """
    And the connection parameters with the Oracle
    | Host                    | Protocol | Port  |
    | challenge01.root-me.org | TCP      | 51031 |

  Scenario: Success: Chosen RSA ciphertext attack
    Given The problem is to reverse an RSA
    And RSA is vulnerable to this attack because of its multiplicative property
    And I have 'e'
    """
    e = 65537
    """
    And I have 'c'
    """
    c = 4166241049490033597886572013392990002729748149314322302670411233
        9997247425350599249812554512606167456298217619549359408254657263
        8749184585187537446249660962016088195118586642686855293361631811
        56329400702800322067190861310616
    """
    And I have 'n'
    """
    n = 4563789028582909074152736763264597585018635874558890464152994142
        9081277615885109100864399224350552995741720983588216915335646693
        9122622249355759661863573516345589069208441886191855002128064647
        429111920432377907516007825359999
    """
    And I have an oracle that decrypt any 'c' except the one I have
    And I choose a number 'r' to be my favorite number
    """
    r = 48
    """
    And I compute
    """
    cp = c * r**e % n
    cp = 1874251418684683169881084816410273625325608899606050353819738632
         2526040540669876772790320640500082291047507646232995460062899820
         0502394324002683405398289687902868521567891339815678053572142832
         4142179876071621245905195039368538972010869714027007454905554065
         1119540624836048255435120717440485016260426094994237802405227828
         7120514952393493820450508275210703505909026111128283387449436745
         8990749408857986905832407196963330363682945147323158687786083114
         4
    """
    When I ask the oracle to decrypt 'cp'
    """
    $ nc challenge01.root-me.org 51031
      Hi! Welcome to the RSA Fortress!
      What's the ciphertext you want me to decrypt?
        18742514186846831698810848164102736253256088996060503
        53819738632252604054066987677279032064050008229104750
        76462329954600628998200502394324002683405398289687902
        86852156789133981567805357214283241421798760716212459
        05195039368538972010869714027007454905554065111954062
        48360482554351207174404850162604260949942378024052278
        28712051495239349382045050827521070350590902611112828
        33874494367458990749408857986905832407196963330363682
        9451473231586877860831144
    """
    Then the oracle answer my query with a decimal number 'x'
    """
      The corresponding plaintext is:
        35001959910872672068104544662686855134173709371924453
        285804459600560236143474200565492771636552720
    """
    When I compute
    # mod_inv is the modular multiplicative inverse of 'r' modulo 'n'
    """
    m = x * mod_inv(r, n) % n
    """
    Then I get an hexadecimal number
    """
    0x57656c6c20646f6e65212054686520666c616720697320533164334368346e6e
      336c34747434636b
    """
    When I put this number into a an ASCII converter
    Then I get the flag
    """
    Well done! The flag is S1d3Ch4nn3l4tt4ck
    """
    And I solve the challenge

    # ==== Chosen RSA Ciphertext Attack ========================================
    # you have 'c', 'e', 'n' and you want to decipher 'm'
    #   c = m**e % n
    # you have a way to decipher any ciphertext,
    #   except the 'c' you have, of course
    #
    # to do it:
    #   chose a number 'r', I like 48
    #   compute
    #     cp = c * r**e % n
    #   ask the oracle to decrypt cp
    #     you get 'mr % n'
    #   you are ready to go
    #     m = (mr % n) * mod_inv(r, n) % n
    # ==========================================================================
