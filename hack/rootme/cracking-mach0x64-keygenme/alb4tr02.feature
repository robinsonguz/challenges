## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    Alb4tr02
  Goal:
    Get a key activation for a mach0 file

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Windows OS      | 10           |
    | Google Chrome   | 83.0.4103.106|
    | Ghidra          | 9.1.2        |
    | radare2         | 2.3.0        |
    | maloader        | 1            |
    | Ubuntu          | 18.04.4      |

  Machine information:
    Given the file ch26.zip at the link
    """
    http://challenge01.root-me.org/cracking/ch26/ch26.zip
    """
    Then I am able to successfully execute the macho file trough
    """
    ./maloader/ld-mac macho
    """

  Scenario: Fail: Patching binary file
    Given that I have the binary file
    Then I reverse the binary with Ghidra
    And I see that the file have two functions, main and auth function
    Then I realise that the auth function returns and int
    And I see that main verify the value returned by auth in the following lines
    """
    iVar1 = _auth(local_58,(ulong)local_6c,local_38);
    if (iVar1 == 0) {
    ...
    }
    """
    And I see the assembly code for the address of the if statement
    """
    100000d86 83 f8 00        CMP        EAX,0x0
    """
    Then I open the binary file with radare2 and change the if statement by
    """
    0x100000d86      83f801         cmp eax, 1
    """
    Then I execute the patched binary and get the following output
    """
    $ ./maloader/ld-mac macho
    .username.
    Alb4tr02
    .activation key.
    12345
    Authenticated! You can use this password to valid8
    .,!18-dkb>7fw
    """
    Then I try the password in the challenge page and get the following
    """
    Validation
    Just try again !
    """

  Scenario: Success: Executing patched file with rigth key
    Given the patched binary
    Then I read carefully the auth function
    And I discover a harcoded number in the following extract
    """
    if ((param_2 == local_34) && (param_2 == 0x5f2548)) {
      local_c = 0;
    }
    """
    And I discover that param_2 is actually the user activation key
    Then I calculate the decimal conversion of 0x5f2548
    And I execute the binary file again with that number
    And get the following output
    """
    $ ./maloader/ld-mac macho
    .username.
    Alb4tr02
    .activation key.
    6235464
    Authenticated! You can use this password to valid8
    zjo-dDbupA0
    """
    And I try the new password in the challenge page and get the following
    """
    Well done you win 25 points
    """
    And finish the challenge
