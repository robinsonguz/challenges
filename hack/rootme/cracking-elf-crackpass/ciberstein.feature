## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Find the password in the "Crack" file

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | Radare2         | 3.1.3            |
    | WinRAR          | 5.61             |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    Statement
    Compiled with : gcc -fno-stack-protector -o
    Crack && strip Crack on Linux x86 (Debian)

    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the RAR file

  Scenario: Success:Reverse engineer
    Given I need to access the source code of the file
    And I have WinRAR on my system
    Then I proceed to uncompress the RAR file
    And I obtain the file "Crack" without extension
    And I open the file with Radare2
    """
    $ radare2 -w Crack
    [0x08048440]> aaa
    [x] Analyze all flags starting with sym. and entry0 (aa)
    [x] Analyze len bytes of instructions for references (aar)
    [x] Analyze function calls (aac)
    [ ] [*] Use -AA or aaaa to perform additional experimental analysis.
    [x] Constructing a function name for fcn.* and sym.func.* functions (aan))
    """
    Then I exploring
    And I saw this
    """
    [0x08048440]> s 0x08048610
    [0x08048610]> pD 48
    |           0x08048610      89742404       mov dword [esp + local_4h], esi
    |           0x08048614      891c24         mov dword [esp], ebx
    |           0x08048617      e810feffff     call sym.imp.strcmp
    |           0x0804861c      85c0           test eax, eax
    |       ,=< 0x0804861e      7512           jne 0x8048632
    <<<<------- NOP THIS ONE
    |       |   0x08048620      895c2404       mov dword [esp + local_4h], ebx
    |       |   0x08048624      c70424e88704.  mov dword
    [esp], str.Good_work__the_password_is_:__n_n_s_n ; [0x80487e8:4]=0x646f6f47
    LEA str.Good_work__the_password_is_:__n_n_s_n ;
    "Good work, the password is : ..%s." @ 0x80487e8
    |       |   0x0804862b      e8dcfdffff     call sym.imp.printf
    |      ,==< 0x08048630      eb0c           jmp 0x804863e
    |      ||   ; JMP XREF from 0x0804861e (sub.strcmp_5a5)
    |      |`-> 0x08048632      c70424b48704.  mov dword [esp],
    str.Is_not_the_good_password__ ; [0x80487b4:4]=0x6e207349 LEA
    str.Is_not_the_good_password__ ; "Is not the good password !" @ 0x80487b4
    """
    Then I use NOP the jne at address 0x0804861e
    And I wait to go straight to the correct message
    """
    $ rasm2 nop
    90
    [0x08048610]> wx 9090 @ 0x0804861e
    [0x08048610]> q
    """
    And I run the crack with any argument
    """
    $ ./Crack dummy
    Good work, the password is :

    ff07031d6fb052490149f44b1d5e94f1592b6bac93c06ca9
    """
    Then I put as answer ff07031d6fb052490149f44b1d5e94f1592b6bac93c06ca9
    And I saw that the answer is correct.
    Then I solved the challenge.
