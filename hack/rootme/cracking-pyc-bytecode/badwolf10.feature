# Version 2.0
# language: en

Feature:  Cracking - Root Me
  Site:
     www.root-me.org
  Category:
    Cracking
  User:
    badwolf10
  Goal:
    Crack the password of a python application

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Cracking/PYC-ByteCode
    """
    And The challenge statement
    """
    Retrieve the password to validate this challenge.
    """
    Then I download the application binary "ch19.pyc"

  Scenario: Success: Crack application password
    Given the executable "ch19.pyc"
    Then I run it in console to see its behavior
    """
    $ python3 ch19.pyc
    Enter the Flag:
    """
    Then I use python tool uncompyle6 to decompyle the binary
    """
    $ uncompyle6 -o . ch19.py
    """
    And I get "ch19.py" file
    And I check at the code
    """
    01 if __name__ == '__main__':
    02 print('Welcome to the RootMe python crackme')
    03 PASS = input('Enter the Flag: ')
    04 KEY = 'I know, you love decrypting Byte Code !'
    05 I = 5
    06 SOLUCE = [57, 73, 79, 16, 18, 26, 74, 50, 13, 38, 13, 79, 86, 86, 87]
    07 KEYOUT = []
    08 for X in PASS:
    09     KEYOUT.append((ord(X) + I ^ ord(KEY[I])) % 255)
    10     I = (I + 1) % len(KEY)
    11 if SOLUCE == KEYOUT:
    12     print('You Win')
    13 else:
    14     print('Try Again !')
    """
    And I see the password is encrypted into an array
    And I see the encryption algorithm
    Then I write a python script to do a brute force attack
    And I run the script
    Then I get the password is "I_hate_RUBY_!!!"
    And I solve the challenge
