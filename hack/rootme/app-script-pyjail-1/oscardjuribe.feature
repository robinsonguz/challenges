## Version 2.0
## language: en

Feature: python-pyjail-1
  Site:
    https://www.root-me.org/es/Challenges/App-Script/Python-PyJail-1
  User:
    alestorm980 (wechall)
  Goal:
    Retrieve validation password to get out of this jail

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7         |
  Machine information:
    Given A SSH access
    Then I have some credentials
    Then I login into the Python jail

  Scenario: Fail: Try to exit normally
    Given The Python jail
    When I see that I have the exit function
    Then I try to exit from Python interpreter
    And I get the "Denied" message

  Scenario: Fail: Import Python os library
    Given The Python jail
    When I decide to use the os library
    Then I try to import it
    """
    import os
    """
    And I get the "Denied" message

  Scenario: Success: Inspect live objects
    Given The Python jail
    When I search in Google how to inspect functions
    Then I get a Python documentation
    """
    https://docs.python.org/2/library/inspect.html
    """
    And I find that I can convert the function into a 'code' object
    """
    >>> print exit.func_code
    <code object exit at 0xb7bfff08,
    file "/challenge/app-script/ch8/ch8.py", line 27>
    """
    When I search in the documentation for function of the code object
    Then I find some interesting functions
    """
    co_code
    co_consts
    """
    When I try to get the code using 'co_code'
    """
    >>> print exit.func_code.co_code
    """
    Then I get the raw bytecode of the function
    But I am not able to decode it
    When I use the co_consts function
    """
    >>> print exit.func_code.co_consts
    (None, 'flag-WQ0dSFrab3LGADS1ypA1', -1, 'cat .passwd', 'You cannot escape!')
    """
    Then I realize that the code should be something like
    """
    def exit(argument):
      // exit method
      if argument == 'flag-WQ0dSFrab3LGADS1ypA1':
        os.system('cat .passwd')
      else:
        print 'You cannot escape !'
    """
    When I store the flag value inside a variable
    """
    >>> flag = exit.func_code.co_consts[1]
    """
    Then I can call the exit function with that argument
    """
    >>> exit(flag)
    Well done flag : <flag>
    Connection to challenge02.root-me.org closed.
    """
    And I solve the challenge
