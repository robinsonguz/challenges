## Version 2.0
## language: en

Feature: forensic-malicious-word-macro
  Site:
    https://www.root-me.org/es/Challenges/Forense/Malicious-Word-macro
  Category:
    forensic
  User:
    Sierris (wechall)
  Goal:
    Find the favorite site of the user on a ".dmp" file.

  Background:
  Hacker's software:
    | Volatility  | 2.6     |
    | Ubuntu      | 18.04.3 |
    | oletools    | 0.54    |
    | grep        | 3.1     |
    | strings     | 2.30    |
  Machine information:
    Given a ".txz" file
    When I uncompress the file
    Then I get a folder "ch20/"
    And Inside the folder there is a ".dmp" file (memory dump file)

  Scenario: Fail: Using strings and grep
    Given I a ".dmp" file (memory dump file)
    When I use "strings" to see the text
    Then A lot of information shows, nothing worth
    And I don't find the flag

  Scenario: Success: Extract the malicious .docm file from the .dmp file
    Given I a ".dmp" file (memory dump file)
    When I use volatility to search the files information
    """
    $ volatility imageinfo -f memory.dmp
    """
    Then I get the image possible profile [evidence](img1.png)
    And I use volatility to check which processes were running
    """
    $ volatility pslist --profile=Win7SP1x86_23418 -f memory.dmp
    """
    Then I verify that "Word" was running
    And I get Word's PID
    When I use volatility to check how the process was ran
    """
    $ volatility cmdline --profile=Win7SP1x86_23418 -p 3248 -f memory.dmp
    """
    Then I get the name of the file
    """
    Volatility Foundation Volatility Framework 2.6
    ************************************************************************
    WINWORD.EXE pid:   3248
    Command line : "C:\Program Files\Microsoft Office\Office15\WINWORD.EXE"
      /n "C:\Users\fraf\Downloads\Very_sexy.docm
    """
    And I extract the file [evidence](img2.png)
    """
    volatility dumpfiles --profile=Win7SP1x86_23418 -p 3248 -n Very_sexy.docm
      -f memory.dmp --dump-dir volatility-out/
    """
    And I search and rename the actual file
    """
    mv file.3248.0x84cb24e8.Very_sexy.docm.dat Very_sexy.docm
    """

    Scenario: Success: Analyze the given .docm file to find the flag
    Given a ".docm" file
    When I search "word document analysis" in google
    Then I find "oletools" as an usefull tool
    When I use "oletool" I see usefull information about the macro
    """
    $ olevba Very_sexy.docm
    """
    And I can see the IP the wacro was conneting [evidence](img3.png)
    And I notice it is kind of like a proxy
    Then I go back and use "strings" and "grep" to find more information
    """
    $ strings memory.dmp | grep 192.168.0.19
    """
    And I get some interesting results
    """
      return "PROXY 192.168.0.19:8080";
      return "PROXY 192.168.0.19:8080";
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
        return "PROXY 192.168.0.19:8080";
    tp://192.168.0.19:8080/BenNon.prox
    tp://192.168.0.19:8080/BenNon.prox
    192.168.0.19
        return "PROXY 192.168.0.19:8080";
    OXY 192.168.0.19
    Host: 192.168.0.19:8080
    http://192.168.0.19:8080/BenNon.prox
    192.168.0.19
    http://192.168.0.19:8080/BenNon.prox
    http://192.168.0.19:8080/BenNon.prox
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
    192.168.0.19
    192.168.0.19
    http://192.168.0.19:8080/BenNon.prox
    Host: 192.168.0.19:8080
    Host: 192.168.0.19:8080
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
        return "PROXY 192.168.0.19:8080";
    """
    When I get the lines next to the results limiting to one output
    """
    strings memory.dmp | grep 192.168.0.19 -m1 -C 5
    """
    Then I get the flag [evidence](img4.png)
