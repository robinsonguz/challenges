"""
Retrieve connection MAC address and phone name
> pylint test.py
------------------------------------
Your code has been rated at 10.00/10
"""
import hashlib
from btsnoop import btsnoop
B = btsnoop.btsnoop("ch18.bin")
RINFO = B.get_remote_info()[0]
print hashlib.sha1((RINFO['peer_addr'] + RINFO['name']).encode()).hexdigest()

# python test.py
# c1d0349c153ed96fe2fadf44e880aef9e69c122b
