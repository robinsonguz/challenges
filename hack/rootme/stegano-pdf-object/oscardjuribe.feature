## Version 2.0
## language: en

Feature: stegano-pdf-object
  Site:
    https://www.root-me.org/en/Challenges/Steganography/PDF-Object
  User:
    alestorm980 (wechall)
  Goal:
    Recover the flag from the pdf

Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7.15+     |
    | Peepdf          | 0.2 r158    |
    | Binwalk         | v2.1.2      |
    | Kali Linux      | 4.19.0      |
  Machine information:
    Given A ".zip" file
    Then I unzip the file
    And I get a PDF file "epreuve_BAC_2004.pdf"

  Scenario: Fail: Command binwalk in the file
    Given A PDF file
    When I run "binwalk" to extract files
    """
    $ binwak -d epreuve_BAC_2004.pdf
    """
    Then I get files containing some random data
    """
    DECIMAL       HEXADECIMAL     DESCRIPTION
    -----------------------------------------------------------------------
    0             0x0             PDF document, version: "1.3"
    73            0x49            Zlib compressed data, default compression
    261           0x105           Zlib compressed data, default compression
    """
    When I search for useful information
    Then I don't find anything

  Scenario: Success: Use Peepdf
    Given A PDF file
    When I search on Google info about analyzing PDF files
    Then I find a tool called "peepdf"
    And I download it from Google
    """
    https://storage.googleapis.com/google-code-archive-downloads/
    v2/code.google.com/peepdf/peepdf_0.2-BlackHatVegas.zip
    """
    When I run the tool "peepdf" to analyze my PDF
    """
    $ python peepdf.py -i epreuve_BAC_2004
    """
    Then I get a list of objects and suspicious files
    """
    Objects (78): [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
    50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
    68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78]

    Encoded (25): [8, 14, 20, 26, 32, 38, 44, 50, 56, 62,
    68, 74, 77, 5, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72]
    Suspicious elements:
    /Names: [1]
    """
    When I copy the command "object 1"
    Then I have info about an embedded file
    """
    PPDF> object 1
    << /Type /Catalog
    /Pages 3 0 R
    /Names << /Embeddedfiles << /Names[ Hidden_b33rs.txt 78 0 R ] >> >> >>
    """
    When I go to the object 78
    Then I know that the file content is inside the object 77
    """
    PPDF> object 78
    << /F Hidden_b33rs.txt
    /Type /Filespec
    /EF << /F 77 0 R >> >>
    """
    When I extract the content of that object
    """
    PPDF> object 77 > data.output
    """
    Then I open the content of the file
    And It is base64 encoded data
    When I delete the first lines and the last one
    """
    << /Length 79749
    /Type /Embeddedfile
    /Filter /FlateDecode
    /Params << /Size 108542
    /Checksum VY<84>½<9e>)^A$<8b>]<8a>¿átâ± >>
    /Subtype /text/plain >>
    stream
    ...
    endstream
    """
    Then I can try to decode the data
    When I run the command
    """
    $ cat data.output | base64 -d > decode
    """
    Then I get an image
    """
    $ file decode
    decode: JPEG image data, JFIF standard 1.01, resolution (DPI),
    density 96x96, segment length 16, comment: "CREATOR: gd-jpeg
    v1.0 (using IJG JPEG v62), quality = 95", baseline, precision 8,
    500x417, frames 3
    """
    When I open the file
    Then I see a text on the image [evidence](image1.png)
    And I have the flag
