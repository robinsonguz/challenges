## language: en

Feature: Solve the challenge
  CTF site:
    www.root-me.org
  Category:
    Cryptanalysis
  Page name:
    Encoding - UU
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.04.1 LTS (amd64)       |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
  CTF site information:
    Given I'm at the challenge page
    And the statement says
    """
    Get the validation password.
    """
    And there is an online text file
    """
    01 _=_
    02 _=_ Part 001 of 001 of file root-me_challenge_uudeview
    03 _=_
    04
    05 begin 644 root-me_challenge_uudeview
    06 B5F5R>2!S:6UP;&4@.RD*4$%34R`](%5,5%)!4TE-4$Q%"@``
    07 `
    08 end
    """

  Scenario: Capturing the flag
  Finding the right solution and solving the challenge
    When I use the online tool
    """
    http://uuencode.online-domain-tools.com/
    """
    And I enter the coded text
    """
    B5F5R>2!S:6UP;&4@.RD*4$%34R`](%5,5%)!4TE-4$Q%"@``
    """
    Then I get the flag
    """
    Very simple ;)
    PASS = ULTRASIMPLE
    """
    When I enter "ULTRASIMPLE" as password
    Then I solve the challenge
