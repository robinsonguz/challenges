## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist
  User:
    mr_once
  Goal:
    Get access to a restricted area

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux       | 2020.07.01  |
    | Brave           | 1.10.97     |
    | curl            | 7.71.1      |
  Machine information:
    Given I am accessing the challenge at
    """
    http://challenge01.root-me.org/realiste/ch7/hackers.php?page=accueil
    """
    And it looks like a hacker's site

  Scenario: Fail: Try to steal admin's cookie
    Given I am in the site
    And I see a contact form
    And the clue of the challenge are docs about XSRF
    And I get a free host where I can upload a PHP script to log petitions
    When I go to
    """
    http://challenge01.root-me.org/realiste/ch7/hackers.php?page=contact
    """
    Then I try sending the following message
    """
    [img]hppt://mywebpage/log.php[/img]
    """
    And I see that after the message was sent, the logs did not change
    And I can't steal admin's cookie this way

  Scenario: Success: Get admin's logs
    Given the news section says (translated)
    """
    08/22/2011 10:00 PM Addition of an event logging system for the admin.
    """
    When I go to
    """
    http://challenge01.root-me.org/realiste/ch7/log/
    """
    Then I see that there is a log.php file
    But it asks for authentication
    When I try to make an OPTION request with curl
    """
    curl -X OPTIONS http://challenge01.root-me.org/realiste/ch7/log/log.php
    """
    Then I get the logs
    """
    ------------------------ Log ---------------------
    login=toto / password=titi / no such user
    login=toto / password=toto / no such user
    login=toto / password=tutu / no such user
    login=administrateur / password=4Dm1N_de_ste / Password rejected for \
    administrateur
    login=administrateur / password=4Dm1N_de_site / Accepted password for \
    administrateur
    --------------------------------------------------
    """
    And I can see that the credentials are administrateur:4Dm1N_de_site

  Scenario: Fail: Login as admin
    Given the credentials found in the previous step
    When I try lo login as admin
    Then I get the message that admin is already logged [evidence](logged.png)
    And I can not access

  Scenario: Success: logging out the admin
    Given that now I know I have to logged out the admin
    Then I try the approach I follow when trying to steal the admin's cookie
    And in the contact form I send to the admin the message:
    """
    [img]http://challenge01.root-me.org/realiste/ch7/
    hackers.php?page=deconnect[/img]
    """
    When I go to the members page
    Then I can see that the admin is now logged out [evidence](offline.png)

  Scenario: Success: getting access to the restricted area
    Given that the admin is now logged out
    When I try to log in I succeed
    Then I can access the restricted area [evidence](flag.txt)
    And I solve the challenge
