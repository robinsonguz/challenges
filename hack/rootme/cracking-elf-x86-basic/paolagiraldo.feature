## Version 2.0
## language: en

Feature: rootme-cracking-ELF-x86-Basic

  Site:
    https://www.root-me.org
  Category:
    cracking
  User:
    paolagiraldo
  Goal:
    Find the validation password


  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 19.10           |
    | Google Chrome   | 81.0.4044.138   |

  Machine information:
    Given I am accesing the site from my browser
    Then I started the challenge
    And download the .zip file
    Then I extract the .bin file

  Scenario: Fail: Normal use case

    Given the elf file
    Then I give execution permission to the file
    Then I excuted in terminal
    And I can see a banner:
    """
    ############################################################
    ##        Bienvennue dans ce challenge de cracking        ##
    ############################################################

    username:
    """
    Then I see a welcome message and a text to enter the username
    Given I write <username> in the console
    When I press enter key
    Then I get <results>

      | <username> | <results>     |
      | username   | Bad username  |
      | user       | Bad username  |

    And I could not capture the flag

  Scenario: Success: Strings
    Given The related ressource(s) in the challenge's site
    Then I go to read The GNU binary utils (Administration/Unix/Linux)
    Then I try the strings command
    Then I run strings ch2.bin
    And I got a lot of string lines
    Then I navigate throw the terminal
    Then I see the welcome banner and other strings
    """
    %s : "%s"
    Allocating memory
    Reallocating memory
    john
    the ripper
    ############################################################
    ##        Bienvennue dans ce challenge de cracking        ##
    ############################################################
    username:
    password:
    987654321
    Bien joue, vous pouvez valider l'epreuve avec le mot de passe : %s !
    Bad password
    Bad username
    """
    Then I decided to prove 987654321 as the challenge's password
    Then I got the message Well done valindating the password
    And I caught the flag
