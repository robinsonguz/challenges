## Version 2.0
## language: en

Feature: Root Me - Web-Server - http-open-redirect
  Site:
    Root Me
  Category:
    Steganography
  Challenge:
    Gunnm
  User:
    synapkg
  Goal:
    Find the message.

  Background:
  Hacker's software:
    | <Software name > |   <version>       |
    | Ubuntu           | 16.04 LTS (64-bit)|
    | Mozilla Firefox  | 63.0.3 (64-bit)   |

  Scenario: Success: Analyze the image
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Steganography/Gunnm
    """
    Then I opened that URL with Mozilla Firefox
    And I opened the follow link to start challenge
    """
    http://challenge01.root-me.org/steganographie/ch1/ch1.png
    """
    Then I saw a image
    And I started looking for letters
    And on the top right I saw something
    Then increase the size of the image as much as I could
    And he got to see something that said "PASS=TOTORO"
    Then I put "TOTORO" as answer and I was right.
