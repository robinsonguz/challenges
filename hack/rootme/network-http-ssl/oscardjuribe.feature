## Version 2.0
## language: en

Feature: network-http-ssl
  Site:
    https://www.root-me.org/es/Challenges/
    Redes/SSL-HTTP-exchange
  User:
    alestorm980 (wechall)
  Goal:
    Find the password inside the "pcap" file

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 4.19.0      |
    | Wireshark       | 2.6.8       |
    | Firefox         | 60.6.2      |
  Machine information:
    Given The challenge description
    And A "pcap" file
    Then I have to find the password inside it

  Scenario: Fail: Searching it using wireshark
    Given A "pcap" file
    When I try to search information inside the "pcap"
    Then The information is encrypted
    When I search for strings
    Then I can't find nothing usefull
    And I decide to explore another way

  Scenario: Succcess: Decrypting the network capture
    Given A "pcap" file
    When I read the challenge description
    Then I search in google using the dork "inurl:server.pem"
    When I see the network capture
    Then I find that the comunication is between two apple devices
    When I see the results of my search on google
    Then I find an Apple page containing a ".pem" file [evidence](image1.png)
    When I take the rsa private key from the ".pem" file
    Then I paste it into one file
    And I run wireshark again
    When I go to the options of the ssl protocol
    Then I change the configuration [evidence](image2.png)
    When I see the output of the debug file "ssl.txt"
    Then I read the content of the file
    When I search for app data fragments
    Then I find the password [evidence](image3.png)
    And I solve the challenge
