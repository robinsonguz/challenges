## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Retrieve the password asked

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | dnSpy           | 5.0.10 (x86)     |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    PE DotNet - 0 protection

    Retrieve the password asked by this binary.
    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the exe file

  Scenario: Fail:Run the application
    Given I dowload the exe file
    And I have Windows version 10.0.17134 (x64)
    Then I proceed to execute the exe file
    Then I saw a window with a form [evidence](window.png)
    And I try put the password '1234' but the answer is wrong
    Then I conclude that I should use another method
    And I could not capture the flag

  Scenario: Success:Decompilation of the file
    Given I have dnSpy installed on my system
    Then I proceed to open the exe file with the dnSpy
    Then I look in the internal archives of the exe file
    And I get the file with the flag
    And I saw the following word in the source code [evidence](dnspy.png)
    Then I can finally read the flag from the exe file
    And I conclude that dnSpy worked
    Then I put as answer 'DotNetOP' and the answer is correct.
    Then I caught the flag
