## Version 2.0
## language: en

Feature: Cryptanalysis-Root Me
  Site:
    Root Me
  Category:
    Cryptanalysis
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                 |
    | Ubuntu          | 18.04.1 LTS (amd64)       |
    | Google Chrome   | 70.0.3538.77 (64-bit)     |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    Transposition - Rail Fence

    USA, American Civil War, August 3, 1862. You are on patrol around the camp
    when you see an enemy rider. Once you intercepted him, you discover that he
    carries a message but nobody at the camp manages to uncipher it. You are the
    only hope to find the hidden information. It could be crucial !
    """
    And the ciphertext is
    """
    Wnb.r.ietoeh Fo"lKutrts"znl cc hi ee ekOtggsnkidy hini cna neea civo lh
    """

  Scenario: Fail:Use the confederate cipher disk
    Given the statement locates the scenario on the American Civil War
    Then I look for the known cyptographic methods used at that time
    When I read about the confederate cipher disk
    Then I conclude that this is based on the polyalphabetic vigenere cipher
    When I use an online tool to perform an statistical analysis
    """
    https://www.dcode.fr/vigenere-cipher
    """
    Then I get the following results
    | key   | fragment of deciphered text                               |
    | TAACH | DNBPBLTOCAMOLINARTQSULCAAPEECDVTGELUKIBROINGVUANCXHCITHSH |
    | ARA   | WWBRRETXEHOOLTUTATSINLLCHREENKOCGGBNKRDYQINRCNJNENACRVOUH |
    | LAA   | LNBGIEIOEWFOAKUIRTHZNACCWIETEKDTGVSNZIDNHICICCANTEARIVDLH |
    | LRA   | LWBGREIXEWOOATUIATHINALCWRETNKDCGVBNZRDNQICRCCJNTNARRVDUH |
    | AA    | WNBRIETOEHFOLKUTRTSZNLCCHIEEEKOTGGSNKIDYHINICNANEEACIVOLH |
    And I conclude the message may not be written in english
    And I conclude the message may be ciphered with another method

  Scenario: Success:The challenge title is the cipher method used
    Given the challenge title is
    """
    Transposition - Rail Fence
    """
    Then I believe that transposition means to transpose the text
    When I search in google for cryptographic transposition
    Then I find a wikipedia page
    """
    https://en.wikipedia.org/wiki/Transposition_cipher
    """
    And I find a method named "Rail Fence"
    When I use an online tool to perform a brute force attack on the levels
    """
    https://www.dcode.fr/rail-fence-cipher
    """
    Then I get the following results
    | level | fragment oc deciphered ciphertext                                |
    | 2     | W·nebk.Ort.gigestnokeihd·yF·oh"ilnKiu·tcrntas·"nzenela··ccci·vho |
    | 3     | Wuitnr·tbsc".znnrla·.c·ci·nheie·teeeo·aeek·Ohtcg·gisFnvkoiod"y·· |
    | 4     | W·en·Fnoeek"blOetK.ugagtrrs·nt.skci"izdiynel·vh·tcionco·i··heicl |
    | 5     | Wolnak·enhci·dc·bF·yc·ho."ihii·lrKenvieu.t··oceritkn·aOse"t·lngz |
    | 6     | Wts·hcie"onezkniiOnhb·lt·vcg·F.ocgnoasc"rl·n··nkhK.uiieled·tirey |
    | 7     | WiKcgii·g·uentthscvnnirobet·kao·iesh.·"edn·ey·zFrone·elahkl".l·O |
    | 8     | Will·invade·Kentucky·on·October·the·eighth.·signal·is·"Frozen·ch |
    And I conclude the zig-zag level for the algorithm is eight
    When I try this key to decipher the ciphertext
    Then I get the deciphered ciphertext
    """
    Will·invade·Kentucky·on·October·the·eighth.·signal·is·"Frozen·chicken".
    """
    When I use "Frozen chicken" as the flag
    Then I solve the challenge
