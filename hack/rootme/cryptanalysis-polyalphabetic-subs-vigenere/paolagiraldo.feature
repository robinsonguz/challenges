## Version 2.0
## language: en

Feature: rootme-cryptanalysis-polyalphabetic-subs-vigenere

  Site:
    https://www.root-me.org
  Category:
    cryptoanalysis
  User:
    paolagiraldo
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 19.10           |
    | Google Chrome   | 81.0.4044.138   |

  Machine information:
    Given I am accesing the site from my browser
    Then I read the challenge's statement
    """
    We need your expert opinion on this document.
    This is an old letter and it appears that it is important for
    the pirates that we are searching for. Your mission is to decipher
    the text and give us the full name of the author
    (example : "John Doe").
    """
    Then I started the challenge
    And I see a crypted text

  Scenario: Success: Vigenere Decrypt
    Given The text in the challenge
    Then I look for a vigeniere decoder
    And I found
    """
    https://www.dcode.fr/cifrado-vigenere
    """
    And I pasted the first line of the text
    """
    Moi Tepdsi Fhrujrlhf
    """
    Then No good results obtained
    Then I pasted 3 lines of the text
    """
    Moi Tepdsi Fhrujrlhf
    Nu egxex g'vla jmmg ifvgkvq ehcclkk'lgm, p'xgk ihvfshm rrgz pqw
    whiighyj.
    "Wptbutsi: Gr nwccxzgqrg tfixai bshk qibti urshfdtamcyr,
    "Tfixzxmxvhb u'nu 'lmgxxf' riyie pr iwitaesi q'nbv uhrcyr"...
    """
    Then It returns the text
    """
    The Hacker Manifesto
    Un autre s'est fait prendre aujourd'hui, c'est partout dans les
    journaux.
    "Scandale: Un adolescent arrete pour crime informatique,
    "Arrestation d'un 'hacker' apres le piratage d'une banque"...
    """
    And The key
    """
    THEMENTOR
    """
    Then I continue pasting the text in blocks of 3 or 4 lines
    And I a got a large paragraph in French
    Then I paste the text in french into Google Translate
    And I got the text in Spanish
    """
    El Manifiesto Hacker
    Otro quedó atrapado hoy, está en todos los periódicos. "Escándalo:
    adolescente arrestado por delito informático,
    "Detención de un 'pirata informático' después de la piratería
    de un banco"
    """
    Then I looked in google for "El Manifiesto Hacker"
    And Found the Wikipedia page
    Then I see the info about the autor
    """
    es un breve ensayo escrito el 8 de enero de 1986 por un hacker en
    seguridad informática bajo el seudónimo de "The Mentor"
    Loyd Blankenship
    """
    And I try THEMENTOR as the flag and got "Lastima, animo !"
    Then I try THE MENTOR as the flag and got "Lastima, animo !"
    Then I try The Mentor as the flag and got "Lastima, animo !"
    Then I try Loyd Blankenship as the flag and got
    """
    Bien jugado, ganas 20 Puntos
    """
    And Solved the challenge
    And I caught the flag
