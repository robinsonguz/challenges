## Version 2.0
## language: en

Feature: 4-cryptography-electrica
  Code:
    4
  Site:
    electrica
  Category:
    cryptography
  User:
    edr3mo
  Goal:
    Decrypt the cryptogram and find the answer

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | Tessa 19.1  |
    | Mozilla Firefox | 66.0.2      |
  Machine information:
    Given the challenge URL
    """
    http://www.caesum.com/game/
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    Your task is to decrypt it and find the answer to this problem
    """
    Then I see the cryptogram
    """
    dvoowlmvgsvdliwblfhvvprhzgyzhs
    """
    And I see an input field for the answer

  Scenario: Success:Replace letters
    Given the problem statement says
    """
    Now the cryptogram below does not use the Caesar cipher
    but uses an even older method of encipherment
    """
    Then I read about cryptography history
    And I find an old cypher called Atbash
    And I see this cypher shifts the first letter for the last one
    Then I see it shifts the second letter for the penultimate
    And I shift the corresponding letters of the cryptogram
    Then I see the decrypted message
    """
    welldonethewordyouseekisatbash
    """
    Then I conclude the answer is atbash
    And I capture the flag
    And I solve the challenge
