## Version 1.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    Cristian Guatuzmal
  Goal:
    Discover the decrypted text

  Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
    | notepad              | 1803           |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=5
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    For the beginning here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    4D586CFC2DB449D47B0CF99C3BC46CFC7B0C
    """
    And an input format which allows me to cipher text
    And uses the same cipher pattern as the text

  Scenario: Success: building a values table
    Given the web page has an input field and an Encrypt Button
    Then I start to put some values in the input field
    And I start to encrypt them
    Then I notice that each character has a hexadecimal value
    And I can find a pattern but it is incomplete
    And I start to organize the table based on that incomplete pattern
    Then I obtain the following table of values
    """
    | 2  | 3 | 6 | A | D |
    | eq | 0 | a | b | c |

    | 3  | 1 | 4 | 8 | B | F |
    | eq | d | e | f | g | h |

    | 4  | 2 | 6 | 9 | D |
    | eq | i | j | k | l |

    | 5  | 0 | 4 | 7 | B | E |
    | eq | m | n | o | p | q |

    | 6  | 2 | 5 | 9 | C |
    | eq | r | s | t | u |

    | 7  | 0 | 4 | 7 | B | E |
    | eq | v | w | x | y | z |

    |F   | 9 |
    | eq |" "|
    """
    And instead the original values ara quartets
    Then it is not necessary to convert all the quartet
    Then I replaced every character with its code equivalent
    And I got the following text
    """
    lucky guy
    """
    Then I post as the solution
    And it works
    And I solve the challenge
