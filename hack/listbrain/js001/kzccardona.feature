## Version 1.0
## language: en

Feature: html-listbrain
  Site:
    http://listbrain.awardspace.biz
  Category:
    html
  User:
    kevinc
  Goal:
    Find the password to pass the login and capture the flag, using the
    inspect tool to see the html code and look for any clue

  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | google chrome        | 75.0  (64 bit) |
  Machine information:
    Given I am accessing the web page using google chrome browser
    And Windows 10 as OS
    And ip 192.168.36.29

  Scenario: fail: Searching for a sqli vulnerability
    Given I opened te following url
    """
    http://listbrain.awardspace.biz/index.php?p=JS001
    """
    Then I see a login form
    Then I think it could be sql injection
    Then I try to insert sql code to try pass the login
    And gave me error after several attempts
    And I decide to think in another posibilities

  Scenario: Success: Searching in the html source code trought
  google chrome element inspector
    Given I opened te following url
    """
    http://listbrain.awardspace.biz/index.php?p=JS001
    """
    And open inpector to see the html code
    Then I read the html code to try find a clue
    And I find the password in a comment line
    Then I put this password in the login form
    And I pass the login form
    And I solve the challenge
