## Version 1.0
## language: en

Feature: javaScript-listbrain
  Site:
    http://listbrain.awardspace.biz
  Category:
    javaScript
  User:
    lekriss
  Goal:
    Find the password to capture the flag in the source code
    loaded by the browser

  Background:
  Hacker's software:
    |<software>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given I am accessing the web page using firefox browser
    And http in the port 80

  Scenario: Success: Searching in the source code trough
  Firefox's debugger
    Given I opened the elements in the inspector of firefox
    And I navigate to the debugger
    Then I can see the source code of the page
    And I try to analyze the javascript source in the debugger
    And I get the script which compares the password
    Then I can actually read the flag from the script
    And I conclude that going into the inspector worked
    Then I solve the challenge
    And I capture the flag
